// decode html text into html entity
const decodeHtmlEntity = function(str) {
  if (str) {
    return str.replace(/&#(\d+);/g, function(match, dec) {
      return String.fromCharCode(dec);
    });
  } else {
    return "";
  }
};

export {
  decodeHtmlEntity
};
