import "./App.css";
import { Fragment, useState, useContext } from "react";
import {
  Route,
  Routes,
  Navigate,
  useLocation,
  useNavigate,
} from "react-router-dom";
import NotFound from "./pages/NotFound";
import Login from "./pages/Login";
import CourseList from "./pages/CourseList";
import Course from "./pages/Course";
import Header from "./components/Header";
import Footer from "./components/Footer";
import Dashboard from "./pages/Dashboard";
import Results from "./pages/Results";
import Scheduler from "./pages/Scheduler";
import QuizResults from "./pages/QuizResults";
import Profile from "./pages/Profile";
import MiniQuiz from "./pages/MiniQuiz";
import { Typography, useTheme } from "@mui/material";
import { trySetJwtTokenFromWpCookies } from "./facades/UserFacade";
import RouteRequiresLogin from "./routes/RouteRequiresLogin";
import QuizContextProvider from "./contexts/QuizContext";
import { UserContext } from "./contexts/userContext";
import Scratchpad from "./components/quiz/Scratchpad";
import GlobalStyles from "@mui/material/GlobalStyles";
import CommonUnit from "./pages/CommonUnit";
import Knowledgebase from "./pages/Knowledgebase";
import ContactUs from "./pages/ContactUs";
import MyTracker from "./components/tracker/MyTracker";
import { Helmet, HelmetProvider } from "react-helmet-async";

import Announcements from "./pages/Announcements";
import Forum from "./components/forums/Forum";
import SingleForum from "./components/forums/SingleForum";
import Topic from "./components/forums/Topic";
import MyCertificates from "./components/certificates/MyCertificates";
import { Kaila } from "./components/kaila/Kaila";
//import { resolve } from "path/posix";

const scratchpadHeight = 400;

// const useStyles = makeStyles((theme) =>
//   createStyles({
//     "@global": {
//       img: {
//         maxWidth: "100%",
//       },
//       iframe: {
//         maxWidth: "100%",
//       },
//     },
//     scratchpad: {
//       height: scratchpadHeight,
//     },
//     scratchpadPaper: {
//       height: scratchpadHeight,
//       padding: "3px 20px",
//     },
//     content: {
//       transition: theme.transitions.create("margin", {
//         easing: theme.transitions.easing.sharp,
//         duration: theme.transitions.duration.leavingScreen,
//       }),
//       marginBottom: -scratchpadHeight,
//     },
//     contentShift: {
//       transition: theme.transitions.create("margin", {
//         easing: theme.transitions.easing.easeOut,
//         duration: theme.transitions.duration.enteringScreen,
//       }),
//       marginBottom: 0,
//     },
//   })
// );

// const GlobalStyles = () => {
//   //  useStyles();
//   return null;
// };

const checkLoggedIn = async () => {
  /*
    const savedUser = localStorage.getItem("user");
    const user = JSON.parse(savedUser);
    if(user !== null){
        let token = user.token;
        let decoded_token = decode(token);
        console.log("DECODED", decoded_token);


        return new Promise((resolve, reject) => {
            resolve(true);
        });
    }*/

  return trySetJwtTokenFromWpCookies()
    .then((res) => res.json())
    .then((res) => {
      if (res.length === 0) return false;

      localStorage.setItem("user", JSON.stringify(res));

      return true;
    });
};

function App() {
  const theme = useTheme();

  const [loginCheck, setLoginCheck] = useState(true);
  const { pathname } = useLocation();
  const userContext = useContext(UserContext);
  const [scratchpadVisible, setScratchpadVisible] = useState(false);

  const navigate = useNavigate();
  /*
    if(!loginCheck &&  pathname!==`${process.env.REACT_APP_AB_URL_PREFIX}/login`)
 checkLoggedIn()
    .then(ok => {
        console.log("login", ok)

        if(!ok)
            navigate(`${process.env.REACT_APP_AB_URL_PREFIX}/login`);
        else
            setLoginCheck(ok);
    })
*/
  return userContext.user == null &&
    pathname !== `${process.env.REACT_APP_AB_URL_PREFIX}/login` ? (
    <Typography>Loading...</Typography>
  ) : (
    <>
      <HelmetProvider>
        <Helmet>
          <title>KnowledgEquity - Apply Knowledge. Improve Performance.</title>
          <script src="https://www.gstatic.com/dialogflow-console/fast/df-messenger/prod/v1/df-messenger.js"></script>
        </Helmet>
        <GlobalStyles
          styles={(theme) => ({
            "div.wpb_wrapper img": {
              maxWidth: "100%",
            },
            iframe: {
              maxWidth: "100%",
            },
          })}
        />
        <QuizContextProvider>
          <main
            style={{
              transition: scratchpadVisible
                ? theme.transitions.create("margin", {
                    easing: theme.transitions.easing.easeOut,
                    duration: theme.transitions.duration.enteringScreen,
                  })
                : theme.transitions.create("margin", {
                    easing: theme.transitions.easing.sharp,
                    duration: theme.transitions.duration.leavingScreen,
                  }),
              marginBottom: scratchpadVisible ? 0 : -scratchpadHeight,
            }}
          >
            <Header user={userContext.user} />
            <Routes>
              <Route from="/:url*(/)" to={pathname.slice(0, -1)} />
              <Route path="/" exact element={<Login />} />
              <Route path="/info/contact-us" exact element={<ContactUs />} />
              <Route
                path={`${process.env.REACT_APP_AB_URL_PREFIX}/login`}
                component={Login}
              />
              <Route
                path={`${process.env.REACT_APP_AB_URL_PREFIX}/dashboard`}
                element={
                  <RouteRequiresLogin>
                    <Dashboard />
                  </RouteRequiresLogin>
                }
              />
              <Route
                path={`${process.env.REACT_APP_AB_URL_PREFIX}/info/:info_page_name`}
                element={
                  <RouteRequiresLogin>
                    <CommonUnit />
                  </RouteRequiresLogin>
                }
              />
              <Route
                path={`${process.env.REACT_APP_AB_URL_PREFIX}/info/announcements`}
                element={
                  <RouteRequiresLogin>
                    <Announcements />
                  </RouteRequiresLogin>
                }
              />
              <Route
                path={`${process.env.REACT_APP_AB_URL_PREFIX}/info/my-tracker`}
                element={
                  <RouteRequiresLogin>
                    <MyTracker />
                  </RouteRequiresLogin>
                }
              />
              <Route
                path={`${process.env.REACT_APP_AB_URL_PREFIX}/info/my-certificates`}
                element={
                  <RouteRequiresLogin>
                    <MyCertificates />
                  </RouteRequiresLogin>
                }
              />
              <Route
                exact
                path={`${process.env.REACT_APP_AB_URL_PREFIX}/course/:course_url/:unit_url?`}
                element={
                  <RouteRequiresLogin>
                    <Course
                      scratchpadVisible={scratchpadVisible}
                      setScratchpadVisible={setScratchpadVisible}
                    />
                  </RouteRequiresLogin>
                }
              />
              <Route
                path={`${process.env.REACT_APP_AB_MINIQUIZ_PREFIX}/quiz/:quiz_name`}
                element={
                  <RouteRequiresLogin>
                    <MiniQuiz
                      scratchpadVisible={scratchpadVisible}
                      setScratchpadVisible={setScratchpadVisible}
                    />
                  </RouteRequiresLogin>
                }
              />
              <Route
                path={`${process.env.REACT_APP_AB_URL_PREFIX}/ate/`}
                element={
                  <RouteRequiresLogin>
                    <Forum />
                  </RouteRequiresLogin>
                }
              />
              <Route
                path={`${process.env.REACT_APP_AB_URL_PREFIX}/ate/:forum_name`}
                element={
                  <RouteRequiresLogin>
                    <SingleForum />
                  </RouteRequiresLogin>
                }
              />
              <Route
                path={`${process.env.REACT_APP_AB_URL_PREFIX}/ate/:forum_name/:topic_name`}
                element={
                  <RouteRequiresLogin>
                    <Topic />
                  </RouteRequiresLogin>
                }
              />
              <Route
                path={`${process.env.REACT_APP_AB_URL_PREFIX}/course-list`}
                component={CourseList}
              />
              <Route
                path={`${process.env.REACT_APP_AB_URL_PREFIX}/my-results`}
                element={
                  <RouteRequiresLogin>
                    <Results />
                  </RouteRequiresLogin>
                }
              />
              <Route
                path={`${process.env.REACT_APP_AB_URL_PREFIX}/knowledgebase`}
                element={
                  <RouteRequiresLogin>
                    <Knowledgebase />
                  </RouteRequiresLogin>
                }
              />
              <Route
                path={`${process.env.REACT_APP_AB_URL_PREFIX}/my-results/:quiz_id`}
                element={
                  <RouteRequiresLogin>
                    <QuizResults />
                  </RouteRequiresLogin>
                }
              />
              <Route
                path={`${process.env.REACT_APP_AB_URL_PREFIX}/schedule`}
                element={
                  <RouteRequiresLogin>
                    <Scheduler />
                  </RouteRequiresLogin>
                }
              />
              <Route
                path={`${process.env.REACT_APP_AB_URL_PREFIX}/profile`}
                element={
                  <RouteRequiresLogin>
                    <Profile />
                  </RouteRequiresLogin>
                }
              />
              <Route
                path={`${process.env.REACT_APP_AB_URL_PREFIX}/kaila/:course_name`}
                element={
                  <RouteRequiresLogin>
                    <Kaila />
                  </RouteRequiresLogin>
                }
              />
              <Route
                path={`${process.env.REACT_APP_AB_URL_PREFIX}/subscriptions`}
                element={
                  <RouteRequiresLogin>
                    <Profile />
                  </RouteRequiresLogin>
                }
              />{" "}
              <Route path="*" exact={true} component={NotFound} />
              {/*<Route path="/course/unit/:id" component={Unit} />
            <Route path="*" component={Notfound} />  */}
            </Routes>
            <div style={{ flexGrow: 1 }}></div>
            <Footer />
          </main>

          <Scratchpad
            scratchpadVisible={scratchpadVisible}
            setScratchpadVisible={setScratchpadVisible}
            scratchpadHeight={scratchpadHeight}
          />
        </QuizContextProvider>
      </HelmetProvider>
    </>
  );
}

export default App;

/**
 * 
 * [
    {
        "Condition": {
            "KeyPrefixEquals": "course/*"
        },
        "Redirect": {
            "ReplaceKeyWith": "index.html"
        }
    },
    {
        "Condition": {
            "KeyPrefixEquals": "dashboard*"
        },
        "Redirect": {
            "HostName": "test.knowledgequity.com.au",
            "ReplaceKeyWith": "index.html"
        }
    },
    {
        "Condition": {
            "KeyPrefixEquals": "login"
        },
        "Redirect": {
            "ReplaceKeyWith": "index.html"
        }
    },
    {
        "Condition": {
            "KeyPrefixEquals": "quiz/*"
        },
        "Redirect": {
            "ReplaceKeyWith": "index.html"
        }
    },
    {
        "Condition": {
            "HttpErrorCodeReturnedEquals": "403"
        },
        "Redirect": {
            "ReplaceKeyWith": "index.html"
        }
    },
    {
        "Condition": {
            "HttpErrorCodeReturnedEquals": "404"
        },
        "Redirect": {
            "ReplaceKeyWith": "index.html"
        }
    }
]
 * 
 * 
 */
