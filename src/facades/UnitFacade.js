const getUnit = (unitid, token) => {
        return fetch(`${process.env.REACT_APP_BASE_URL}/wp-json/app/v1/unit/${unitid}`
        ,{
            headers : { 
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Authorization': `Bearer ${token}`
        }});
    }

const submitUnitRatingTag = (user, unitid, tag, increment) => {
    const data = new FormData();
    data.append("rating", tag);
    data.append("increment", increment);

    const params = new URLSearchParams(data);
  
    return fetch(
        `${process.env.REACT_APP_BASE_URL}/wp-json/app/v1/unit/${unitid}/rate`,
      {
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
          Authorization: `Bearer ${user.token}`,
          "Content-Type": "application/x-www-form-urlencoded",
        },
        method: "POST",
        body: params,
      }
    );
  }

export {getUnit, submitUnitRatingTag}
