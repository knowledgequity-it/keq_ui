const getUserForums = (user) => {
    return fetch(
      `${process.env.REACT_APP_BASE_URL}/wp-json/app/v1/forums`,
      {
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
          Authorization: `Bearer ${user.token}`,
        },
      }
    );
  };

  const getForumTopics = (user, forumid) => {
    return fetch(
      `${process.env.REACT_APP_BASE_URL}/wp-json/app/v1/forums/${forumid}/topics`,
      {
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
          Authorization: `Bearer ${user.token}`,
        },
      }
    );
  };

  const getTopicReplies = (user, forumid, topicid) => {
    return fetch(
      `${process.env.REACT_APP_BASE_URL}/wp-json/app/v1/forums/${forumid}/${topicid}/replies`,
      {
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
          Authorization: `Bearer ${user.token}`,
        },
      }
    );
  };

  const uploadForumImage = (user, forumName, topicName, file) => {
    const data = new FormData();
    data.append("hm_bbpui_file", file);
    //const params = new URLSearchParams(data);
    //?hm_bbpui_do_upload=1
    return fetch(
    `${process.env.REACT_APP_BASE_URL}/wp-json/app/v1/forums/upload`,
    {
        headers: {
            Authorization: `Bearer ${user.token}`,
        },
        method: "POST",
        body: data
    })
  }

  const saveReply = (user, formData, forum, topic) => {
    return fetch(
        `${process.env.REACT_APP_BASE_URL}/wp-json/app/v1/forums/${forum}/${topic}/reply`,
        {
            headers: {
                Authorization: `Bearer ${user.token}`,
            },
            method: "POST",
            body: formData
        })

  }

  const saveTopic = (user, formData, forum) => {
    return fetch(
      `${process.env.REACT_APP_BASE_URL}/wp-json/app/v1/forums/${forum}/new-topic`,
      {
          headers: {
              Authorization: `Bearer ${user.token}`,
          },
          method: "POST",
          body: formData
      })
  }

  export {getUserForums, getForumTopics, getTopicReplies, uploadForumImage, saveReply, saveTopic}