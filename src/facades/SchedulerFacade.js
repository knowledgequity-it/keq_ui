const getEventsForResources= (user, courseid) =>
{
    const url = `${process.env.REACT_APP_SCHEDULER_API_BASE}/schedule/event`;
    return fetch(`${url}?resource_id=${courseid}`
    ,{
        headers : { 
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': `Bearer ${user.token}`
    }});
}
const getLocationsForEvent = (user, eventid) =>
{
    const url = `${process.env.REACT_APP_SCHEDULER_API_BASE}/schedule`;
    return fetch(`${url}/event/${eventid}/location`
    ,{
        headers : { 
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': `Bearer ${user.token}`
    }});
}

const getSlotsForEventLocation = (user, eventid, locationid) =>
{
    const url = `${process.env.REACT_APP_SCHEDULER_API_BASE}/schedule`;
    return fetch(`${url}/event/${eventid}/location/${locationid}/slot`
    ,{
        headers : { 
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': `Bearer ${user.token}`
    }});
}

const getUserBookings = (user) => 
{
    const url = `${process.env.REACT_APP_SCHEDULER_API_BASE}/schedule`;
    return fetch(`${url}/user/booking`
    ,{
        headers : { 
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': `Bearer ${user.token}`
    }});
}

const createUserBooking = (user, event_slot_id) => 
{
    const url = `${process.env.REACT_APP_SCHEDULER_API_BASE}/schedule`;
    return fetch(`${url}/user/booking`
    ,{
        method: 'POST',
        headers : { 
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': `Bearer ${user.token}`
            },
        body : JSON.stringify({
            'event_slot_id' : event_slot_id
        })
    });
}


export {getEventsForResources, getLocationsForEvent, getSlotsForEventLocation, getUserBookings, createUserBooking} 