export function getCourse(courseid, user) {
  return fetch(
    `${process.env.REACT_APP_BASE_URL}/wp-json/app/v1/course/${courseid}`,
    {
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
        Authorization: `Bearer ${user.token}`,
      },
    }
  );
}

export function getCourseByName(name, user) {
  return fetch(
    `${process.env.REACT_APP_BASE_URL}/wp-json/app/v1/course?name=${name}`,
    {
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
        Authorization: `Bearer ${user.token}`,
      },
    },
  );
}

export function getUserCourseQuiz(quizid, user) {
  return fetch(
    `${process.env.REACT_APP_BASE_URL}/wp-json/app/v1/user/${user.id}/quiz/${quizid}`,
    {
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
        Authorization: `Bearer ${user.token}`,
      },
    },
  );
}

export function getUserCourseQuizInstructions(quizid, user) {
  return fetch(
    `${process.env.REACT_APP_BASE_URL}/wp-json/app/v1/user/${user.id}/quiz/${quizid}/instructions`,
    {
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
        Authorization: `Bearer ${user.token}`,
      },
    },
  );
}
