import UserCourseSummary from "../components/entities/UserCourseSummary";

const getToken = (options) => {
    return fetch(
        `${process.env.REACT_APP_BASE_URL}/wp-json/jwt-auth/v1/token`,
        options,
    );
};

const trySetJwtTokenFromWpCookies = () => {
    console.log("CHECKING LOGGED IN STATUS");
    const data = new FormData();
    data.append("action", "get_token");
    const params = new URLSearchParams(data);
    return fetch(`${process.env.REACT_APP_BASE_URL}/wp-admin/admin-ajax.php`, {
        headers: {
            "Content-Type": "application/x-www-form-urlencoded",
        },
        credentials: "include",
        method: "POST",
        body: params,
    });
};

const getUserCourse = (courseid, user) => {
    return fetch(
        `${process.env.REACT_APP_BASE_URL}/wp-json/app/v1/user/${user.id}/course/${courseid}`,
        {
            headers: {
                "Content-Type": "application/json",
                Accept: "application/json",
                Authorization: `Bearer ${user.token}`,
            },
        },
    );
};

const enrolInCourse = (courseid, user) => {
    console.log("ENROLINCOURSE");
    const data = new FormData();
    data.append("courseid", courseid);
    const params = new URLSearchParams(data);
    return fetch(
        `${process.env.REACT_APP_BASE_URL}/wp-json/app/v1/user/${user.id}/course/${courseid}/enrol`,
        {
            headers: {
                "Content-Type": "application/json",
                Accept: "application/json",
                Authorization: `Bearer ${user.token}`,
                "Content-Type": "application/x-www-form-urlencoded",
            },
            method: "POST",
            body: params,
        },
        12,
    );
};

const getUser = (user) => {
    return fetch(
        `${process.env.REACT_APP_BASE_URL}/wp-json/app/v1/user/${user.id}`,
        {
            headers: {
                "Content-Type": "application/json",
                Accept: "application/json",
                Authorization: `Bearer ${user.token}`,
            },
        },
    );
};

const getUserAvatar = (user) => {
    return fetch(
        `${process.env.REACT_APP_BASE_URL}/wp-json/app/v1/user/${user.id}/avatar`,
        {
            headers: {
                "Content-Type": "application/json",
                Accept: "application/json",
                Authorization: `Bearer ${user.token}`,
            },
        },
    );
};

const getUserResults = (user) => {
    return fetch(
        `${process.env.REACT_APP_BASE_URL}/wp-json/app/v1/user/${user.id}/results`,
        {
            headers: {
                "Content-Type": "application/json",
                Accept: "application/json",
                Authorization: `Bearer ${user.token}`,
            },
        },
    );
};

const getSingleQuizResults = (user, quiz_id) => {
    return fetch(
        `${process.env.REACT_APP_BASE_URL}/wp-json/app/v1/user/${user.id}/quiz/${quiz_id}/results`,
        {
            headers: {
                "Content-Type": "application/json",
                Accept: "application/json",
                Authorization: `Bearer ${user.token}`,
            },
        },
    );
};

const getUserCourseSummary = (user, course_id) => {
    return fetch(
        `${process.env.REACT_APP_BASE_URL}/wp-json/app/v1/user/${user.id}/course/${course_id}/summary`,
        {
            headers: {
                "Content-Type": "application/json",
                Accept: "application/json",
                Authorization: `Bearer ${user.token}`,
            },
        },
    )
        .then((response) => {
            return response.json();
        })
        .then((summary) => {
            return new UserCourseSummary(summary);
        });
};
const getUserCertificates = (user) => {
    return fetch(
        `${process.env.REACT_APP_BASE_URL}/wp-json/app/v1/user/${user.id}/certificates`,
        {
            headers: {
                "Content-Type": "application/json",
                Accept: "application/json",
                Authorization: `Bearer ${user.token}`,
            },
        },
    ).then((response) => {
        return response.json();
    });
};

const setUnitCompleted = (user, course_id, unit_id, completed) => {
    const data = new FormData();
    data.append("complete", completed);
    const params = new URLSearchParams(data);

    return fetch(
        `${process.env.REACT_APP_BASE_URL}/wp-json/app/v1/user/${user.id}/course/${course_id}/unit/${unit_id}/setcomplete`,
        {
            headers: {
                "Content-Type": "application/json",
                Accept: "application/json",
                Authorization: `Bearer ${user.token}`,
                "Content-Type": "application/x-www-form-urlencoded",
            },
            method: "POST",
            body: params,
        },
    );
};

const setUserPledge = (user, course_id, pledge) => {
    const data = new FormData();
    data.append("pledge", pledge);
    data.append("courseid", course_id);
    const params = new URLSearchParams(data);

    return fetch(
        `${process.env.REACT_APP_BASE_URL}/wp-json/app/v1/user/${user.id}/pledge`,
        {
            headers: {
                "Content-Type": "application/json",
                Accept: "application/json",
                Authorization: `Bearer ${user.token}`,
                "Content-Type": "application/x-www-form-urlencoded",
            },
            method: "POST",
            body: params,
        },
    );
};

const completeUserCourse = (user, course_id) => {
    const data = new FormData();
    data.append("courseid", course_id);
    const params = new URLSearchParams(data);

    return fetch(
        `${process.env.REACT_APP_BASE_URL}/wp-json/app/v1/user/${user.id}/course/${course_id}/complete`,
        {
            headers: {
                "Content-Type": "application/json",
                Accept: "application/json",
                Authorization: `Bearer ${user.token}`,
                "Content-Type": "application/x-www-form-urlencoded",
            },
            method: "POST",
            body: params,
        },
    );
};

const searchKnowledgebase = (user, searchTerm, offset, forumId) => {
    const data = new FormData();
    data.append("term", searchTerm);
    data.append("offset", offset);

    if (forumId) data.append("forum", forumId);

    const params = new URLSearchParams(data);

    return fetch(
        `${process.env.REACT_APP_BASE_URL}/wp-json/app/v1/knowledgebase/search`,
        {
            headers: {
                "Content-Type": "application/json",
                Accept: "application/json",
                Authorization: `Bearer ${user.token}`,
                "Content-Type": "application/x-www-form-urlencoded",
            },
            method: "POST",
            body: params,
        },
    );
};

const recordUserFom = (user, date, fom) => {
    const data = new FormData();
    data.append("fom", fom);
    data.append("date", date);
    const params = new URLSearchParams(data);

    return fetch(
        `${process.env.REACT_APP_BASE_URL}/wp-json/app/v1/user/${user.id}/record-fom`,
        {
            headers: {
                "Content-Type": "application/json",
                Accept: "application/json",
                Authorization: `Bearer ${user.token}`,
                "Content-Type": "application/x-www-form-urlencoded",
            },
            method: "POST",
            body: params,
        },
    );
};
const setPracticeExamDate = (user, course_id, practice_exam, date) => {
    const data = new FormData();
    data.append("practice_exam", practice_exam);
    data.append("courseid", course_id);
    data.append("date", date);
    const params = new URLSearchParams(data);

    return fetch(
        `${process.env.REACT_APP_BASE_URL}/wp-json/app/v1/user/${user.id}/practice-exam`,
        {
            headers: {
                "Content-Type": "application/json",
                Accept: "application/json",
                Authorization: `Bearer ${user.token}`,
                "Content-Type": "application/x-www-form-urlencoded",
            },
            method: "POST",
            body: params,
        },
    );
};

const sendContactUsMessage = (name, email, phone, queryType, message) => {
    const data = new FormData();
    data.append("name", name);
    data.append("email", email);
    data.append("phone", phone);
    data.append("queryType", queryType);
    data.append("message", message);
    const params = new URLSearchParams(data);

    return fetch(
        `${process.env.REACT_APP_BASE_URL}/wp-json/app/v1/info/contact`,
        {
            headers: {
                "Content-Type": "application/json",
                Accept: "application/json",
                "Content-Type": "application/x-www-form-urlencoded",
            },
            method: "POST",
            body: params,
        },
    );
};

const submitStudyData = (user, subjectCode, date, hours) => {
    const data = new FormData();

    data.append("hours", hours);
    data.append("date", date);
    data.append("course", subjectCode);

    const params = new URLSearchParams(data);

    return fetch(
        `${process.env.REACT_APP_BASE_URL}/wp-json/app/v1/user/${user.id}/studydata`,
        {
            headers: {
                "Content-Type": "application/json",
                Accept: "application/json",
                "Content-Type": "application/x-www-form-urlencoded",
                Authorization: `Bearer ${user.token}`,
            },
            method: "POST",
            body: params,
        },
    );
};

const getAnnouncements = (user) => {
    return fetch(
        `${process.env.REACT_APP_BASE_URL}/wp-json/app/v1/user/${user.id}/announcements`,
        {
            headers: {
                "Content-Type": "application/json",
                Accept: "application/json",
                Authorization: `Bearer ${user.token}`,
            },
        },
    );
};

const getUserStudyData = (user) => {
    return fetch(
        `${process.env.REACT_APP_BASE_URL}/wp-json/app/v1/user/${user.id}/studydata`,
        {
            headers: {
                "Content-Type": "application/json",
                Accept: "application/json",
                Authorization: `Bearer ${user.token}`,
            },
        },
    );
};

const markAnnouncementAsRead = (user, announceid) => {
    const data = new FormData();
    data.append("announceid", announceid);
    const params = new URLSearchParams(data);

    return fetch(
        `${process.env.REACT_APP_BASE_URL}/wp-json/app/v1/user/${user.id}/announcement-read`,
        {
            headers: {
                "Content-Type": "application/json",
                Accept: "application/json",
                Authorization: `Bearer ${user.token}`,
                "Content-Type": "application/x-www-form-urlencoded",
            },
            method: "POST",
            body: params,
        },
    );
};
const submitUserCPDQuestion = (user, quizId, questionId, questionAnswer) => {
    const data = new FormData();
    data.append("question_id", questionId);
    data.append("answer", questionAnswer);

    const params = new URLSearchParams(data);

    return fetch(
        `${process.env.REACT_APP_BASE_URL}/wp-json/app/v1/user/${user.id}/quiz/${quizId}/submit-cpd`,
        {
            headers: {
                "Content-Type": "application/json",
                Accept: "application/json",
                Authorization: `Bearer ${user.token}`,
                "Content-Type": "application/x-www-form-urlencoded",
            },
            method: "POST",
            body: params,
        },
    );
}
export {
    getToken,
    getUser,
    getUserAvatar,
    getUserCourse,
    getUserResults,
    getSingleQuizResults,
    trySetJwtTokenFromWpCookies,
    setUnitCompleted,
    getUserCourseSummary,
    setUserPledge,
    setPracticeExamDate,
    searchKnowledgebase,
    sendContactUsMessage,
    recordUserFom,
    getAnnouncements,
    markAnnouncementAsRead,
    getUserStudyData,
    submitStudyData,
    completeUserCourse,
    getUserCertificates,
    enrolInCourse,
    submitUserCPDQuestion
};
