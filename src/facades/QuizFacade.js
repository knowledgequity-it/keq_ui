//http://localhost/wp-json/app/v1/user/14826/quiz/6620/start

const startQuiz = (quizid, user) => {
  return fetch(
    `${process.env.REACT_APP_BASE_URL}/wp-json/app/v1/user/${user.id}/quiz/${quizid}/start`,
    {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
        Authorization: `Bearer ${user.token}`,
      },
    }
  );
};

const getServerTime = () => {
  return fetch(
    "https://0ichcl0ofk.execute-api.ap-southeast-2.amazonaws.com/prod",
    {
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    }
  );
};

const quizStatus = (user, quiz_id) => {
  return fetch(
    `${process.env.REACT_APP_BASE_URL}/wp-json/app/v1/user/${user.id}/quiz/${quiz_id}/status`,
    {
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
        Authorization: `Bearer ${user.token}`,
      },
    }
  );
};

const quizTitle = (user, quiz_id) => {
  return fetch(
    `${process.env.REACT_APP_BASE_URL}/wp-json/app/v1/user/${user.id}/quiz/${quiz_id}/title`,
    {
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
        Authorization: `Bearer ${user.token}`,
      },
    }
  );
};

const quizCourse = (user, quiz_id) => {
  return fetch(
    `${process.env.REACT_APP_BASE_URL}/wp-json/app/v1/user/${user.id}/quiz/${quiz_id}/course`,
    {
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
        Authorization: `Bearer ${user.token}`,
      },
    }
  );
};

const submitQuiz = (user, quiz_id) => {
  return fetch(
    `${process.env.REACT_APP_BASE_URL}/wp-json/app/v1/user/${user.id}/quiz/${quiz_id}/submit`,
    {
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
        Authorization: `Bearer ${user.token}`,
      },
      method: "POST",
    }
  );
};
const submitQuestionAnswer = (user, quiz_id, question_id, answer_option) => {
  return fetch(
    `${process.env.REACT_APP_BASE_URL}/wp-json/app/v1/user/${user.id}/quiz/${quiz_id}/submit-question`,
    {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
        Authorization: `Bearer ${user.token}`,
      },
      body: JSON.stringify({
        answer: answer_option,
        question_id: question_id,
      }),
    }
  );
};

const writeScratchpadContent = (user, quiz_id, content) => {
  return fetch(
    `${process.env.REACT_APP_BASE_URL}/wp-json/app/v1/user/${user.id}/quiz/${quiz_id}/write-scratchpad`,
    {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
        Authorization: `Bearer ${user.token}`,
      },
      body: JSON.stringify({
        content: content,
      }),
    }
  );
};

const getScratchpadContent = (user, quiz_id) => {
  return fetch(
    `${process.env.REACT_APP_BASE_URL}/wp-json/app/v1/user/${user.id}/quiz/${quiz_id}/scratchpad`,
    {
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
        Authorization: `Bearer ${user.token}`,
      },
    }
  );
};

const quizStatsGraph = (user, quiz_id) => {
  return fetch(
    `${process.env.REACT_APP_BASE_URL}/wp-json/app/v1/user/${user.id}/quiz/${quiz_id}/stats-graph`,
    {
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
        Authorization: `Bearer ${user.token}`,
      },
    }
  );
};

const quizStats = (user, quiz_id) => {
  return fetch(
    `${process.env.REACT_APP_BASE_URL}/wp-json/app/v1/user/${user.id}/quiz/${quiz_id}/stats`,
    {
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
        Authorization: `Bearer ${user.token}`,
      },
    }
  );
};

export {
  startQuiz,
  submitQuestionAnswer,
  quizStatus,
  quizTitle,
  quizCourse,
  submitQuiz,
  getServerTime,
  writeScratchpadContent,
  getScratchpadContent,
  quizStatsGraph,
  quizStats,
};
