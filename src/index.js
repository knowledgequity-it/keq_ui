import React, { useEffect } from "react";
import { createRoot } from "react-dom/client";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import {
  blue,
  lightGreen,
  pink,
  indigo,
  teal,
  deepOrange,
  grey
} from "@mui/material/colors";
import CssBaseline from "@mui/material/CssBaseline";
import { BrowserRouter, useLocation } from "react-router-dom";
import UserContextProvider from "./contexts/userContext";

export default function ScrollToTop() {
  const { pathname } = useLocation();

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [pathname]);

  return null;
}

// overriding the default theme pallate to reflect KE colors across the app
const font = "'Inter', sans-serif";

const theme = createTheme({
  typography: {
    fontFamily: font,
  },
  palette: {
    primary: lightGreen,
    secondary: blue,
    gradeP: {
      main: pink[500],
      dark: pink[800],
      contrastText: "#ffffff",
    },
    gradeD: {
      main: indigo[500],
      dark: indigo[800],
      contrastText: "#ffffff",
    },
    gradeHD: {
      main: teal[500],
      dark: teal[800],
      contrastText: "#ffffff",
    },
    gradeC: {
      main: deepOrange[500],
      dark: deepOrange[800],
      contrastText: "#ffffff",
    },
    gradeNone : {
      main: grey[500],
      dark: grey[800],
      contrastText: "#ffffff",
    },
    continue: {
      main: indigo[800],
      dark: indigo[900],
      contrastText: "#ffffff",
    },
    headerNavigation: {
      main: "#656565",
      contrastText: "#ffffff",
    },
    courseNavigation: {
      main: "#232b2d",
      contrastText: "#ffffff",
    },
    white: {
      main: "#ffffff",
    },
    text: {
      primary: "rgba(0, 0, 0, 0.9)",
    },
    background: {
      default: "#fafafa",
    },
  },
});

const root = createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <BrowserRouter>
        <ScrollToTop />
        <UserContextProvider>
          <App />
        </UserContextProvider>
      </BrowserRouter>
    </ThemeProvider>
  </React.StrictMode>,
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
