import React, { useState, createContext } from "react";
import { trySetJwtTokenFromWpCookies } from "../facades/UserFacade";
import { configureLiveLogs } from "../hooks/useLiveLogging";

/*
const UserContext = React.createContext({
    
    updateUser:(u) => {this.user = u;},
    updateUserCourses:(uc) => {this.userCourses = uc;}
}); 
*/
export const UserContext = createContext();

const UserContextProvider = (props) => {

  const [userCourses, addUserCourse] = useState([]);

  const [user, setUser] = useState(() => {
    trySetJwtTokenFromWpCookies()
      .then((res) => res.json())
      .then((res) => {
        if (res.length === 0) {
          let wp_login = "/wp-login.php";
          //navigate.push("/wp-login.php");
          if (!window.location.href.endsWith(wp_login))
            window.location.href = wp_login;
          setUser(null);
          return false;
        }
        else if(res['error'])
        {
          let wp_login = "/cpa-support-gb/";
         // navigate.push("/wp-login.php");
          if (!window.location.href.endsWith(wp_login))
            window.location.href = wp_login;
          setUser(null);
          return false;
        }

        configureLiveLogs(res.token);
        localStorage.setItem("user", JSON.stringify(res));
        setUser(res);
        return true;
      });

    /*
        const savedUser = localStorage.getItem("user");
        const user = JSON.parse(savedUser);

        console.log("SAVING:", user);
        return user;
        */
  });

  return (
    <UserContext.Provider value={{ user, setUser, userCourses, addUserCourse }}>
      {props.children}
    </UserContext.Provider>
  );
};
export default UserContextProvider;
