import React, { useState, createContext, useEffect } from "react";

/*
const UserContext = React.createContext({
    
    updateUser:(u) => {this.user = u;},
    updateUserCourses:(uc) => {this.userCourses = uc;}
}); 
*/
export const QuizContext = createContext();

const QuizContextProvider = (props) => {
  const [quizData, setQuizData] = useState({});
  const [quizId, setQuizId] = useState("");
  const [scratchpadClipboard, setScratchpadClipboard] = useState("");

  return (
    <QuizContext.Provider
      value={{
        quizData,
        setQuizData,
        quizId,
        setQuizId,
        scratchpadClipboard,
        setScratchpadClipboard,
      }}
    >
      {props.children}
    </QuizContext.Provider>
  );
};
export default QuizContextProvider;
