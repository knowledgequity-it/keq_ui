
import Error404 from "../components/errorPages/Error404";




export default function NotFound()
{

 return <Error404 href={`${process.env.REACT_APP_AB_URL_PREFIX}/dashboard`} linkText="Go back to the dashboard" title="Uh Oh! We can't find what you're looking for!"/>

}