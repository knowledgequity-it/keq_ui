import { Grid, Typography, Container } from "@mui/material";
import MarkUnitCompletePanel from "../components/course/MarkUnitCompletePanel";
import GetStartedUnit from "../components/course/GetStartedUnit";
import { useContext, useEffect, useState } from "react";
import { UserContext } from "../contexts/userContext";
import { getUnit } from "../facades/UnitFacade";
import Unit from "../components/Unit";
import { useLocation, useParams } from "react-router-dom";
import Config from "../components/entities/Config";
import { setUnitCompleted } from "../facades/UserFacade";
import { Helmet } from "react-helmet-async";
import { decodeHtmlEntity } from "../utils";

export default function CommonUnit() {
  const userContext = useContext(UserContext);
  const [current_unit, setCurrentUnit] = useState();
  const [pageTitle, setPageTitle] = useState("");
  const [unit, setUnit] = useState({});
  const [course, setCourse] = useState({});
  const [courseIds, setCourseIds] = useState([]);
  const [showMarkUnitComplete, setShowMarkUnitComplete] = useState(false);
  const [completed, setCompleted] = useState(false);
  const location = useLocation();

  useEffect(() => {
    if (location.state) {
      setUnit(location.state.unit);
      setCourse(location.state.course);
      setCourseIds(location.state.courseIds);
    }
  }, [location]);

  const { info_page_name } = useParams();

  useEffect(() => {
    if (info_page_name) {
      let pageID = "";

      if (info_page_name === "study-skills") {
        pageID = Config.STUDY_ESSENTIALS_UNIT;
        setPageTitle("Study Essentials");
        setShowMarkUnitComplete(true);
      } else if (info_page_name === "key-dates") {
        pageID = Config.KEY_DATES_UNIT;
        setPageTitle("My Key Dates");
        setShowMarkUnitComplete(false);
      }

      setCurrentUnit(pageID);
    }
  }, []);

  const unitComplete = (newStatus) => {
    //set all units complete
    courseIds.forEach((c) => {
      setUnitCompleted(userContext.user, c, unit.id, newStatus);
      setCompleted(newStatus);
    });
  };

  if (!current_unit) return null;
  return (
    <Container>
      <Helmet>
        <title>{pageTitle} - KnowledgEquity</title>
      </Helmet>
      <Grid container sx={{ mt: 3, mb: 3 }} spacing={2} alignItems="flex-start">
        <Grid item order={{ xs: 3, sm: 3, md: 1 }} xs={12} md={9}>
          <Typography component="div">
            <Typography variant="h5" component="h2" gutterBottom>
              {pageTitle}
            </Typography>
            <GetStartedUnit
              selected_unit={current_unit}
              unit_url={info_page_name}
            />
            {showMarkUnitComplete && (
              <MarkUnitCompletePanel
                next_unit={{}}
                previous_unit={{}}
                course={course}
                unit={unit}
                onUnitCompleted={unitComplete}
                completionStatus={completed}
              />
            )}
          </Typography>
        </Grid>
      </Grid>
    </Container>
  );
}
