import React, { useContext, useEffect, useState } from "react";
import { useTheme } from "@mui/material/styles";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import Divider from "@mui/material/Divider";
import ListItemText from "@mui/material/ListItemText";
import ListItemAvatar from "@mui/material/ListItemAvatar";
import Typography from "@mui/material/Typography";
import logo from "./../assets/logo.png";
import UserContextProvider from "../contexts/userContext";
import { useNavigate } from "react-router-dom";
import CircularProgress from "@mui/material/CircularProgress";
import Box from "@mui/material/Box";

function fetchCourses(user) {
  return new Promise((resolve) => {
    fetch("http://localhost/wp-json/app/v1/course/3745", {
      headers: {
        //'Content-Type': 'application/json',
        //'Accept': 'application/json',
        Authorization: "Bearer " + user.jwt,
      },
    })
      .then(function (response) {
        return response.json();
      })
      .then(function (myJson) {});
  });
}

export default function CourseList() {
  const theme = useTheme();
  const { user, updateUser } = useContext(UserContextProvider);
  const navigate = useNavigate();
  const [courses, setCourses] = useState([]);

  useEffect(() => {
    fetchCourses(user).then((courses) => {
      setCourses(courses);
    });
  }, []);

  // simulating an api call to fetch all courses user can see

  const handleCourseClick = (name) => {
    navigate({
      pathname: `${process.env.REACT_APP_AB_URL_PREFIX}/course`,
      state: { courseName: name },
    });
  };
  return (
    <Box
      sx={{
        position: "relative",
        marginTop: theme.spacing(8),
        display: "flex",
        flexDirection: "column",
        alignItems: "stretch",
      }}
    >
      {courses.length == 0 && (
        <CircularProgress
          sx={{
            position: "absolute",
            marginTop: "20%",
            marginLeft: "40%",
          }}
          color="primary"
          style
        />
      )}
      {courses.length > 0 && (
        <div>
          <h1>hi {user.name} </h1>
          <List
            sx={{
              position: "relative",
              marginTop: theme.spacing(8),
              display: "flex",
              flexDirection: "column",
              alignItems: "stretch",
            }}
          >
            {courses.map((course) => {
              return (
                <div>
                  {" "}
                  <ListItem
                    alignItems="flex-start"
                    onClick={() => handleCourseClick(course.title)}
                  >
                    <ListItemAvatar>
                      <img src={logo} alt="Logo" />
                    </ListItemAvatar>
                    <ListItemText
                      primary={
                        <Typography
                          variant="h6"
                          sx={{
                            fontSize: "2em",
                            fontWeight: "400",
                            color: "#aaa",
                          }}
                        >
                          {course.title}
                        </Typography>
                      }
                      secondary={
                        <React.Fragment>
                          <Typography
                            component="span"
                            variant="body2"
                            sx={{ display: "inline" }}
                            color="textPrimary"
                          ></Typography>
                          {course.details}
                        </React.Fragment>
                      }
                    />
                  </ListItem>
                  <Divider variant="inset" component="li" />
                </div>
              );
            })}
          </List>
        </div>
      )}
    </Box>
  );
}
