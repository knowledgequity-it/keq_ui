import React, { useContext, useEffect, useState } from "react";
import Grid from "@mui/material/Grid";
import { UserContext } from "../contexts/userContext";
import CourseCard from "../components/CourseCard";
import { getUser, getUserCourse } from "../facades/UserFacade";
import Container from "@mui/material/Container";
import LinearProgress from "@mui/material/LinearProgress";
import ResultsCard from "../components/cards/ResultsCard";
import SchedulerCard from "../components/scheduler/SchedulerCard";
import useLiveLogging from "../hooks/useLiveLogging";
import { useTheme } from "@mui/material/styles";
import CircularProgress from "@mui/material/CircularProgress";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Divider from "@mui/material/Divider";
import Paper from "@mui/material/Paper";
import Stack from "@mui/material/Stack";













import EasyAccessCardRow from "../components/dashboard/EasyAccessCardRow";
import DashboardSubjectRow from "../components/DashboardSubjectRow";
import { Helmet } from "react-helmet-async";
import Link from "@mui/material/Link";
import DailyTip from "../components/daily-tip/DailyTip";

// const useStyles = makeStyles((theme) => ({
//   root: {
//     flexGrow: 1,
//     paddingTop: theme.spacing(3),
//     paddingLeft: 0,
//     paddingRight: 0,
//     height: "100%",
//   },
//   progressBar: {
//     width: "100%",
//     "& > * + *": {
//       marginTop: theme.spacing(2),
//     },
//   },
// }));

export default function Dashboard() {
  const theme = useTheme();
  const userContext = useContext(UserContext);
  const [userCourseIds, setUserCoursesIds] = useState([]);
  const [study_essentials, setStudyEssentials] = useState();
  const [userCourses, setUserCourses] = useState([]);
  const [loadingPct, setLoadingPct] = useState(0);
  const loadingRef = React.createRef();
  loadingRef.current = loadingPct;
  const candidateAction = useLiveLogging();
  const [loading, setLoading] = useState(true);
  const [loadError, setLoadError] = useState(false);

  useEffect(() => {
    candidateAction({
      component: "Dashboard",
      event: "initialising Dashboard for user",
    });

    getUser(userContext.user)
      .then((response) => response.json())
      .then((userDetails) => {
        setUserCoursesIds(userDetails.courses);
        setStudyEssentials(userDetails.study_essentials);
        setLoading(false);
      })
      .catch((error) => {
        candidateAction({
          component: "Dashboard",
          event: "Error loading user details for Dashboard",
          error: error,
        });
        setLoadError(true);
      });
  }, []);

  const makePanels = (courses) => {
    if (!courses) return;

    return courses.map(function (course, index) {
      return (
        <Grid item xs={12} sm={6} md={4} key={index}>
          <CourseCard
            course={course.title}
            course_id={course.id}
            course_url={course.url_name}
            course_img={course.image}
          ></CourseCard>
        </Grid>
      );
    });
  };

  const getProgress = () => {
    return Math.round(
      (loadingRef.current /
        (userCourseIds.length == 0 ? 1 : userCourseIds.length)) *
        100,
    );
  };

  return loading ? (
    <>
      <Helmet>
        <title>Dashboard - KnowledgEquity</title>
      </Helmet>
      <Box
        sx={{
          width: "100%",
          "& > * + *": {
            marginTop: theme.spacing(2),
          },
        }}
      >
        <LinearProgress color="primary" />

        <Grid
          container
          justifyContent="center"
          direction="column"
          alignItems="center"
        >
          <Grid item xs={12}>
            <Box sx={{ position: "relative", display: "inline-flex" }}>
              <CircularProgress variant="determinate" value={getProgress()} />
              <Box
                sx={{
                  top: 0,
                  left: 0,
                  bottom: 0,
                  right: 0,
                  position: "absolute",
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                <Typography variant="caption" component="div" color="secondary">
                  {`${getProgress()}%`}
                </Typography>
              </Box>
            </Box>
          </Grid>
          <Grid item>
            <Typography>One moment while we load your courses.</Typography>
          </Grid>
        </Grid>
      </Box>
    </>
  ) : userCourseIds.length == 0 || loadError ? (
    <Container maxWidth="xl">
      <Helmet>
        <title>Dashboard - KnowledgEquity</title>
      </Helmet>
      <Box
        sx={{
          flexGrow: 1,
          paddingTop: theme.spacing(3),
          paddingLeft: 0,
          paddingRight: 0,
          height: "100%",
          mb: 3,
        }}
      >
        <Stack spacing={3} alignItems="center">
          {userCourseIds.length == 0 && (
            <>
              <Typography>
                Sorry you don't seem to be enrolled in any courses.
              </Typography>
              <Typography component="div">
                If you are having any issues with your enrolment please email us
                on{" "}
                <Link
                  onClick={() => {
                    console.log("Poop!");
                  }}
                  href="mailto:help@keq.com.au"
                  color="secondary"
                >
                  help@keq.com.au
                </Link>
              </Typography>
            </>
          )}{" "}
          {loadError && (
            <>
              <Typography>
                Sorry, an error occurred while loading your courses. Please try
                again later.
              </Typography>
              <Typography component="div">
                If the problem continues please contact us at{" "}
                <Link
                  onClick={() => {
                    console.log("Poop!");
                  }}
                  href="mailto:help@keq.com.au"
                  color="secondary"
                >
                  help@keq.com.au
                </Link>
              </Typography>
            </>
          )}
        </Stack>
      </Box>
    </Container>
  ) : (
    <Container maxWidth="xl">
      <Helmet>
        <title>Dashboard - KnowledgEquity</title>
      </Helmet>
      <Box
        sx={{
          flexGrow: 1,
          paddingTop: theme.spacing(3),
          paddingLeft: 0,
          paddingRight: 0,
          height: "100%",
          mb: 3,
        }}
      >
        <Stack spacing={3}>
          <EasyAccessCardRow
            userCourseIds={userCourseIds}
            studyEssentials={study_essentials}
          />
          <DailyTip/>
          <Box>
            <Typography variant="h5" sx={{ mb: 1 }}>
              My Courses
            </Typography>
            <Paper
              elevation={0}
              sx={{
                "&.MuiPaper-elevation0": {
                  backgroundColor: theme.palette.grey[200],
                  borderRadius: 5,
                },
              }}
            >
              {userCourseIds.length > 0 ? (
                userCourseIds.map(function (course, index) {
                  return <DashboardSubjectRow course={course} key={index} />;
                })
              ) : (
                <Typography> You are not enrolled in any Courses. </Typography>
              )}
            </Paper>
          </Box>
        </Stack>
      </Box>
    </Container>
  );
}
