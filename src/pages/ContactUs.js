import { Grid, Stack, Typography } from "@mui/material";
import ContactUsForm from "../components/contact-us/ContactUsForm";
import FaqContactUs from "../components/contact-us/FaqContactUs";
import SocialLinks from "../components/contact-us/SocialLinks";
import { Helmet } from "react-helmet-async";

export default function ContactUs()
{
  return (
      <>
      <Helmet>
      <title>Contact Us - KnowledgEquity</title>
      </Helmet>
      <Grid container direction="column" alignItems="center" justifyContent="center">
        <Grid item>
            <Typography variant="h2">Contact Us</Typography>
        </Grid>
        <Grid item>
            <Grid container direction="row">
                <Grid item xs={12} sm={6}>
                    <FaqContactUs/>
                </Grid>
                <Grid item xs={12} sm={6}>
                    <Grid container direction="column">
                        <Grid item>
                            <ContactUsForm/>
                        </Grid>
                        <Grid item>
                            <SocialLinks/>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
        
    </Grid>
      </>
	 )
}
