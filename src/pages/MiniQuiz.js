import React, { useEffect, useContext } from "react";
import QuizPage from "../components/quiz/QuizPage";
import { useState } from "react";
import Paper from "@mui/material/Paper";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import Box from "@mui/material/Box";
import { useTheme } from "@mui/material/styles";
import QuizContextProvider from "../contexts/QuizContext";
import { quizTitle } from "../facades/QuizFacade";
import { UserContext } from "../contexts/userContext";
import { useLocation } from "react-router-dom";
import { Helmet } from "react-helmet-async";
import { decodeHtmlEntity } from "../utils";

// const useStyles = makeStyles((theme) => ({
//   root: {
//     paddingTop: theme.spacing(2),
//   },
// }));

const normaliseUri = (uri) => {
  let x = uri.replace(`${process.env.REACT_APP_AB_MINIQUIZ_PREFIX}`, "");
  if (x.substr(-1) === "/") x = x.substring(0, x.length - 1);

  return x;
};

export default function MiniQuiz({ scratchpadVisible, setScratchpadVisible }) {
  let unit_return = new URLSearchParams(useLocation().search).get("ru");
  const theme = useTheme();
  const userContext = useContext(UserContext);

  const [quiz_name] = useState(() => {
    let parts = normaliseUri(window.location.pathname).split("/");

    if (parts.length === 3) return parts[2];
    else return null;
  });

  const [quiz_title, setQuizTitle] = useState(null);

  useEffect(() => {
    if (!quiz_name) return;
    else {
      quizTitle(userContext.user, quiz_name)
        .then((response) => response.json())
        .then((content) => {
          setQuizTitle(content.title);
        });
    }
  }, [quiz_name, userContext.user]);

  return (
    <Container>
      <Helmet>
	<title>{ decodeHtmlEntity(quiz_title) } - KnowledgEquity</title>
      </Helmet>
      <Box sx={{ paddingBottom: theme.spacing(2) }} spacing={2}>
        <h2 dangerouslySetInnerHTML={{ __html: quiz_title }}></h2>

        <Typography component="div">
          <QuizContextProvider>
            <QuizPage
              quiz_id={quiz_name}
              unit_return={unit_return}
              scratchpadVisible={scratchpadVisible}
              setScratchpadVisible={setScratchpadVisible}
            />
          </QuizContextProvider>
        </Typography>
      </Box>
    </Container>
  );
}
