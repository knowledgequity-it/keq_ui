import React, { useContext, useEffect, useState } from "react";
import { getEventsForResources, getLocationsForEvent, getSlotsForEventLocation, getUserBookings, createUserBooking } from '../facades/SchedulerFacade';
import { getUser} from '../facades/UserFacade'
import { UserContext } from "../contexts/userContext";
import SchedulerForm from '../components/scheduler/SchedulerForm';
import { Grid, Typography } from "@mui/material";
import BookingList from '../components/scheduler/BookingsList'
import ItemFilterList from "../components/scheduler/ItemFilterList";
import List from '@mui/material/List'
import ListItem from '@mui/material/ListItem'
import { ListItemText } from "@mui/material";
import { Collapse } from "@mui/material";
import { ExpandLess, ExpandMore } from "@mui/icons-material";
import { ListItemIcon } from "@mui/material";
import useLiveLogging from "../hooks/useLiveLogging";

export default function Scheduler(props)
{
    const userContext = useContext(UserContext);
    const [courses, setUserCoursesIds] = useState([]);
    const [userEvents, setUserEvents] = useState([]);
    const [userLocations, setUserLocations] = useState([]);
    const [userBookings, setUserBookings] = useState([]);

    const [selectedEventId, setSelectedEventId] = useState({})
    const [selectedLocationId, setSelectedLocationId] = useState({})
    const [selectedSlotId, setSelectedSlotId] = useState({});

    const [filteredLocations, setFilteredLocations] = useState([]);
    const [filteredSlots, setFilteredSlots] = useState([]);
    const [filteredEvents, setFilteredEvents] = useState([]);

    const [slotsByLocation, setSlotsByLocation] = useState([])
    const [slotsByTimeslot, setSlotsByTimeslot] = useState([])

    const [openBookings, setOpenBookings] = useState(false);
    const candidateAction = useLiveLogging();

    const handleBookingsClick = () => {

        candidateAction({
            component: "Scheduler",
            event: `Your Bookings Selection  ${(openBookings ? "Collapsed" : "Expanded")}`
        });

        setOpenBookings(!openBookings);
      };
    const [openTimes, setOpenTimes] = useState(false);
    const handleTimesClick = () => {

        candidateAction({
            component: "Scheduler",
            event: `Available Times Selection ${(openTimes ? "Collapsed" : "Expanded")}`
        });
        setOpenTimes(!openTimes);
      };
    const [openLocations, setOpenLocations] = useState(false);
    const handleLocationsClick = () => {

          candidateAction({
            component: "Scheduler",
            event: `Available Locations Selection ${(openLocations ? "Collapsed" : "Expanded")}`
          });
          setOpenLocations(!openLocations);
        };

    const eventSelectionChanged = (theSelectedEvent) => {
        //console.log("CHANGE EVENT", theSelectedEvent, userLocations, filteredLocations);

        candidateAction({
            component: "Scheduler",
            event: "Event Selection Changed to "+ theSelectedEvent.event_id + " : " + theSelectedEvent.name 
        });
        
        setSelectedEventId(theSelectedEvent.event_id);
    }
    const locationSelectionChanged = (theSelectedLocation) => {
        //console.log("CHANGE LOC", theSelectedLocation);

        candidateAction({
            component: "Scheduler",
            event: "Location Selection Changed to "+ theSelectedLocation.location_id + " : " + theSelectedLocation.name 
        });

        setSelectedLocationId(theSelectedLocation.location_id);
    }
    const dateSelectionChanged = (theSelectedDate) => {
        //console.log("CHANGE DATE", theSelectedDate);

        candidateAction({
            component: "Scheduler",
            event: "Date Selection Changed to "+ theSelectedDate.event_slot_id + " : " + theSelectedDate.event_slot_time 
        });

        setSelectedSlotId(theSelectedDate.event_slot_id);
    }

    const saveBooking = () => {
        //console.log("Booking data", selectedSlotId, selectedLocationId, selectedEventId);

        candidateAction({
            component:"Scheduler",
            event: "Saving booking for Slot Id " 
              + selectedSlotId + " Location Id " + selectedLocationId + " Event Id " + selectedEventId});

        createUserBooking(userContext.user, selectedSlotId)
        .then(response => response.json())
        .then(res => {
            //console.log("SAVED BOOKING", res);
            candidateAction({
                event: "Saved Booking " + res,
                component: "Scheduler"
            });
        
        });
    }

    const findDistinctByKey = (array, key) => {
        return array.filter((value, index, arr) => arr.findIndex(ele => (ele[key] === value[key]))===index)
    }
    const createLocationsByTimeslot = () => {
        let slotsByTimeslot = [];
       // let distinctSlots = findDistinctByKey(filteredSlots, 'event_slot_id')//filteredSlots.filter((value, index, array) => array.findIndex(ele => (ele.event_slot_time === value.event_slot_time))===index)

        userEvents.forEach(slot => {
            let the_slot = {}
            if(slot.event_id === selectedEventId)
            {
                the_slot['key'] = slot;
                let the_slot_locations = userLocations.filter(ul => ul.location_id === slot.location_id)            
                the_slot['values'] = the_slot_locations;
                slotsByTimeslot.push(the_slot);
            }
        })
        return slotsByTimeslot
    }

    const createSlotsByLocation = () => {
        let slotsByLocation = [];

        filteredLocations.forEach(loc => {
            let the_location = {}
            the_location['key'] = loc;
            let the_location_slots = userEvents.filter(ue => ue.location_id === loc.location_id && ue.event_id === selectedEventId);
            the_location['values'] = the_location_slots;
            slotsByLocation.push(the_location);
        }) 

        return slotsByLocation
    }

    const getUSerBookingsDisplayFormat = (ub) => {
        let bookings = []
        ub.forEach(booking => {
            let book = {}
            book['key'] = booking
            book['values'] = []
            bookings.push(book)
        })
        return bookings;
    }
    useEffect(() => {        
        getUser(userContext.user)
        .then((response) => response.json())
        .then((userDetails) => {
            setUserCoursesIds(userDetails.courses);
        });
    }, []);

    useEffect(() => {        
        getUserBookings(userContext.user)
        .then((response) => response.json())
        .then((bookings) => {
            setUserBookings(getUSerBookingsDisplayFormat(bookings));
        });
    }, []);
    
    useEffect(() => {   
        setSlotsByLocation(createSlotsByLocation())
        setSlotsByTimeslot(createLocationsByTimeslot())
      }
    ,[selectedEventId]);

    useEffect(() => {
        let eventList = [];
        getEventsForResources(userContext.user, courses)
        .then(response => response.json())
        .then(results => {
                let events = results.flat(1);
                setUserEvents(events)
                setFilteredEvents(events);
            }
        )
    }, [courses]);


    useEffect(() => {
        let distinctEvents = findDistinctByKey(userEvents, 'event_id')
        .map(event => {return {event_id :event.event_id,
                               name : event.name
                                }
        })
        setFilteredEvents(distinctEvents)
    }, [userEvents]);

    useEffect(()=> {
        Promise.all(filteredEvents.map(e => e.event_id).map(id => getLocationsForEvent(userContext.user, id)))
        .then(responses => {return Promise.all(responses.map(r => {return r.json()}))})
        .then(results => {
                let locs = results.flat(1)
                setUserLocations(locs);
                setFilteredLocations(findDistinctByKey(locs, 'location_id'));
        });
    }, [filteredEvents])

    useEffect(() => {
            setFilteredSlots(userEvents.filter(event => event.event_id === selectedEventId && event.location_id === selectedLocationId));
    }, [selectedLocationId, selectedEventId])

    useEffect(() => {
        setFilteredLocations(userLocations.filter(location => location.event_id === selectedEventId));
        setFilteredSlots(userEvents.filter(event => event.event_id === selectedEventId && event.location_id === selectedLocationId));
    }, [selectedEventId])

    return (
    <Grid container spacing={2} direction="column">
        <Grid item>
                <SchedulerForm user_events={filteredEvents}
                            event_slots={filteredSlots} 
                            event_locations={filteredLocations} 
                            onEventSelection={eventSelectionChanged} 
                            onLocationSelection={locationSelectionChanged}
                            onDateSelection={dateSelectionChanged}
                            onBookingSave={saveBooking}></SchedulerForm>
        </Grid>
        <Grid item>
            <Grid container spacing={2} direction="row">
                <Grid item xs={12} md={4} lg={4}>
                    <List>
                    <ListItem onClick={handleBookingsClick}>
                        <ListItemIcon>
                        </ListItemIcon>
                        <ListItemText primary="Your Bookings" />
                        {openBookings ? <ExpandLess /> : <ExpandMore />}
                    </ListItem>
                    <Collapse in={openBookings} timeout="auto" unmountOnExit>
                        <Typography variant="h5">
                            Your Bookings 
                        </Typography>
                        <BookingList
                            title="Please make sure the details of your bookings are correct."
                            data = {userBookings}
                            label_text="event_slot_time"
                            primary_list_key="user_event_slot_id"
                            primary_list_display_key="event_name"
                            primary_list_sub_display_key={['name','address_1','address_2','address_3','state','country','postcode']}
                            primary_icon="time"
                            secondary_list_key="event_location_id"
                            secondary_list_display_key="name"
                            secondary_icon="location">   
                    </BookingList>
                    </Collapse>
                    </List>
                </Grid>
                <Grid item xs={12} md={4} lg={4}>
                    <List>
                    <ListItem onClick={handleTimesClick}>
                        <ListItemIcon>
                        </ListItemIcon>
                        <ListItemText primary="Available Locations by Time" />
                        {openTimes ? <ExpandLess /> : <ExpandMore />}
                    </ListItem>
                    <Collapse in={openTimes} timeout="auto" unmountOnExit>
                    <ItemFilterList
                        title="Available Times"
                        data = {slotsByTimeslot}
                        primary_list_key="event_slot_id"
                        primary_list_display_key="event_slot_time"
                        primary_list_sub_display_key={[]}
                        primary_icon="time"
                        secondary_list_key="event_location_id"
                        secondary_list_display_key="name"
                        secondary_icon="location"
                    >   
                    </ItemFilterList>
                    </Collapse>
                    </List>
                </Grid>
                <Grid item xs={12} md={4} lg={4}> 
                <List>
                    <ListItem onClick={handleLocationsClick}>
                        <ListItemIcon>
                        </ListItemIcon>
                        <ListItemText primary="Available Times by Location" />
                        {openLocations ? <ExpandLess /> : <ExpandMore />}
                    </ListItem>
                    <Collapse in={openLocations} timeout="auto" unmountOnExit>
                <ItemFilterList
                        title="Available Locations"
                        data = {slotsByLocation}
                        primary_list_key="location_id"
                        primary_list_display_key="name"
                        primary_list_sub_display_key={['address_1', 'address_2', 'address_3', 'state','postcode','country']}
                        primary_icon="location"
                        secondary_list_key="event_slot_id"
                        secondary_list_display_key="event_slot_time"
                        secondary_icon="time"
                    >   
                    </ItemFilterList>
                    </Collapse>
                    </List>
                </Grid>
            </Grid>
            
        </Grid>
    </Grid>)
    
    
}


/*
 
*/