import { useContext, useState, useEffect } from "react";
import { useTheme } from "@mui/material/styles";
import Typography from "@mui/material/Typography";
import { Grid } from "@mui/material";
import CourseSidebar from "../components/CourseSidebar";
import UnitTypeRenderer from "../components/course/UnitTypeRenderer";
import { getCourseByName } from "../facades/CourseFacade";
import { getUserCourse, enrolInCourse } from "../facades/UserFacade";
import { UserContext } from "../contexts/userContext";
import Container from "@mui/material/Container";
import Error404 from "../components/errorPages/Error404";
import { useNavigate, useLocation } from "react-router-dom";
import Box from "@mui/material/Box";
import CircularProgress from "@mui/material/CircularProgress";
import UserCourse from "../components/entities/UserCourse";
import CourseObj from "../components/entities/Course";
import { Helmet } from "react-helmet-async";
import { decodeHtmlEntity } from "../utils";

export default function Course({ scratchpadVisible, setScratchpadVisible }) {
  const theme = useTheme();
  const userContext = useContext(UserContext);
  const location = useLocation();

  const { state } = useLocation();
  const navigate = useNavigate();

  const [userCourse, setUserCourse] = useState(null);
  const [course, setCourse] = useState(new CourseObj({}));
  const [selected_unit, setSelectedUnit] = useState({});
  const [current_section, setCurrentSection] = useState("");
  const [course_url, _setCourseUrl] = useState(null);
  const [show_sidebar, setShowSidebar] = useState(true);
  const [enrolment_change, setEnrolmentChange] = useState(false);

  const [isError, setIsError] = useState(false);
  const [isLoading, setLoading] = useState(true);

  const normaliseUri = (uri) => {
    let normalised = uri.replace(`${process.env.REACT_APP_AB_URL_PREFIX}`, "");
    if (normalised.substr(-1) == "/")
      normalised = normalised.substring(0, normalised.length - 1);
    return normalised;
  };

  const getUnitUrl = () => {
    let parts = normaliseUri(window.location.pathname).split("/");
    if (parts.length === 4)
      //course url with unit part (/course/coursename/unitname) return last bit
      return parts[3];
    else return null;
  };
  const [unit_url, _setUnitUrl] = useState(getUnitUrl());

  const setCourseUrl = () => {
    let parts = normaliseUri(window.location.pathname).split("/");
    if (parts.length === 3 || parts.length === 4)
      //course url with and without unit part (/course/coursename/unitname)
      _setCourseUrl(parts[2]);
    else _setCourseUrl(null);
  };

  const setUnitUrl = () => {
    _setUnitUrl(getUnitUrl());
  };

  useEffect(() => {
    setUnitUrl();
    setCourseUrl();
  }, [location]);

  useEffect(() => {
    if (course.id) {
      //      if (course.id == 372381 || course.id == 372523 || course.id == 85853) {
      if (course.id == 85853) {
        window.location.replace(
          `${process.env.REACT_APP_BASE_URL}/classic/course/${course.url_name}`,
        );
        return;
      }
      //console.log("GETTING COURSE DEETS", isLoading)
      getUserCourse(course.id, userContext.user)
        .then((res) => {
          if (!res.ok) {
            throw new Error(`Response status: ${res.status}`);
          }
          return res.json();
        })
        .then((userCourse) => {
          // userCourse.user_course_details.forEach((item, index) => {
          //   if (item.title.match(/(?<=&#8211; ).*/g) != null) {
          //     userCourse.user_course_details[index].title =
          //       item.title.match(/(?<=&#8211; ).*/g)[0];
          //   }
          // });
          console.log("SETTING USER COURSE");
          setUserCourse(new UserCourse(userCourse));
        })
        .catch((error) => {
          console.log(error);
          navigate(`/course/${course.url_name}`);
        });
    }
  }, [course, userContext.user, enrolment_change]);

  useEffect(() => {
    if (
      unit_url !== null &&
      userCourse &&
      userCourse?.user_course_details.length > 0
    ) {
      userCourse.getUnit(unit_url);
      setSelectedUnit(userCourse.getUnit(unit_url));
    }
    setLoading(false);
  }, [userCourse, unit_url]);

  useEffect(() => {
    if (course_url != null) {
      setLoading(true);
      getCourseByName(course_url, userContext.user)
        .then((response) => {
          if (!response.ok) throw response.status;
          else return response.json();
        })
        .then((course) => {
          setIsError(false);
          setSelectedUnit({});
          // console.log("course", course);
          // course.curriculum.forEach((item, index) => {
          //   if (item.type == "unit" || item.type == "quiz") {
          //     console.log(
          //       "MATCH",
          //       item.title,
          //       item.title.match(/(?<=&#8211; ).*/g),
          //     );
          //     if (item.title.match(/(?<=&#8211; ).*/g) != null) {
          //       course.curriculum[index].title =
          //         item.title.match(/(?<=&#8211; ).*/g)[0];
          //     }
          //   }
          // });
          // console.log("course2", course);
          setCourse(new CourseObj(course));
        })
        .catch((error) => {
          if (error === 404) {
            setIsError(true);
            setLoading(false);
          }
        });
    }
  }, [course_url, userContext.user]);

  // useEffect(() => {
  //   if (course.curriculum) {
  //     course.curriculum.every((item, index) => {
  //       console.log("ITEM", item, "SELECTED", selected_unit);
  //       console.log(selected_unit.id === item.id);
  //       if (selected_unit.id === item.id) {
  //         for (var i = index; i--; i >= 0) {
  //           console.log(course.curriculum[i].id);
  //           if (course.curriculum[i].id === undefined)
  //             setCurrentSection(course.curriculum[i].title);
  //         }

  //         return false;
  //       }
  //       return true;
  //     });
  //   }
  // }, [selected_unit, course]);

  const onEnrolClicked = () => {
    console.log("ENROL CLICKED", course.id, userContext.user);
    enrolInCourse(course.id, userContext.user)
      .then((response) => {
        if (!response.ok) throw response.status;
        else return response.json();
      })
      .then((course) => {
        setEnrolmentChange(true);
      })
      .catch((error) => {
        if (error === 404) {
        }
      });
  };

  const onUnitCompleted = (unitid) => {
    userCourse.setUnitComplete(unitid);
    setUserCourse(userCourse.makeCopy());
  };

  var getStartedButton = (
    <Box sx={{ display: "flex" }}>
      <CircularProgress />
    </Box>
  );
  var backToClassicButton = null;

  //  console.log("sut", selected_unit.title);
  //  console.log("userCourse", userCourse);
  if (!course?.id) return null;
  return isError ? (
    <>
      <Helmet>
        <title>Error - KnowledgEquity</title>
      </Helmet>
      <Error404
        href={`${process.env.REACT_APP_AB_URL_PREFIX}/dashboard`}
        linkText="Go back to the dashboard"
        title="Uh Oh! We can't find that course"
      />
    </>
  ) : (
    <Container maxWidth="xl">
      <Helmet>
        <title>{decodeHtmlEntity(selected_unit.title)} - KnowledgEquity</title>
      </Helmet>
      <Grid container sx={{ mt: 3, mb: 3 }} spacing={2} alignItems="flex-start">
        {!state && (
          <Grid
            item
            order={{ xs: 1, sm: 1, md: 1 }}
            xs={12}
            md={3}
            lg={3}
            sx={{ "border-color": "#000000" }}
          >
            <CourseSidebar
              course={course}
              course_url={course_url}
              userCourseDetails={userCourse?.user_course_details}
              selected_unit={selected_unit}
              setUnitUrl={setUnitUrl}
              hide_ATE={userCourse?.hide_ate}
              hide_KB={userCourse?.hide_kb}
              onEnrolClicked={onEnrolClicked}
            />
          </Grid>
        )}
        <Grid item order={{ xs: 3, sm: 3, md: 2 }} xs={12} md={9}>
          <Typography component="div">
            <Typography variant="h5" component="h2" gutterBottom>
              <span dangerouslySetInnerHTML={{ __html: selected_unit.title }} />
            </Typography>
            <UnitTypeRenderer
              onUnitCompleted={onUnitCompleted}
              unitSelected={setSelectedUnit}
              course={course}
              selected_unit={selected_unit}
              next_unit={userCourse?.getNextUnit(selected_unit.url_name)}
              previous_unit={userCourse?.getPreviousUnit(
                selected_unit.url_name,
              )}
              course_url={course_url}
              unit_url={unit_url}
              scratchpadVisible={scratchpadVisible}
              setScratchpadVisible={setScratchpadVisible}
            />
          </Typography>
        </Grid>
      </Grid>
    </Container>
  );
}
