import React, { useReducer, useContext, useState } from "react";
import Button from "@mui/material/Button";
import CssBaseline from "@mui/material/CssBaseline";
import { useNavigate } from "react-router-dom";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import logo from "./../assets/logo.png";
import { UserContext } from "../contexts/userContext";
import { getToken } from "../facades/UserFacade";
import Alert from "@mui/lab/Alert";
import useLiveLogging, { configureLiveLogs } from "../hooks/useLiveLogging";
import Link from "@mui/material/Link";
import Box from "@mui/material/Box";
import { useTheme } from "@mui/material/styles";
import { Helmet } from "react-helmet-async";

// const useStyles = makeStyles((theme) => ({
//   paper: {
//     marginTop: theme.spacing(8),
//     display: "flex",
//     flexDirection: "column",
//     alignItems: "center",
//   },
//   avatar: {
//     margin: theme.spacing(1),
//     backgroundColor: theme.palette.secondary.main,
//   },
//   form: {
//     width: "100%", // Fix IE 11 issue.
//     marginTop: theme.spacing(3),
//   },
//   submit: {
//     margin: theme.spacing(3, 0, 2),
//   },
// }));

// reducer for state management of Login Page
function loginReducer(state, action) {
  switch (action.type) {
    case "changeEmail":
      return { ...state, email: action.value };
    case "changePassword":
      return { ...state, password: action.value };

    default:
      throw new Error(`${action.type} is not valid action`);
  }
}

export default function Login() {
  const theme = useTheme();
  const [{ email, password }, dispatch] = useReducer(loginReducer, {});
  const userContext = useContext(UserContext);
  const [loginError, setLoginError] = useState();
  const navigate = useNavigate();

  const candidateAction = useLiveLogging();
  const requestOptions = {
    method: "POST",
    headers: { "Content-Type": "application/x-www-form-urlencoded" },
    body: "",
  };
  const handleLogin = () => {
    requestOptions.body = `username=${email}&password=${password}`;
    getToken(requestOptions)
      .then((response) => response.json())
      .then((data) => {
        if (!data.success) setLoginError(data.message);
        else {
          let userData = data.data;
          let user = {
            token: userData.token,
            id: userData.id,
            email: userData.email,
            nicename: userData.email,
            firstName: userData.firstName,
            lastName: userData.lastNAme,
            displayName: userData.displayName,
          };
          userContext.setUser(user);

          // logging can only be configured when we have a jwt token
          configureLiveLogs(userData.token);

          candidateAction({
            component: "Login",
            event: "login successfully",
            user_email: email,
            page: "Login",
          });

          localStorage.setItem("user", JSON.stringify(user));

          navigate(`${process.env.REACT_APP_AB_URL_PREFIX}/dashboard`);
        }
      });
  };

  return (
    <Container component="main" maxWidth="xs">
      <Helmet>
      <title>Login - KnowledgEquity</title>
      </Helmet>
      <CssBaseline />
      <Box
        sx={{
          marginTop: theme.spacing(8),
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
        }}
      >
        <img src={logo} alt="Logo" />

        <Typography component="h1" variant="h5">
          Session Expired
        </Typography>
        <Typography component="h1" variant="h5">
          <Link href="/wp-login.php" underline="hover">
            Click here to log in.
          </Link>
        </Typography>

        {loginError && <Alert severity="error">{loginError}</Alert>}
      </Box>
    </Container>
  );
}

/*
                <form className={classes.form} noValidate>
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <TextField
                                variant="outlined"
                                required
                                fullWidth
                                id="email"
                                label="Email Address"
                                name="email"
                                autoComplete="email"
                                onChange = {e => dispatch({type: 'changeEmail', value: e.target.value})}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                variant="outlined"
                                required
                                fullWidth
                                name="password"
                                label="Password"
                                type="password"
                                id="password"
                                autoComplete="current-password"
                                onChange = {e => dispatch({type: 'changePassword', value: e.target.value})}
                            />
                        </Grid>
                    </Grid>
                    <Button
                        //type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                        onClick={handleLogin}
                    >
                        Login
                    </Button>
                </form>
                */
