import Grid from "@mui/material/Grid";
import FoMPreference from "../components/FoMPreference";
import MailchimpSignupForm from "../components/MailchimpSignupForm";
import Card from "@mui/material/Card";
import Container from "@mui/material/Container";
import Typography from "@mui/material/Typography";
import { useTheme } from "@mui/material/styles";
import { Helmet } from "react-helmet-async";
import { decodeHtmlEntity } from "../utils";

// const useStyles = makeStyles((theme) => ({
//   root: {
//     paddingTop: theme.spacing(2),
//     flexGrow: 1,
//     // 'margin-left' : '10px'
//   },
//   title: {
//     paddingBottom: theme.spacing(2),
//   },
//   form: {
//     marginTop: theme.spacing(2),
//   },
// }));

export default function Course(props) {
  const theme = useTheme();
  return (
      <Container>
      <Helmet>
    <title>Subscriptions - KnowledgEquity</title>
    </Helmet>
      <Grid
        container
        spacing={2}
        sx={{ paddingTop: theme.spacing(2), flexGrow: 1 }}
        justifyContent="center"
      >
        <Grid item>
          <Typography
            variant="h5"
            component="h2"
            gutterBottom
            sx={{ paddingBottom: theme.spacing(2) }}
          >
            Email Subscriptions
          </Typography>
          <Typography gutterBottom>
            If you've unsubscribed from a KnowledgEquity Mailing list, we can't
            add you back without your permission.
          </Typography>
          <Typography gutterBottom sx={{ paddingBottom: theme.spacing(2) }}>
            To manually subscribe yourself to KnowledgEquity's Mailing list,
            please enter your Name and Email below:
          </Typography>

          <MailchimpSignupForm />
        </Grid>
      </Grid>
    </Container>
  );
}
