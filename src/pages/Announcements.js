import { Stack, Typography, Box, Grid } from "@mui/material";
import { useContext, useEffect, useState } from "react";
import { getAnnouncements } from "../facades/UserFacade";
import { UserContext } from "../contexts/userContext";
import Announcement from "../components/announcements/Announcement";

export default function Announcements({})
{
    const [announcements, setAnnouncements] = useState(null);
    const userContext = useContext(UserContext);

    useEffect(() => {
        if(announcements != null)
            return;

        getAnnouncements(userContext.user)
        .then( res => res.json())
        .then( a => {

            setAnnouncements(a);
        })
        
    }, [])

    if(announcements == null)
        return null;

    return <Box display="flex" justifyContent="center" alignItems="center" sx={{pt:2}}>
            <Grid container  direction="column"  alignItems="center" justifyContent="center" spacing={2}>
                {
                    announcements.map((ann, index) => {
                        return <Grid item xs={12} md={9} key={index}><Announcement the_announcement={ann} ></Announcement></Grid>
                    })
                }
            </Grid>
            </Box>
}