import React from 'react';
import { makeStyles } from '@mui/material/styles';


const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        '& > *': {
            margin: theme.spacing(1),
            width: theme.spacing(16),
            height: theme.spacing(16),
        },
    },
}));

export default function Unit(props) {
    const classes = useStyles();

    return (
        <div className={classes.root}>
       
           
                    This is {props.location.state.unitName}
     
        
        </div>
    );
}