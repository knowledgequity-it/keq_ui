import { Box, Button, Card, CardActions, CardContent, CardHeader, Grid, TextField, Typography } from "@mui/material";
import { Stack } from "@mui/system";
import { useContext, useState } from "react";
import { searchKnowledgebase } from "../facades/UserFacade";
import { UserContext } from "../contexts/userContext";
import ArrowCircleRightOutlinedIcon from '@mui/icons-material/ArrowCircleRightOutlined';
import { Link, useNavigate, useNavigation } from "react-router-dom";
import { Helmet } from "react-helmet-async";
import { decodeHtmlEntity } from "../utils";


const CARD_WIDTH=400;

export default function Knowledgebase({})
{

    const [searchTerm, setSearchTerm] = useState("");
    const [searchResults, setSearchResults] = useState({});
    const [offset, setOffset] = useState(0);
    const userContext = useContext(UserContext);
    const navigate = useNavigate();

    const searchKB = (clearResults) => {

        searchKnowledgebase(userContext.user, searchTerm, offset)
        .then(res => res.json())
        .then(results => {
            if(!clearResults)
            {
                results = 
                setSearchResults( { 
                    ...searchResults,
                    posts : [...searchResults.posts, ...results.posts],
                    topics : [...searchResults.topics, ...results.topics],
                    units : [...searchResults.units, ...results.units]
                })
            }
            else
                setSearchResults(results);

            setOffset(offset+10)
        });
    }

  return (
      <>
      <Helmet>
    <title>Knowledge Base - KnowledgEquity</title>
    </Helmet>
    <Stack alignItems="flex-start" justifyContent={"center"}>
            <Box sx={{paddingLeft:75}}>
            <Typography variant="h5" sx={{padding:1}}>Search Our Knowledge Base</Typography>
            <Grid container alignItems="center" justifyContent="flex-start" spacing={5}>
                <Grid item>
                    <TextField value={searchTerm} onChange={(e) => setSearchTerm(e.target.value)}></TextField>
                </Grid>
                <Grid item>
                    <Button onClick={() => {searchKB(true)}} variant="outlined">Search</Button>
                </Grid>
            </Grid>
            </Box>
    <Grid container direction="row" alignItems="flex-start" justifyContent="center" spacing={5}>
        <Grid item sx={12} md={5}>
            <Stack direction="column" spacing={2}>
                    { searchResults.topics && <Typography variant="h5"> Forum Results</Typography>}
                    {
                        searchResults.topics && 
                        searchResults.topics.map(topic => {
                            return <Card sx={{width : CARD_WIDTH}} key={topic.url_name}>
                                <CardContent>
                                    <Typography variant="h5">{topic.title}</Typography>
                                    
                                    <Typography variant="body2">{topic.content.slice(0, topic.content.length - 8)+"..."}</Typography>
                                    
                                </CardContent>
                                <CardActions sx={{
                                            alignSelf: "stretch",
                                            display: "flex",
                                            justifyContent: "flex-end",
                                            alignItems: "flex-start",
                                            // 👇 Edit padding to further adjust position
                                            p: 0,
                                            }}>
                                    <Link to={topic.guid} target="_blank" rel="noopener noreferrer" underline="none">
                                    <Button >open
                                        
                                    <ArrowCircleRightOutlinedIcon/>
                                    </Button>
                                    </Link>
                                </CardActions>
                            </Card>
                        })
                    }
                </Stack>
        </Grid>
        <Grid item>
            <Stack direction="column" spacing={2}>
                    {searchResults.units &&  <Typography variant="h5"> Units</Typography> }
                    {
                        searchResults.units && 
                        searchResults.units.map(unit => {
                            return <Card sx={{width : CARD_WIDTH}} key={unit.url_name}>
                                <CardContent>
                                    <Typography variant="h5">{unit.title}</Typography>
                                    <Typography variant="body2">{unit.content.slice(0, unit.content.length - 8)+"..."}</Typography>
                                    
                                </CardContent>
                                <CardActions sx={{
                                            alignSelf: "stretch",
                                            display: "flex",
                                            justifyContent: "flex-end",
                                            alignItems: "flex-start",
                                            // 👇 Edit padding to further adjust position
                                            p: 0,
                                            }}>
                                    <Link to={`${process.env.REACT_APP_AB_URL_PREFIX}/course/${unit.course}/${unit.url_name}`} target="_blank" rel="noopener noreferrer">
                                    <Button>open
                                    <ArrowCircleRightOutlinedIcon/>
                                    </Button>
                                    </Link>
                                </CardActions>
                            </Card>
                        })
                    }
                </Stack>
        </Grid>
        <Grid item>
            
        </Grid>
        
    </Grid>
      </Stack>
      <Stack>
        <Button onClick={()=>searchKB(false)}>Get more results...</Button>
      </Stack>
      </>
  )
}
