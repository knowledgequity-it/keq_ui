import ResultsTable from "../components/ResultsTable";
import { Grid, Container, Typography } from "@mui/material";
import { Helmet } from "react-helmet-async";

export default function Course(props) {
  return (
    <Container maxWidth="xl">
      <Helmet>
        <title>My Results - KnowledgEquity</title>
      </Helmet>
      <Grid sx={{ padding: "20px" }}>
        <Typography sx={{ p: 1 }} variant="h5">
          My Results
        </Typography>
        <ResultsTable />
      </Grid>
    </Container>
  );
}
