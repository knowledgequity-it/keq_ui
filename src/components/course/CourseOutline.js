import React from "react";
import Card from "@mui/material/Card";
import CardHeader from "@mui/material/CardHeader";
import CardContent from "@mui/material/CardContent";
import Divider from "@mui/material/Divider";
import Typography from "@mui/material/Typography";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import MenuBookIcon from "@mui/icons-material/MenuBook";
import AssessmentIcon from "@mui/icons-material/Assessment";

export default function CourseOutline(props) {

  const renderHTML = (rawHTML: string) =>
    React.createElement("span", {
      dangerouslySetInnerHTML: { __html: rawHTML },
    });

  const minutesToHoursAndMins = (minutes) => {
    const remainder = minutes % 60;
    const quotient = (minutes - remainder) / 60;

    var hour_output = quotient > 0 ? quotient + " hours" : "";
    var min_output = remainder > 0 ? remainder + " minutes" : "";

    var output = hour_output + " " + min_output;

    return output.trim();
  };

  if (props.curriculum.length < 1) {
    return null;
  } else {
    return (
      <>
        <Card>
          <CardHeader title="Course Materials" />
          <Divider />
          <CardContent>
            <List>
              {props.curriculum.map((item, index) => {
               

                if (item.type === "section")
                  return (
                    <ListItem key={item.title}>
                      <ListItemText
                        primary={
                          <Typography sx={{ fontWeight: 600 }}>
                            {item.title}
                          </Typography>
                        }
                      />
                    </ListItem>
                  );
                else
                  return ( 
                    <ListItem key={item.title}>
                      <ListItemIcon>
                        {item.type === "unit" && <MenuBookIcon />}
                        {item.type === "quiz" && <AssessmentIcon />}
                      </ListItemIcon>
                      <ListItemText
                        primary={
                          <Typography>
                            {renderHTML(item.title)}
                            {item.type === "quiz" && (
                              <Typography
                                component="span"
                                sx={{ color: "#999" }}
                              >
                                &nbsp;(
                                {minutesToHoursAndMins(
                                  (item.duration * item.duration_unit) / 60
                                )}
                                )
                              </Typography>
                            )}
                          </Typography>
                        }
                      />
                    </ListItem>
                  );
              })}
            </List>
          </CardContent>
        </Card>
      </>
    );
  }
}
