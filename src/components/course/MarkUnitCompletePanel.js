import React from "react";
import CheckCircleOutlineIcon from "@mui/icons-material/CheckCircleOutline";
import HighlightOffIcon from "@mui/icons-material/HighlightOff";
import ChevronLeftIcon from "@mui/icons-material/ChevronLeft";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import { Grid, Typography, Stack } from "@mui/material";
import { Button } from "@mui/material";
import { useState, useContext } from "react";
import { useTheme } from "@mui/material/styles";
import { blue, grey, green, white } from "@mui/material/colors";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Tooltip from "@mui/material/Tooltip";
import useLiveLogging from "../../hooks/useLiveLogging";
import { UserContext } from "../../contexts/userContext";
import { setUnitCompleted } from "../../facades/UserFacade";
import { useNavigate } from "react-router-dom";
import useMediaQuery from "@mui/material/useMediaQuery";

export default function MarkUnitCompletePanel({
  unit,
  course,
  completionStatus,
  onUnitCompleted,
  next_unit,
  previous_unit,
  unit_selected,
  course_url,
  unitSelected,
}) {
  const userContext = useContext(UserContext);
  const candidateAction = useLiveLogging();
  const navigate = useNavigate();
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up("xs"));

  const toggleUnitCompletionStatus = () => {
    candidateAction({
      event:
        "Candidate Mark Unit " + completionStatus ? "Incomplete" : "Complete",
      component: "MarkUnitCompletePanel",
    });
    let newStatus = !completionStatus;


    setUnitCompleted(userContext.user, course.id, unit.id, newStatus).then(
      (results) => {
        onUnitCompleted(newStatus);
      }
    );
  };

  const handleUnitButtonClick = (unit, button, e) => {
    unitSelected(unit);
    candidateAction({
      event: `${button} unit button clicked ${unit.id}`,
      component: "MarkUnitCompletePanel",
    });
    navigate(
      `${process.env.REACT_APP_AB_URL_PREFIX}/course/${course_url}/${unit.url_name}`
    );
  };

  if (typeof previous_unit !== "object" || typeof next_unit !== "object") {
    return null;
  } else {
    return (
      <>
        <Card
          elevation={0}
          sx={{
            borderRadius: 5,
            backgroundColor: theme.palette.courseNavigation.main,
            color: theme.palette.courseNavigation.contrastText,
            width: "100%",
          }}
        >
          <CardContent
            sx={{
              p: 1,
              "&:last-child": {
                pb: 1,
              },
            }}
          >
            <Grid
              container
              justifyContent="center"
              alignItems="center"
              direction="row"
            >
              <Grid
                container
                item
                xs={12}
                md={4}
                justifyContent={matches ? "flex-start" : "center"}
              >
                {Object.keys(previous_unit).length !== 0 && (
                  <Button
                    sx={{
                      color: theme.palette.common.white,
                      "&:hover": {
                        color: theme.palette.primary.main,
                      },
                    }}
                    startIcon={<ChevronLeftIcon />}
                    onClick={(e) =>
                      handleUnitButtonClick(previous_unit, "Previous", e)
                    }
                  >
                    Previous
                  </Button>
                )}
              </Grid>
              <Grid item container xs={12} md={4} justifyContent="center">
                <Tooltip
                  title={
                    <Typography sx={{ fontSize: theme.typography.pxToRem(14) }}>
                      {completionStatus
                        ? "You have completed this unit. You can mark it incomplete if you wish."
                        : "This unit is incomplete. You can mark it completed (and unmark it later if you wish)"}
                    </Typography>
                  }
                >
                  <Button
                    sx={{
                      color: theme.palette.common.white,
                      "&:hover": {
                        color: theme.palette.primary.main,
                      },
                    }}
                    startIcon={
                      completionStatus ? (
                        <CheckCircleOutlineIcon sx={{ color: green[700] }} />
                      ) : (
                        <HighlightOffIcon sx={{ color: green[700] }} />
                      )
                    }
                    onClick={(e) => toggleUnitCompletionStatus(e)}
                  >
                    {completionStatus
                      ? "Mark This Unit Not Complete"
                      : "Mark This Unit Complete"}
                  </Button>
                </Tooltip>
              </Grid>
              <Grid
                item
                container
                xs={12}
                md={4}
                justifyContent={matches ? "flex-end" : "center"}
              >
                {Object.keys(next_unit).length !== 0 && (
                  <Button
                    sx={{
                      color: theme.palette.common.white,
                      "&:hover": {
                        color: theme.palette.primary.main,
                      },
                    }}
                    endIcon={<ChevronRightIcon />}
                    onClick={(e) => handleUnitButtonClick(next_unit, "Next", e)}
                  >
                    Next
                  </Button>
                )}
              </Grid>
            </Grid>
          </CardContent>
        </Card>
      </>
    );
  }
}
