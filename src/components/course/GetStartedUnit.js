import PropTypes from "prop-types";
import { useTheme } from "@mui/material/styles";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import React, { useContext, useEffect, useState } from "react";
import { UserContext } from "../../contexts/userContext";
import { getUnit } from "../../facades/UnitFacade";
import Typography from "@mui/material/Typography";
import useMediaQuery from "@mui/material/useMediaQuery";
import Unit from "../Unit";
import { Interweave, Node } from "interweave";
import Link from "@mui/material/Link";
import useLiveLogging from "../../hooks/useLiveLogging";
import ReactPlayer from "react-player";
import ReactPlayerLoader from "@brightcove/react-player-loader";

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <Box
      role="tabpanel"
      hidden={value !== index}
      id={`vertical-tabpanel-${index}`}
      aria-labelledby={`vertical-tab-${index}`}
      sx={{ pl: 3, pr: 3, pt: 0 }}
      {...other}
    >
      {value === index && (
        <Grid container>
          <Grid item>
            <Box>{children}</Box>
          </Grid>
        </Grid>
      )}
    </Box>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

const TabPanels = ({ html, value, selected_unit }) => {
  const candidateAction = useLiveLogging();
  const transform = (node, children) => {
    if (node.tagName === "A") {
      return (
        <Link
          color="secondary"
          target={node.getAttribute("target")}
          rel={node.getAttribute("rel")}
          onClick={() => {
            candidateAction({
              component: "Unit: " + selected_unit,
              event: "Link Clicked: " + node.getAttribute("href"),
            });
            console.log("clicky clicky");
          }}
          href={node.getAttribute("href")}
        >
          {children}
        </Link>
      );
    }

    if (node.getAttribute("src")) {
      console.log(node.getAttribute("src").includes("players.brightcove.net"));
    }

    if (
      node.tagName === "IFRAME" &&
      node.getAttribute("src") &&
      !node.getAttribute("src").includes("players.brightcove.net")
    ) {
      return (
        <ReactPlayer
          url={node.getAttribute("src")}
          controls
          width={node.getAttribute("width") + "px"}
          height={node.getAttribute("height") + "px"}
          config={{
            vimeo: {
              playerOptions: {
                title: true,
              },
            },
          }}
        />
      );
    }

    if (
      node.tagName === "IFRAME" &&
      node.getAttribute("src") &&
      node.getAttribute("src").includes("players.brightcove.net")
    ) {
      var src = node.getAttribute("src").slice(31);
      var accountId = src.substring(0, src.indexOf("/"));
      var videoId = src.slice(src.lastIndexOf("=") + 1);

      return (
        //   https://players.brightcove.net/6055873642001/ABuXeXRwPY_default/index.html?videoId=6318782193112
        <ReactPlayerLoader accountId={accountId} videoId={videoId} />
      );
    }
  };

  if (typeof html !== "string") {
    return null;
  }

  const parser = new DOMParser();
  let unitDoc = parser.parseFromString(html, "text/html");
  let sections = unitDoc.getElementsByClassName("vc_tta-panel");
  let panels = Array.from(sections).map((section, index) => {
    return (
      <TabPanel key={index} value={value} index={index}>
        <Interweave content={section.innerHTML} transform={transform} />
      </TabPanel>
    );
  });

  return <>{panels} </>;
};

const WebinarTabs = ({ html, value, handleChange, getSectionHeader }) => {
  const parser = new DOMParser();
  let unitDoc = parser.parseFromString(html, "text/html");
  let sections = unitDoc.getElementsByClassName("vc_tta-tab");

  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up("md"));

  return (
    <Tabs
      orientation={matches ? "vertical" : "horizontal"}
      variant="scrollable"
      value={value}
      onChange={handleChange}
      aria-label="Webinars"
      sx={{
        [theme.breakpoints.up("md")]: {
          borderRight: 1,
          borderColor: "divider",
        },
      }}
    >
      {Array.from(sections).map((section, index) => {
        return (
          <Tab
            sx={{
              "&.MuiButtonBase-root.MuiTab-root": {
                justifyContent: "start",
                fontSize: 12,
              },
            }}
            key={index}
            label={getSectionHeader(section)}
            {...a11yProps(index)}
          />
        );
      })}
    </Tabs>
  );
};

function a11yProps(index) {
  return {
    id: `vertical-tab-${index}`,
    "aria-controls": `vertical-tabpanel-${index}`,
  };
}

// const useStyles = makeStyles((theme) => ({
//   root: {
//     flexGrow: 1,
//     //backgroundColor: theme.palette.background.paper,
//     display: "flex",
//     height: "100%",
//   },
//   tabCard: {
//     marginBottom: 15,
//   },
//   tabs: {
//     [theme.breakpoints.up("md")]: {
//       borderRight: `1px solid ${theme.palette.divider}`,
//     },
//     // alignItems: "flex-start",
//     //display:"flex"
//   },
//   wrapper: {
//     ///alignItems: "baseline"
//   },
//   tabpanel: {
//     padding: "0 30px",
//     width: "100%",
//   },
//   flexContainerVertical: {
//     display: "flex",
//     alignItems: "flex-start",
//     borderRadius: "5px",
//   },
//   intro: {
//     marginBottom: 30,
//   },
//   tabLabel: {
//     fontSize: 13,
//     fontWeight: 500,
//     [theme.breakpoints.up("md")]: {
//       textAlign: "left",
//       paddingRight: theme.spacing(2),
//     },
//     paddingLeft: 5,
//   },
//   card: {
//     marginBottom: theme.spacing(2),
//   },
// }));
export default React.memo(function GetStartedUnit(props) {
  const theme = useTheme();
  const [value, setValue] = React.useState(0);
  const [currentUnit, setCurrentUnit] = useState({});
  const userContext = useContext(UserContext);

  useEffect(() => {
    let isMounted = true;
    getUnit(props.selected_unit, userContext.user.token)
      .then((result) => result.json())
      .then((unit) => {
        if (isMounted) {
          setCurrentUnit(unit.content);
        }
      });
    return () => {
      isMounted = false;
    };
  }, [props.selected_unit, userContext.user.token]);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const htmlHasTabs = (html) => {
    const parser = new DOMParser();
    let unitDoc = parser.parseFromString(html, "text/html");
    let sections = unitDoc.getElementsByClassName("vc_tta-tab");

    if (sections.length === 0) return false;
    else return true;
  };

  const getSectionIntro = (html) => {
    const parser = new DOMParser();
    let unitDoc = parser.parseFromString(html, "text/html");
    let intro = unitDoc.querySelector(
      "div.wpb_content_element div.wpb_wrapper",
    );
    if (intro) return intro.innerHTML;
    else return null;
  };

  const getSectionHeader = (section) => {
    let headers = section.getElementsByClassName("vc_tta-title-text");
    if (headers) {
      return headers[0].innerHTML;
    } else return null;
  };

  let theUnit = null;
  if (htmlHasTabs(currentUnit))
    theUnit = (
      <div>
        <Typography
          component="div"
          dangerouslySetInnerHTML={{ __html: getSectionIntro(currentUnit) }}
        />
        <Card elevation={0} sx={{ marginBottom: theme.spacing(2) }}>
          <CardContent>
            <Grid container direction="row">
              <Grid item sm={12} md={2}>
                <WebinarTabs
                  html={currentUnit}
                  value={value}
                  handleChange={handleChange}
                  getSectionHeader={getSectionHeader}
                />
              </Grid>
              <Grid item sm={12} md={10}>
                <TabPanels
                  html={currentUnit}
                  value={value}
                  selected_unit={props.selected_unit}
                />
              </Grid>
            </Grid>
          </CardContent>
        </Card>
      </div>
    );
  else
    theUnit = (
      <Unit selected_unit={props.selected_unit} unit_url={props.unit_url} />
    );

  return <div>{theUnit}</div>;
});
