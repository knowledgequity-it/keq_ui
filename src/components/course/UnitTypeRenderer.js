import GetStartedUnit from "./GetStartedUnit";
import MarkUnitCompletePanel from "./MarkUnitCompletePanel";
import QuizPage from "../quiz/QuizPage";
import CourseOutline from "./CourseOutline";
import TagRatings from "../ratings/TagRatings";
import InteractiveQuiz from "../interactive/InteractiveQuiz";

export default function UnitTypeRenderer({
    selected_unit,
    course,
    unitSelected,
    onUnitCompleted,
    next_unit,
    previous_unit,
    course_url,
    unit_url,
    scratchpadVisible,
    setScratchpadVisible,
  })
  {
    let type = selected_unit.type;
    if (selected_unit.title?.toLowerCase().includes("accessibility"))
      return (
        <>
          <GetStartedUnit selected_unit={selected_unit.id} />
        </>
      );
    switch (type) {
      case "unit":
        return (
          <>
            <GetStartedUnit
              selected_unit={selected_unit.id}
              unit_url={unit_url}
            />
            <TagRatings unitId={selected_unit.id}/>
            <MarkUnitCompletePanel
              unit={selected_unit}
              course={course}
              completionStatus={selected_unit.completed}
              onUnitCompleted={() => onUnitCompleted(selected_unit.id)}
              next_unit={next_unit}
              previous_unit={previous_unit}
              course_url={course_url}
              unitSelected={unitSelected}
            />
          </>
        );
      case "quiz":
        if(!selected_unit.q_type.includes('Interactive'))
          return (
            <>
              <QuizPage
                quiz_id={selected_unit.id}
                onUnitCompleted={() => onUnitCompleted(selected_unit.id)}
                scratchpadVisible={scratchpadVisible}
                setScratchpadVisible={setScratchpadVisible}
              />
            </>
          );
        else 
          return (
            <>
              <InteractiveQuiz quiz_id={selected_unit.id} onUnitCompleted={() => onUnitCompleted(selected_unit.id)}/>
              <MarkUnitCompletePanel
              unit={selected_unit}
              course={course}
              completionStatus={selected_unit.completed}
              onUnitCompleted={() => onUnitCompleted(selected_unit.id)}
              next_unit={next_unit}
              previous_unit={previous_unit}
              course_url={course_url}
              unitSelected={unitSelected}
            />
            </>
        )
      default:
        if (unit_url === null) {
          return (
            <>
              <div dangerouslySetInnerHTML={{ __html: course.excerpt }} />
              {Object.keys(course).length !== 0 && (
                <CourseOutline curriculum={course.curriculum} />
              )}
            </>
          );
        } else return null;
    }
  };
