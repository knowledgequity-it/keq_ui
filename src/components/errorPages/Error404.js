import { Typography } from "@mui/material";
import { Link, Grid } from "@mui/material"
import React from "react";

export default function Error404(props) 
{

    return (
        <React.Fragment>
        <Grid container alignItems="center" direction="column">
            <Grid item>
                <Typography variant="h3">
                    {props.title}
                </Typography>
            </Grid>
            <Grid item>
                <Link variant="button" href={props.href}>
                    {props.linkText}
                </Link>
            </Grid>
        </Grid>
        </React.Fragment>
    )
}