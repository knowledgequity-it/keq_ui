import { Paper, Box, Grid } from "@mui/material";
import ResultsCard from "../ResultsCard";
import KeyDatesCard from "../cards/KeyDatesCard";
import KnowledgeBaseCard from "../cards/KnowledgeBaseCard";
import StudySkillsCard from "../cards/StudySkillsCard";
import { useTheme } from "@mui/material/styles";
import { useEffect, useState } from "react";
import FeelingMeter from "../ratings/FeelingMeter";


export default function EasyAccessCardRow({userCourseIds, studyEssentials})
{
    const theme = useTheme();

    const [courseIds, setCourseIds] = useState();

    useEffect(() => {
      if(!userCourseIds)
        return;
      setCourseIds(userCourseIds);
    }, [userCourseIds]); 

  
 
    return <Paper
    elevation={0}
    sx={{
      "&.MuiPaper-elevation0": {
        backgroundColor: theme.palette.grey[200],
        borderRadius:5
      },
    }}
  >
    <Box sx={{ p: 3, pb: 0 }}>
      <Grid container spacing={3}>
        <Grid item xs={12} sm={6} md={3}>
          <ResultsCard />
        </Grid>
        <Grid item xs={12} sm={6} md={3} >
          <KeyDatesCard/>
        </Grid>
        <Grid item xs={12} sm={6} md={3} >
          <KnowledgeBaseCard/>
        </Grid>
        <Grid item xs={12} sm={6} md={3}>
          <StudySkillsCard courseIds={courseIds} studyEssentials={studyEssentials}/>
        </Grid>
      </Grid>
      <FeelingMeter/>
    </Box>
  </Paper>
}