import {
  Stack,
  Typography,
  Container,
  Grid,
  Paper,
  Box,
  Button,
} from "@mui/material";
import { useContext, useEffect, useState, useRef } from "react";
import { UserContext } from "../../contexts/userContext";
import { getUserCertificates } from "../../facades/UserFacade";
import dayjs from "dayjs";
import { getUnixTime } from "date-fns";
import { Helmet } from "react-helmet-async";
import ReactToPrint from "react-to-print";

const Certificate = ({ certificate }) => {
  const contentRef = useRef(null);

  return (
    <>
      <Paper
        sx={{
          backgroundImage: `url('${certificate.background}')`,
          height: 550,
          width: 740,
          backgroundSize: "cover",
          marginLeft: "auto",
          marginRight: "auto",
        }}
        id={certificate.id}
        ref={contentRef}
      >
        <Box sx={{ height: "100%" }}>
          <Stack
            sx={{ height: "100%" }}
            direction="column"
            alignItems="center"
            justifyContent="center"
          >
            <div
              dangerouslySetInnerHTML={{
                __html: certificate.content,
              }}
            />
          </Stack>
        </Box>
      </Paper>
      <ReactToPrint
        trigger={() => {
          return <Button variant="text">Print</Button>;
        }}
        content={() => contentRef.current}
        documentTitle="new document"
        pagestyle="print"
      />
    </>
  );
};

export default function MyCertificates({}) {
  const userContext = useContext(UserContext);
  const [certificates, setCertificates] = useState([]);
  const certificateRefs = useRef(new Array());

  useEffect(() => {
    getUserCertificates(userContext.user).then((res) => setCertificates(res));
  }, []);

  const getNiceDate = (unixtime) => {
    let day = dayjs.unix(unixtime);
    return day.format("dddd D MMMM YYYY");
  };
  return (
    <Container maxWidth="xl">
      <Helmet>
        <title>My Results - KnowledgEquity</title>
      </Helmet>
      <Grid sx={{ padding: "20px" }}>
        <Typography sx={{ p: 1 }} variant="h5">
          My Certificates
        </Typography>
        <Stack alignItems="center" spacing={2}>
          {certificates.map((certificate) => {
            console.log(certificate);
            const getRef = (element) => certificateRefs.current.push(element);
            return <Certificate certificate={certificate} />;
          })}
        </Stack>
      </Grid>
    </Container>
  );
}
