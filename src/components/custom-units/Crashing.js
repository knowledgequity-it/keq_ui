import React, { useEffect, useState } from 'react';
import { Line, Bubble,  } from 'react-chartjs-2';
import { Chart as ChartJS, CategoryScale, LinearScale, PointElement, LineElement, Title, Tooltip, Legend, BubbleController } from 'chart.js';
import { Button, Typography } from '@mui/material';

ChartJS.register(CategoryScale, LinearScale, PointElement, LineElement, Title, Tooltip, Legend, BubbleController);


const WIDTH = "1000px";
const HEIGHT = "400px";
const START_X = 50;
const START_Y = 200;
const NODE_RADIUS = 25;
const GRAPH_MAX = 4;

const initialTasks = [
    { id: 1, name: 'A', description: "Tendering", duration: 9, crashable: false, crashCostPerWeek: 0, source: 1, target: 2, critical: true, max_crash: 0 },
    { id: 2, name: 'B', description: "Customer review & acceptance", duration: 2, crashable: false, crashCostPerWeek: 0, source: 2, target: 3, critical: true, max_crash: 0 },
    { id: 3, name: 'C', description: "Design", duration: 11, crashable: true, crashCostPerWeek: 4000, source: 3, target: 4, critical: false, max_crash: 4 },
    { id: 4, name: '-', description: "", duration: 0, crashable: false, crashCostPerWeek: 0, source: 4, target: 5, critical: false, max_crash: 3 },
    { id: 5, name: 'D', description: "Purchasing (inc. waiting for deliveries)", duration: 21, crashable: true, crashCostPerWeek: 14000, source: 3, target: 5, critical: true, max_crash: 3 },
    { id: 6, name: 'E', description: "Receive/Ship goods to site (inc. clearing customs)", duration: 8, crashable: true, crashCostPerWeek: 45000, source: 5, target: 6, critical: true, max_crash: 4 },
    { id: 7, name: 'F', description: "Installation", duration: 6, crashable: false, crashCostPerWeek: 0, source: 6, target: 7, critical: true, max_crash: 0 },
    { id: 8, name: 'G', description: "Commissioning", duration: 4, crashable: true, crashCostPerWeek: 12000, source: 7, target: 8, critical: true, max_crash: 2 },
    { id: 9, name: 'H', description: "Customer Training & documentation (manuals)", duration: 3, crashable: true, crashCostPerWeek: 5000, source: 7, target: 9, critical: false, max_crash: 1 },
    { id: 10, name: '-', description: "", duration: 0, crashable: false, crashCostPerWeek: 0, source: 9, target: 8, critical: false, max_crash: 0 },
];

const initialNodes = [
    { id: 1, text: 1 },
    { id: 2, text: 2 },
    { id: 3, text: 3 },
    { id: 4, text: 4 },
    { id: 5, text: 5 },
    { id: 6, text: 6 },
    { id: 7, text: 7 },
    { id: 8, text: 8 },
    { id: 9, text: 9 },
];

const generateCostValues = (numelements) => {
    return Array.from({ length: numelements }, (_, i) => 2*(i*i)+2)
    .map((e, i) => {return {x: i, y:e}})
}

const generateBenefitValues = (numelements) => {
    return Array.from({ length: numelements }, (_, i) => (i*i+5))
    .map((e, i) => {return {x: i, y:e}})
}

const Crashing = () => {

   const [crashSelections, setCrashSelections] = useState([]);
   const [graphData, setGraphData] = useState({datasets: [
    {
        label: 'Crash Cost',
        data: generateCostValues(GRAPH_MAX),
        borderColor: 'rgba(75,192,192,1)',
        backgroundColor: 'rgba(75,192,192,0.2)',
        fill: false,
        tension: 0.4,
        borderWidth: 2

    },
    {
        label: 'Benefit',
        data: generateBenefitValues(GRAPH_MAX),
        borderColor: 'rgba(153,102,255,1)',
        backgroundColor: 'rgba(153,102,255,0.2)',
        fill: false,
        tension: 0.4,
        borderWidth: 2
    },]});
   const [point, setPoint] = useState({x: 0, y: 0});

    React.useEffect(() => {
        let coords = [];
        var c = document.getElementById("myCanvas");
        var ctx = c.getContext("2d");
        ctx.moveTo(START_X, START_Y);

        initialNodes.forEach(node => {
            let centerX = START_X + (node.id - 1) * 115;
        
            let centerY = findNumberOfTargetNodesOfSourceNode(node) == 1 ? START_Y : START_Y + 100;
            if (nodeHasMultipleIncomingLinks(node)) {
                centerY = centerY - 200;
            }
            drawCircle(centerX, centerY, ctx);
            ctx.fillStyle = 'black';
            ctx.font = "24px serif";
            ctx.fillText(node.text, centerX - 5, centerY + 5);

            coords.push({ id: node.id, x: centerX, y: centerY });
        });

        initialTasks.forEach(task => {

            let start = coords.find(coord => coord.id === task.source);
            let end = coords.find(coord => coord.id === task.target);

            //given a line starts at coords x1, y1, at the center of a circle of radius 20 and ends at x2, y2, at the center of a circle of radius 20
            //calculate the coordinates of the line that starts and ends at the circumference of each circle
            let x1 = start.x;
            let y1 = start.y;
            let x2 = end.x;
            let y2 = end.y;

            let dx = x2 - x1;
            let dy = y2 - y1;
            let distance = Math.sqrt(dx * dx + dy * dy);
            let unitDx = dx / distance;
            let unitDy = dy / distance;
            let x3 = x1 + (NODE_RADIUS + 4) * unitDx;
            let y3 = y1 + (NODE_RADIUS + 4) * unitDy;
            let x4 = x2 - (NODE_RADIUS + 4) * unitDx;
            let y4 = y2 - (NODE_RADIUS + 4) * unitDy;
            ctx.fillStyle = task.critical ? 'lightgreen' : 'lightgrey';
            ctx.lineWidth = 3;
            ctx.strokeStyle = task.critical ? 'lightgreen' : 'lightgrey';
            ctx.moveTo(x3, y3);
            ctx.lineTo(x4, y4);
            ctx.stroke();

            //draw a triangle at the end of the line
            let angle = Math.atan2(y2 - y1, x2 - x1);

            ctx.beginPath();
            ctx.moveTo(x4, y4);
            ctx.lineTo(x4 - 10 * Math.cos(angle - Math.PI / 6), y4 - 10 * Math.sin(angle - Math.PI / 6));
            ctx.lineTo(x4 - 10 * Math.cos(angle + Math.PI / 6), y4 - 10 * Math.sin(angle + Math.PI / 6));
            ctx.closePath();
            ctx.fill();

            //add text at the midpoint of the line between the two circles offset by 10 pixels
            if (task.duration !== 0) {
                let midX = ((x1 + x2) / 2) - 25;
                let midY = (y1 + y2) / 2;
                ctx.fillStyle = 'black';
                ctx.font = "16px serif";
                ctx.fillText(`${task.name}`, midX, midY);
                ctx.font = "13px serif";
                ctx.fillText(`${task.duration} weeks`, midX - 10, midY + 18);
            }
        });

    }, []);

    useEffect(() => {
        const data = {
            //labels: Array.from({ length: dataPoints }, (_, i) => Math.round((i / (dataPoints - 1)) * totalCrashCost)),
            datasets: [
                {
                    label: 'Crash Cost',
                    data: generateCostValues(GRAPH_MAX),
                    borderColor: 'rgba(75,192,192,1)',
                    backgroundColor: 'rgba(75,192,192,0.2)',
                    fill: false,
                    tension: 0.4,
                    borderWidth: 2
    
                },
                {
                    label: 'Benefit',
                    data: generateBenefitValues(GRAPH_MAX),
                    borderColor: 'rgba(153,102,255,1)',
                    backgroundColor: 'rgba(153,102,255,0.2)',
                    fill: false,
                    tension: 0.4,
                    borderWidth: 2
                },
                {
                    label: 'Your Selection',
                    backgroundColor: 'rgba(0,0,0,0.7)',
                    borderColor: '#fff',
                    borderWidth: 1,
                    radius: 7,
                    type: 'bubble',
                    data: [
                        {   x: point.x, y: point.y, r:10 }
                    ]
                }
            ],
        };

        setGraphData(data);
    }, [point])

    const resetSelections = () => {
        setCrashSelections([]);
        setPoint({x: 0, y: 0});
    }
    const drawCircle = (x, y, context) => {
        let circle = new Path2D();  // <<< Declaration
        circle.arc(x, y, NODE_RADIUS, 0, 2 * Math.PI, false);

        context.fillStyle = 'white';
        context.fill(circle); //   <<< pass circle to context

        context.lineWidth = 7;
        context.strokeStyle = 'lightblue';
        context.stroke(circle);  // <<< pass circle here too
    }

    const nodeHasMultipleIncomingLinks = (node) => {
        return initialTasks.filter(task => task.target === node.id).length > 1;
    }
    const findNumberOfTargetNodesOfSourceNode = (sourceNode) => {
        //all links that have the source node as their target
        let inboundLinks = initialTasks.filter(task => task.target === sourceNode.id)
        //if we have more than 1 link
        //go through all the previous nodes
        for (let i = 0; i < inboundLinks.length; i++) {
            let outboundConnections = initialTasks.filter(task => task.source == inboundLinks[i].source).length;
            if (outboundConnections > 1) {
                return outboundConnections;
            }
        }
        return 1
    }

    const handleCrashChange = (taskId, weeks) => {
        let currentSelections = [...crashSelections]
        let existingSelection = currentSelections.find(selection => selection.taskId === taskId);
        if (existingSelection) {
            existingSelection.weeks = weeks;
        } else {
            let selectedTask = initialTasks.find(task => task.id === taskId);
            currentSelections.push({ taskId : taskId, weeks : weeks, costPerWeek : selectedTask.crashCostPerWeek, critical : selectedTask.critical });
        }
        setCrashSelections(currentSelections);
        //calculateSelectedCrashBenefits();
        console.log(currentSelections)
    }

    
    
    const calculateSelectedCrashBenefits = () => {
        let totalCost = crashSelections.reduce((acc, selection) => acc + (selection.weeks * selection.costPerWeek), 0);
        console.log("TOTAL Selections: ", crashSelections)
        let maxTime = initialTasks.reduce((acc, task) => acc + (task.critical ? task.duration : 0), 0);
        let totalCrashTime = crashSelections.reduce((acc, selection) => acc + (selection.critical ?  selection.weeks : 0), 0);
        

        const totalCrashCost = initialTasks.reduce((acc, task) => acc + (task.crashable ? task.crashCostPerWeek * task.max_crash : 0), 0);
        const minDuration = initialTasks.reduce((acc, task) => acc + (task.crashable ? task.duration - task.max_crash : 0), 0);
        const maxDuration = initialTasks.reduce((acc, task) => acc + (task.critical ? task.duration : 0), 0);
        
        let scaled = (totalCost/totalCrashCost );
        let scaledX = 0;
        let CORRECT_COST = 71000;
        let midpoint = (GRAPH_MAX-1) * ((Math.sqrt(GRAPH_MAX -1)/(GRAPH_MAX-1)));
        let ratio_over_correct = (totalCost - CORRECT_COST) / (totalCrashCost - CORRECT_COST);
        let ratio_under_correct = (CORRECT_COST - totalCost) / (CORRECT_COST);
        
        console.log("CALCS", totalCost, maxTime, totalCrashTime, totalCrashCost, "ratio:", ratio_over_correct)
        if(totalCost === CORRECT_COST)
            scaledX =midpoint
        else if(totalCost > CORRECT_COST)
            scaledX = midpoint + (ratio_over_correct * ((GRAPH_MAX-1) - midpoint))//(GRAPH_MAX-1) * ((Math.sqrt(GRAPH_MAX -1)/(GRAPH_MAX-1))) + ((scaled * (GRAPH_MAX -1))* ((Math.sqrt(GRAPH_MAX -1)/(GRAPH_MAX-1))));
        else
            scaledX = scaled * (GRAPH_MAX -1);

        let scaledY = 2*(scaledX*scaledX) + 2
        setPoint( {x : scaledX, y: scaledY})
    }

    
    const totalCrashCost = initialTasks.reduce((acc, task) => acc + (task.crashable ? task.crashCostPerWeek * task.max_crash : 0), 0);
    const minDuration = initialTasks.reduce((acc, task) => acc + (task.crashable ? task.duration - task.max_crash : 0), 0);
    const maxDuration = initialTasks.reduce((acc, task) => acc + (task.critical ? task.duration : 0), 0);
    const dataPoints = 5; // Number of data points
    console.log("COST, MIN, MAX: ", totalCrashCost, minDuration, maxDuration);
    

    const options = {
        responsive: true,
        scales: {
            x: {
              type: 'linear',
              ticks : {
                callback: () => ('')
              }
              
            },
            y: {
                ticks : {
                    callback: () => ('')
                }
                
            }
          },
        plugins: {
            legend: {
                position: 'top',
            },
            title: {
                display: true,
                text: 'Chrashing Benefit Chart',
            },
            datalabels: {
                display: false,
            },
        },
    };

    console.log("POINT : ", point)
    return (<>
        <div style={{ width: "100%" }}>
            <h1>Crashing Exercise</h1>
            <canvas
                id="myCanvas"
                width={WIDTH}
                height={HEIGHT}
                style={{ border: "1px solid #d3d3d3" }}>
                Your browser does not support the HTML canvas tag.
            </canvas>
        </div>
        <div>
            <table>
                <thead>
                    <tr>
                        <th></th>
                        <th>Actiity to Crash</th>
                        <th>Expected Time (wks)</th>
                        <th>Expected Time After Max Crashing</th>
                        <th>Crash by (wks)</th>
                        <th>Crash Cost Per Week</th>
                       
                    </tr>
                </thead>
                <tbody>
                {initialTasks.map(task => (
                    task.duration !== 0 &&
                            <tr key={task.id}>
                                <td>{task.name}</td>
                                <td>{task.description}</td>
                                <td>{task.duration}</td>
                                <td>{(task.crashable ? task.duration - task.max_crash : '-')}</td>
                                <td>
                                    {task.crashable ? (
                                        <select
                                            value={crashSelections.find(s => s.taskId ==task.id)?.weeks || 0}
                                            onChange={(e) => handleCrashChange(task.id, parseInt(e.target.value))}
                                        >
                                            {[...Array(task.max_crash + 1).keys()].map(week => (
                                                <option key={week} value={week}>{week}</option>
                                            ))}
                                        </select>
                                    ) : '-'}
                                </td>
                                <td>{task.crashable ? `$${task.crashCostPerWeek}` : '-'}</td>
                                
                            </tr>
                        ))}
                </tbody>
            </table>
            <div style={{ display: 'flex', direction:"row", justifyContent: 'center'}}>
                <Typography variant="h6" style={{marginRight:"20px"}}>Total Crash Cost: {crashSelections.reduce((acc, selection) => acc + (selection.weeks * selection.costPerWeek), 0)}</Typography>
                <Typography variant="h6" style={{marginRight:"20px"}}>Time Reduction (wks): {crashSelections.reduce((acc, selection) => acc + (selection.critical ?  selection.weeks : 0), 0)} weeks</Typography>
            </div>
            <div style={{ display: 'flex', direction:"row", justifyContent: 'center'}}>
                 <Button variant="outlined" onClick={()=>resetSelections()} >Reset</Button>
                 <Button variant="outlined" onClick={()=>calculateSelectedCrashBenefits()} style={{marginLeft:"20px"}}>Calculate Selections</Button>
            </div>
            <div >
                <Line data={graphData} options={options} />
            </div>
        </div>
    </>
    );
};

export default Crashing;