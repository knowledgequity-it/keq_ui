import { Button } from '@mui/material';
import React, { useEffect, useState } from 'react';
import { DndProvider, useDrag, useDrop } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';

const ItemTypes = {
    RECTANGLE: 'rectangle',
};

const valueChainsData = {
    "Seed Company": {
        initialRectangles: [
            { id: 1, text: '1. Purchase of seed' },
            { id: 2, text: '2. Operating cost of bulk storage facilities for grains.' },
            { id: 3, text: '3. Negotiating with customers on price, quality and quantity.' },
            { id: 4, text: '4. Collection of forecast and actual weather data.' },
            { id: 5, text: '5. Identifying suitable agricultural land for purchase' },
            { id: 6, text: '6. Wages of workers engaged in planting.' },
            { id: 7, text: '7. Maintenance costs of irrigation water pumps.' },
            { id: 8, text: '8. Cost of separating stems and leaves from grain.' },
        ],
        componentTexts: {
            component1: 'Procurement of inputs',
            component2: 'Production',
            component3: 'Bulking, cleaning, grading',
            component4: 'Selling',
            support: 'Support Activities : HR, Finance, IT, etc.',
        },
        correctAssignments: {
            component1: [1, 5],
            component2: [7, 6],
            component3: [2, 8],
            component4: [3],
            support: [4],
        },
    },
    "Generic Company": {
        initialRectangles: [
            { id: 1, text: '1. Research and Development' },
            { id: 2, text: '2. Manufacturing' },
            { id: 3, text: '3. Marketing' },
            { id: 4, text: '4. Sales' },
            { id: 5, text: '5. Distribution' },
            { id: 6, text: '6. Customer Service' },
            { id: 7, text: '7. Procurement' },
            { id: 8, text: '8. Human Resources' },
        ],
        componentTexts: {
            component1: 'R&D',
            component2: 'Manufacturing',
            component3: 'Marketing & Sales',
            component4: 'Distribution',
            support: 'Support Activities',
        },
        correctAssignments: {
            component1: [1],
            component2: [2],
            component3: [3, 4],
            component4: [5],
            support: [6, 7, 8],
        },
    },
};

const Rectangle = ({ id, text }) => {
    const [{ isDragging }, drag] = useDrag(() => ({
        type: ItemTypes.RECTANGLE,
        item: { id },
        collect: (monitor) => ({
            isDragging: !!monitor.isDragging(),
        }),
    }));

    return (
        <div
            ref={drag}
            style={{
                opacity: isDragging ? 0.5 : 1,
                padding: '8px',
                margin: '4px',
                backgroundColor: 'lightblue',
                cursor: 'move',
            }}
        >
            {text}
        </div>
    );
};

const Circle = ({ number }) => {
    return (
        <div
            style={{
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
                width: '20px',
                height: '20px',
                borderRadius: '50%',
                backgroundColor: 'lightblue',
                fontSize: '16px',
                color: 'black',
                border: '1px solid black',
            }}
        >
            {number}
        </div>
    );
};


const DropZone = ({ id, accept, onDrop, children, isArrow, correctAssignments }) => {
    const [{ isOver, canDrop }, drop] = useDrop(() => ({
        accept,
        drop: (item) => onDrop(item, id),
        canDrop: (item) => true, //correctAssignments[id]?.includes(item.id),
        collect: (monitor) => ({
            isOver: !!monitor.isOver(),
            canDrop: !!monitor.canDrop(),
        }),
    }), [correctAssignments]);

    const getBackgroundColor = () => {
        if (isOver && !canDrop) {
            return 'salmon';
        } else if (isOver && canDrop) {
            return 'lightgreen';
        } else {
            return 'lightgrey';
        }
    };

    return (
        <div
            ref={drop}
            style={{
                padding: isArrow ? '0' : '16px',
                width: isArrow ? '250px' : '800px',
                margin: '8px',
                paddingLeft: isArrow ? '60px' : '8px',
                backgroundColor: getBackgroundColor(),
                minHeight: '90px',
                minWidth: '200px',
                clipPath: isArrow ? 'polygon(0 0, 90% 0, 100% 50%, 90% 100%, 0 100%, 10% 50%, 0 0)' : 'none',
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                justifyContent: 'center',
            }}
        >
            {children}
        </div>
    );
};

const ValueChain = () => {


    const initialValueChain = {
        component1: [],
        component2: [],
        component3: [],
        component4: [],
        support: [],
    }


    const [selectedValueChain, setSelectedValueChain] = useState("Seed Company");
    const [rectangles, setRectangles] = useState();
    const [valueChain, setValueChain] = useState();
    const [correctAssignments, setCorrectAssignments] = useState();
    const [userSelections, setUserSelections] = useState(initialValueChain);
    const [marks, setMarks] = useState(null);

    useEffect(() => {
        resetState();
    }, [selectedValueChain])

    const resetState = () => {
        setRectangles(valueChainsData[selectedValueChain].initialRectangles);
        setValueChain(valueChainsData[selectedValueChain].componentTexts);
        setUserSelections(initialValueChain);
        setCorrectAssignments(valueChainsData[selectedValueChain].correctAssignments);
        setMarks(null);
    };

    const handleValueChainSwitch = (valueChain) => {
        setSelectedValueChain(valueChain);
    }

    const handleDrop = (item, component) => {

            setUserSelections((prev) => {
                const newValueChain = { ...prev };
                newValueChain[component].push(item.id);

                return newValueChain;
            });

            setRectangles((prev) => prev.filter((rect) => rect.id !== item.id));
    };


    const allCorrectlyPlaced = () => {
        return Object.keys(correctAssignments).every(component =>
            correctAssignments[component].every(id => userSelections[component].includes(id))
        );
    };

    const markPlacements = () => {
        let components = Object.keys(correctAssignments)

        let correctAnswers = 0;
        let inCorrectAnswers = 0;


        //find how many user selections are correct and how many are incorrect
        components.forEach(component => {
            correctAssignments[component].forEach(id => {
                if (userSelections[component].includes(id)) {
                    correctAnswers++;
                } else {
                    inCorrectAnswers++;
                }
            })
        })
        setMarks( { correct : correctAnswers, incorrect : inCorrectAnswers })
    }

    if (!rectangles || !valueChain || !correctAssignments) {
        return null;
    }
    let count = 0;

    const table = Object.keys(valueChain).sort((c1, c2) => c1.id < c2.id).map((component, cindex) =>
        userSelections[component].map((id, index) => {
            count++;
            return <tr key={index} style={{ backgroundColor: count % 2 === 0 ? 'lightblue' : 'white' }}>
                <td>{valueChainsData[selectedValueChain].initialRectangles.find(rect => rect.id === id)?.text}</td>
                <td>{valueChain[component]}</td>
            </tr>
        })
    )
    return (
        <DndProvider backend={HTML5Backend}>
            <div>
                <div>
                    {Object.keys(valueChainsData).map((key) => (
                        <Button key={key} onClick={() => handleValueChainSwitch(key)} variant="contained" style={{ margin: "20px" }}>
                            {key}
                        </Button>
                    ))}
                </div>
                <h1>{selectedValueChain} Value Chain</h1>
                <div style={{ display: 'flex', justifyContent: 'center' }}>
                    {Object.keys(valueChain).filter(component => component !== 'support').map((component) => (
                        <DropZone
                            key={component}
                            id={component}
                            accept={ItemTypes.RECTANGLE}
                            onDrop={handleDrop}
                            isArrow={true}
                            correctAssignments={correctAssignments}
                        >
                            <h3 style={{ margin: 4 }}>{valueChain[component]}</h3>
                            <div style={{ display: 'flex', flexDirection: 'row' }}>
                                {userSelections[component].map((comp, index) => {
                                    return <div key={index}><Circle number={comp} /></div>
                                })}
                            </div>
                        </DropZone>
                    ))}
                </div>
                <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', marginTop: '20px' }}>
                    <DropZone
                        key="support"
                        id="support"
                        accept={ItemTypes.RECTANGLE}
                        onDrop={handleDrop}
                        correctAssignments={correctAssignments}
                    >
                        <h2>{valueChain.support}</h2>
                        <div style={{ display: 'flex', flexDirection: 'row' }}>
                            {userSelections.support.map((id) => (
                                <div key={id} ><Circle number={id} /></div>
                            ))}
                        </div>
                    </DropZone>
                </div>

                <div style={{ display: 'flex', justifyContent: 'center', marginTop: '20px' }}>
                    {rectangles.map((rect) => (
                        <Rectangle key={rect.id} id={rect.id} text={rect.text} />
                    ))}
                </div>
            </div>
            <div style={{ display: 'flex', direction: "row", justifyContent: 'center', marginTop: '20px' }}>
                <Button variant="outlined" onClick={resetState}>Reset Exercise</Button>
                <Button variant="outlined" onClick={markPlacements}>Check My Answers</Button>
            </div>
            {
                marks && (
                    <div style={{  display: 'flex', direction: "row", marginTop: '20px' }} >
                        <h3>You correctly placed {marks.correct} components and incorrectly placed {marks.incorrect} components</h3>
                    </div>
                )
            }
            {allCorrectlyPlaced() && (
                <>
                    <div style={{ marginTop: '20px' }} >
                        <h2>All Activities Correctly Placed!</h2>
                        <table style={{ margin: '0 auto' }}>
                            <thead>
                                <tr>
                                    <th>Activity</th>
                                    <th>Value Chain Component</th>
                                </tr>
                            </thead>
                            <tbody className="tbl-container">
                                {table}
                            </tbody>
                        </table>
                    </div>
                </>
            )}
        </DndProvider>
    );
};

export default ValueChain;