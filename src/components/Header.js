import React, { useState, useEffect } from "react";
import { useTheme } from "@mui/material/styles";
import AppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import IconButton from "@mui/material/IconButton";
import AccountCircle from "@mui/icons-material/AccountCircle";
import AssignmentIcon from "@mui/icons-material/Assignment";
import MenuBookIcon from "@mui/icons-material/MenuBook";
import MenuItem from "@mui/material/MenuItem";
import Divider from "@mui/material/Divider";
import Menu from "@mui/material/Menu";
import Container from "@mui/material/Container";
import Tooltip from "@mui/material/Tooltip";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import Link from "@mui/material/Link";
import Avatar from "@mui/material/Avatar";
import DashboardIcon from "@mui/icons-material/Dashboard";
import { useNavigate } from "react-router";
import LogoText from "./../assets/logo_text.png";
import useMediaQuery from "@mui/material/useMediaQuery";
import MyCoursesMenu from "../components/MyCoursesMenu";
import { getUserAvatar } from "../facades/UserFacade";
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import AnnouncementsNotifier from "./announcements/AnnouncementsNotifier";

// const useStyles = makeStyles((theme) => ({
//   root: {
//     flexGrow: 1,
//   },
//   appbar: {
//     backgroundColor: theme.palette.common.white,
//   },
//   menuButton: {
//     marginRight: theme.spacing(2),
//   },
//   title: {
//     flexGrow: 1,
//   },
//   spacer: {
//     flexGrow: 1,
//   },
//   color: "#FFFFFF",
//   logoIcon: {
//     height: "50px",
//     width: "50px",
//   },
//   iconRoot: {
//     textAlign: "center",
//   },
//   logoText: {
//     height: 35,
//     [theme.breakpoints.down("sm")]: {
//       height: 20,
//     },
//   },
//   title: {},
//   headerNav: {
//     backgroundColor: theme.palette.headerNavigation.main,
//     color: theme.palette.headerNavigation.contrastText,
//     padding: "5px 0px",
//   },
//   headerNavLink: {
//     color: theme.palette.grey[400],
//     fontWeight: "bold",
//     textTransform: "uppercase",
//     "&:hover": {
//       color: theme.palette.grey[100],
//       textDecoration: "none",
//     },
//     marginLeft: 40,
//   },
// }));

export default function Header({ user }) {
  const theme = useTheme();
  const auth = true;
  const [accountAnchorEl, setAccountAnchorEl] = useState(null);
  const [myCoursesAnchorEl, setMyCoursesAnchorEl] = useState(null);
  const [userAvatar, setUserAvatar] = useState(null);
  const navigate = useNavigate();

  const show_menu = false;

  useEffect(() => {
    if (user)
      getUserAvatar(user)
        .then((res) => res.json())
        .then((userAvatar) => {
          setUserAvatar(userAvatar);
        });
  }, [user]);

  const handleClick = (event, url) => {
    setAccountAnchorEl(null);
    navigate(`${process.env.REACT_APP_AB_URL_PREFIX}/${url}`);
  };

  const handleLogout = () => {
    setAccountAnchorEl(null);
    localStorage.removeItem("user");
    window.location.href = `${process.env.REACT_APP_AB_URL_PREFIX}/wp-login.php?action=logout`;
  };

  const gotoDashboard = () => {
    setAccountAnchorEl(null);
    navigate(`${process.env.REACT_APP_AB_URL_PREFIX}/dashboard`);
  };

  const isSM = useMediaQuery((theme) => theme.breakpoints.down("sm"));

  return (
    <div style={{ flexGrow: 1 }} >
      <AppBar
        position="static"
        sx={{ backgroundColor: theme.palette.common.white }}
      >
        <Grid container>
          <Grid item xs={12}>
            <Container maxWidth="xl">
              <Toolbar disableGutters>
                <img
                  src={LogoText}
                  height={23}
                  style={{
                    height: 35,
                    [theme.breakpoints.down("sm")]: {
                      height: 20,
                    },
                  }}
                />
                <Box sx={{ flexGrow: 1 }} />

                {auth && (
                  <div>
                    {
                      <Tooltip>
                        <div
                          style={{ display: "inline", alignItems: "center" }}
                        >
                          <AnnouncementsNotifier type="icon" />
                        </div>
                      </Tooltip>
                    }
                    {isSM ? (
                      <Tooltip title="My Results">
                        <IconButton
                          size="small"
                          onClick={(event) => handleClick(event, "my-results")}
                        >
                          <AssignmentIcon />
                        </IconButton>
                      </Tooltip>
                    ) : (
                      <Button
                        sx={{
                          color: theme.palette.grey[800],
                          "&:hover": {
                            color: theme.palette.grey[900],
                            backgroundColor: theme.palette.grey[100],
                          },
                        }}
                        startIcon={<AssignmentIcon />}
                        onClick={(event) => handleClick(event, "my-results")}
                      >
                        My Results
                      </Button>
                    )}
                    {isSM ? (
                      <Tooltip title="My Courses">
                        <IconButton
                          size="small"
                          onClick={(event) =>
                            setMyCoursesAnchorEl(event.currentTarget)
                          }
                        >
                          <MenuBookIcon />
                        </IconButton>
                      </Tooltip>
                    ) : (
                      <Button
                        sx={{
                          color: theme.palette.grey[800],
                          "&:hover": {
                            color: theme.palette.grey[900],
                            backgroundColor: theme.palette.grey[100],
                          },
                        }}
                        startIcon={<MenuBookIcon />}
                        onClick={(event) =>
                          setMyCoursesAnchorEl(event.currentTarget)
                        }
                      >
                        My Courses
                      </Button>
                    )}
                    <MyCoursesMenu
                      anchorEl={myCoursesAnchorEl}
                      setAnchorEl={setMyCoursesAnchorEl}
                    />
                    {isSM ? (
                      <Tooltip title="Dashboard">
                        <IconButton
                          onClick={gotoDashboard}
                          size={isSM ? "small" : "medium"}
                        >
                          <DashboardIcon />
                        </IconButton>
                      </Tooltip>
                    ) : (
                      <Button
                        sx={{
                          color: theme.palette.grey[800],
                          "&:hover": {
                            color: theme.palette.grey[900],
                            backgroundColor: theme.palette.grey[100],
                          },
                        }}
                        startIcon={<DashboardIcon />}
                        onClick={gotoDashboard}
                      >
                        Dashboard
                      </Button>
                    )}
                    {isSM ? (
                      <Tooltip title="Account">
                        <IconButton
                          onClick={(event) =>
                            setAccountAnchorEl(event.currentTarget)
                          }
                          size={isSM ? "small" : "medium"}
                        >
                          {userAvatar !== null &&
                          userAvatar?.url !== undefined ? (
                            <Avatar
                              alt="Name Here"
                              src={userAvatar.url}
                              style={{
                                width: "24px",
                                height: "24px",
                              }}
                            >
                              <AccountCircle />
                            </Avatar>
                          ) : (
                            <AccountCircle />
                          )}
                        </IconButton>
                      </Tooltip>
                    ) : (
                      <Button
                        sx={{
                          color: theme.palette.grey[800],
                          "&:hover": {
                            color: theme.palette.grey[900],
                            backgroundColor: theme.palette.grey[100],
                          },
                        }}
                        startIcon={
                          userAvatar !== null &&
                          userAvatar?.url !== undefined ? (
                            <Avatar
                              alt="Name Here"
                              src={userAvatar.url}
                              style={{
                                width: "24px",
                                height: "24px",
                              }}
                            >
                              <AccountCircle />
                            </Avatar>
                          ) : (
                            <AccountCircle />
                          )
                        }
                        onClick={(event) =>
                          setAccountAnchorEl(event.currentTarget)
                        }
                      >
                        {userAvatar !== null &&
                        userAvatar?.display_name !== undefined
                          ? userAvatar.display_name
                          : ""}
                      </Button>
                    )}
                    <Menu
                      id="menu-appbar"
                      //getContentAnchorEl={null}
                      anchorEl={accountAnchorEl}
                      keepMounted
                      anchorOrigin={{
                        vertical: "bottom",
                        horizontal: "center",
                      }}
                      transformOrigin={{
                        vertical: "top",
                        horizontal: "center",
                      }}
                      open={Boolean(accountAnchorEl)}
                      onClose={() => {
                        setAccountAnchorEl(null);
                      }}
                    >
                      <MenuItem
                        onClick={(event) => handleClick(event, "my-results")}
                      >
                        My Results
                      </MenuItem>
                      <MenuItem onClick={gotoDashboard}>Dashboard</MenuItem>
                      <MenuItem
                        onClick={() => {
                          setAccountAnchorEl(null);
                          navigate(
                            `${process.env.REACT_APP_AB_URL_PREFIX}/info/my-certificates`,
                          );
                        }}
                      >
                        My Certificates
                      </MenuItem>
                      <MenuItem
                        onClick={() => {
                          setAccountAnchorEl(null);
                          navigate(
                            `${process.env.REACT_APP_AB_URL_PREFIX}/subscriptions`,
                          );
                        }}
                      >
                        Subscriptions
                      </MenuItem>
                      <MenuItem
                        onClick={() => {
                          setAccountAnchorEl(null);
                          navigate(
                            `${process.env.REACT_APP_AB_URL_PREFIX}/info/contact-us`,
                          );
                        }}
                      >
                        Contact Us
                      </MenuItem>
                      <Divider />
                      <MenuItem onClick={handleLogout}>Log out</MenuItem>
                    </Menu>
                  </div>
                )}
              </Toolbar>
            </Container>
          </Grid>
          <Grid
            item
            container
            xs={12}
            sx={{
              backgroundColor: theme.palette.headerNavigation.main,
              color: theme.palette.headerNavigation.contrastText,
              padding: "5px 0px",
            }}
          >
            {show_menu && (
              <Container>
                <Box display="flex" justifyContent="flex-end">
                  <Link
                    onClick={() => handleClick()}
                    sx={{
                      color: theme.palette.grey[400],
                      fontWeight: "bold",
                      textTransform: "uppercase",
                      "&:hover": {
                        color: theme.palette.grey[100],
                        textDecoration: "none",
                      },
                      marginLeft: 40,
                    }}
                  >
                    CPA Subjects
                  </Link>
                  <Link
                    onClick={() => handleClick()}
                    sx={{
                      color: theme.palette.grey[400],
                      fontWeight: "bold",
                      textTransform: "uppercase",
                      "&:hover": {
                        color: theme.palette.grey[100],
                        textDecoration: "none",
                      },
                      marginLeft: 40,
                    }}
                  >
                    Navigation
                  </Link>
                  <Link
                    onClick={() => handleClick()}
                    sx={{
                      color: theme.palette.grey[400],
                      fontWeight: "bold",
                      textTransform: "uppercase",
                      "&:hover": {
                        color: theme.palette.grey[100],
                        textDecoration: "none",
                      },
                      marginLeft: 40,
                    }}
                  >
                    About Us
                  </Link>
                </Box>
              </Container>
            )}
          </Grid>
          <Grid item xs={12}>
            <Box
              sx={{ background: "#FF5050" }}
              justifyContent="center"
              display="flex"
              alignItems="center"
            >
              <AnnouncementsNotifier type="bar" />
            </Box>
          </Grid>
        </Grid>
      </AppBar>
    </div>
  );
}
