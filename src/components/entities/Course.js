export default class CourseObj {
  constructor(course) {
    this.id = course.id;
    this.title = course.title;
    this.image = course.image;
    this.content = course.content;
    this.instructor = course.instructor;
    this.instructor_avatar = course.instructor_avatar;
    this.url_name = course.url_name;
    this.excerpt = course.excerpt;
    this.curriculum = course.curriculum;
    this.can_enrol_from_link = course.can_enrol_from_link;
  }
}
