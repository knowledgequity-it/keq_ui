export default class KeAnnouncement
{

    constructor(announcement)
    {
        this.content = announcement.content;
        this.dismissedStatus = announcement.dismissedStatus;
        this.type = announcement.type;
        this.published = announcement.published;
        this.title = announcement.title;
        this.url_name = announcement.url_name;
        this.announceid = announcement.announceid;
        this.subjects = announcement.subjects;
        this.showBarUntil = announcement.show_bar_until;
    }
}
