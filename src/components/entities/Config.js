export default class Config {
  /* config values needed by dashboard to figure out which units are "special"

       These tags are set in the WPLMS back end as Custom fields. 

       See each one for an axplanation. 

    */

  //These are units which should not be shown in the course sidebar. Mainly bacause they have their own card on the dashboard
  static EXCLUDED_UNIT_TAG = "ke_exclude_from_curriculum";
  //Study Essentials unit. This unit is common across all Courses. If none exists, enter NONE as the value. 304796
  static STUDY_ESSENTIALS_UNIT = 357846; //304796;
  static STUDY_ESSENTIALS_UNIT_TAG = "ke_study_essentials_unit";
  //This is the URL of the FAQ PDF for each course. It lives in the WP media library.
  static FAQ_DOWNLOAD_URL = "ke_faq_download_url";
  //Tag to define each courses faq unit so we can link to it on the dashboard.
  static FAQ_UNIT_TAG = "ke_faq_unit";

  //Study planner unit. Has it's own dashboard card and is unique to each Course.
  static TIMETABLE_STUDYPLANNER_TAG = "ke_study_planner";
  //toggles whether a unit shouldbe in the curriculum sidebar or not
  static UNIT_VISISBILITY_TAG = "ke_visible_in_course";
  //prefix for WP usermeta key for pledges. Course ID is appended in the back end before saving.
  static COURSE_PLEDGE_PREFIX = "ke_pledge_";
  //Practice exam prefix for usermeta key. Course ID appended in WPLMS back end.
  static PE1_PREFIX = "ke_pe1_date_";
  //Practice exam prefix for usermeta key. Course ID appended in WPLMS back end.
  static PE2_PREFIX = "ke_pe1_date_";
  //Key dates unit ID. It is a WP post and is common for all units. has own card. Enter NONE if it doesn't exist
  static KEY_DATES_UNIT = 73109;
  static KEY_DATES_UNIT_TAG = "ke_key_dates_unit"; //73109;
}
