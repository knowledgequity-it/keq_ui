import Config from "./Config";

export default class UserCourse {
  constructor(userCourse) {
    this.id = userCourse.id;
    this.hide_ate = userCourse.hide_ate;
    this.hide_kb = userCourse.hide_kb;
    this.image = userCourse.image;
    this.title = userCourse.title;
    this.url_name = userCourse.url_name;
    this.user_course_details = userCourse.user_course_details;

    this.currentUnit = null;
  }

  getNextUnit(currentUnitName) {
    if (!this.user_course_details) return {};
    let found = {};
    this.user_course_details.forEach((unit, index) => {
      if (unit.url_name === currentUnitName) {
        found =
          index < this.user_course_details.length - 1
            ? this.user_course_details[index + 1]
            : {};
      }
    });
    return found;
  }

  getPreviousUnit(currentUnitName) {
    if (!this.user_course_details) return {};
    let found = {};
    this.user_course_details.forEach((unit, index) => {
      if (unit.url_name === currentUnitName)
        found = index > 0 ? this.user_course_details[index - 1] : {};
    });
    return found;
  }

  getUnit(unitName) {
    if (!this.user_course_details) return {};
    let found = {};
    this.user_course_details.forEach((unit, index) => {
      if (unit.url_name === unitName) {
        found = unit;
      }
    });
    return found;
  }

  getNextIncompleteUnit() {}

  getStudySkillsUnit() {
    return this.user_course_details.find(
      (u) => u.unitId == Config.STUDY_SKILLS_UNIT,
    );
  }
  setUnitComplete(unitId) {
    this.user_course_details.forEach((unit) => {
      if (parseInt(unit.id) === parseInt(unitId)) {
        if (unit.type == "quiz") unit.completed = true;
        else unit.completed = !unit.completed;
      }
    });
  }

  makeCopy() {
    let x = new UserCourse(this);
    return x;
  }
}
