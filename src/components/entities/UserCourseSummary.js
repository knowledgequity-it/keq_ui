import Config from "./Config";
import SpecialUnit from "./SpecialUnit";

export default class UserCourseSummary {
  constructor(userData) {
    if (!userData) return;
    this.id = userData?.id;
    this.imageUrl = userData?.image;
    this.progress = Number(userData?.progress);
    this.title = userData?.title;
    this.course_url = userData?.url_name;
    this.next_unit = userData?.next_unit;
    this.next_unit_title = userData?.next_unit_title;
    this.pledge = userData?.pledge;
    this.extras = userData.extras;
    this.pe1Date = userData.pe1Date;
    this.pe2Date = userData.pe2Date;
    //this.faq = userData.faq;
  }

  getTimetableAndStudyPlanUnit() {
    if (!this.extras || this.extras.length == 0) return null;

    let unit = this.extras[Config.TIMETABLE_STUDYPLANNER_TAG];
    if (!unit) return null;
    return new SpecialUnit(
      unit,
      Config.TIMETABLE_STUDYPLANNER_TAG,
      this.course_url,
    );
  }

  getFAQDownloadUrl() {
    return this.extras[Config.FAQ_DOWNLOAD_URL].url;
  }

  getFAQUnitUrl() {
    if (this.course_url && this.extras[Config.FAQ_UNIT_TAG])
      return `${this.course_url}/${this.extras[Config.FAQ_UNIT_TAG].url_name}`;
    else return null;
  }
}
