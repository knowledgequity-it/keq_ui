

export default class User
{

    constructor(userData)
    {
        this.id = userData.id;
        this.email = userData.email;
        this.name = userData.name;
        this.surname = userData.surname;
        this.display_name = userData.display_name;
        this.courses = userData.courses;
        this.roles= userData.roles;   
        this.study_essentials = userData.study_essentials;
    }

    userIsInCourse(courseId)
    {
        return this.courses.includes(courseId);
    };

    userHasRole(role)
    {
        return this.roles.includes(role);
    }
}