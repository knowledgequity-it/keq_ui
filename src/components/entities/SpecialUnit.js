export default class SpecialUnit{


    constructor(data, type, course_url)
    {
        this.id = data.id;
        this.url_name = data.url_name;
        this.course_url = course_url;

        this.type = type; 
    }
}