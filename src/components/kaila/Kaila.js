import { Button, Grid, Typography } from "@mui/material"
import { useContext, useEffect, useState } from "react"
import { UserContext } from "../../contexts/userContext";
import { getUser } from "../../facades/UserFacade";
import { useParams, useNavigate } from "react-router-dom";


const courseMapping = {
    3745: "ethics-governance",
    191474: "global-strategy-leadership",
    3806: "financial-reporting",
    3961: "strategic-management-accounting",
    5126: "advanced-audit-assurance",
    5187: "australia-taxation",
    5190: "contemporary-business-issues",
    5192: "financial-risk-management",
    94943: "australia-taxation-advanced",
    249353: "digital-finance"
}

const getCourseFullName = (course_name) => {

    switch (course_name) {
        case "ethics-governance":
            return "Ethics & Governance"
        case "australia-taxation-advanced":
            return "Australia Taxation - Advanced"
        case "global-strategy-leadership":
            return "Global Strategy & Leadership"
        case "financial-reporting":
            return "Financial Reporting"
        case "strategic-management-accounting":
            return "Strategic Management Accounting"
        case "advanced-audit-assurance":
            return "Advanced Audit & Assurance"
        case "australia-taxation":
            return "Australia Taxation"
        case "contemporary-business-issues":
            return "Contemporary Business Issues"
        case "financial-risk-management":
            return "Financial Risk Management"
        case "digital-finance":
            return "Digital Finance"
        default:
            return "the CPA program"
    }
}


const EthicsGovernance = () => {
    return <><df-messenger
        project-id="kestudyguideagents"
        agent-id="9687dc19-765f-4d1e-ab0e-686dca31b543"
        language-code="en"
        max-query-length="-1">
        <df-messenger-chat
            chat-title="Kaila">
        </df-messenger-chat>
    </df-messenger>
        <style>
            df-messenger {`z-index: 999; position: absolute; bottom: 0; right: 0; top: 0;`}
        </style>
    </>
}

const AustraliaTaxationAdvanced = () => {
    return <><df-messenger
        project-id="kestudyguideagents"
        agent-id="be6423b9-b98c-41c4-b40a-d6d46990b91b"
        language-code="en"
        max-query-length="-1">
        <df-messenger-chat
            chat-title="Kaila">
        </df-messenger-chat>
    </df-messenger>
        <style>
            df-messenger {`z-index: 999; position: absolute; bottom: 0; right: 0; top: 0;`}
        </style>
    </>
}
export function Kaila() {

    const userContext = useContext(UserContext);
    const navigate = useNavigate();
    const { course_name } = useParams();
    const [userCourses, setUserCourses] = useState(null);

    useEffect(() => {
        if (!userContext)
            return;

        getUser(userContext.user)
            .then(res => res.json())
            .then((user) => {
                setUserCourses(user.courses);
            })

    }, [userContext])

    const getChatForCourse = (course_name) => {
        switch (course_name) {
            case "ethics-governance":
                return <EthicsGovernance />
            case "australia-taxation-advanced":
                return <AustraliaTaxationAdvanced />
            default:
                return "Hello"
        }
    }

    const course_id = Object.keys(courseMapping).find(key => courseMapping[key] === course_name)

    const goToCourse = () => {
        //navigate to the course page using useNavigate   
        navigate(`${process.env.REACT_APP_AB_URL_PREFIX}/course/${course_name}`);
    }

    //console.log("XXXXXX", userCourses)

    if (!userCourses)
        return null
    //check if key with valuue of course_name in courseMappings is in the values of userCourses
    if (course_id && userCourses.includes(course_id))
        return <Grid container item direction="column" alignItems="center" justifyContent="center" xs={12} md={6} style={{ margin: "auto" }}>
            <Grid item>
                <Typography fontSize={72} fontWeight={900} style={{ color: "green" }}>
                    Hi, I'm Kaila.
                </Typography>
            </Grid>

            <Grid item sx={{ m: 2 }}>
                <div>
                    <span style={{ fontWeight: "bold" }}>K</span>nowledgequity &nbsp;
                    <span style={{ fontWeight: "bold" }}>AI</span>  &nbsp;
                    <span style={{ fontWeight: "bold" }}>L</span>earning  &nbsp;
                    <span style={{ fontWeight: "bold" }}>A</span>ssistant
                </div>
            </Grid>
            <Grid item>
                <Typography fontSize={32} fontWeight={500} style={{ color: "lightgrey", fontStyle: 'italic' }}>limited βeta</Typography>
            </Grid>
            <Grid item sx={{ m: 4, color: "grey", fontStyle: "italic" }} variant="caption">
                <Typography>
                    I'm trained in {getCourseFullName(course_name)}.  I hope I can help you with your studies.
                </Typography>
                <Typography>
                    Kaila is new and in BETA testing.
                </Typography>
                <Typography>
                    Kaila might not always understand your question. Like all AI, it’s still learning language nuances. If it doesn’t get you, try rephrasing.
                </Typography>
                <Typography>
                    Kaila’s answers aren’t 100% accurate—always check the provided links and review the material yourself.
                </Typography>
                <Typography>
                    What Kaila doesn't  answer Guided Learning quiz or practice exam questions. Post these in the forum and check provided explanations first.
                    Kaila won’t create summary notes or an index. Learning happens when you make your own notes
                </Typography>


            </Grid>
            <Grid>
                <Button onClick={() => goToCourse()} >
                    Back to {getCourseFullName(course_name)}
                </Button>
            </Grid>
            <Grid item sx={{ mb: 6 }} >
                {(getChatForCourse(course_name))}

            </Grid>
        
        </Grid >
    else
        return <Grid container direction="column" alignItems="center" justifyContent="center" style={{ margin: "auto" }}>
            <Grid item>
                <Typography fontSize={72} fontWeight={900} style={{ color: "green" }}>
                    Hi, I'm Kaila.
                </Typography>
            </Grid>

            <Grid item sx={{ m: 2 }}>
                <div>
                    <span style={{ fontWeight: "bold" }}>K</span>nowledgequity &nbsp;
                    <span style={{ fontWeight: "bold" }}>AI</span>  &nbsp;
                    <span style={{ fontWeight: "bold" }}>L</span>earning  &nbsp;
                    <span style={{ fontWeight: "bold" }}>A</span>ssistant
                </div>
            </Grid>
            <Grid item>
                <Typography fontSize={32} fontWeight={500} style={{ color: "lightgrey", fontStyle: 'italic' }}>limited βeta</Typography>
            </Grid>
            <Grid item sx={{ m: 4, color: "grey", fontStyle: "italic" }} variant="caption">
                <Typography>
                    Kaila is not available for this course, or you don't have access to this course
                </Typography>
            </Grid>
        </Grid>
}