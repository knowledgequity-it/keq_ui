import {
  Grid,
  Paper,
  Typography,
  Stack,
  Button,
  Modal,
  Box,
} from "@mui/material";
import { useEffect, useState, useContext } from "react";
import { setUserPledge } from "../../facades/UserFacade";
import { UserContext } from "../../contexts/userContext";
import PublishedWithChangesIcon from "@mui/icons-material/PublishedWithChanges";
import LiveHelpOutlinedIcon from "@mui/icons-material/LiveHelpOutlined";
let pcconfig = require("./PledgeCardConfig.json");

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  boxShadow: 24,
  p: 4,
};

class Pledge {
  static PASS = "Pass";
  static CREDIT = "Credit";
  static DISTINCTION = "Distinction";
  static HIGH_DISTINCTION = "High Distinction";
}

const getPledgeColor = (pledge) => {
  switch (pledge) {
    case Pledge.PASS:
      return "gradeP";
    case Pledge.CREDIT:
      return "gradeC";
    case Pledge.DISTINCTION:
      return "gradeD";
    case Pledge.HIGH_DISTINCTION:
      return "gradeHD";
    default:
      return "gradeP";
  }
};

const SinglePledgeButton = ({ pledge, resetFunc }) => {
  const reset = () => {
    resetFunc();
  };
  return (
    <Grid item xs={12}>
      <Button
        color={getPledgeColor(pledge)}
        variant="contained"
        sx={{
          textTransform: "none",
          width: "100%",
          height: "50%",
          borderRadius: 7,
        }}
      >
        {pledge}
      </Button>
      <Stack alignItems="center" justifyContent="center">
        <Button
          sx={{ padding: 2 }}
          size="large"
          variant="text"
          endIcon={<PublishedWithChangesIcon />}
          onClick={reset}
        >
          Change Pledge
        </Button>
      </Stack>
    </Grid>
  );
};
export default function PledgeCard({ courseId, userPledge }) {
  const [pledge, setPledge] = useState("");
  const [openHelpModal, setOpenHelpModal] = useState(false);
  const [openPledgeModal, setOpenPledgeModal] = useState(false);
  const userContext = useContext(UserContext);

  const handlePledge = (pledge) => {
    setPledge(pledge);
    setOpenPledgeModal(true);
  };

  const recordPledge = () => {
    if (pledge !== "")
      setUserPledge(userContext.user, courseId, pledge).then((res) => {
        console.log("done");
        setOpenPledgeModal(false);
      });
  };
  useEffect(() => {}, [pledge]);

  const toggleModal = () => {
    setOpenHelpModal(!openHelpModal);
  };

  const togglePledgeModal = () => {
    setOpenPledgeModal(!openPledgeModal);
  };

  const resetPledge = () => {
    let noPledge = "";
    setUserPledge(userContext.user, courseId, noPledge).then((res) => {
      setPledge(noPledge);
    });
  };

  useEffect(() => {
    if (!userPledge) return;

    switch (userPledge) {
      case Pledge.PASS:
        setPledge(Pledge.PASS);
        break;
      case Pledge.CREDIT:
        setPledge(Pledge.CREDIT);
        break;
      case Pledge.DISTINCTION:
        setPledge(Pledge.DISTINCTION);
        break;
      case Pledge.HIGH_DISTINCTION:
        setPledge(Pledge.HIGH_DISTINCTION);
        break;
      default:
        setPledge("");
    }
  }, [userPledge]);

  let allPledges = (
    <>
      <Grid item xs={6}>
        <Button
          onClick={() => {
            handlePledge(Pledge.HIGH_DISTINCTION);
          }}
          color="gradeHD"
          variant="contained"
          sx={{
            textTransform: "none",
            width: "100%",
            height: "100%",
            borderRadius: 5,
          }}
        >
          High Distinction
        </Button>
      </Grid>
      <Grid item xs={6}>
        <Button
          onClick={() => {
            handlePledge(Pledge.DISTINCTION);
          }}
          color="gradeD"
          variant="contained"
          sx={{
            textTransform: "none",
            width: "100%",
            height: "100%",
            borderRadius: 5,
          }}
        >
          Distinction
        </Button>
      </Grid>
      <Grid item xs={6}>
        <Button
          onClick={() => {
            handlePledge(Pledge.CREDIT);
          }}
          color="gradeC"
          variant="contained"
          sx={{
            textTransform: "none",
            width: "100%",
            height: "100%",
            borderRadius: 5,
          }}
        >
          Credit
        </Button>
      </Grid>
      <Grid item xs={6}>
        <Button
          onClick={() => {
            handlePledge(Pledge.PASS);
          }}
          color="gradeP"
          variant="contained"
          sx={{
            textTransform: "none",
            width: "100%",
            height: "100%",
            borderRadius: 5,
          }}
        >
          Pass
        </Button>
      </Grid>
    </>
  );

  let display =
    pledge && pledge !== "" ? (
      <SinglePledgeButton pledge={pledge} resetFunc={resetPledge} />
    ) : (
      allPledges
    );

  return (
    <Paper sx={{ height: "50%", p: 2, borderRadius: 5, overflow: "hidden" }}>
      <Modal
        open={openHelpModal}
        onClose={toggleModal}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Typography id="modal-modal-title" variant="h6" component="h2">
            What is a Pledge?
          </Typography>
          <Typography sx={{ pb: 2 }}>
            A pledge is a motivational technique to achieve a particular
            outcome.
          </Typography>
          <Typography>
            <b>It’s not a guess about what result you think you’ll get</b>– it’s
            a commitment towards achieving a specific result and then planning
            the steps required and (just as importantly) implementing those
            steps successfully.
          </Typography>
          <Button
            variant="outlined"
            onClick={(e) => {
              toggleModal();
            }}
            sx={{ mt: 2 }}
          >
            Got it!
          </Button>
        </Box>
      </Modal>
      <Modal open={openPledgeModal} onClose={togglePledgeModal}>
        <Box sx={style}>
          <Typography id="modal-modal-title" variant="h6" component="h2">
            What is a Pledge?
          </Typography>
          <Typography sx={{ pb: 2 }}>
            A pledge is a motivational technique to achieve a particular
            outcome.
          </Typography>
          <Typography>
            You have chosen <b>{pledge}</b>
          </Typography>
          <Typography
            dangerouslySetInnerHTML={{
              __html: pcconfig[courseId] ? pcconfig[courseId][pledge] : "",
            }}
          ></Typography>
          <Button
            variant="outlined"
            onClick={(e) => {
              recordPledge();
            }}
            sx={{ mt: 2 }}
          >
            Got it!
          </Button>
        </Box>
      </Modal>

      <Stack
        useFlexGap
        spacing={1}
        justifyContent="flex-start"
        sx={{ height: "100%" }}
      >
        <div
          style={{
            display: "flex",
            alignItems: "center",
            flexWrap: "wrap",
          }}
        >
          <Typography sx={{ fontWeight: "bold", fontSize: 20 }}>
            My Pledge
          </Typography>
          <LiveHelpOutlinedIcon
            onClick={(e) => toggleModal()}
            sx={{ ml: 2, cursor: "pointer" }}
          />
        </div>
        <Grid
          container
          spacing={1}
          sx={{ "&.MuiGrid-root": { pl: 0 }, height: "100%" }}
        >
          {display}
        </Grid>
      </Stack>
    </Paper>
  );
}
