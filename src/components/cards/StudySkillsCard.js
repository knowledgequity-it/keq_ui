import { useContext, useEffect, useState } from "react";
import laptopLogo from "../../assets/laptop.png";
import { Card, CardActionArea, CardContent, CircularProgress, Typography } from "@mui/material";
import { getUserCourse } from "../../facades/UserFacade";
import { UserContext } from "../../contexts/userContext";
import UserCourse from "../entities/UserCourse";
import { useNavigate } from "react-router-dom";
import useLiveLogging from "../../hooks/useLiveLogging";
import GenericDialog from "../dialogs/GenericDialog";


export default function StudySkillsCard({courseIds, studyEssentials})
{
    const [courses, setCourses] = useState([]);
    const [userCourse, setUserCourse] = useState(null);
    const [showPopup, setShowPopup] = useState(false);
    const userContext = useContext(UserContext);
    const navigate = useNavigate();
    const candidateAction = useLiveLogging();
    


    const toggleDialog = () => {
      setShowPopup(!showPopup);
    }

    const goToStudySkillsPage = () => {
      candidateAction({
        component: "EasyAccessRow",
        event: "User Clicked the Study skills card",
        });
        if(userCourse == null)
          toggleDialog();
        else
        {
          if(userCourse !== null)
          {  let unit = userCourse.getStudySkillsUnit()
            navigate(`${process.env.REACT_APP_AB_URL_PREFIX}/info/study-skills`, { state : { unit : unit, course : userCourse, courseIds : courses }});
          }
        }
    }


    useEffect(() => {
      if(!courseIds)
        setCourses(userContext.user.courses);
    }, [courseIds, userContext])

    useEffect(() => {
      if(!courseIds)
        return;

      let se_index = -1;
      for(let i = 0; i< courseIds.length; i++)
      {
        
        if(courseIds[i] === studyEssentials)
        {
          se_index = i;
        }
      }
        
      if(se_index < 0)
        return;

      getUserCourse(courseIds[se_index], userContext.user)
        .then(res => res.json()) 
        .then(uc => {
          setUserCourse(new UserCourse(uc))
      })

    }, [courseIds, studyEssentials, userContext])

    return <><Card sx={{ mb: 3, borderRadius:5 }}>
    <CardActionArea onClick={(e) => goToStudySkillsPage()}>
      <CardContent>
        <img src={laptopLogo} alt=""/>
        <Typography
          sx={{ height: "50px" }}
          gutterBottom
          variant="h5"
          component="h2"
        >
          Study Skills
          {userCourse === null && <CircularProgress/>}
        </Typography>
        
        <Typography
          sx={{ height: "20px", fontSize:"11pt"}}
          color="textSecondary"
          component="p"
        >
        Study Essentials | Bootcamp | Exam Prep
        </Typography>
      </CardContent>
    </CardActionArea>
  </Card>
  <GenericDialog title="Study Essentials not Available" text="The Study Essentials materials are not currently available. Please check back later." toggleDialog={toggleDialog} showDialog={showPopup}/>
  </>
}