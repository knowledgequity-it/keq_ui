import {
  Box,
  List,
  ListItem,
  Paper,
  Stack,
  TextField,
  Typography,
} from "@mui/material";
import { darken } from "@mui/material/styles";
import GenericDialog from "../dialogs/GenericDialog";

export default function StudyDataCard({ clickFunc }) {
  return (
    <Paper
      sx={{
        p: 2,
        ":hover": {
          backgroundColor: darken("#ffffff", 0.03),
          cursor: "pointer",
        },
        height: "50%",
        borderRadius: 5,
        overflow: "hidden",
      }}
      onClick={(e) => clickFunc()}
    >
      <Stack spacing={1} alignItems="center" justifyContent="space-between">
        <Typography sx={{ fontWeight: "bold", fontSize: 20 }}>
          Your Study Tracker
        </Typography>

        <Box sx={{ display: "flex", justifyContent: "flex-end" }}></Box>
        <Typography>See how you're tracking against your pledge.</Typography>
      </Stack>
    </Paper>
  );
}
