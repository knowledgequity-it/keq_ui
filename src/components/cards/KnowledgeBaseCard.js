import knowledgeLogo from "../../assets/knowledge.png";
import { Card, CardActionArea, CardContent, Typography } from "@mui/material";
import { useNavigate } from "react-router-dom";
import useLiveLogging from "../../hooks/useLiveLogging";


export default function KnowledgeBaseCard({})
{
    const navigate = useNavigate();
    const candidateAction = useLiveLogging();

    function goToKnowledgebase(e) {
        candidateAction({
        component: "KBCard",
        event: "User Clicked the Knowledgebase card",
        });
        navigate(`${process.env.REACT_APP_AB_URL_PREFIX}/ate`);
    }

    return <Card sx={{ mb: 3, borderRadius:5 }} onClick={(e) => goToKnowledgebase(e)}>
    <CardActionArea>
      <CardContent>
        <img src={knowledgeLogo} />
        <Typography
          sx={{ height: "50px" }}
          gutterBottom
          variant="h5"
          component="h2"
        >
          Knowledge Base
        </Typography>
        <Typography
          sx={{ height: "20px" }}
          variant="subtitle1"
          color="textSecondary"
          component="p"
        >
          Find your answer here
        </Typography>
      </CardContent>
    </CardActionArea>
  </Card>
}