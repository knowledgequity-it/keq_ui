import { Card, CardActionArea, CardContent, Typography } from "@mui/material"
import calendarLogo from "../../assets/calendar.png";
import { useNavigate } from "react-router-dom";
import useLiveLogging from "../../hooks/useLiveLogging";
import GenericDialog from "../dialogs/GenericDialog";
import { useState } from "react";
import Config from "../entities/Config";


export default function KeyDatesCard()
{
  const navigate = useNavigate();
  const candidateAction = useLiveLogging();

  const [showPopup, setShowPopup] = useState(false);

    const toggleDialog = () => {
      setShowPopup(!showPopup);
    }

  const goToKeyDatesPage = () => {
    candidateAction({
      component: "KBCard",
      event: "User Clicked the Key dates card",
      });
      if(Config.KEY_DATES_UNIT === "NONE")
        toggleDialog();
      else
        navigate(`${process.env.REACT_APP_AB_URL_PREFIX}/info/key-dates`);
  }

    return <><Card sx={{ mb: 3, borderRadius:5}}>
    <CardActionArea onClick={(e)=>goToKeyDatesPage()}>
      <CardContent>
        <img src={calendarLogo} alt=""/>
        <Typography
          sx={{ height: "50px" }}
          gutterBottom
          variant="h5"
          component="h2"
        >
          Key Dates
        </Typography>
        <Typography
          sx={{ height: "20px" }}
          variant="subtitle1"
          color="textSecondary"
          component="p"
        >
          Important dates and events
        </Typography>
      </CardContent>
    </CardActionArea>
  </Card>
  <GenericDialog title="Key Dates not Available" text="Key dates for this semester are not yet available. Please check back later." toggleDialog={toggleDialog} showDialog={showPopup}/>
  </>
}