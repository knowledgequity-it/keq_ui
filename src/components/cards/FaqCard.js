import {
  Stack,
  Box,
  Paper,
  Typography,
  ListItem,
  List,
  Grid,
} from "@mui/material";
import faqLogo from "../../assets/faq.png";
import { darken } from "@mui/material/styles";
import GenericDialog from "../dialogs/GenericDialog";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";

export default function FaqCard({ url }) {
  const [showPopup, setShowPopup] = useState(false);
  const [gaqurl, setFaqUrl] = useState();
  const navigate = useNavigate();

  const toggleDialog = () => {
    setShowPopup(!showPopup);
  };
  const triggerDownload = () => {
    if (!url) toggleDialog();
    else {
      navigate(`${process.env.REACT_APP_AB_URL_PREFIX}/course/${url}`);
    }
  };

  useEffect(() => {
    if (url) setFaqUrl(url);
  }, []);

  return (
    <Paper
      sx={{
        p: 2,
        ":hover": {
          backgroundColor: darken("#ffffff", 0.03),
          cursor: "pointer",
        },
        height: "50%",
        borderRadius: 5,
        overflow: "hidden",
      }}
      onClick={(e) => triggerDownload()}
    >
      <Stack spacing={1} alignItems="center" justifyContent="space-between">
        <Typography sx={{ fontWeight: "bold", fontSize: 20 }}>
          Frequently Asked Questions
        </Typography>

        <Grid
          container
          direction="row"
          spacing={2}
          justifyContent="space-between"
        >
          <Grid item>
            <Box>
              <img src={faqLogo} />
            </Box>
          </Grid>
          <Grid item sx={{ pl: 2 }}>
            <List sx={{ listStyleType: "disc" }}>
              <ListItem sx={{ display: "list-item", p: 0 }}>
                Study Guide Corrections
              </ListItem>
              <ListItem sx={{ display: "list-item", p: 0 }}>
                Exam information
              </ListItem>
              <ListItem sx={{ display: "list-item", p: 0 }}>
                Module FAQs
              </ListItem>
            </List>
          </Grid>
        </Grid>
      </Stack>
      <GenericDialog
        title="FAQ not available"
        text="The FAQ & Errata document for this course is currently unavailable."
        toggleDialog={toggleDialog}
        showDialog={showPopup}
      />
    </Paper>
  );
}
