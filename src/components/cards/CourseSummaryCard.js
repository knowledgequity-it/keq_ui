import { Paper, Stack, Box, Typography, Button } from "@mui/material";
import LinearProgressWithLabel from "../LinearProgressWithLabel";

export default function CourseSummaryCard({
  continueFunction,
  progress,
  nextUnit,
  url
}) {
  return (
    <Paper sx={{ height: "100%", p: 2, 
                 borderRadius:5, 
                 backgroundRepeat: "no-repeat", 
                 backgroundPosition: "center center",
                 backgroundSize: "cover",
                 backgroundImage: `linear-gradient(rgba(255,255,255,1.0), rgba(255,255,255,0.95), rgba(255,255,255,0.9), rgba(255,255,255,0.4)), url(${url})`}}>
      <Stack spacing={1} justifyContent="space-between" sx={{ height: "100%" }}>
        <Box sx={{}}>
        <Typography sx={{ fontWeight: "bold", fontSize:20}}>Videos and Quizzes</Typography>
        <Box sx={{}}>
          <Typography sx={{ fontWeight: "bold" }}>Progress</Typography>

          <LinearProgressWithLabel
            variant="determinate"
            thickness={10}
            value={progress}
          />
        </Box>
        </Box>
        <Typography sx={{ fontWeight: "bold" }}>
          Up Next:
          <br />
          {nextUnit}
        </Typography>
        <Stack direction="row" justifyContent="flex-end">
          <Button
            color="continue"
            onClick={(e) => continueFunction(e)}
            variant="contained"
            sx={{ textTransform: "none", borderRadius:5}}
          >
            Continue
          </Button>
        </Stack>
      </Stack>
    </Paper>
  );
}
