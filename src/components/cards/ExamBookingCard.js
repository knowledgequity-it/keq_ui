import { Stack, Paper, Typography, Modal, Box, Button } from "@mui/material";
import { useEffect, useState, useContext } from "react";
import dayjs from "dayjs";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import { setPracticeExamDate } from "../../facades/UserFacade";
import { UserContext } from "../../contexts/userContext";
import LiveHelpOutlinedIcon from "@mui/icons-material/LiveHelpOutlined";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  boxShadow: 24,
  p: 4,
};

export default function ExamBookingCard({ courseId, pe1Date, pe2Date }) {
  const [pe1SelectedDay, setPE1SelectedDay] = useState(null);
  const [pe2SelectedDay, setPE2SelectedDay] = useState(null);
  const [openHelpModal, setOpenHelpModal] = useState(false);
  const userContext = useContext(UserContext);

  const toggleModal = () => {
    setOpenHelpModal(!openHelpModal);
  };

  const changeDay = (exam, day) => {
    if (exam === "PE1") {
      setPracticeExamDate(
        userContext.user,
        courseId,
        "PE1",
        day.format("YYYY-MM-DD"),
      ).then((res) => {
        setPE1SelectedDay(day);
      });
    }
    if (exam === "PE2") {
      setPracticeExamDate(
        userContext.user,
        courseId,
        "PE2",
        day.format("YYYY-MM-DD"),
      ).then((res) => {
        setPE2SelectedDay(day);
      });
    }
  };

  const checkPracticeExamDate = (date, exam) => {
    let pe1StartDate = dayjs("2025-03-18", ["YYYY-MM-DD"]);
    let pe2StartDate = dayjs("2025-03-21", ["YYYY-MM-DD"]);
    let peEndDate = dayjs("2025-04-28", ["YYYY-MM-DD"]);

    let theDate = dayjs(date);

    if (exam === "PE1")
      return theDate.isBefore(pe1StartDate) || theDate.isAfter(peEndDate);
    else if (exam === "PE2")
      return theDate.isBefore(pe2StartDate) || theDate.isAfter(peEndDate);
    else return false;
  };

  useEffect(() => {
    if (dayjs(pe1Date).isValid())
      setPE1SelectedDay(dayjs(pe1Date, ["YYYY-MM-DD"]));
    if (dayjs(pe2Date).isValid())
      setPE2SelectedDay(dayjs(pe2Date, ["YYYY-MM-DD"]));
  }, [pe1Date, pe2Date]);

  const disableKeyboardEntry = (e) => {
    if (e?.preventDefault) {
      e?.preventDefault();
      e?.stopPropagation();
    }
  };
  return (
    <Paper sx={{ height: "100%", p: 2, borderRadius: 5, overflow: "hidden" }}>
      <Modal
        open={openHelpModal}
        onClose={toggleModal}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Typography id="modal-modal-title" variant="h6" component="h2">
            About Your Practice Exams
          </Typography>
          <Typography sx={{ mt: 2 }}>
            <b>Practice Exam 1</b> is released on 18th March.
          </Typography>
          <Typography sx={{ mt: 2 }}>
            <b>Practice Exam 2</b> is released on 21st March.
          </Typography>
          <Typography sx={{ mt: 2 }}>
            Both will remain open until the end of the CPA Program Exam Period
            on 28th April.
          </Typography>
          <Typography sx={{ mt: 2 }}>
            We recommend doing Practice Exam 1 at least 2 weeks before your CPA
            Exam.
          </Typography>
          <Button
            variant="outlined"
            onClick={(e) => {
              toggleModal();
            }}
            sx={{ mt: 2 }}
          >
            Got it!
          </Button>
        </Box>
      </Modal>
      <Stack
        useFlexGap
        spacing={1}
        justifyContent="space-between"
        sx={{ height: "100%" }}
      >
        <div
          style={{
            display: "flex",
            alignItems: "center",
            flexWrap: "wrap",
          }}
        >
          <Typography sx={{ fontWeight: "bold", fontSize: 20 }}>
            Practice Exam Booking
          </Typography>
          <LiveHelpOutlinedIcon
            onClick={(e) => toggleModal()}
            sx={{ ml: 1, cursor: "pointer" }}
          />
        </div>
        <Typography variant="body1">
          <strong>Practice Exam 1:</strong>
          <br />
        </Typography>
        <LocalizationProvider dateAdapter={AdapterDayjs}>
          <DatePicker
            format="ddd D MMM YYYY"
            label="Book Practice Exam 1"
            value={pe1SelectedDay}
            onChange={(newValue) => {
              changeDay("PE1", newValue);
            }}
            shouldDisableDate={(date) => checkPracticeExamDate(date, "PE1")}
            slotProps={{
              textField: {
                placeholder: "",
                disabled: false,
                onBeforeInput: disableKeyboardEntry,
                InputProps: {
                  size: "medium",
                  sx: { fontSize: 22, maxWidth: 225 },
                  disableUnderline: true,
                  readOnly: true,
                },

                variant: "standard",
              },
            }}
          />
        </LocalizationProvider>

        <Typography variant="body1">
          <strong>Practice Exam 2:</strong>
          <br />
        </Typography>
        <LocalizationProvider dateAdapter={AdapterDayjs}>
          <DatePicker
            format="ddd D MMM YYYY"
            label={pe2SelectedDay == null ? "Book Practice Exam 2" : ""}
            value={pe2SelectedDay}
            onChange={(newValue) => {
              changeDay("PE2", newValue);
            }}
            shouldDisableDate={(date) => checkPracticeExamDate(date, "PE2")}
            slotProps={{
              textField: {
                placeholder: "",
                disabled: false,
                onBeforeInput: disableKeyboardEntry,
                InputProps: {
                  size: "medium",
                  sx: { fontSize: 22, maxWidth: 225 },
                  disableUnderline: true,
                  readOnly: true,
                },
                variant: "standard",
              },
            }}
          />
        </LocalizationProvider>
      </Stack>
    </Paper>
  );
}
