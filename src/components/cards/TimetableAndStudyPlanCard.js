import { Stack, Paper, Box } from "@mui/material";
import { Typography } from "@mui/material";
import scheduleLogo from "../../assets/schedule.png";
import { darken } from "@mui/material/styles";
import { useNavigate } from "react-router-dom";
import GenericDialog from "../dialogs/GenericDialog";
import { useState } from "react";

export default function TimetableAndStudyCard({ unit }) {
  const navigate = useNavigate();
  const [showPopup, setShowPopup] = useState(false);

  const togglePopup = () => {
    setShowPopup(!showPopup);
  };

  const handleClick = () => {
    if (!unit) togglePopup();
    else
      navigate(
        `${process.env.REACT_APP_AB_URL_PREFIX}/course/${unit.course_url}/${unit.url_name}`,
        { state: { unit } },
      );
  };
  return (
    <Paper
      sx={{
        p: 2,
        ":hover": {
          backgroundColor: darken("#ffffff", 0.03),
          cursor: "pointer",
        },
        borderRadius: 5,
        overflow: "hidden",
      }}
      onClick={(e) => handleClick()}
    >
      <Stack spacing={1}>
        <Typography sx={{ fontWeight: "bold", fontSize: 20 }}>
          Timetable and Study Plan
        </Typography>
        <Box sx={{ display: "flex", justifyContent: "center" }}>
          <img src={scheduleLogo} />
        </Box>
        <Typography variant="body1">Stay on track.</Typography>
      </Stack>
      <GenericDialog
        title="Resource Currently Unavailable"
        text="The Timetable and Study Plan resources for this course are not currently available."
        showDialog={showPopup}
        toggleDialog={setShowPopup}
      />
    </Paper>
  );
}
