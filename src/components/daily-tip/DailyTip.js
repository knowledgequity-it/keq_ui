import { Grid, Paper, Typography } from "@mui/material";
import TipsAndUpdatesIcon from '@mui/icons-material/TipsAndUpdates';
import { useState } from "react";

export default function DailyTip({})
{
    const [visible, setVisible] = useState(false)

    const toggleVisibility = () => {
        setVisible(false);
    }


    return visible && <Paper sx={{p:2}}>
        <Grid container direction="row" alignItems="center" justifyContent="center">
            <Grid item xs={2}>
                <TipsAndUpdatesIcon  sx={{color : "blue", fontSize : 40}}/>
            </Grid>
            <Grid item xs={9}>
                <Typography variant="h5">Did you know?</Typography>
                <Typography variant="h6">6 minutes and 30 seconds - this is the time you should spend on each MCQ in your final exam</Typography>
            </Grid>
            <Grid item onClick={toggleVisibility}>
                Got it!
            </Grid>
        </Grid>
    </Paper>
}