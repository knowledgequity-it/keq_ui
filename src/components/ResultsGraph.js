import React from "react";
import Box from "@mui/material/Box";

import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend,
} from "chart.js";
import { Bar } from "react-chartjs-2";
import ChartDataLabels from "chartjs-plugin-datalabels";

ChartJS.register(
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend,
  ChartDataLabels,
);
const options = {
  responsive: true,
  maintainAspectRatio: false,
  aspectRatio: 4,
  indexAxis: "y",
  scales: {
    y: [{ ticks: { mirror: true, autoSkip: false } }],
    x: {
      min: 0,
      max: 100,
    },
  },
  elements: {
    bar: {
      borderWidth: 2,
    },
  },
  plugins: {
    legend: {
      display: false,
    },
    title: {
      display: true,
      text: "Results by Module",
    },
    datalabels: {
      anchor: "start",
      align: "end",
      clamp: true,
      formatter: function (value, context) {
        let label = `${value.marks} / ${value.max_marks} = ${Math.round(
          (value.marks / value.max_marks) * 100,
        )}%`;

        if (context.dataIndex !== 0) {
          label =
            label +
            `  |  Weighting ${Math.round(
              (value.max_marks / value.total_marks) * 100,
            )}%`;
        }
        return label;
      },
    },
  },
};

function makeGraph(quiz_data, tagData) {
  var data = [];
  var background_color = [];
  var border_color = [];
  var total_marks = 0;
  const excluded_question_types = ["largetext", "smalltext"];

  var tag_matcher =
    tagData?.custom_tag_matcher !== ""
      ? tagData?.custom_tag_matcher
      : tagData?.default_tag_matcher;
  var tag_label =
    tagData?.custom_tag_matcher !== ""
      ? tagData?.custom_tag_label
      : tagData?.default_tag_label;

  //modules
  quiz_data.forEach((q) => {
    if (q.tags && Array.isArray(q.tags)) {
      q.tags.forEach((t) => {
        var matched = t.match(tag_matcher); //t.match("[\\w]+-m[\\d]+$");
        //console.log("MMM",t, matched);
        if (matched !== null) {
          if (!excluded_question_types.includes(q.question_type)) {
            //console.log("BBB",Number.isInteger(q.marks),t.replace(/\D/g,''), parseInt(q.marks) )
            var idx = parseInt(t.replace(/\D/g, ""));
            total_marks += q.available_marks;

            if (!data[idx]) {
              data[idx] = {
                y: tag_label + " " + idx,
                pct: 0,
                marks: 0,
                max_marks: 0,
              };
              background_color[idx] = "rgba(255, 184, 0, 0.2)";
              border_color[idx] = "rgba(255, 184, 0)";
            }
            if (Number.isInteger(parseInt(q.marks))) {
              data[idx].marks += parseInt(q.marks);
              data[idx].max_marks += q.available_marks;
              data[idx].pct = (data[idx].marks / data[idx].max_marks) * 100;
            } else {
              data[idx].max_marks += q.available_marks;
              data[idx].pct = (data[idx].marks / data[idx].max_marks) * 100;
            }
          }
        }
      });
    }
    //total
    if (!excluded_question_types.includes(q.question_type)) {
      if (!data[0]) {
        data[0] = { y: "Total", pct: 0, marks: 0, max_marks: 0 };
        background_color[0] = "rgba(54, 162, 235, 0.2)";
        border_color[0] = "rgba(54, 162, 235, 1)";
      }
      if (Number.isInteger(parseInt(q.marks))) {
        data[0].marks += parseInt(q.marks);
        data[0].max_marks += q.available_marks;
        data[0].pct = (data[0].marks / data[0].max_marks) * 100;
      } else {
        data[0].max_marks += q.available_marks;
        data[0].pct = (data[0].marks / data[0].max_marks) * 100;
      }
    }
  });
  data.forEach((d) => (d.total_marks = total_marks));
  data = data.filter((a) => a);
  background_color = background_color.filter((a) => a);
  border_color = border_color.filter((a) => a);

  //return data;
  const cfg = {
    datasets: [
      {
        label: "%age by Module",
        data: data,
        backgroundColor: background_color,
        borderColor: border_color,
        borderWidth: 1,
        minBarLength: 1,
        parsing: {
          xAxisKey: "pct",
        },
      },
    ],
  };
  return cfg;
}

export default function ResultsGraph({ quiz_data, tagData }) {
  return quiz_data.length !== 0 ? (
    <Box>
      <Bar
        options={options}
        data={makeGraph(quiz_data, tagData)}
        height={300}
      />
    </Box>
  ) : (
    <Box></Box>
  );
}
