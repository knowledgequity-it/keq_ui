import NetworkCheckIcon from "@mui/icons-material/NetworkCheck";
import { useEffect, useState } from "react";
import Snackbar from "@mui/material/Snackbar";
import Slide from "@mui/material/Slide";
import useLiveLogging from "../hooks/useLiveLogging";

export default function QuizNetworkSpeedCheck(props) {
  const TIMER_INTERVAL = 10000; //timer interval to check connection
  const DEBUG = false;

  const [networkSpeed, setNetworkSpeed] = useState(null);
  const [showSlowInternetNotice, setShowSlowInternetNotice] = useState(false);
  const [lastShowedSlowInternetNotice, setLastShowedSlowInternetNotice] =
    useState(-1);
  const [ticker, setTicker] = useState(true);
  const [online, isOnline] = useState(navigator.online);
  const [userMessage, setUserMessage] = useState("Connection OK");
  const candidateAction = useLiveLogging();

  function SlowNetworkSliderTransition(props) {
    return <Slide {...props} direction="right" />;
  }

  const setOnline = () => {
    if (DEBUG) console.log("We are online!");
    isOnline(true);
  };
  const setOffline = () => {
    if (DEBUG) console.log("We are offline!");
    isOnline(false);
  };

  // Register event listeners. NAvigator.online returns true if there's a network connection, but could have no internet access, so need this.
  useEffect(() => {
    window.addEventListener("offline", setOffline);
    window.addEventListener("online", setOnline);

    // cleanup if we unmount
    return () => {
      window.removeEventListener("offline", setOffline);
      window.removeEventListener("online", setOnline);
    };
  }, []);

  useEffect(() => {
    const timer = setTimeout(() => {
      setTicker(!ticker);
      if (
        !checkNetSpeed() &&
        (lastShowedSlowInternetNotice >= 300000 ||
          lastShowedSlowInternetNotice == -1)
      ) {
        console.log("slow branch" + lastShowedSlowInternetNotice);
        setShowSlowInternetNotice(true);
      } else {
        console.log("normal branch" + lastShowedSlowInternetNotice);
        setShowSlowInternetNotice(false);
        if (lastShowedSlowInternetNotice >= 0)
          setLastShowedSlowInternetNotice(
            lastShowedSlowInternetNotice + TIMER_INTERVAL,
          );
      }
    }, TIMER_INTERVAL);
    return () => clearTimeout(timer);
  }, [ticker]);

  const checkNetSpeed = () => {
    if (DEBUG) console.log("NAVIGATOR", navigator, navigator.onLine);
    // first handle the situation when user wifi has turned off. this issue can not be handled by measuring downlink speed
    if (navigator && !navigator.onLine) {
      setUserMessage(
        "You internet connection has dropped. Please connect to a stable, wired network to improve your connection",
      );
      candidateAction({
        component: "Quiz",
        activity: "Quiz",
        event: "no_connection_warning_shown",
      });

      return false;
    }

    var connection =
      navigator.connection ||
      navigator.mozConnection ||
      navigator.webkitConnection;

    if (DEBUG) console.log("CONNECTION", connection);

    if (connection && connection.downlink < 0.5) {
      setNetworkSpeed(connection.downlink);
      setUserMessage(
        `Your current internet speed (${connection.downlink}Mb/s) is below the recommended system requirements (1 Mb/s). Please connect to a stable, wired network to improve your connection`,
      );
      candidateAction({
        component: "Quiz",
        activity: "Quiz",
        event: "slow_connection_warning_shown",
      });
      return false;
    }
    //setNetworkSpeed(connection.downlink)
    return true;
  };

  return (
    <div>
      {/* {networkSpeed && <Typography>
                <NetworkCheckIcon />  {networkSpeed} {"MBPS"}
            </Typography>} */}
      <Snackbar
        ContentProps={{
          sx: {
            opacity: 0.7,
          },
        }}
        open={showSlowInternetNotice || !isOnline}
        onClose={() => {
          setShowSlowInternetNotice(false);
          setLastShowedSlowInternetNotice(0);
        }}
        autoHideDuration={TIMER_INTERVAL - 4000}
        TransitionComponent={SlowNetworkSliderTransition}
        message={userMessage}
      />
    </div>
  );
}
