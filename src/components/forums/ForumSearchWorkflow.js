import { Grid, Typography, Card, CardContent, Paper, Stack, CardActions, Link, Button  } from "@mui/material";
import ArrowCircleRightOutlinedIcon from "@mui/icons-material/ArrowCircleRightOutlined";
import StudyGuideFilter from "./StuyGuideFilter";
import { useContext, useEffect, useState } from "react";
import ForumEditor from "./ForumEditor";
import { searchKnowledgebase } from "../../facades/UserFacade";
import { UserContext } from "../../contexts/userContext";
import { useParams } from "react-router-dom";
const CARD_WIDTH = 200

export default function ForumSearchWorkflow({subjectCode, forumid, moduleNumber, sec, htmlSec})
{
    const [selection, setSelection] = useState(null);
    const [studyGuideSubjectsSelected, setStudyGuideSubects] = useState(null)
    const [searchResults, setSearchResults] = useState([]);
    const [theSubjectCode, setTheSubjectCode] = useState(null);
    const [theForumId, setTheForumId] = useState(null);
    const [theModuleNumber, setTheModuleNumber] = useState(null);

    const urlParams = useParams();
    
    const userContext = useContext(UserContext);

    useEffect(() => {   
        if(subjectCode)
            setTheSubjectCode(subjectCode);
    }, [subjectCode])

    useEffect(() => {
        if(forumid)
            setTheForumId(forumid);
    },[forumid])

    useEffect(() => {
        if(moduleNumber)
            setTheModuleNumber(moduleNumber);
    },[moduleNumber])
  

    const searchSelectedSgSections = (searchTerm) => {
        if(searchTerm && searchTerm !== "" )
            searchKnowledgebase(userContext.user, searchTerm, 10, theForumId)
        .then(res => res.json())
        .then(res => {

            console.log(res);
            setSearchResults(res.topics);
            setStudyGuideSubects(searchTerm)

            setSelection("sg");
            
        })
    }

    let glQueryEditor = null;
    let sgQueryEditor = null
    let studyGuideDropdowns = null;
    let searchResultsPanel = null;


    if(selection == "gl")
        glQueryEditor = <ForumEditor forumName={Object.values(urlParams)[0]}  forumID={theForumId} htmlSec={htmlSec} sec={sec}/>

    if(studyGuideSubjectsSelected)
        sgQueryEditor = <ForumEditor forumName={Object.values(urlParams)[0]}  forumID={theForumId} htmlSec={htmlSec} sec={sec}/>
        
    if(selection == "sg")
        studyGuideDropdowns = <StudyGuideFilter subjectCode={theSubjectCode} moduleNumber={theModuleNumber} searchCallback={searchSelectedSgSections}/>

    if(searchResults)
        searchResultsPanel = <Stack direction="row" spacing={2}>
        { searchResults && <Typography variant="h5"> Forum Results</Typography>}
        {
            searchResults && 
            searchResults.map((topic, index) => {
                if(index <= 5)
                return <Card sx={{width : CARD_WIDTH}} key={topic.url_name}>
                    <CardContent>
                        <Typography variant="h5">{topic.title}</Typography>
                        
                        <Typography variant="body2">{topic.content.slice(0, topic.content.length - 8)+"..."}</Typography>
                        
                    </CardContent>
                    <CardActions sx={{
                                alignSelf: "stretch",
                                display: "flex",
                                justifyContent: "flex-end",
                                alignItems: "flex-start",
                                // 👇 Edit padding to further adjust position
                                p: 0,
                                }}>
                       <Link href={`${process.env.REACT_APP_AB_URL_PREFIX}/ate/${topic.forum}/${topic.url_name}`} target="_blank" rel="noopener noreferrer" underline="none">
                        <Button>open
                            
                        <ArrowCircleRightOutlinedIcon/>
                        </Button>
                        </Link>
                    </CardActions>
                </Card>
            })
        }
    </Stack>                                   


    return <>
    <Grid container direction="row" alignItems="center" justifyContent="center" spacing={3}>
        <Grid item sx={{m:3}}>
            <Card onClick={() => setSelection("gl")} sx={{width:330, height:150, ':hover': { boxShadow: 10 }, borderRadius:5}} >
                <CardContent>
                    <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
                        Ask a question about 
                    </Typography>
                    <Typography variant="h4"  sx={{fontWeight: 'bold', color:"grey"}} component="div">
                        Guided Learning
                    </Typography>
                </CardContent>    
            </Card>
        </Grid>
        <Grid item sx={{m:3}}>
            <Card onClick={() => setSelection("sg")} sx={{width:330, height:150, ':hover': { boxShadow: 10 }, borderRadius:5}}>
                <CardContent>
                <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
                        Ask a question about the 
                    </Typography>
                    <Typography variant="h4" sx={{fontWeight: 'bold', color:"grey"}} component="div">
                        Study Guide
                    </Typography>
                </CardContent>    
            </Card>
        </Grid>
        <Grid item>
            {
                searchResults.map((result, index) => {
                    <Paper>
                        {result}
                    </Paper>
                })
            }
        </Grid>
    </Grid>
    {studyGuideDropdowns}
    {searchResultsPanel}
    {glQueryEditor}
    {sgQueryEditor}
    </>
}