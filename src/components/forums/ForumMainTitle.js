import { Paper, Typography } from "@mui/material";
import KBSearchPanel from "../knowledgebase/KBSearchPanel";

export default function ForumMainTitle({bigTitle, subTitle, forumId}){

    return <>
        <Paper elevation={0} sx={{h:"200px", bgcolor:"#e7f9d5", p:4, m:2, borderRadius:5}}>
        <Typography sx={{fontSize:32, fontWeight:700}}  color="text.secondary">{bigTitle}</Typography>
        <Typography variant="h5"  color="text.secondary">{subTitle}</Typography>
        <KBSearchPanel forumId={forumId}></KBSearchPanel>
        </Paper>
    </>
}