import { SubjectTwoTone } from '@mui/icons-material';
import { FormControl, Grid, InputLabel, MenuItem, Select, Typography } from '@mui/material';
import { useEffect, useState } from 'react';

let sgconfig = require('./StudyGuideConfig.json');


export default function StudyGuideFilter({subjectCode, moduleNumber, searchCallback})
{
    const [section, setSection] = useState("");
    const [subSection, setSubSection] = useState("");

    const [moduleSections, setModuleSections] = useState([]);
    const [moduleSubsections, setModuleSubsections] = useState([]);

    const [theSubjectCode, setTheSubjectCode] = useState(null);
    const [theModuleNumber, setTheModuleNumber] = useState(null);


    const searchCurrentTerms = () => {
        searchCallback(`${section ? section : ""} ${subSection ? subSection : ""}`)
    }

    const sectionSelect = (e) => {
        setSection(e.target.value);
        setModuleSubsections(sgconfig[subjectCode][moduleNumber][e.target.value])
        searchCurrentTerms();
    }
    const subSectionSelect = (e) => {
        setSubSection(e.target.value);
        searchCurrentTerms();
    }

    useEffect(()=> {
        console.log("SCODE", moduleNumber)
        if(moduleNumber)
            setTheModuleNumber(moduleNumber);
    }, [moduleNumber])

    useEffect(() => {
        if(subjectCode)
            setTheSubjectCode(subjectCode);
    }, [subjectCode])

    useEffect(() => {
        console.log("setting sections", theSubjectCode, theModuleNumber)
        if(theSubjectCode && theModuleNumber)
        {
            //console.log(sgconfig[subjectCode][moduleNumber])
            if(theSubjectCode)
                setModuleSections(sgconfig[theSubjectCode][theModuleNumber])
        }
        },[theSubjectCode, theModuleNumber])
    

    return <Grid container direction="column" alignItems="stretch" justifyContent="center" spacing={2}>
            <Grid item xs={12}>
                <Typography>
                    Selet the sections your query is about so we can help you faster. 
                </Typography>
            </Grid>
            <Grid item xs={12} sm={8}>
                <FormControl sx={{width: "100%", padding: 2, background:"white"}}>
                    <InputLabel id="section-label">Section</InputLabel>
                    <Select
                        labelId="section-label"
                        id="section-select"
                        value={section}
                        label="Section"
                        onChange={sectionSelect}
                    >
                        {Object.keys(moduleSections).map((sec, index) => {
                            return <MenuItem value={sec} key={index}>{sec}</MenuItem>
                        })}
                    </Select>
                </FormControl>
            </Grid>
            <Grid item xs={12} sm={9}>
                <FormControl sx={{width: "100%", padding: 2, background:"white"}}>
                <InputLabel id="sub-section-label">Sub Section</InputLabel>
                <Select
                    labelId="sub-section-label"
                    id="sub-section-select"
                    value={subSection}
                    label="Sub section"
                    onChange={subSectionSelect}
                >
                    {moduleSubsections.map((sec, index) => {
                        return <MenuItem value={sec} key={index}>{sec}</MenuItem>
                    })}
                </Select>
                </FormControl>
            </Grid>
    </Grid>

}