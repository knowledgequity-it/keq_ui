import { Grid, Card, CardContent, ToggleButton, ToggleButtonGroup, Typography, Divider, Link } from "@mui/material";
import { React, useContext, useEffect, useState } from "react";
import { getUserForums } from "../../facades/ForumsFacade";
import { UserContext } from "../../contexts/userContext";
import { useNavigate } from "react-router-dom";
import ForumMainTitle from "./ForumMainTitle";
import ForumFAQ from "./ForumFAQ";
import TandCDialog from "./TandCDialog";



export default function Forum()
{

    
    const [userForums, setUserForums] = useState([]);
    const userContext = useContext(UserContext);
    const navigate = useNavigate();

    useEffect(() => {
        getUserForums(userContext.user)
        .then(res => res.json())
        .then((forums) => {
            setUserForums(forums);
        })
    }, [])
  
    let forumPanel = null;

    const goToForum = (forumName, niceName, id) => {
        navigate(`${process.env.REACT_APP_AB_URL_PREFIX}/ate/${forumName}`, {
            state: {
              nicename: niceName,
              forumid: id
            },
          });

    }

    const getNumberFromString = (theString) => {
        let thenum = theString.match(/\d+/)
        if(thenum)
            return parseInt(thenum[0], 10)
        else
            return "";
    }
    if(userForums.length != 0)
    {
        userForums.sort((a, b) => {a.course_name.localeCompare(b.course_name)});
        let forumGroups = {};

        //EG, FR, GSL, SMA
        //
        console.log("====", userForums)

        userForums.forEach(forum => {
            const courseKey = forum['course'];

            if (!forumGroups[courseKey]) {
                forumGroups[courseKey] = [];
            }

            forumGroups[courseKey].push(forum);
        });

        
        let groupedForums = Object.values(forumGroups).sort((a, b) => a[0].course_name.localeCompare(b[0].course_name));
        
        forumPanel = groupedForums.map((forumGroup, index) => {

                return <Grid item  >
                        <Grid container direction="column">
                            <Grid item>
                                <Grid container direction="row" spacing={1} alignItems="center" justifyContent="flex-start">
                                    <Grid item>
                                        <Typography fontSize={30} fontWeight={700}>
                                            {forumGroup[0].course_name}
                                        </Typography>
                                    </Grid>
                                    <Grid item>
                                        <Link href={`${process.env.REACT_APP_AB_URL_PREFIX}/course/${forumGroup[0].course_link}`}>
                                            Back to course >>
                                        </Link>
                                    </Grid>
                                </Grid>
                                
                            </Grid>
                            <Grid item >
                                <Grid container direction="row" key={index} alignItems="flex-start" >
                                {
                                    forumGroup.map((singleForum, idx) => {
                                        return <>
                                        <Card sx={{width:150, height:150, ml:2,mt:2, ':hover': { boxShadow: 4 }, border:"none", boxShadow:'none', borderRadius:0}} key={idx} onClick={() => goToForum(singleForum['post_name'], singleForum['course_name'], singleForum['ID'])}>
                                            
                                            <CardContent>
                                                <div>
                                                    <div style={{position: 'absolute', zIndex:0, overflow:"hidden", marginLeft:70, fontSize:96, fontWeight:900, color:"lightgreen", opacity:0.4}}>
                                                        <p style={{margin:0, zIndex:0}}>{getNumberFromString(singleForum.post_title)}</p>
                                                    </div>
                                                    <div style={{zIndex:500}}>
                                                        <Typography fontSize={20} fontWeight={600}>
                                                            {singleForum.post_title.replace(`${singleForum.course_name} - `, "")}
                                                        </Typography>
                                                        <Typography>
                                                            {singleForum.topic_count == 0 ? "" : singleForum.topic_count} {singleForum.topic_count == 0 ? "" : singleForum.topic_count == 1 ? "topic" : "topics"}
                                                        </Typography>
                                                        <Typography>
                                                            {singleForum.reply_count == 0 ? "" : singleForum.reply_count} {singleForum.reply_count == 0 ? "" : singleForum.reply_count == 1 ? "reply" : "replies"}
                                                        </Typography>
                                                    </div>
                                                </div>
                                            </CardContent>
                                            </Card>
                                            <Divider orientation="vertical" variant="middle" flexItem />
                                            </>

                                    })
                                }
                                </Grid>
                            </Grid>
                    </Grid>
                </Grid>
            
        })
    }

    

    return <>
        
        <Grid container xs={12} direction="row" alignItems="flex-start" justifyContent="space-between" >
            <Grid item xs={0}>
                <TandCDialog/>
            </Grid>
            <Grid item xs={4}>
                <ForumMainTitle bigTitle="Welcome to the Knowledge Center." subTitle="Search our Knowledgebase for answers to your questions, or Ask the Expert."></ForumMainTitle>
                
            </Grid>
            <Grid item xs={8}>
                <Typography sx={{fontSize:22, fontWeight:600 }}>Your Subject Forums</Typography>
                <div style={{backgroundColor: "white"}}>
                <Grid container  direction="column" alignItems="flex-start"  justifyContent="center" sm={12}>
                {forumPanel}
                </Grid>
                </div>
        </Grid>
    </Grid>
    </>
}