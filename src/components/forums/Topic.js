import {
  Paper,
  Typography,
  Grid,
  Avatar,
  Card,
  CardHeader,
  CardContent,
  CardActions,
  Button,
  Divider,
  Link,
} from "@mui/material";
import { useContext, useEffect, useState } from "react";
import { getTopicReplies } from "../../facades/ForumsFacade";
import { UserContext } from "../../contexts/userContext";
import { useParams } from "react-router-dom";
import ForumEditor from "./ForumEditor";
import SendIcon from "@mui/icons-material/Send";
import NotificationAddIcon from "@mui/icons-material/NotificationAdd";

export default function Topic() {
  const [topicReplies, setTopicReplies] = useState([]);
  const [topic, setTopic] = useState();
  const [topicForum, setTopicForum] = useState();
  const [topicForumCourse, setTopicForumCourse] = useState();
  const [topicForumCourseLink, setTopicForumCourseLink] = useState();
  const [canReply, setCanReply] = useState(false);
  const [sec, setSec] = useState();
  const [htmlSec, setHtmlSec] = useState();
  const userContext = useContext(UserContext);
  const urlParams = useParams();

  const [currentTopicBeingReplyiedTo, setCurrentTopicBeingReplyiedTo] =
    useState();

  const showEditorForReply = (parentReplyId) => {
    setCurrentTopicBeingReplyiedTo(parentReplyId);
  };

  useEffect(() => {
    const paramValues = Object.values(urlParams);

    getTopicReplies(userContext.user, paramValues[0], paramValues[1])
      .then((res) => res.json())
      .then((res) => {
        setTopicReplies(res.replies);
        setTopic(res.topic);
        setTopicForum(res.topic_forum);
        setSec(res.sec);
        setHtmlSec(res.html_rep);
        setTopicForumCourse(res.course_name);
        setTopicForumCourseLink(res.course_link);
        console.log("res.topic.date", res.topic.date);
        console.log("res.reply_from_date", res.reply_from_date);
        if (Date.parse(res.topic.date) >= Date.parse(res.reply_from_date)) {
          setCanReply(true);
        }
      });
  }, [urlParams]);

  if (!topic) return null;

  let margin = 10;

  return (
    <>
      <Grid container alignItems="center" justifyContent="center" spacing={1}>
        <Grid item sm={9} xs={12}>
          <Typography
            sx={{ fontSize: 36, fontWeight: 700, pt: 2 }}
            color="text.secondary"
          >
            {topicForumCourse}
          </Typography>
          <Typography>
            <Link href={`${process.env.REACT_APP_AB_URL_PREFIX}/ate/${Object.values(urlParams)[0]}`} >Back to forum topics>></Link>
          </Typography>
          <Typography
            sx={{ fontSize: 36, fontWeight: 700, pt: 2 }}
            color="text.secondary"
          >
            {topic.title}
          </Typography>
          <Button endIcon={<NotificationAddIcon />}>Subscribe</Button>
        </Grid>
        <Grid item sm={9} xs={12}>
          <Card sx={{ borderRadius: 5, mt: 2 }}>
            <CardHeader
              sx={{ borderLeft: 10, borderLeftColor: "green" }}
              avatar={
                <Avatar
                  sx={{ bgcolor: "grey" }}
                  aria-label="author"
                  alt={topic.author_name}
                  src={topic.avatar}
                ></Avatar>
              }
              title={topic.author_name}
              subheader={topic.date}
            />
            <CardContent sx={{ borderLeft: 10, borderColor: "green" }}>
              <div
                style={{ whiteSpace: "pre-line" }}
                dangerouslySetInnerHTML={{ __html: topic.content }}
              ></div>
            </CardContent>
            {canReply && (
              <CardActions sx={{ borderLeft: 10, borderColor: "green" }}>
                <Button
                  onClick={() => showEditorForReply(topic.ID)}
                  endIcon={<SendIcon />}
                >
                  Reply
                </Button>
              </CardActions>
            )}
          </Card>
          {currentTopicBeingReplyiedTo != topic.ID ? null : (
            <ForumEditor
              forumName={Object.values(urlParams)[0]}
              topicName={Object.values(urlParams)[1]}
              replyTo={topic.ID}
              topicID={topic.ID}
              forumID={topicForum}
              htmlSec={htmlSec}
              sec={sec}
            />
          )}
        </Grid>
        <Grid item>
          <Divider flexItem />
        </Grid>
        {topicReplies.map((reply, index) => {
          if (index > 0) {
            if (topicReplies[index].reply_to == topicReplies[index - 1].ID)
              margin += 10;
            else margin = 10;
          }
          return (
            <Grid item key={index} sm={9} xs={12}>
              <Card sx={{ ml: margin, borderRadius: 5 }}>
                <CardHeader
                  avatar={
                    <Avatar
                      sx={{ bgcolor: "grey" }}
                      aria-label="author"
                      alt={reply.author_name}
                      src={reply.avatar}
                    ></Avatar>
                  }
                  title={reply.author_name}
                  subheader={reply.date}
                />
                <CardContent>
                  <div
                    dangerouslySetInnerHTML={{ __html: reply.content }}
                  ></div>
                </CardContent>
                {canReply && (
                  <CardActions>
                    <Button
                      onClick={() => showEditorForReply(reply.ID)}
                      endIcon={<SendIcon />}
                    >
                      Reply
                    </Button>
                  </CardActions>
                )}
              </Card>
              {currentTopicBeingReplyiedTo != reply.ID ? null : (
                <ForumEditor
                  forumName={Object.values(urlParams)[0]}
                  topicName={Object.values(urlParams)[1]}
                  replyTo={reply.ID}
                  topicID={topic.ID}
                  forumID={topicForum}
                  htmlSec={htmlSec}
                  sec={sec}
                />
              )}
            </Grid>
          );
        })}
        {canReply && (
          <Grid item sm={9} xs={12}>
            <Typography>Reply to "{topic.title}"</Typography>
            <ForumEditor
              forumName={Object.values(urlParams)[0]}
              topicName={Object.values(urlParams)[1]}
              replyTo={topic.ID}
              topicID={topic.ID}
              forumID={topicForum}
              htmlSec={htmlSec}
              sec={sec}
            />
          </Grid>
        )}
      </Grid>
    </>
  );
}
