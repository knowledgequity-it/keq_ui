import { useState, useEffect, useContext } from "react";
import { Editor } from "@tinymce/tinymce-react";
import {
  saveReply,
  saveTopic,
  uploadForumImage,
} from "../../facades/ForumsFacade";
import { UserContext } from "../../contexts/userContext";
import {
  Button,
  CircularProgress,
  Paper,
  Stack,
  TextField,
  Typography,
} from "@mui/material";

export default function ForumEditor({
  initialValue,
  forumName,
  topicName,
  replyTo,
  topicID,
  forumID,
  htmlSec,
  sec,
}) {
  const [value, setValue] = useState(initialValue ?? "");
  const [title, setTitle] = useState("");
  const [topic, setTopicName] = useState(null);
  const [forum, setForumName] = useState(null);
  const [subscribe, setSubscribe] = useState("");

  const [submitClicked, setSubmitClicked] = useState(false);

  useEffect(() => setValue(initialValue ?? ""), [initialValue]);
  useEffect(() => {
    if (topicName) {
      setTopicName(topicName);
    }
  }, [topicName]);
  useEffect(() => {
    if (forumName) {
      setForumName(forumName);
    }
  }, [forumName]);

  const userContext = useContext(UserContext);

  const submitButtonText = () => {
    return !topic ? "Submit Question" : "Submit Reply";
  };

  const submitPostOrTopic = () => {
    //if there's no topic name then the editor component is to submit a new Topic
    //Else, it is replying to a topic or another reply.

    setSubmitClicked(true);
    if (!topic) submitTopic();
    else submitPost();
  };

  const submitTopic = () => {
    let postData = new FormData();
    postData.append("bbp_topic_title", title);
    postData.append("bbp_topic_content", value);
    postData.append("bbp_stick_topic", "unstick");
    //postData.append('bbp_topic_status', 'publish');
    postData.append("bbp_topic_subscription", "bbp_subscribe");
    postData.append("bbp_topic_submit", "");
    postData.append("action", "bbp-new-topic");
    postData.append("bbp_forum_id", forumID);
    postData.append("_bbp_unfiltered_html_topic", htmlSec);
    postData.append("_wpnonce", sec);
    postData.append("_wp_http_referer", "");
    postData.append(
      "redirect_to",
      `${process.env.REACT_APP_AB_URL_PREFIX}/ate/${forum}/`,
    );

    saveTopic(userContext.user, postData, forum).then((res) => {
      console.log(res);

      if (res.redirected) window.location.href = res.url;
    });

    //bbp_topic_title: TEST POsT
    //bbp_topic_content: Test text
    //bbp_stick_topic: unstick
    //bbp_topic_status: publish
    //bbp_topic_subscription: bbp_subscribe
    //bbp_topic_submit:
    //bbp_forum_id: 153966
    //action: bbp-new-topic
    //_bbp_unfiltered_html_topic: e63e8b0d93
    //_wpnonce: ec3b305c2a
    //_wp_http_referer: /forums/forum/advanced-audit-assurance-module-01-forum/
  };
  const submitPost = () => {
    let postData = new FormData();
    postData.append("bbp_reply_content", value);
    postData.append("bbp_topic_id", topicID);
    postData.append("bbp_reply_submit", "");
    postData.append("bbp_reply_to", replyTo);
    postData.append("action", "bbp-new-reply");
    postData.append("_bbp_unfiltered_html_reply", htmlSec);
    postData.append("_wpnonce", sec);
    postData.append("_wp_http_referer", "");
    postData.append(
      "redirect_to",
      `${process.env.REACT_APP_AB_URL_PREFIX}/ate/${forum}/${topic}`,
    );

    saveReply(userContext.user, postData, forum, topic).then((res) => {
      if (res.redirected) window.location.href = res.url;
    });
  };

  return (
    <>
      <Stack
        direction="row"
        alignItems="center"
        justifyContent="center"
        sx={{ width: "75%", pt: 2, pb: 2 }}
        spacing={2}
      >
        <Typography sx={{ fontSize: 72, fontWeight: 900 }} color="green">
          1.
        </Typography>
        <Paper
          sx={{
            borderRadius: 5,
            background: "#f3c0ef",
            height: "100px",
            p: 2,
            width: "100%",
          }}
          elevation={0}
        >
          <Typography sx={{ fontStyle: "italic", fontSize: 18 }}>
            Attach a screenshot or the text of the question or text from the
            guided Learning materials
          </Typography>
        </Paper>
        <Typography sx={{ fontSize: 72, fontWeight: 900 }} color="green">
          2.
        </Typography>
        <Paper
          sx={{
            borderRadius: 5,
            background: "#f3c0ef",
            height: "100px",
            p: 2,
            width: "100%",
          }}
          elevation={0}
        >
          <Typography sx={{ fontStyle: "italic", fontSize: 18 }}>
            Add the explanation, or what you think the answer is.
          </Typography>
        </Paper>
      </Stack>
      {topicName ? null : (
        <>
          <Typography
            sx={{ fontSize: 18, fontWeight: 700 }}
            color="text.secondary"
          >
            Post Title (required)
          </Typography>
          <TextField
            inputProps={{ maxLength: 80 }}
            InputProps={{
              sx: { borderRadius: 10, background: "white" },
            }}
            placeholder="Forum post title, make it descriptive!"
            fullWidth
            onChange={(e) => setTitle(e.target.value)}
          ></TextField>
        </>
      )}
      <Typography sx={{ fontSize: 18, fontWeight: 700 }} color="text.secondary">
        Write your question :
      </Typography>
      <Editor
        tinymceScriptSrc="%PUBLIC_URL%/tinymce.min.js"
        init={{
          convert_urls: false,
          promotion: false,
          menubar: false,
          file_picker_types: "image",
          images_upload_handler: async (blobInfo) => {
            return new Promise((resolve, reject) => {
              uploadForumImage(userContext.user, null, null, blobInfo.blob())
                .then((res) => res.text())
                .then((data) => {
                  resolve(data);
                })
                .catch((e) => {
                  reject(e);
                });
            });
          },
          plugins: ["charmap", "lists", "media", "link", "fullscreen", "image"],
        }}
        toolbar="formatselect bold italic bullist numlist blockquote alignleft aligncenter alignright link unlink image"
        initialValue={initialValue}
        value={value}
        onEditorChange={(newValue, editor) => setValue(newValue)}
      />
      <Button onClick={() => submitPostOrTopic()} disabled={submitClicked}>
        {submitButtonText()}
        {submitClicked && (
          <CircularProgress
            size={24}
            sx={{
              //color: green[500],
              position: "absolute",
              top: "50%",
              left: "50%",
              marginTop: "-12px",
              marginLeft: "-12px",
            }}
          />
        )}
      </Button>
    </>
  );
}
