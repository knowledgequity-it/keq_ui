import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from "@mui/material";
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";

export default function TandCDialog() {
  const LOCALSTORAGE_KEY = "AcceptTCs";
  const TC_URL =
    "http://test.knowledgequity.com.au/wp-content/uploads/2022/01/Ask-The-Expert-Terms-Conditions.pdf";

  const [open, setOpen] = useState(true);

  useEffect(() => {
    let accept = localStorage.getItem(LOCALSTORAGE_KEY);
    if (accept === "true") setOpen(false);
  }, []);

  const acceptConditions = () => {
    localStorage.setItem(LOCALSTORAGE_KEY, JSON.stringify(true));
    setOpen(false);
  };

  return (
    <Dialog open={open}>
      <DialogTitle id="alert-dialog-title">
        {"Ask the Expert Terms and conditions"}
      </DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          By using the Ask the Expert forums, you are agreeing to the{" "}
          <Link href={TC_URL} target="_blank">
            Terms and Conditions
          </Link>
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={acceptConditions}>Accept</Button>
      </DialogActions>
    </Dialog>
  );
}
