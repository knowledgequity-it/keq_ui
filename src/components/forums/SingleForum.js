import { Typography, Grid, Paper, Avatar, Link } from "@mui/material";
import ForumSearchWorkflow from "./ForumSearchWorkflow";
import { useLocation, useNavigate, useParams } from 'react-router-dom';
import { useContext, useEffect, useState } from "react";
import { getForumTopics } from "../../facades/ForumsFacade";
import { UserContext } from "../../contexts/userContext";
import ForumMainTitle from "./ForumMainTitle";
import { Box, Stack } from "@mui/system";


export default function SingleForum()
{
    const userContext = useContext(UserContext);
    const [topics, setTopics] = useState([]);
    const [forumId, setForumID] = useState("");
    const [forumNiceName, setForumNiceName] = useState("");
    const [courseCode, setCourseCode] = useState(null);
    const [courseLink, setCourseLink] = useState(null);
    const [sec, setSec] = useState();
    const [htmlSec, setHtmlSec] = useState()
    const params = useParams();
    const navigate = useNavigate();


  

    const getModuleString = () => {
        const paramValues = Object.values(params)
        let forumName = paramValues[0]
        const regex = /module-0[1-9]\d?/; // Regular expression to match the pattern "Module 0X" where X is a number
        const match = regex.exec(forumName); // Execute the regular expression on the input string
        if (match) {
            
            let found = match[0];
            found = found.charAt(0).toUpperCase() + found.slice(1);  //capitalise M in module
            found = found.replace("-", " ")
            return found; // Return the matched substring
        } else {
            return null; // Return null if no match found
        }
        
    }


    const getCourseCodeFromName = () => {

        //console.log("PROCESSING CC", forumNiceName,forumNiceName.split("–")[0].trim() );
        
        //need this because Australia Taxation and Australia Taxation Advanced get confused with the - split 
        if(forumNiceName.includes("Australia Taxation – Advanced"))
            return 'ATA';

        switch (forumNiceName.split("–")[0].trim()){
            case 'Australia Taxation':
                return 'AT';
            case 'Ethics & Governance':
                return 'EG';
            case 'Financial Reporting':
                return 'FR';
            case 'Financial Risk Management':
                return 'FRM';
            case 'Contemporary Business Issues':
                return 'CBI';
            case 'Global Strategy & Leadership':
                return 'GSL';
            case 'Advanced Audit & Assurance':
                return 'AAA';
            case 'Strategic Management Accounting':
                return 'SMA';
            case 'Digital Finance':
                return 'DF';
            case 'Digital Finance Digital Certificate':
                return 'DFDC';
            case 'Financial Accounting and Reporting':
                return 'FAR';    
            default:
                return null;     
        }
    }


    useEffect(() => {

        const paramValues = Object.values(params)

        getForumTopics(userContext.user, paramValues[0] )
        .then(res => res.json())
        .then(res => {
            setTopics(res.topics);
            setForumNiceName(res.forum_name);
            setSec(res.sec);
            setHtmlSec(res.html_rep)
            setForumID(res.forum_id)
	    setCourseLink(res.course_link)
        })
    }, [])

    useEffect(() => {
        if(forumNiceName !== "")
            setCourseCode(getCourseCodeFromName());
    }, [forumNiceName])



    const goToTopicPage = (topicName) => {
        const paramValues = Object.values(params);
        navigate(`${process.env.REACT_APP_AB_URL_PREFIX}/ate/${paramValues[0]}/${topicName}`);
    }

    return <Grid container alignItems="center" justifyContent="center">
            <Grid item xs={12} sm={7}>
                <Grid container direction="column" spacing={2}  alignItems="stretch" justifyContent="center" >
                    <Grid item xs={6} >
                        <Grid container direction="row" alignItems="center" justifyContent="flex-start" spacing={2}>
                            <Grid item>
                                <Typography  sx={{fontWeight:700, fontSize:32}} color="text.secondary"> {forumNiceName}</Typography>  
                            </Grid>
                            <Grid item>
    <Link href={`${process.env.REACT_APP_AB_URL_PREFIX}/course/${courseLink}`}>
                                            Back to course >>
                                </Link>
                            </Grid>
                        </Grid>
                        <Typography  sx={{fontWeight:700, fontSize:24}} color="text.secondary"> Ask the Expert Forums</Typography>  
                        <Typography sx={{fontWeight:500, fontSize:22}} color="text.secondary">You can ask our experts to help with any difficulty that you're having.</Typography>
                        <Typography sx={{fontWeight:500, fontSize:22}} color="text.secondary">Make sure to use the search function to see if your question has been asked before.</Typography>
                    </Grid>
                    <Grid item>
                        <ForumMainTitle subTitle="Search for your query" forumId={forumId}></ForumMainTitle>
                    </Grid>
                    <Grid item sx={{m:2}} sm={12}>
                        <ForumSearchWorkflow moduleNumber={getModuleString()} subjectCode={courseCode} forumid={forumId} htmlSec={htmlSec} sec={sec}></ForumSearchWorkflow>
                    </Grid>
                
                    {
                        topics.map((topic, idx) => {
                            return <Grid item xs={12} sm={9} key={idx}>
                                <Paper key={idx} onClick={()=>goToTopicPage(topic.name)} sx={{borderRadius:5, p:2, m:1}}>
                                <Grid container direction="column">
                                    <Grid item>
                                        <Grid container direction="row" spacing={2}>
                                            <Grid item sx={{width: "50px"}}>
                                                <Avatar sx={{ bgcolor: 'grey' }} aria-label="author" alt={topic.author_name} src={topic.avatar}></Avatar>
                                                
                                            </Grid>
                                            <Grid item>
                                            </Grid>
                                            <Grid item>
                                                <Typography sx={{textSize : 18, fontWeight:600}}>{topic.title}</Typography>
                                                <Typography sx={{textSize : 18, fontWeight:600, color:"green"}} >{topic.replies == 0 ? "" : topic.replies} {topic.replies == 0 ? "" : "replies"}</Typography>
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                    <Grid item>
                                        <Typography sx={{fontSize : 12}}>{topic.author_name ==""? "":"Posted by "} {topic.author_name}</Typography>
                                    </Grid>
                                </Grid>
                                
                        
                                
                            </Paper>
                            </Grid>
                        })
                    }
                
                </Grid>
            </Grid>
    </Grid>
    
}
