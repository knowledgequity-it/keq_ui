import { Grid, Typography } from "@mui/material";

export default function ForumFAQ()
{

    return <Grid container direction="column" alignContent="center" sx={{width:"50%"}} spacing={2}>
        <Grid item xs={12} sm={9}>
            <Typography>GENERAL - FAQ's How do I access the PDF study guide?</Typography>
            <Typography>Log into My Online Learning, select the Ethics and Governance subject, scroll down to the Primary Resources section, and click on the ‘Access’ button next to ‘Study Guide PDF’. </Typography>
        </Grid>
        <Grid item>
            <Typography>Which edition of references should I refer to (such as APES 110, APES GN40, OECD Principles)?</Typography>
            <Typography>The study guide uses key guidance document such as the Code of Ethics (APES110) and the OECD Principles and ASX Principles. Sometimes a new edition of these items is published after the study guide has been published. Always use the edition referred to in the study guide, as this is what your whole exam is based on.</Typography>
        </Grid>
        <Grid item>
            <Typography>When should we start attempting Business simulation in My Online Learning?</Typography>
            <Typography>Once you have completed the whole study guide, because it refers to concepts that are across a range of modules, and may be confusing if you haven’t finished the study guide. For example, it starts with a discussion on governance checks, which is only covered in Module 3. Treat this as an exam revision tool.</Typography>
        </Grid>
        <Grid item>
            <Typography>Do I need to read and/or print the relevant legislation? </Typography>
            <Typography>It is recommended that all candidates become familiar with how to locate and read the relevant sections of legislation referred to in the study guide. However, note that only the content in the study guide is examinable. It is a personal preference as to whether candidates print any legislation for the purposes of the exam, but it is not necessary.</Typography>
        </Grid>
        <Grid item>
            <Typography>Will the content of the readings, or MYOL case studies and simulations be directly examinable?</Typography>
            <Typography>No, they are not directly examinable. You should still work through this content because they are designed to help you apply the study guide concepts, so they are useful exam revision.</Typography>
        </Grid>
        <Grid item>
            <Typography>When are the mid-semester test (MST) and practice exams released?</Typography>
            <Typography></Typography>
        </Grid>

    </Grid>
}