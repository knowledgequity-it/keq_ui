import React from "react";
import Paper from "@mui/material/Paper";
// #mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; }
//#mc_embed_signup label{padding-left: 10px}
//
export default function MailchimpSignupForm(props) {
  const iframe =
    '<iframe scrolling="no" height="900px" width="800px" frameborder="no" src="subscriptions.html"></iframe>';
  return (
    <Paper>
      <div dangerouslySetInnerHTML={{ __html: iframe }}></div>
    </Paper>
  );
}
