import { useTheme } from "@mui/material/styles";
import Card from "@mui/material/Card";
import CardActionArea from "@mui/material/CardActionArea";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import { useNavigate } from "react-router-dom";
import LinearProgress from "@mui/material/LinearProgress";
import useLiveLogging from "../hooks/useLiveLogging";
import { useState, useEffect, useContext } from "react";
import { getUserCourseSummary } from "../facades/UserFacade";
import { UserContext } from "../contexts/userContext";
import { CircularProgress } from "@mui/material";
import Grid from "@mui/material/Grid";

// const BorderLinearProgress = withStyles((theme) => ({
//   root: {
//     height: 12,
//     borderRadius: 3,
//   },
//   colorPrimary: {
//     backgroundColor:
//       theme.palette.grey[theme.palette.type === "light" ? 200 : 700],
//   },
//   bar: {
//     borderRadius: 5,
//     backgroundColor: "#8A87E7", //theme.palette.secondary,
//   },
// }))(LinearProgress);

export default function CourseCard({ course_id }) {
  const navigate = useNavigate();
  const userContext = useContext(UserContext);
  const candidateAction = useLiveLogging();
  const [completion, setCompletion] = useState(false);
  const [course, setCourse] = useState("");
  const [course_url, setCourseUrl] = useState("");
  const [course_img, setCourseImg] = useState("");

  const [isError, setIsError] = useState(false);

  function handleClick(e, course_url) {
    candidateAction({
      component: "Course Card",
      event: "Course Clicked: " + course_url,
      page: "Dashboard",
    });

    navigate(`${process.env.REACT_APP_AB_URL_PREFIX}/course/${course_url}`);
  }
  useEffect(() => {
    if (!course_id) return;

    getUserCourseSummary(userContext.user, course_id)
      .then((response) => {
        if (response.ok) {
          return response.json();
        }
        throw new Error("error retrieving course summary");
      })
      .then((result) => {
        //console.log("result.progress", result.progress);
        setCompletion(result.progress == "" ? 0 : result.progress);
        setCourseUrl(result.url_name);
        setCourseImg(result.image);
        setCourse(result.title);
      })
      .catch((err) => {
        setIsError(true);
      });
  }, [course_id]);

  let the_title = <CircularProgress />;
  let the_progress = <CircularProgress />;

  if (isError) return null;

  if (course)
    the_title = (
      <Typography
        sx={{ height: "50px", paddingLeft: "10px" }}
        gutterBottom
        variant="h5"
        component="h2"
      >
        {course}
      </Typography>
    );

  if (completion >= 0)
    the_progress = (
      <Typography
        sx={{ height: "20px" }}
        variant="subtitle1"
        color="textSecondary"
        component="p"
      >
        Completed : {completion} %
      </Typography>
    );

  return (
    <Card
      sx={{
        width: "100%",

        border: "1px solid lightgrey",
      }}
    >
      <CardActionArea onClick={(e) => handleClick(e, course_url)}>
        {the_title}
        <CardMedia
          component="img"
          sx={{ height: 140 }}
          src={course_img}
          title="Course"
        />
        <CardContent>
          {the_progress}

          <br></br>
          <LinearProgress
            variant="determinate"
            thickness={4}
            value={parseInt(completion)}
          />
        </CardContent>
      </CardActionArea>
    </Card>
  );
}
