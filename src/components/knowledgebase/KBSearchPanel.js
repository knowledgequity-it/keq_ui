import { Box, Button, Card, CardActions, CardContent, CardHeader, Grid, TextField, Typography } from "@mui/material";
import { Stack } from "@mui/system";
import { useContext, useEffect, useState } from "react";
import { searchKnowledgebase } from "../../facades/UserFacade";
import { UserContext } from "../../contexts/userContext";
import ArrowCircleRightOutlinedIcon from '@mui/icons-material/ArrowCircleRightOutlined';
import { Link, useNavigate, useNavigation } from "react-router-dom";



const CARD_WIDTH=250;

export default function KBSearchPanel({forumId})
{

    const [searchTerm, setSearchTerm] = useState("");
    const [searchResults, setSearchResults] = useState({});
    const [theForumId, setForumId] = useState(null);
    const [offset, setOffset] = useState(0);
    const userContext = useContext(UserContext);
    const navigate = useNavigate();

    useEffect(() => {
        console.log("setting FID", forumId)
        if(forumId)
            setForumId(forumId)
    }, [forumId])


    const searchKB = (clearResults, fid) => {

        searchKnowledgebase(userContext.user, searchTerm, clearResults ? 0 : offset, fid)
        .then(res => res.json())
        .then(results => {
            if(!clearResults)
            {
                results = 
                setSearchResults( { 
                    ...searchResults,
                    posts : [...searchResults.posts, ...results.posts],
                    topics : [...searchResults.topics, ...results.topics],
                    units : [...searchResults.units, ...results.units]
                })
            }
            else
                setSearchResults(results);

            setOffset(offset+10)
        });
    }

  return (
      <>
    <Stack>
            <Typography sx={{pt:2, pl:2, fontSize:22, fontWeight:600}} color="text.secondary"></Typography>
            <Grid container direction="row">
                <Grid item xs={7}>
                    <TextField value={searchTerm} onChange={(e) => setSearchTerm(e.target.value)} onKeyUp={(e)=>{ if(e.key==="Enter"){ searchKB(true, theForumId)} }} InputProps={{ sx: { borderRadius: 10, background:"white" } }}
                    sx={{background: "#e7f9d5", width:"100%", height:"100%", }}></TextField>
                </Grid>
                <Grid item xs={3}>
                    <Button onClick={() => {searchKB(true, theForumId)}}  variant="outlined" sx={{background:"white", height:"100%", width:200, ml:2, borderRadius:10}}>Search</Button>
                </Grid>
            </Grid>
        
    <Grid container direction="row" alignItems="flex-start" justifyContent="center" spacing={5}>
        <Grid item xs={6}>
            <Stack direction="column" spacing={1}>
                    { searchResults.topics && <Typography variant="h5"> Forum Results</Typography>}
                    {
                        searchResults.topics && 
                        searchResults.topics.map(topic => {
                            return <Card sx={{width : CARD_WIDTH}} key={topic.url_name}>
                                <CardContent>
                                    <Typography variant="h5">{topic.title}</Typography>
                                    
                                    <Typography variant="body2">{topic.content.slice(0, topic.content.length - 8)+"..."}</Typography>
                                    
                                </CardContent>
                                <CardActions sx={{
                                            alignSelf: "stretch",
                                            display: "flex",
                                            justifyContent: "flex-end",
                                            alignItems: "flex-start",
                                            // 👇 Edit padding to further adjust position
                                            p: 0,
                                            }}>
                                    <Link to={`${process.env.REACT_APP_AB_URL_PREFIX}/ate/${topic.forum}/${topic.url_name}`} target="_blank" rel="noopener noreferrer" underline="none">
                                    <Button >open
                                        
                                    <ArrowCircleRightOutlinedIcon/>
                                    </Button>
                                    </Link>
                                </CardActions>
                            </Card>
                        })
                    }
                </Stack>
        </Grid>
        <Grid item xs={6}>
            <Stack direction="column" spacing={1}>
                    {searchResults.units &&  <Typography variant="h5"> Units</Typography> }
                    {
                        searchResults.units && 
                        searchResults.units.map(unit => {
                            return <Card sx={{width : CARD_WIDTH}} key={unit.url_name}>
                                <CardContent>
                                    <Typography variant="h5">{unit.title}</Typography>
                                    <Typography variant="body2">{unit.content.slice(0, unit.content.length - 8)+"..."}</Typography>
                                    
                                </CardContent>
                                <CardActions sx={{
                                            alignSelf: "stretch",
                                            display: "flex",
                                            justifyContent: "flex-end",
                                            alignItems: "flex-start",
                                            // 👇 Edit padding to further adjust position
                                            p: 0,
                                            }}>
                                    <Link to={`${process.env.REACT_APP_AB_URL_PREFIX}/course/${unit.course}/${unit.url_name}`} target="_blank" rel="noopener noreferrer">
                                    <Button>open
                                    <ArrowCircleRightOutlinedIcon/>
                                    </Button>
                                    </Link>
                                </CardActions>
                            </Card>
                        })
                    }
                </Stack>
        </Grid>
        <Grid item>
            
        </Grid>
        
    </Grid>
      </Stack>
      <Stack>
        <Button onClick={()=>searchKB(false, theForumId)}>Get more results...</Button>
      </Stack>
      </>
  )
}
