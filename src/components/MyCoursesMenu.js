import React, { useContext, useEffect, useState, useRef } from "react";
import { useTheme } from "@mui/material/styles";
import { UserContext } from "../contexts/userContext";
import { getUser, getUserCourseSummary } from "../facades/UserFacade";
import MenuItem from "@mui/material/MenuItem";
import Menu from "@mui/material/Menu";
import useLiveLogging from "../hooks/useLiveLogging";
import { useNavigate } from "react-router-dom";
import { CircularProgress } from "@mui/material";
import { ConstructionOutlined } from "@mui/icons-material";

export default function MyCoursesMenu({ anchorEl, setAnchorEl }) {
  const theme = useTheme();
  const userContext = useContext(UserContext);
  const [userCourseIds, setUserCoursesIds] = useState([]);
  const [userCourses, setUserCourses] = useState([]);
  const candidateAction = useLiveLogging();
  const navigate = useNavigate();

  useEffect(() => {
    candidateAction({
      component: "My Courses Menu",
      event: "initialising My Courses Menu for user",
    });
/*
    getUser(userContext.user)
      .then((response) => response.json())
      .then((userDetails) => {
        setUserCoursesIds(userDetails.courses);
      })
      .catch((error) => {
        candidateAction({
          component: "My Courses Menu",
          event: "Error loading user details for My Courses Menu",
          error: error,
        });
      });
      */

      
  }, []);
/*** 
  useEffect(() => {
    let courseDeets = [];
    userCourseIds.forEach((course) => {
      courseDeets.push(getUserCourseSummary(userContext.user, course));
    });

    Promise.all(courseDeets) //api call promise
      .then((results) => {
        return Promise.all(results);
      }) //resolve api calls
      .then((r) => {
        return Promise.all(r.map((x) => x));
      }) //resolve json content stream
      .then((c) => {
        // finally our data...
        setUserCourses(c);

        candidateAction({
          component: "My Courses Menu",
          event: "My Courses Menu initialized for user with courses",
        });
      })
      .catch((error) => {
        candidateAction({
          component: "My Courses Menu",
          event: "Failed to obtain content to initialize My Courses Menu",
          error: error,
        });
      });
  }, [userCourseIds]);
*///
  function handleClick(e, course) {
    setAnchorEl(null);
    candidateAction({
      component: "My Courses Menu",
      event: "Course Clicked: " + course.course_url,
    });

    if (course.id == 372381 || course.id == 372523 || course.id == 85853) {
      window.location.assign(
        `${process.env.REACT_APP_BASE_URL}/classic/course/${course.course_url}`,
      );
    } else {
      navigate(
        `${process.env.REACT_APP_AB_URL_PREFIX}/course/${course.course_url}/${course.next_unit}`,
      );
    }
  }

  let the_menu_items = <CircularProgress sx={{ p: 2 }} />;

  if (userContext.userCourses.length != 0) //(userCourses && userCourses.length !== 0)
    the_menu_items = userContext.userCourses.map(function (course, index) {
      return (
        <MenuItem key={index} onClick={(event) => handleClick(event, course)}>
          {course.title}
        </MenuItem>
      );
    });

  return (
    <Menu
      id="menu-mycourses"
      //getContentAnchorEl={null}
      anchorEl={anchorEl}
      keepMounted
      anchorOrigin={{
        vertical: "bottom",
        horizontal: "center",
      }}
      transformOrigin={{
        vertical: "top",
        horizontal: "center",
      }}
      open={Boolean(anchorEl)}
      onClose={() => {
        setAnchorEl(null);
      }}
    >
      {the_menu_items}
    </Menu>
  );
}
