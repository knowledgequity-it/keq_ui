import React, { useContext, useEffect, useState } from "react";
import { useTheme } from "@mui/material/styles";
import Card from "@mui/material/Card";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import {
  Avatar,
  CardHeader,
  CardMedia,
  CardContent,
  Grid,
} from "@mui/material";
import CourseTreeView from "../components/CourseTreeView";
import LinearProgressWithLabel from "../components/LinearProgressWithLabel";
import Box from "@mui/material/Box";
import { useNavigate } from "react-router-dom";
import useLiveLogging from "../hooks/useLiveLogging";
import { completeUserCourse } from "../facades/UserFacade";
import { UserContext } from "../contexts/userContext";
import DoneOutlineIcon from "@mui/icons-material/DoneOutline";

function BookExamsButton({ action, the_class }) {
  return (
    <Button
      variant="contained"
      className={the_class}
      disableElevation
      onClick={(e) => action(e)}
    >
      Book my practice exams
    </Button>
  );
}

export default function CourseSidebar({
  course,
  userCourseDetails,
  selected_unit,
  setUnitUrl,
  course_url,
  hide_KB,
  hide_ATE,
  onEnrolClicked,
}) {
  const theme = useTheme();
  const navigate = useNavigate();
  const candidateAction = useLiveLogging();

  const [_course, setCourse] = useState(null);
  const userContext = useContext(UserContext);

  var completion = 0;

  function handleClick(e, course_url) {
    candidateAction({
      component: "Course Card",
      event: "Course Clicked: course/" + course_url,
      page: "Dashboard",
    });

    navigate(`${process.env.REACT_APP_AB_URL_PREFIX}/course/${course_url}`);
    setUnitUrl();
  }

  function handleClassicSiteClick(e) {
    candidateAction({
      component: "Course Sidebar",
      event: "Back to Classic Site clicked",
      page: "Course",
    });

    window.location.replace(
      `${process.env.REACT_APP_BASE_URL}/classic/course/${course_url}`,
    );
  }

  const handleBookExamClick = (e) => {
    candidateAction({
      component: "Course Sidebar",
      event: "Book Exam button clicked",
      page: "Course",
    });

    window.location.replace(
      `${process.env.REACT_APP_BASE_URL}/practice-exam-booking-form/`,
    );
  };

  const handleEnrolClick = (e) => {
    candidateAction({
      component: "Course Sidebar",
      event: "Enrol in Course button clicked",
      page: "Course",
    });

    onEnrolClicked();
  };

  if (userCourseDetails?.length > 0) {
    //filter duplicate unit ids before calculating completion
    //since we sometimes include a quiz in the curriculum in multiple locations
    const ids = userCourseDetails.map(({ id }) => id);
    const filtered = userCourseDetails.filter(
      ({ id }, index) => !ids.includes(id, index + 1),
    );
    filtered.forEach((cd) => {
      if (cd.completed) {
        completion += 1;
      }
    });

    completion = (completion / userCourseDetails.length) * 100;
  } else {
    completion = 0;
  }

  const checkCourseCompleted = () => {
    const notCompleted = (element) => !element.completed;

    console.log("CHECKING", userCourseDetails.some(notCompleted));
    return userCourseDetails.some(notCompleted);
  };

  const completeCourse = (e) => {
    console.log("Completing course");
    completeUserCourse(userContext.user, course.id).then((res) => {
      console.log(res);
      window.location.replace(
        `${process.env.REACT_APP_BASE_URL}/info/my-certificates#${course.id}`,
      );
    });
  };
  let rounded = +(Math.round(completion + "e+2") + "e-2");
  return (
    <>
      <Typography component="div">
        <Typography variant="h5" component="h2" gutterBottom>
          {course.title}
        </Typography>
      </Typography>
      {userCourseDetails && (
        <Box sx={{ width: "100%", mb: 2 }}>
          <LinearProgressWithLabel
            variant="determinate"
            thickness={4}
            value={completion}
          />
        </Box>
      )}
      {!hide_KB && userCourseDetails && (
        <Button
          variant="contained"
          color="continue"
          sx={{
            width: "100%",
            marginBottom: theme.spacing(2),
            borderRadius: 5,
          }}
          disableElevation
          href="/knowledgebase"
        >
          Search Knowledge Base
        </Button>
      )}
      {!hide_ATE && userCourseDetails && (
        <Button
          variant="contained"
          color="continue"
          sx={{
            width: "100%",
            marginBottom: theme.spacing(2),
            borderRadius: 5,
          }}
          disableElevation
          href="/ate"
        >
          Ask The Expert
        </Button>
      )}
      {course_url == "ethics-governance" | course_url == "australia-taxation-advanced" 
      &&
      (
        <Button
          variant="contained"
          color="continue"
          sx={{
            width: "100%",
            marginBottom: theme.spacing(2),
            borderRadius: 5,
          }}
          disableElevation
          href={`/kaila/${course_url}`}
          target="_blank"
        >
          Talk With Kaila AI
        </Button>
      )}

      {userCourseDetails && (
        <>
          <Card sx={{ width: "100%", mb: 3 }} elevation={0}>
            <CardContent>
              <CourseTreeView
                course={course}
                course_url={course_url}
                userCourseDetails={userCourseDetails}
                selected_unit={selected_unit}
                setUnitUrl={setUnitUrl}
              ></CourseTreeView>
            </CardContent>
          </Card>

          <Button
            variant="contained"
            color="continue"
            sx={{
              width: "100%",
              marginBottom: theme.spacing(2),
              borderRadius: 5,
            }}
            disableElevation
            onClick={(e) => handleClick(e, course_url)}
          >
            Back To Course
          </Button>
        </>
      )}
      {userCourseDetails &&
        course.can_enrol_from_link &&
        !checkCourseCompleted() && (
          <Button
            onClick={(e) => completeCourse()}
            variant="contained"
            color="continue"
            startIcon={<DoneOutlineIcon />}
            sx={{
              width: "100%",
              marginBottom: theme.spacing(2),
              borderRadius: 5,
            }}
            disableElevation
          >
            Complete Course
          </Button>
        )}
      {!userCourseDetails && course.can_enrol_from_link && (
        <Button
          variant="contained"
          color="continue"
          sx={{
            width: "100%",
            marginTop: theme.spacing(2),
            marginBottom: theme.spacing(2),
            borderRadius: 5,
          }}
          disableElevation
          onClick={(e) => handleEnrolClick(e)}
        >
          Enrol in Course
        </Button>
      )}
      {!userCourseDetails && !course.can_enrol_from_link && (
        <Typography>Private course</Typography>
      )}
    </>
  );
}
