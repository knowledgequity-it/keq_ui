import React, { useContext, useEffect, useState } from "react";
import { useTheme } from "@mui/material/styles";

import { Interweave, Node } from "interweave";

import { UserContext } from "../contexts/userContext";
import Box from "@mui/material/Box";
import Card from "@mui/material/Card";
import Container from "@mui/material/Container";
import { getUnit } from "../facades/UnitFacade";
import { CardContent, CardHeader } from "@mui/material";
import Timeline from "@mui/lab/Timeline";
import TimelineSeparator from "@mui/lab/TimelineSeparator";
import TimelineDot from "@mui/lab/TimelineDot";
import TimelineContent from "@mui/lab/TimelineContent";
//import { TimelineItem as KeTimelineItem } from "@mui/lab/";
import TimelineOppositeContent from "@mui/lab/TimelineOppositeContent";
import VideocamIcon from "@mui/icons-material/Videocam";
import MenuBookIcon from "@mui/icons-material/MenuBook";
import TimelineConnector from "@mui/lab/TimelineConnector";
import Grid from "@mui/material/Grid";
import useLiveLogging from "../hooks/useLiveLogging";
import Skeleton from "@mui/lab/Skeleton";
import useMediaQuery from "@mui/material/useMediaQuery";
import Avatar from "@mui/material/Avatar";
import Link from "@mui/material/Link";
import parse from "html-react-parser";
import ValueChain from "./custom-units/ValueChain";
import Crashing from "./custom-units/Crashing";

// const TimelineItem = withStyles({
//   missingOppositeContent: {
//     "&:before": {
//       display: "none",
//     },
//     paddingRight: "0px",
//   },
// })(KeTimelineItem);

// const useStyles = makeStyles((theme) => ({
//   missingOppositeContent: {
//     paddingRight: "0px",
//   },
//   tlHeader: {},
//   unit_step_card: {
//     border: "1px solid lightgrey",
//     marginBottom: 10,
//   },
//   sectionText: {
//     paddingRight: 10,
//   },
//   unit_step_card_content: {
//     paddingTop: 0,
//   },
//   unit_step_card_header: {
//     paddingBottom: 0,
//   },
// }));

const KeTimeline = (props) => {
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up("sm"));
  const candidateAction = useLiveLogging();

  const isVideoSection = (section) => {
    return section.getElementsByTagName("iframe").length !== 0;
  };

  const getVideoIframe = (section) => {
    return isVideoSection(section)
      ? section.getElementsByTagName("iframe")[0]
      : null;
  };

  const removeSectionHeader = (section) => {
    let newSection = section.cloneNode(true);
    let head = getSectionHeader(newSection);
    if (head !== null) head.remove();

    return newSection;
  };

  const removeVideoIframe = (section) => {
    let newSection = section.cloneNode(true);
    let iframe = getVideoIframe(newSection);
    if (iframe !== null) iframe.remove();

    return newSection;
  };

  const getSectionHeader = (section) => {
    let headers = section.getElementsByTagName("h2");
    if (headers) {
      if (
        headers[0] != null &&
        headers[0].innerHTML.toLowerCase().includes("step")
      )
        return headers[0];
      else return null;
    } else return null;
  };

  const processMiniQuizHref = (section) => {
    let newSection = section.cloneNode(true);
    let anchor = [...newSection.getElementsByTagName("a")];
    if (anchor.length != 0) {
      anchor.forEach((atag) => {
        if (
          atag != null &&
          atag.getAttribute("href").includes("knowledgequity.com.au/quiz")
        ) {
          let newHref = atag
            .getAttribute("href")
            .replace(
              "knowledgequity.com.au/quiz",
              `knowledgequity.com.au${process.env.REACT_APP_AB_MINIQUIZ_PREFIX}/quiz`,
            );
          if (newHref.substr(-1) === "/") {
            newHref = newHref.substring(0, newHref.length - 1);
          }

          newHref = newHref.concat("?ru=" + props.unit_url);
          atag.setAttribute("href", newHref);
        }
      });
      return newSection;
      /*
      if(anchor[0] !== null && anchor[0].getAttribute('href').includes("knowledgequity.com.au/quiz"))
      {
        let newHref = anchor[0].getAttribute('href').replace("knowledgequity.com.au/quiz", `knowledgequity.com.au${process.env.REACT_APP_AB_URL_PREFIX}/quiz`)
        anchor[0].setAttribute("href", newHref)
        return newSection;
      }*/
    }
    return section;
  };

  const sectionIsEmpty = (section) => {
    return !section.innerHTML.trim() ? true : false;
  };

  const transform = (node, children) => {
    if (node.tagName === "A") {
      return (
        <Link
          color="secondary"
          target={node.getAttribute("target")}
          rel={node.getAttribute("rel")}
          onClick={() => {
            candidateAction({
              component: "Unit: " + props.selected_unit,
              event: "Link Clicked: " + node.getAttribute("href"),
            });
            console.log("clicky clicky");
          }}
          href={node.getAttribute("href")}
        >
          {children}
        </Link>
      );
    }
  };

  //console.log(typeof html);
  if (typeof props.html !== "string") {
    return (
      <React.Fragment>
        <Skeleton variant="text" height={40} />
        <Grid container spacing={3}>
          <Grid item>
            <Grid container spacing={3}>
              <Grid item x={12}>
                <Skeleton variant="circle" width={40} height={40} />
              </Grid>
              <Grid item xs={5}>
                <Skeleton variant="text" />
                <Skeleton variant="text" />
                <Skeleton variant="text" />
              </Grid>
              <Grid item xs={5}>
                <Skeleton variant="rect" height={100} />
              </Grid>
            </Grid>
          </Grid>
          <Grid item xs={12}>
            <Grid container spacing={3}>
              <Grid item x={12}>
                <Skeleton variant="circle" width={40} height={40} />
              </Grid>
              <Grid item xs={5}>
                <Skeleton variant="text" />
                <Skeleton variant="text" />
                <Skeleton variant="text" />
              </Grid>
              <Grid item xs={5}>
                <Skeleton variant="rect" height={100} />
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        <Skeleton variant="text" />
        <Skeleton variant="text" />
        <Skeleton variant="text" />
      </React.Fragment>
    );
  }

  const replaceValueChainePlaceholder = (domhandlerNode) => {
    if (domhandlerNode.type === "text" && domhandlerNode.data.includes("[[VALUE_CHAIN]]")) 
    {
      return <ValueChain />;
    }
    if (domhandlerNode.type === "text" && domhandlerNode.data.includes("[[CRASHING_EXERCISE]]")) 
        {
          return <Crashing />;
        }

  };
  const parser = new DOMParser();
  let unitDoc = parser.parseFromString(props.html, "text/html");
  let sections = unitDoc.querySelectorAll(
    "div.wpb_content_element > div.wpb_wrapper",
  );

  //find <p> tag in unitDoc with [[VALUE_CHAIN]] as the content
  let isCustomUnit = false;
  unitDoc.querySelectorAll("p").forEach((p) => {
    if (p.innerHTML.includes("[[VALUE_CHAIN]]")) {
        isCustomUnit = true;
    }
    if (p.innerHTML.includes("[[CRASHING_EXERCISE]]")) {
        isCustomUnit = true;
    }
  });

  const parse = require("html-react-parser").default;
  const final_html = parse(props.html, {
    replace: replaceValueChainePlaceholder,
  });

  //replace the content with the value chain component

  if (sections.length == 0) {
    return (
      <Card sx={{ mb: 3, borderRadius: 5 }}>
        <CardContent>
          {isCustomUnit ? (
            <div>{final_html}</div>
          ) : (
            <div dangerouslySetInnerHTML={{ __html: props.html }} />
          )}
        </CardContent>
      </Card>
    );
  }

  let intro = sections.length > 0 ? sections[0] : null;
  let outro = sections.length > 1 ? sections[sections.length - 1] : null;
  let x = Array.from(sections)
    //.slice(1, sections.length - 1)
    .map((section, index) => {
      let isVideo = isVideoSection(section);
      let header = getSectionHeader(section);

      if (
        section.getElementsByClassName("wpb_wrapper").length == 0 &&
        !sectionIsEmpty(section)
      )
        return (
          <Card sx={{ mb: 3, borderRadius: 5 }} key={index}>
            <CardHeader
              sx={{ paddingBottom: 0 }}
              avatar={
                <Avatar>{isVideo ? <VideocamIcon /> : <MenuBookIcon />}</Avatar>
              }
              title={
                header !== null ? (
                  <h2
                    dangerouslySetInnerHTML={{
                      __html: getSectionHeader(section).innerHTML,
                    }}
                  />
                ) : null
              }
            ></CardHeader>
            <CardContent sx={{ paddingTop: 0 }}>
              <Grid
                container
                alignItems="flex-start"
                justifyContent="center"
                direction="row"
              >
                <Grid
                  item
                  xs={12}
                  md={isVideo ? 6 : 12}
                  sx={{ paddingRight: 10 }}
                >
                  <Interweave
                    content={
                      removeSectionHeader(
                        removeVideoIframe(processMiniQuizHref(section)),
                      ).innerHTML
                    }
                    transform={transform}
                  />
                </Grid>
                {isVideo ? (
                  <Grid item xs={12} md={6} align="center">
                    {isVideo ? (
                      <div
                        dangerouslySetInnerHTML={{
                          __html: getVideoIframe(section).outerHTML,
                        }}
                      />
                    ) : null}
                  </Grid>
                ) : null}
              </Grid>
            </CardContent>
          </Card>
        );
    });
  return <React.Fragment>{x}</React.Fragment>;
};

export default React.memo(function Unit({ selected_unit, unit_url }) {
  const userContext = useContext(UserContext);
  const [current_unit, setCurrentUnit] = useState({});
  const candidateAction = useLiveLogging();

  useEffect(() => {
    let isMounted = true;
    getUnit(selected_unit, userContext.user.token)
      .then((result) => result.json())
      .then((unit) => {
        if (isMounted) {
          setCurrentUnit(unit.content);

          candidateAction({
            componenet: "Unit",
            event: "successfully loaded Unit" + unit.id,
          });
        }
      })
      .catch((error) => {
        candidateAction({
          componenet: "Unit",
          event: "Failed to load Unit" + selected_unit,
          error: error,
        });
      });
    return () => {
      isMounted = false;
    };
  }, [selected_unit, userContext.user.token, candidateAction]);

  return (
    <Box>
      <KeTimeline
        html={current_unit}
        unit_url={unit_url}
        selected_unit={selected_unit}
      />
    </Box>
  );
});
