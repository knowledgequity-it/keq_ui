import { Typography } from "@mui/material";
import { Checkbox } from "@mui/material";
import { FormControlLabel } from "@mui/material";
import { FormGroup } from "@mui/material";
import React, {useContext, useState} from "react";
import { UserContext } from "../contexts/userContext";


export default function FoMPreference(props) {

    const [showEmojis, setShowEmojis] = useState(); 
    const userContext = useContext(UserContext);


return <FormGroup row>
    <FormControlLabel
        control={
          <Checkbox
            checked={showEmojis}
            onChange={() => setShowEmojis(!showEmojis)}
            i
            name="show-emojis"
            color="primary"
          />
        }
        label="Show Emojis"
      />
    <Typography variant="overline">
        User emoji reponses are used to improve the learning experience
    </Typography>
</FormGroup>
}
