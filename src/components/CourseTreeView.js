import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { useTheme } from "@mui/material/styles";
import TreeView from "@mui/lab/TreeView";
import TreeItem from "@mui/lab/TreeItem";
import Typography from "@mui/material/Typography";
import MailIcon from "@mui/icons-material/Mail";
import CheckCircle from "@mui/icons-material/CheckCircle";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import MenuBook from "@mui/icons-material/MenuBook";
import Create from "@mui/icons-material/Create";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import { green, grey } from "@mui/material/colors";
import HighlightOffIcon from "@mui/icons-material/HighlightOff";
import Box from "@mui/material/Box";
import useLiveLogging from "../hooks/useLiveLogging";
import { useNavigate } from "react-router-dom";

//remove the first bit of unit names eg. AT M1 - 
const  removePrefix = (text) => {
  const pattern = /^[A-Z]{2,3} M[0-9]{1,2} &#8211; /;
  const result = text.replace(pattern, "");
  return result;
}

function StyledTreeItem(props) {
  const theme = useTheme();
  const {
    labelText,
    labelIcon: LabelIcon,
    labelInfo,
    color,
    bgColor,
    ...other
  } = props;

  //console.log(labelText)
  const blank = (
    <Box
      sx={{
        display: "flex",
        alignItems: "center",
        padding: theme.spacing(0.5, 0),
      }}
    >
      <Box
        sx={{
          fontWeight: "bold",
          fontSize: 14,
          color: "inherit",
        }}
      >
        <div dangerouslySetInnerHTML={{ __html: labelText }} />
      </Box>

      <Typography variant="caption" color="inherit">
        {labelInfo}
      </Typography>
    </Box>
  );

  let iconned = {};
  if (props.labelIcon != null)
    iconned = (
      <Box
        sx={{
          display: "flex",
          alignItems: "center",
          padding: theme.spacing(0.5, 0),
        }}
      >
        <LabelIcon
          sx={{
            fontSize: "15px",
            color: theme.palette.secondary,
            //    fontWeight: theme.typography.fontWeightMedium,
            marginRight: theme.spacing(1),
          }}
        />
        <Box sx={{ fontWeight: "normal", fontSize: 12, flexGrow: 1 }}>
          <div dangerouslySetInnerHTML={{ __html: removePrefix(labelText) }} />
        </Box>
        {props.usercomplete === "true" ? (
          <CheckCircle style={{ color: green[500] }} />
        ) : (
          <HighlightOffIcon style={{ color: grey[300] }} />
        )}
        <Typography variant="caption" color="inherit">
          {labelInfo}
        </Typography>
      </Box>
    );

  return (
    <TreeItem
      label={LabelIcon == null ? blank : iconned}
      style={{
        "--tree-view-color": color,
        "--tree-view-bg-color": bgColor,
      }}
      classes={{
        root: {
          color: theme.palette.text.primary,
          "&:hover > $content": {
            backgroundColor: theme.palette.action.hover,
          },
          "&:focus > $content, &$selected > $content": {
            backgroundColor: `var(--tree-view-bg-color, ${theme.palette.secondary})`,
            color: "var(--tree-view-color)",
          },
          "&:focus > $content $label, &:hover > $content $label, &$selected > $content $label":
            {
              backgroundColor: "transparent",
            },
        },
        content: {
          color: theme.palette.text.secondary,
          //borderTopRightRadius: theme.spacing(2),
          //borderBottomRightRadius: theme.spacing(2),
          paddingRight: theme.spacing(1),
          fontWeight: theme.typography.fontWeightMedium,
          "$expanded > &": {
            fontWeight: theme.typography.fontWeightRegular,
          },
        },
        expanded: {},
        selected: {},
        group: {
          marginLeft: 0,
          "& $content": {
            paddingLeft: theme.spacing(2),
          },
        },
        label: {
          fontWeight: "bold",
          fontSize: 14,
          color: "inherit",
        },
      }}
      {...other}
    />
  );
}

StyledTreeItem.propTypes = {
  bgColor: PropTypes.string,
  color: PropTypes.string,
  labelIcon: PropTypes.elementType,
  labelInfo: PropTypes.string,
  labelText: PropTypes.string.isRequired,
};

export default function CourseTreeView(props) {
  const theme = useTheme();
  const [treeData, setTreeData] = useState([]);
  const candidateAction = useLiveLogging();
  //const [currentCourse, setCurrentCourse] = useState(() => {return props.course});
  const navigate = useNavigate();
  const [userCourseDetails, setUserCourseDetails] = useState([]);
  const [selected, setSelected] = useState("");
  const [expanded, setExpanded] = useState([]);

  useEffect(() => {
    const processCourseItems = () => {
      var the_course = [];
      let subsection = [];
      let subsectionName = "";
      let subsectionIndex = -1;
      props.course.curriculum.forEach((item, index) => {
        if (item.type === "section" && subsectionName === "") {
          subsectionName = item.title;
          subsectionIndex += 1;
        } else if (item.type === "section" && subsectionName !== "") {
          the_course.push({ title: subsectionName, sections: subsection });
          subsectionName = item.title;
          subsectionIndex += 1;
          subsection = [];
        } else {
          if (item.include_ui) {
            if (props.selected_unit.id === item.id) {
              setSelected(item.id);
              setExpanded(new Array(subsectionIndex.toString()));
            }
            subsection.push(item);
          }
        }
      });
      the_course.push({ title: subsectionName, sections: subsection });
      setTreeData(the_course);
    };

    if (props.course.id && props.userCourseDetails) {
      setUserCourseDetails(props.userCourseDetails);
      processCourseItems();
    }
  }, [props.course, props.userCourseDetails, props.selected_unit.id]);

  const findAllInRenderedTree = () => {
    if (treeData.length === 0) {
      return (
        <StyledTreeItem
          nodeId="dummy"
          key="dummy"
          labelText="Loading..."
          labelIcon={MailIcon}
        />
      );
    } else {
      return treeData.map((item, index) => {
        return (
          <StyledTreeItem
            nodeId={index.toString()}
            key={index.toString()}
            labelText={item.title.toUpperCase()}
            labelIcon={null}
            sx={{
              ".MuiTreeItem-group": {
                ml: 0,
              },
            }}
          >
            {item.sections.map((sec) => {
              let complete = false;
              for (let index = 0; index < userCourseDetails.length; index++) {
                const element = userCourseDetails[index];
                if (element.id === sec.id) {
                  if (element.completed) {
                    complete = true;
                  }
                  break;
                }
              }
              return (
                <StyledTreeItem
                  usercomplete={complete.toString()}
                  nodeId={sec.id}
                  key={sec.id}
                  labelText={sec.title.toUpperCase()}
                  labelIcon={sec.type === "unit" ? MenuBook : Create}
                ></StyledTreeItem>
              );
            })}
          </StyledTreeItem>
        );
      });
    }
  };

  const handleToggle = (e, nodeIds) => {
    setExpanded(nodeIds);
  };

  const unitClicked = (e, nodeId) => {
    candidateAction({
      component: "CourseTreeView",
      event: "Unit Clicked: " + nodeId,
      page: "Course",
    });

    setSelected(nodeId);

    props.userCourseDetails.forEach((u) => {
      if (u.id === nodeId) {
        if (u.title.toUpperCase().includes("MODULE QUIZ") && u.type == "quiz")
          navigate(
            `${process.env.REACT_APP_AB_URL_PREFIX}/course/${props.course_url}/${u.url_name}?ru=${props.userCourseDetails[0].url_name}`,
          );
        else
          navigate(
            `${process.env.REACT_APP_AB_URL_PREFIX}/course/${props.course_url}/${u.url_name}`,
          );
        props.setUnitUrl();
      }
    });
  };

  return (
    <>
      {
        <TreeView
          onNodeSelect={unitClicked}
          onNodeToggle={handleToggle}
          sx={{
            height: "auto",
            flexGrow: 1,
            maxWidth: 400,
          }}
          defaultCollapseIcon={<ExpandMoreIcon />}
          defaultExpandIcon={<ChevronRightIcon />}
          defaultEndIcon={<div style={{ width: 24 }} />}
          selected={selected}
          expanded={expanded}
        >
          {findAllInRenderedTree()}
        </TreeView>
      }
    </>
  );
}
