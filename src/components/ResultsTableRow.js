import TableCell from "@mui/material/TableCell";
import { useTheme } from "@mui/material/styles";
import React, { useContext, useEffect, useState } from "react";
import { UserContext } from "../contexts/userContext";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import KeyboardArrowUpIcon from "@mui/icons-material/KeyboardArrowUp";
import Box from "@mui/material/Box";
import Collapse from "@mui/material/Collapse";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import TableRow from "@mui/material/TableRow";
import { getSingleQuizResults } from "../facades/UserFacade";
import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import ResultsGraph from "./ResultsGraph";
import { Divider, ListItem } from "@mui/material";
import LinearProgress from "@mui/material/LinearProgress";
import CircularProgress from "@mui/material/CircularProgress";
import List from "@mui/material/List";
import { Avatar } from "@mui/material";
import CheckCircleIcon from "@mui/icons-material/CheckCircle";
import { ListItemAvatar } from "@mui/material";
import { ListItemText } from "@mui/material";
import Badge from "@mui/material/Badge";
import RadioButtonCheckedIcon from "@mui/icons-material/RadioButtonChecked";
import Hidden from "@mui/material/Hidden";
import SingleQuizResults from "./SingleQuizResults";

// const useRowStyles = makeStyles((theme) => ({
//   root: {
//     "& > *": {
//       borderBottom: "unset",
//       paddingTop: 0,
//       paddingBottom: 0,
//     },
//   },
//   colorPrimary: {
//     backgroundColor:
//       theme.palette.grey[theme.palette.type === "light" ? 200 : 700],
//   },
//   barColorPrimary: {
//     backgroundColor: "#1a90ff",
//   },
//   answer_card: {
//     margin: "10px",
//     border: "1px solid lightgrey",
//   },
//   resultsGraph: {
//     maxHeight: "100%",
//   },
//   answerCardText: {
//     padding: "20px",
//   },
//   rowExpanded: {
//     background: "#fafafa",
//   },
// }));

export default function ResultsTableRow(props) {
  const { row } = props;
  const [open, setOpen] = useState(false);
  const theme = useTheme();
  const userContext = useContext(UserContext);

  return (
    <React.Fragment>
      <TableRow
        sx={{
          "& > *": {
            borderBottom: "unset",
            paddingTop: 0,
            paddingBottom: 0,
          },
        }}
      >
        <TableCell>
          <IconButton
            aria-label="expand row"
            size="small"
            onClick={() => setOpen(!open)}
          >
            {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
          </IconButton>
        </TableCell>
        <TableCell component="th" scope="row">
          <Typography variant="button">{row.title}</Typography>
        </TableCell>

        <TableCell align="right">
          <Box display="flex" alignItems="center">
            <Box width="100%" mr={1}>
              <LinearProgress
                variant="determinate"
                classes={{
                  colorPrimary: {
                    backgroundColor:
                      theme.palette.grey[
                        theme.palette.type === "light" ? 200 : 700
                      ],
                  },
                  barColorPrimary: {
                    backgroundColor: "#1a90ff",
                  },
                }}
                value={(row.grade / row.max_grade) * 100}
              />
            </Box>
            <Box minWidth={35}>
              <Typography variant="body2" color="textSecondary">
                {Math.round((row.grade / row.max_grade) * 100)}%
              </Typography>
            </Box>
          </Box>
          <Typography variant="h6">
            {row.grade}/{row.max_grade}
          </Typography>
        </TableCell>
        <Hidden smDown>
          <TableCell align="right">{row.submitted} ago</TableCell>
        </Hidden>
      </TableRow>
      <TableRow sx={{ background: "#fafafa" }}>
        <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
          <Collapse in={open} timeout="auto" unmountOnExit>
            <Box margin={1}>
              <SingleQuizResults
                quiz_id={props.quiz_id}
                open={open}
              ></SingleQuizResults>
            </Box>
          </Collapse>
        </TableCell>
      </TableRow>
    </React.Fragment>
  );
}
