import { Grid, Stack, Paper, Box, Button, Typography } from "@mui/material";
import PledgeCard from "./cards/PledgeCard";
import FaqCard from "./cards/FaqCard";
import CourseSummaryCard from "./cards/CourseSummaryCard";
import TimetableAndStudyCard from "./cards/TimetableAndStudyPlanCard";
import ExamBookingCard from "./cards/ExamBookingCard";
import { useNavigate } from "react-router-dom";
import useLiveLogging from "../hooks/useLiveLogging";
import { useState, useEffect, useContext } from "react";
import { getUserCourseSummary } from "../facades/UserFacade";
import { UserContext } from "../contexts/userContext";
import UserCourseSummary from "./entities/UserCourseSummary";
import StudyDataCard from "./cards/StudyDataCard";
export default function DashboardSubjectRow({ course }) {
  const navigate = useNavigate();
  const userContext = useContext(UserContext);
  const candidateAction = useLiveLogging();
  const [summary, setSummary] = useState(null);

  useEffect(() => {
    if (!course) return;

    getUserCourseSummary(userContext.user, course)
      .then((the_summary) => {
        setSummary(the_summary);
        if (
          userContext.userCourses.findIndex(
            (thing) => thing.id == the_summary.id,
          ) == -1
        ) {
          userContext.userCourses.push(the_summary);
        }

        //userContext.addUserCourseName([...userContext.userCourses]);
      })
      .catch((err) => {
        setIsError(true);
      });
  }, [course]);

  const [isError, setIsError] = useState(false);

  function handleContinueCourse(e) {
    candidateAction({
      component: "Course Card",
      event: "Course Clicked: " + summary.course_url,
      page: "Dashboard",
    });

    if (course == 85853) {
      window.location.assign(
        `${process.env.REACT_APP_BASE_URL}/classic/course/${summary.course_url}`,
      );
    } else {
      navigate(
        `${process.env.REACT_APP_AB_URL_PREFIX}/course/${summary.course_url}/${summary.next_unit}`,
      );
    }
  }

  function goToStudyTracker() {
    navigate(`${process.env.REACT_APP_AB_URL_PREFIX}/info/my-tracker`);
  }

  if (summary === null) return null;
  return (
    <Box sx={{ p: 3, pb: 0 }}>
      <Typography variant="h6" sx={{ mb: 1 }}>
        {summary.title}
      </Typography>
      <Grid
        container
        spacing={3}
        alignItems="stretch"
        sx={{ height: { md: "360px" } }}
      >
        <Grid item xs={12} sm={6} md={3} sx={{ height: "100%" }}>
          <CourseSummaryCard
            continueFunction={handleContinueCourse}
            progress={summary.progress}
            nextUnit={summary.next_unit_title}
            url={summary.imageUrl}
          />
        </Grid>
        {course != 432479 &&
          course != 432610 &&
          course != 432474 &&
          course != 372381 &&
          course != 372523 &&
          course != 85853 && (
            <>
              <Grid item xs={12} sm={6} md={3} sx={{ height: "100%" }}>
                <Stack spacing={1} sx={{ height: "100%" }}>
                  <FaqCard url={summary.getFAQUnitUrl()} />
                  <StudyDataCard clickFunc={goToStudyTracker} />
                </Stack>
              </Grid>
              <Grid item xs={12} sm={6} md={3} sx={{ height: "100%" }}>
                <Stack spacing={1} sx={{ height: "100%" }}>
                  <PledgeCard
                    userPledge={summary.pledge}
                    courseId={summary.id}
                  />
                  <TimetableAndStudyCard
                    unit={summary.getTimetableAndStudyPlanUnit()}
                  />
                </Stack>
              </Grid>
              <Grid item xs={12} sm={6} md={3} sx={{ height: "100%" }}>
                <ExamBookingCard
                  courseId={summary.id}
                  pe1Date={summary.pe1Date}
                  pe2Date={summary.pe2Date}
                />
              </Grid>
            </>
          )}
      </Grid>
    </Box>
  );
}
