import { Grid, Paper, Typography, Stack, Button } from "@mui/material";
import { useState, useEffect, useContext } from "react";
import SentimentVeryDissatisfiedIcon from '@mui/icons-material/SentimentVeryDissatisfied';
import SentimentNeutralIcon from '@mui/icons-material/SentimentNeutral';
import SentimentSatisfiedAltIcon from '@mui/icons-material/SentimentSatisfiedAlt';
import dayjs from "dayjs";
import { recordUserFom } from "../../facades/UserFacade";
import { UserContext } from "../../contexts/userContext";

const iconStyle = {
    fontSize: "50px",  
}
const buttonTextStyle = {
    fontSize: 16, 
    fontWeight:600,
    m:0
}
const buttonStyle = {
    boxShadow:0, 
    p:0.5, 
    mb:1,
    ':hover': { boxShadow: 4, }
}

const FOM_MESSAGES = {
     "unmotivated" : <Typography variant="h5">"The greater the difficulty, the more glory in surmounting it." - Epicurus. <b>Don't worry, you got this!</b></Typography>,
     "ok" : <Typography variant="h5">Well, let's try and get through at least one unit! <b>Eat the elephant a bite at a time.</b></Typography>,
     "motivated" :  <Typography variant="h5">Go you! Let's get cracking! </Typography>
}

export default function FeelingMeter()
{

    const userContext = useContext(UserContext);
    const [items, setItems] = useState(null);
    const [displayFom, setDisplayFom] = useState(false);
    const [displaySpiel, setDisplaySpiel] = useState(false);
    const [spiel, setSpiel] = useState("");

    useEffect(() => {
        const items = JSON.parse(localStorage.getItem('fom-lastShown'));
        if (items) {
            setItems(items);
            let lastShownDate = dayjs(items.lastShownDate, ['YYYY-MM-DD'])
            let today = dayjs();
            if(today.diff(lastShownDate, "day") >= 1)
            {
                setDisplayFom(true);
                let data = {lastShownDate : today.format("YYYY-MM-DD")}
                setItems(data)
                localStorage.setItem("fom-lastShown", JSON.stringify(data));
            }
        }
        else{
            let data = {lastShownDate : dayjs().format("YYYY-MM-DD")}
            setItems(data)
            localStorage.setItem("fom-lastShown", JSON.stringify(data));
            setDisplayFom(true);
        }
    }, []);

    const onFomSelected = (feeling) => {
        
        let data = items;
        let today = dayjs().format("YYYY-MM-DD");

        if(!data)
            data = {lastShownDate : today}
        
        localStorage.setItem("fom-lastShown", JSON.stringify(data));
        recordUserFom(userContext.user, today, feeling)
        .then(res => {})
        setSpiel(FOM_MESSAGES[feeling])
        setDisplayFom(false);
        setDisplaySpiel(true);
    }

    


    if(displayFom)
        return <Paper>
            <Grid container direction="column" alignItems="center" justifyContent="center" spacing={0} sx={{m:1}}>
            <Grid item>
                <Typography>How are you feeling today?</Typography>
            </Grid>
            <Grid item>
                <Grid container direction="row" alignItems="center" justifyContent="center" spacing={4}>
                    <Grid item>
                        <Paper sx={buttonStyle}>
                        <Grid container direction="column" alignItems="center" justifyContent="center">
                            <Grid item>
                                <SentimentVeryDissatisfiedIcon sx={[iconStyle, {color:"#b81d13"}]} onClick={(e)=> onFomSelected("unmotivated")}/>
                            </Grid>
                            <Grid item>
                                <Typography sx={buttonTextStyle}>Unmotivated</Typography>
                            </Grid>
                        </Grid>
                        </Paper>
                    </Grid>
                    <Grid item>
                        <Paper sx={buttonStyle}>
                        <Grid container direction="column"  alignItems="center" justifyContent="center">
                            <Grid item>
                                <SentimentNeutralIcon sx={[iconStyle, {color:"#efb700"}]} onClick={(e)=> onFomSelected("ok")}/>
                            </Grid>
                            <Grid item>
                                <Typography sx={buttonTextStyle}>OK, I guess.</Typography>
                            </Grid>
                        </Grid>
                        </Paper>
                    </Grid>
                    <Grid item>
                        <Paper sx={buttonStyle}>
                        <Grid container direction="column"  alignItems="center" justifyContent="center">
                            <Grid item>
                                <SentimentSatisfiedAltIcon sx={[iconStyle, {color:"#008450"}]} onClick={(e)=> onFomSelected("motivated")}/>
                            </Grid>
                            <Grid item>
                                <Typography sx={buttonTextStyle}>Let's do it!</Typography>
                            </Grid>
                        </Grid>
                        </Paper>
                    </Grid>
                </Grid>
            </Grid>

        </Grid>
        </Paper>
    if(!displayFom && displaySpiel)
        return <Paper  sx={{pt:2, pb:1, pl:4, pr:4, mb:1, width:"100%"}}>
                    <Stack alignItems="center" justifyContent="space-around" >
                        {spiel}
                        <Button onClick={(e) => setDisplaySpiel(false)}>Got it</Button>
                    </Stack>
                </Paper>
        
        
}