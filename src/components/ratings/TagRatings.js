import { Chip, Stack, Typography, Grid, Unstable_Grid2, Paper } from "@mui/material";
import Grid2 from "@mui/material/Unstable_Grid2/Grid2";
import { useContext, useEffect, useState } from "react";
import CheckIcon from '@mui/icons-material/Check';
import { submitUnitRatingTag } from "../../facades/UnitFacade";
import { UserContext } from "../../contexts/userContext";

export default function TagRatings({unitId})
{
    const userContext = useContext(UserContext);
    const [chips, setChips] = useState([
        { key: 0, label: 'complicated', selected:false },
        { key: 1, label: 'engaging', selected:false },
        { key: 2, label: 'boring', selected:false },
        { key: 3, label: 'still confused', selected:false },
        { key: 4, label: 'easy', selected:false },
        { key: 5, label: 'difficult', selected:false},
        { key: 6, label: 'well explained', selected:false},
      ]);
    const [unit, setUnit] = useState();

useEffect(() => {
    if(unitId != unit){
        setUnit(unitId)
        chips.forEach(c => {c.selected = false})
    }
}, [unitId])

const recordSelection = (tag, selected) => {
    submitUnitRatingTag(userContext.user, unit, tag, selected)
    .then(res => console.log("submitted tag"));
}
      
const toggleChipSelection = (clickedChipKey) => {
    
    let chip = chips.find(c => c.key == clickedChipKey)
    chip.selected = !chip.selected;
    recordSelection(chip.label, chip.selected)
    setChips([...chips])

}

return <Paper sx={{mb:1, borderRadius:5, border:0}}>
        <Typography variant="h6" sx={{p:1}}>
            How did you find this unit? 
        </Typography>
        <Grid2 container direction="row" alignItems="center" justifyContent="center" spacing={1} sx={{pb:2}}>
            {
            chips.map(chip => {
                return <Grid2 item key={chip.key}><Chip  
                label={chip.label} 
                clickable 
                variant={chip.selected ? "filled" : "outlined"} 
                color="secondary"
                onClick={(e) => toggleChipSelection(chip.key)}
                icon={chip.selected ? <CheckIcon/> : null}
                />
                </Grid2>
            })
        }
        </Grid2>
</Paper>

}