import React from "react";
import { useTheme } from "@mui/material/styles";
import { FormControl } from "@mui/material";
import { InputLabel } from "@mui/material";
import { Select } from "@mui/material";
import { MenuItem } from "@mui/material";
import { FormHelperText } from "@mui/material";

// const useStyles = makeStyles((theme) => ({
//   formControl: {
//     margin: theme.spacing(0),
//     minWidth: "90%",
//   },
//   selectEmpty: {
//     maxWidth: "300px",
//     marginTop: theme.spacing(2),
//     fontSize: 30,
//   },
// }));

export default function UserCourseDropDown(props) {
  //const userCourses = props.userCourses;
  const theme = useTheme();
  const [course, setCourse] = React.useState("");

  const handleChange = (event) => {
    let x = event.target.value;
    setCourse(event.target.value);
    props.onChange(x);
  };

  return (
    <FormControl sx={{ margin: theme.spacing(0), minWidth: "90%" }}>
      <InputLabel shrink id="select-placeholder-label-label">
        Exam
      </InputLabel>
      <Select
        labelId="select-placeholder-label-label"
        id="select-placeholder-label"
        onChange={handleChange}
        displayEmpty
        value={course}
        sx={{ maxWidth: "300px", marginTop: theme.spacing(2), fontSize: 30 }}
        inputProps={{ style: { fontSize: 40 } }}
      >
        {props.userCourses?.map((course) => {
          return (
            <MenuItem value={course} key={course.event_id}>
              {course.name}
            </MenuItem>
          );
        })}
      </Select>
      <FormHelperText>select your exam</FormHelperText>
    </FormControl>
  );
}
