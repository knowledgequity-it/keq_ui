import React, { useContext, useEffect, useState } from "react";
import { useTheme } from "@mui/material/styles";
import { Box } from "@mui/material";
import { FormControl } from "@mui/material";
import { InputLabel } from "@mui/material";
import { Select } from "@mui/material";
import { MenuItem } from "@mui/material";
import { FormHelperText } from "@mui/material";

// const useStyles = makeStyles((theme) => ({
//   formControl: {
//     margin: theme.spacing(1),
//     minWidth: "90%",
//     maxWidth: 350,
//   },
//   selectEmpty: {
//     marginTop: theme.spacing(2),
//     fontSize: 30,
//   },
// }));

export default function EventLocationsDropdown(props) {
  const theme = useTheme();
  const locations = props.eventLocations;
  const [location, setLocation] = React.useState("");

  const handleChange = (event) => {
    setLocation(event.target.value);
    props.onChange(event.target.value);
  };
  return (
    <FormControl
      sx={{ margin: theme.spacing(1), minWidth: "90%", maxWidth: 350 }}
    >
      <InputLabel shrink id="select-placeholder-label-label">
        Location
      </InputLabel>
      <Select
        labelId="select-placeholder-label-label"
        id="select-placeholder-label"
        onChange={handleChange}
        value={location}
        displayEmpty
        sx={{ marginTop: theme.spacing(2), fontSize: 30 }}
        inputProps={{ style: { fontSize: 40 } }}
      >
        {locations?.map((l) => {
          return (
            <MenuItem value={l} key={l.location_id}>
              {l.name}
            </MenuItem>
          );
        })}
      </Select>
      <FormHelperText>select your preferred location</FormHelperText>
    </FormControl>
  );
}
