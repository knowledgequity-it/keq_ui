import React from "react";
import Card from "@mui/material/Card";
import CardActionArea from "@mui/material/CardActionArea";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import { useNavigate } from "react-router-dom";
import LinearProgress from "@mui/material/LinearProgress";
import Event from "@mui/icons-material/Event";
import useLiveLogging from "../../hooks/useLiveLogging";

// const useStyles = makeStyles((theme) => ({
//   root: {
//     //    maxWidth: 345,
//     //    width: '380px',
//     height: "200px",
//   },
//   media: {
//     height: 140,
//   },
//   title: {
//     height: "50px",
//   },
//   subtitle: {
//     height: "20px",
//     paddingLeft: "20px",
//   },
// }));

export default function SchedulerCard(props) {
  const navigate = useNavigate();
  const candidateAction = useLiveLogging();

  function handleClick(e) {
    candidateAction({
      component: "SchedulerCard",
      event: "User Clicked the SchedulerCard",
    });
    navigate(`${process.env.REACT_APP_AB_URL_PREFIX}/schedule`);
  }

  return (
    <Card sx={{ height: "200px" }}>
      <CardActionArea onClick={(e) => handleClick(e)}>
        <CardContent>
          <Event style={{ fontSize: 70 }} />
          <Typography
            sx={{ height: "50px" }}
            gutterBottom
            variant="h5"
            component="h2"
          >
            Schedule Practice Exams
          </Typography>
        </CardContent>
        <Typography
          sx={{ height: "20px", paddingLeft: "20px" }}
          variant="subtitle1"
          color="textSecondary"
          component="p"
        >
          Schedule your Practice Exams
        </Typography>
      </CardActionArea>
    </Card>
  );
}
