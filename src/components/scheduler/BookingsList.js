import React from "react";
import { useTheme } from "@mui/material/styles";
import ListSubheader from "@mui/material/ListSubheader";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import Collapse from "@mui/material/Collapse";
import ExpandLess from "@mui/icons-material/ExpandLess";
import ExpandMore from "@mui/icons-material/ExpandMore";
import LocationOnIcon from "@mui/icons-material/LocationOn";
import AccessTimeIcon from "@mui/icons-material/AccessTime";
import Chip from "@mui/material/Chip";
import { Typography } from "@mui/material";
import AlarmIcon from "@mui/icons-material/Alarm";
import Grid from "@mui/material/Grid";

// const useStyles = makeStyles((theme) => ({
//   root: {
//     width: "100%",
//     maxWidth: 600,
//     backgroundColor: theme.palette.background.paper,
//   },
//   nested: {
//     paddingLeft: theme.spacing(4),
//   },
// }));

const ItemFilterListItem = (props) => {
  const theme = useTheme();
  const [open, setOpen] = React.useState(true);
  const toggleOpen = () => {
    open ? setOpen(false) : setOpen(true);
  };

  let data = props.data;
  return (
    <React.Fragment>
      <ListItem button onClick={toggleOpen}>
        <ListItemIcon>
          {props.primary_icon === "location" ? (
            <LocationOnIcon />
          ) : (
            <AccessTimeIcon />
          )}
        </ListItemIcon>
        <ListItemText
          primary={
            <React.Fragment>
              <Grid container>
                <Grid item xs={12} xm={6} lg={6}>
                  <Typography variant="h5">{props.text}</Typography>
                </Grid>
                <Grid item xs={12} xm={3} lg={3}>
                  <Chip
                    icon={<AlarmIcon />}
                    label={`${props.label_text}`}
                    style={{
                      backgroundColor: "lightskyblue",
                      marginLeft: "5px",
                      fontSize: "1em",
                    }}
                  ></Chip>
                </Grid>
                <Grid item xs={1}></Grid>
              </Grid>
            </React.Fragment>
          }
          secondary={
            <React.Fragment>
              <Typography variant="body2">{props.subtext}</Typography>
            </React.Fragment>
          }
        />
        {data.length != 0 && open ? <ExpandLess /> : <ExpandMore />}
      </ListItem>
      {data.length != 0 && (
        <Collapse in={open} timeout="auto" unmountOnExit>
          {data.map((i) => (
            <List component="div" disablePadding key={i[props.secondary_key]}>
              <ListItem button sx={{ paddingLeft: theme.spacing(4) }}>
                <ListItemIcon>
                  {props.secondary_icon === "location" ? (
                    <LocationOnIcon />
                  ) : (
                    <AccessTimeIcon />
                  )}
                </ListItemIcon>
                <ListItemText primary={i[props.secondary_display_key]} />
              </ListItem>
            </List>
          ))}
        </Collapse>
      )}
    </React.Fragment>
  );
};

export default function BookingsList(props) {
  const theme = useTheme();
  const [open, setOpen] = React.useState(true);

  const handleClick = () => {
    setOpen(!open);
  };

  function generateListItem() {
    //console.log("PRELIST", props.primary_list)
    //let filteredList = props.primary_list.filter( item => item[props.filter_by] == props.primary_item[props.filter_by] );
    //console.log("FILTERED ITEMS", filteredList);
    return props.data.map((element) => {
      let subtext = props.primary_list_sub_display_key
        .map((key) => {
          return element.key[key];
        })
        .join(" ");
      //{element[props.primary_list_key]
      return (
        <ItemFilterListItem
          key={element.key[props.primary_list_key]}
          text={element.key[props.primary_list_display_key]}
          subtext={subtext}
          label_text={element.key[props.label_text]}
          data={element.values}
          primary_key_value={element[props.primary_list_key]}
          primary_key={props.primary_key}
          secondary_key={props.secondary_list_key}
          primary_icon={props.primary_icon}
          secondary_icon={props.secondary_icon}
          secondary_display_key={props.secondary_list_display_key}
          xs={12}
        ></ItemFilterListItem>
      );
    });
  }
  return (
    <List
      component="nav"
      aria-labelledby="nested-list-subheader"
      subheader={
        <ListSubheader component="div" id="nested-list-subheader">
          {props.title}
        </ListSubheader>
      }
      sx={{
        width: "100%",
        maxWidth: 600,
        backgroundColor: theme.palette.background.paper,
      }}
    >
      {generateListItem()}
    </List>
  );
}
