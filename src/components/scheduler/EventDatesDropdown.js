import React, { useContext, useEffect, useState } from "react";
import { useTheme } from "@mui/material/styles";
import { Box } from "@mui/material";
import { FormControl } from "@mui/material";
import { InputLabel } from "@mui/material";
import { Select } from "@mui/material";
import { MenuItem } from "@mui/material";
import { FormHelperText } from "@mui/material";
import { SentimentDissatisfiedTwoTone } from "@mui/icons-material";

// const useStyles = makeStyles((theme) => ({
//   formControl: {
//     margin: theme.spacing(1),
//     minWidth: "90%",
//   },
//   selectEmpty: {
//     marginTop: theme.spacing(2),
//     fontSize: 30,
//   },
// }));

export default function EventDatesDropdown(props) {
  const dates = props.eventDates;
  const theme = useTheme();
  const [date, setDate] = React.useState("");

  const handleChange = (event) => {
    setDate(event.target.value);
    props.onChange(event.target.value);
  };

  return (
    <FormControl sx={{ margin: theme.spacing(1), minWidth: "90%" }}>
      <InputLabel shrink id="select-placeholder-label-label">
        Event date & time
      </InputLabel>
      <Select
        labelId="select-placeholder-label-label"
        id="select-placeholder-label"
        value={date}
        onChange={handleChange}
        displayEmpty
        sx={{
          selectEmpty: {
            marginTop: theme.spacing(2),
            fontSize: 30,
          },
        }}
      >
        {dates.map((date) => {
          return (
            <MenuItem value={date} key={date.event_slot_id}>
              {date.event_slot_time}
            </MenuItem>
          );
        })}
      </Select>
      <FormHelperText>select from the available sessions</FormHelperText>
    </FormControl>
  );
}
