import React, { useContext, useEffect, useState } from "react";
import { useTheme } from "@mui/material/styles";
import { Grid } from "@mui/material";
import { Paper } from "@mui/material";
import Box from "@mui/material/Box";
import { UserContext } from "../../contexts/userContext";
import UserCourseDropDown from "./UserCourseDropDown";
import { Typography } from "@mui/material";
import EventLocationsDropdown from "./EventLocationsDropdown";
import EventDatesDropdown from "./EventDatesDropdown";
import { Button } from "@mui/material";
import useLiveLogging from "../../hooks/useLiveLogging";

// const useStyles = makeStyles((theme) => ({
//   root: {
//     flexGrow: 1,
//     padding: theme.spacing(1),
//   },
//   paper: {
//     padding: theme.spacing(2),
//     margin: theme.spacing(2),
//     textAlign: "center",
//     color: theme.palette.text.secondary,
//   },
//   centeredItem: {
//     textAlign: "center",
//   },
// }));

export default function SchedulerForm(props) {
  const userContext = useContext(UserContext);
  const theme = useTheme();
  const candidateAction = useLiveLogging();

  const handleEventChange = (e) => {
    props.onEventSelection(e);
  };
  const handleLocationChange = (e) => {
    props.onLocationSelection(e);
  };
  const handleDateChange = (e) => {
    props.onDateSelection(e);
  };

  const bookUserEvent = () => {
    candidateAction({
      component: "SchedulerForm",
      event: "User clicked Book Now Button",
    });
    props.onBookingSave();
  };

  return (
    <Box sx={{ flexGrow: 1, padding: theme.spacing(1) }}>
      <Paper>
        <Grid
          container
          spacing={0}
          direction="row"
          justifyContent="space-around"
          alignItems="center"
        >
          <Grid item xs={12} md={1}>
            <Typography variant="h5">I want to book</Typography>
          </Grid>
          <Grid item xs={12} md={2} sx={{ textAlign: "center" }}>
            <UserCourseDropDown
              userCourses={props.user_events}
              onChange={handleEventChange}
            ></UserCourseDropDown>
          </Grid>
          <Grid item xs={12} md={1} sx={{ textAlign: "center" }}>
            <Typography variant="h5">at</Typography>
          </Grid>
          <Grid item xs={12} md={3} sx={{ textAlign: "center" }}>
            <EventLocationsDropdown
              eventLocations={props.event_locations}
              onChange={handleLocationChange}
            ></EventLocationsDropdown>
          </Grid>
          <Grid item xs={12} md={1} sx={{ textAlign: "center" }}>
            <Typography variant="h5">on</Typography>
          </Grid>
          <Grid item xs={12} md={2} sx={{ textAlign: "center" }}>
            <EventDatesDropdown
              eventDates={props.event_slots}
              onChange={handleDateChange}
            ></EventDatesDropdown>
          </Grid>
          <Grid item xs={12} md={2} sx={{ textAlign: "center" }}>
            <Button variant="contained" color="primary" onClick={bookUserEvent}>
              Book Now!
            </Button>
          </Grid>
        </Grid>
      </Paper>
    </Box>
  );
}
