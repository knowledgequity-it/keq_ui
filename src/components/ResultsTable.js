import { useState, useContext, useEffect } from 'react';
import { UserContext } from '../contexts/userContext';
import { getUserResults } from '../facades/UserFacade';
import React from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Typography from '@mui/material/Typography';
import Paper from '@mui/material/Paper';
import ResultsTableRow from './ResultsTableRow';
import Hidden from '@mui/material/Hidden';
import useLiveLogging from '../hooks/useLiveLogging';


export default function ResultsTable() {

    const [results, setResults] = useState([]);
    const userContext = useContext(UserContext);
    const candidateAction = useLiveLogging();


    useEffect(() => {
        getUserResults(userContext.user)
        .then((results) => results.json())
        .then(results => {
            setResults(results.quiz_results);
        })
        .catch(error => {
          candidateAction({
            component: 'ResultsTable',
            event: "Failed to load ResultsTable",
            error: error
          })
        })
    }, [userContext.user])

  return (
    <TableContainer component={Paper}>
      <Table style={{ width: "100%", tableLayout: "auto" }}>
        <TableHead>
          <TableRow>
            <TableCell />
            <TableCell>
                <Typography variant="h5">
                    Quiz Name
                </Typography>  
            </TableCell>
            <TableCell align="right">
                <Typography variant="h5">
                    Result
                </Typography>  
            </TableCell>
            <Hidden smDown>
            <TableCell align="right">
                <Typography variant="h5">
                    Submitted
                </Typography>     
            </TableCell>
            </Hidden>
          </TableRow>
        </TableHead>
        <TableBody>
          {results.map((row) => (
             <ResultsTableRow key={row.id} row={row} quiz_id={row.id} />
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
