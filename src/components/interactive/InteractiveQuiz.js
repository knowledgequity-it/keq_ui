import React, { useContext, useEffect, useRef, useState } from 'react';
import {
  Accordion,
  AccordionSummary,
  AccordionDetails,
  Typography,
  Radio,
  RadioGroup,
  FormControlLabel,
  FormControl,
  Paper,
  Stack,
  Grid,
  TextField,
  Button,
} from '@mui/material';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { UserContext } from '../../contexts/userContext';
import { QuizContext } from '../../contexts/QuizContext';
import useLiveLogging from '../../hooks/useLiveLogging';
import { quizCourse } from '../../facades/QuizFacade';
import LocalQuizStorage from '../quiz/LocalQuizStorage';
import { useLocation } from 'react-router-dom';
import { getUserCourseQuiz } from '../../facades/CourseFacade';
import Quiz from '../quiz/Quiz';
import { getSingleQuizResults, submitUserCPDQuestion } from '../../facades/UserFacade';
import PsychologyAltIcon from '@mui/icons-material/PsychologyAlt';
import MarkUnitCompletePanel from '../course/MarkUnitCompletePanel';


const FreeTextAnswer = ({
    index,
    questionId,
    submitAnswerCallback,
    hasScratchpad,
  }) => {
    const [wordCount, setWordCount] = useState(0);
    const [answerText, setAnswerText] = useState("");
    const inputRef = useRef();
  
    //if (question && question.user_answer.length == 1)
    //  setAnswerText(question.user_answer[0]);
  
    const handleTextChange = (event) => {

        let totalChars = event.target.value;

        let totalSpaces = totalChars.split(" ").length;

        setAnswerText(totalChars);
        setWordCount(totalSpaces);

    };
  
    const handleBlur = (event) => {
     // setAnswerCallback(index, event.target.value, "largetext");
    };
    
    const submitAnswer = () => {
        submitAnswerCallback(answerText, questionId)
    }
  
    const handlePaste = (event) => {
      if (hasScratchpad) {
        event.preventDefault();
        if (inputRef.current) {
          const [start, end] = [
            inputRef.current.selectionStart,
            inputRef.current.selectionEnd,
          ]; /*
          inputRef.current.setRangeText(
            quizContext.scratchpadClipboard,
            start,
            end,
            "end"
          );*/
          let newstr =
            answerText.substring(0, start) +
            answerText.substring(end);
          setAnswerText(newstr);
          //setAnswerCallback(index, newstr, "largetext");
        }
      }
    };
    return (
      <React.Fragment>
        <Typography>

            { 50 - wordCount >= 0 ? `You have ${50 - wordCount} words left` : `You have reached the minimum 50 words threshold.` }
        </Typography>
        <TextField
          variant="outlined"
          fullWidth
          multiline
          rows={10}
          id="answer-field"
          label="Answer text"
          value={answerText}
          onInput={handleTextChange}
          onPaste={handlePaste}
          onBlur={handleBlur}
          inputRef={inputRef}
        />
        <Button onClick={() => submitAnswer()} disabled={answerText.split(" ").length <=49}>Submit Your Answer</Button>
      </React.Fragment>
    );
  };
function AnswerBlocks({answer_options, correct_answer, answeredCorrectlyCallback, hint, explanation, index})
{

    const [selectedAnswer, setSelectedAnswer] = useState([]);
    const [showHint, setShowHint] = useState(false);
    const [showExplanation, setShowExplanation] = useState(false)


    const handleAnswerChange = (selection, correctAnswers) => {
        if(correctAnswers.includes(selection.toString()))
        {
            if(!selectedAnswer.includes(toString()))
            {
                let newArr = [...selectedAnswer, selection.toString()]
                setSelectedAnswer(newArr);
                const s = new Set(newArr);
                if(correctAnswers.every(e => s.has(e.toString())))
                    answeredCorrectlyCallback(index)
                setShowExplanation(true)
                setShowHint(false)
            }
        }
        else
        {   if(!showExplanation)
                setShowHint(true)
        }
    } 

    return <>
    <Grid container  direction="row" justifyContent="center" alignItems="center">
            <Grid item xs={2}>
                <PsychologyAltIcon style={{ fontSize: 60 }}/>
            </Grid>
            <Grid item xs={10}>
                {
                    showHint &&  (<Paper sx={{p:2}}>
                        <p>Incorrect.</p>
                        {hint}
                    </Paper>)
                }
                {
                    showExplanation && (<Paper sx={{p:2}}>
                        <p>Correct!</p>
                        {explanation}
                    </Paper>)
                }
            </Grid>

            </Grid>
    { answer_options.map((answer, answerIndex) => {
        return <Paper onClick={ () => handleAnswerChange(answerIndex+1, correct_answer )} key={answerIndex}
            sx={{
                p : 2,
                m:2,
                bgcolor: selectedAnswer.includes((answerIndex+1).toString()) ? "#66e0ff":"white",
                "&:hover": {
                bgcolor: "#e6faff",
                }}}>
            <Typography variant="subtitle1">{answer}</Typography>
        </Paper>
        })
    }
    </>

}


export default function InteractiveQuiz({ quiz_id, onUnitCompleted }){

    const userContext = useContext(UserContext);
    const quizContext = useContext(QuizContext);
    const candidateAction = useLiveLogging();
    const [expanded, setExpanded] = useState(0);
    const [quiz, setQuiz] = useState(null);
    const [course_url, setCourseUrl] = useState("");
    const [lastEnabled, setLastEnabled] = useState(0);
    

  const LOCAL_QUIZ_STORAGE = new LocalQuizStorage();

  let url_param = new URLSearchParams(useLocation().search).get("ru");

  const normaliseUri = (uri) => {
    let normalised = uri.replace(`${process.env.REACT_APP_AB_URL_PREFIX}`, "");
    if (normalised.substr(-1) == "/")
      normalised = normalised.substring(0, normalised.length - 1);
    return normalised;
  };

  useEffect(() => {
    if (!quiz_id) return;
    else {
      quizCourse(userContext.user, quiz_id)
        .then((response) => response.json())
        .then((quiz) => {
          if(!quiz.quiz_course && !url_param)//quiz not attached to course, and no ru url param
          {
            let parts = normaliseUri(window.location.pathname).split("/");
            if (parts.length === 4)
              //course url with unit part (/course/coursename/unitname) return last bit
              setCourseUrl(parts[2]);
          }
          else
            setCourseUrl(quiz.quiz_course);
        });
      quizContext.setQuizId(quiz_id);
    }
  }, [quiz_id, userContext.user]);

  useEffect(() => {
    if(quiz_id && userContext.user)
        getQuizDetails(quiz_id);
  },[quiz_id, userContext.user] )


  const getQuizDetails = (quiz_id) => {
    return getSingleQuizResults(userContext.user, quiz_id)
      .then((response) => response.json())
      .then((quiz) => {
        const q = new Quiz(quiz);
        setQuiz(q);
        candidateAction({
          component: "Quiz",
          activity: "Quiz",
          event: "Succesfully loaded quiz questions",
        });
      })
      .catch((error) => {
        candidateAction({
          component: "Quiz",
          event: `Failed to load Quiz Questions ${quiz_id}`,
          error: error,
        });
      });
  };



  
   const isNextSectionEnabled = (sectionIndex) => {
    setLastEnabled(sectionIndex+1);
  };

  const getTitleSection = (html) => {
        const parser = new DOMParser();
        let q = parser.parseFromString(html, "text/html");
        let f = q.getElementsByTagName("div")
        if(f.length == 0)
            f = q.getElementsByTagName("body")
        
        if(f.length != 0)
            return f[0].innerText.substring(0,200)
        else
            return '';
  }

  const submitAnswerCallback = (answerText, questionId) => {
    submitUserCPDQuestion(userContext.user, quiz_id, questionId, answerText)
    .then(res => {})
  }
  if(!quiz?.questions)
    return null;

  return (
    <div>
      {quiz.questions.map((section, sectionIndex) => (
        <Accordion
         // key={sectionIndex}
          expanded={sectionIndex <= lastEnabled}
          disabled={sectionIndex > lastEnabled}
        >
          <AccordionSummary expandIcon={<ExpandMoreIcon />}>
          <Stack direction="column">
            <div>Section {sectionIndex + 1}</div>
            
            
            </Stack>
          </AccordionSummary>
          <AccordionDetails>
          <div dangerouslySetInnerHTML={{__html: section.question}}></div>
           {
            section.question_type === "largetext" ? 
            <FreeTextAnswer submitAnswerCallback={submitAnswerCallback} questionId={section.question_id}/> 
            :
            <AnswerBlocks answer_options={section.answer_options} correct_answer={section.correct_answer} answeredCorrectlyCallback={isNextSectionEnabled} explanation={section.answer_explanation} hint={section.answer_hint} index={sectionIndex}/>
           }
            </AccordionDetails>
        </Accordion>
      ))}
    </div>
   
  );
};

