import React from 'react';
import { makeStyles, withStyles } from '@mui/material/styles';
import Button from '@mui/material/Button';
import useLiveLogging from '../hooks/useLiveLogging';

const useStyles = makeStyles( (theme)=> ({
  button: {
    "width": "100%",
    "marginTop": 20,
    "padding": 20
  }
}));

export default function CourseStartButton({ courseStarted, nextUnit, onClick }) {
  const classes = useStyles();
  
  const candidateAction = useLiveLogging();


  const handleClick = (e, course_url) => {
    candidateAction({
      component: "CourseStartButton",
      event: "Unit Clicked: "+ nextUnit.id,
      page: "Course"
    });

    onClick(nextUnit);
  }
  
  { nextUnit.id != undefined &&
     <Button
      color="primary"
      variant="contained"
      size="large"
      className={classes.button}
      onClick={() => handleClick(nextUnit)}
    >
      { courseStarted ? 'CONTINUE COURSE' : 'START COURSE' }
    </Button> 
  }


}
