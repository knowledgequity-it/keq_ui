import React from "react";
import { useTheme } from "@mui/material/styles";
import AppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import IconButton from "@mui/material/IconButton";
import AccountCircle from "@mui/icons-material/AccountCircle";
import MenuItem from "@mui/material/MenuItem";
import Menu from "@mui/material/Menu";
import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import Link from "@mui/material/Link";
import Divider from "@mui/material/Divider";
import DashboardIcon from "@mui/icons-material/Dashboard";
import { useNavigate } from "react-router";
import { ReactComponent as LogoIcon } from "./../assets/ke_logo.svg";
import Logo from "./../assets/logo.png";

// const useStyles = makeStyles((theme) => ({
//   root: {
//     marginTop: 30,
//     backgroundColor: theme.palette.common.white,
//   },
//   box: {
//     padding: "10px 0px",
//   },
//   logo: {
//     height: 40,
//     marginTop: 5,
//     marginRight: 10,
//   },
//   footer: {
//     fontSize: 12,
//     color: theme.palette.grey[800],
//   },
//   link: {
//     fontSize: 12,
//     color: theme.palette.grey[800],
//     textTransform: "uppercase",
//     "&:hover": {
//       color: theme.palette.grey[600],
//       textDecoration: "none",
//     },
//     marginLeft: 15,
//   },
// }));

export default function Header() {
  const theme = useTheme();
  const auth = true;
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  const navigate = useNavigate();

  const show_menu = false;

  const handleClick = (event) => {};

  const handleMenu = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const goProfile = () => {
    setAnchorEl(null);
    navigate("/profile");
  };

  const handleLogout = () => {
    localStorage.removeItem("user");
    navigate("/");
  };

  const gotoDashboard = () => {
    navigate("/dashboard");
  };

  return (
    <footer
      styles={{ marginTop: 30, backgroundColor: theme.palette.common.white }}
    >
      <Divider />
      <Container maxWidth="xl">
        <Box sx={{ padding: "10px 0px" }}>
          <Grid container alignItems="center">
            <Grid item xs={12} sm={2}>
              <Box display="flex" alignItems="center">
                <img
                  src={Logo}
                  style={{ height: 40, marginTop: 5, marginRight: 10 }}
                />
                <Typography
                  variant="body1"
                  component="span"
                  sx={{ fontSize: 12, color: theme.palette.grey[800] }}
                >
                  <strong>&copy;KNOWLEDGEQUITY</strong>
                  <br />
                  ABN 80 128 394 472
                </Typography>
              </Box>
            </Grid>
            {show_menu && (
              <Grid item container justifyContent="flex-end" xs={12} sm={10}>
                <Link
                  sx={{
                    fontSize: 12,
                    color: theme.palette.grey[800],
                    textTransform: "uppercase",
                    "&:hover": {
                      color: theme.palette.grey[600],
                      textDecoration: "none",
                    },
                    marginLeft: 15,
                  }}
                >
                  Terms &amp; Conditions
                </Link>
                <Link
                  sx={{
                    fontSize: 12,
                    color: theme.palette.grey[800],
                    textTransform: "uppercase",
                    "&:hover": {
                      color: theme.palette.grey[600],
                      textDecoration: "none",
                    },
                    marginLeft: 15,
                  }}
                >
                  Privacy Policy
                </Link>
                <Link
                  sx={{
                    fontSize: 12,
                    color: theme.palette.grey[800],
                    textTransform: "uppercase",
                    "&:hover": {
                      color: theme.palette.grey[600],
                      textDecoration: "none",
                    },
                    marginLeft: 15,
                  }}
                >
                  Refund Policy
                </Link>
                <Link
                  sx={{
                    fontSize: 12,
                    color: theme.palette.grey[800],
                    textTransform: "uppercase",
                    "&:hover": {
                      color: theme.palette.grey[600],
                      textDecoration: "none",
                    },
                    marginLeft: 15,
                  }}
                >
                  Grievance Policy
                </Link>
                <Link
                  sx={{
                    fontSize: 12,
                    color: theme.palette.grey[800],
                    textTransform: "uppercase",
                    "&:hover": {
                      color: theme.palette.grey[600],
                      textDecoration: "none",
                    },
                    marginLeft: 15,
                  }}
                >
                  System Requirements
                </Link>
                <Link
                  sx={{
                    fontSize: 12,
                    color: theme.palette.grey[800],
                    textTransform: "uppercase",
                    "&:hover": {
                      color: theme.palette.grey[600],
                      textDecoration: "none",
                    },
                    marginLeft: 15,
                  }}
                >
                  Navigation
                </Link>
                <Link
                  sx={{
                    fontSize: 12,
                    color: theme.palette.grey[800],
                    textTransform: "uppercase",
                    "&:hover": {
                      color: theme.palette.grey[600],
                      textDecoration: "none",
                    },
                    marginLeft: 15,
                  }}
                >
                  Contact
                </Link>
              </Grid>
            )}
          </Grid>
        </Box>
      </Container>
    </footer>
  );
}
