import { Badge, Button, IconButton } from "@mui/material"
import { MailOutlined } from "@mui/icons-material"
import { getAnnouncements } from "../../facades/UserFacade"
import { useNavigate } from 'react-router-dom';
import { useContext, useEffect, useState } from "react"
import { UserContext } from "../../contexts/userContext";
import NotificationsActiveIcon from '@mui/icons-material/NotificationsActive';
import dayjs from "dayjs";
import KeAnnouncement from "../entities/KeAnnouncement";
import CloseIcon from '@mui/icons-material/Close';
import { markAnnouncementAsRead } from "../../facades/UserFacade";
import React from "react";
export default function AnnouncementsNotifier({type})
{

    const [announcements, setAnnouncements] = useState(null)
    const [showBar, setShowBar] = useState(true)
    const userContext = useContext(UserContext);
    const navigate = useNavigate();

    useEffect(() => {
        if(announcements != null)
            return;
        getAnnouncements(userContext.user)
        .then(res => res.json())
        .then(announcements => {

            setAnnouncements(announcements.map(a => {return new KeAnnouncement(a)}));
        })
    })

    const totalUnreadAnnouncements = () => {

        if(!announcements)
            return 0;

        let count = 0;
        announcements.forEach(ann => {
            if(ann.dismissedStatus != "read")
                count++;
        });
        return count;
    }
    const getCurrentAnnouncementIndex = () => {
        if(!announcements)
            return

        let currentAnnouncement = null;
        let today = dayjs()

        let firstAnnouncement = announcements.findIndex(ann => 
            //console.log( ann.showBarUntil, ann.dismissedStatus, dayjs(ann.showBarUntil,['YYYY-MM-DD']).isAfter(today))
            dayjs(today).isBefore(ann.showBarUntil,['YYYY-MM-DD']) && ann.dismissedStatus != "read"
            );
        return firstAnnouncement;    
    }
    const gotToAnnouncements=()=> {
        let path = `/info/announcements`;
        navigate(path);
      }

    const singleOrPluralText = () => {
        return totalUnreadAnnouncements() < 2 ? "Notification" : "Notifications"
    }
    const dismiss = (index) => {
        
        markAnnouncementAsRead(userContext.user, announcements[index].announceid)
        .then(res => {
           
            if(res.status==200)
            {
               setShowBar(false);
            } 
        });
    }

    if(announcements == null)
        return null;

    if(type == "bar" && getCurrentAnnouncementIndex() != -1)
    {
        return <React.Fragment><Button variant="text" color="white" href="/info/announcements"
            startIcon={<Badge badgeContent={totalUnreadAnnouncements()} color="secondary" sx={{color:"black"}}>
            <NotificationsActiveIcon color="white" sx={{fontSize:40}}/>
        </Badge>}
            ><div dangerouslySetInnerHTML={{__html : announcements[getCurrentAnnouncementIndex()].content}}>
                
                </div></Button>
            <CloseIcon onClick={(e)=>dismiss(getCurrentAnnouncementIndex())} color="white"/>
            </React.Fragment>
    }
    
    if(type == "icon" && totalUnreadAnnouncements() == 0)
        return  <IconButton
                          size="small"
                          onClick={(e) => gotToAnnouncements()}
                          
                        >
                          <MailOutlined color="action" />
                        </IconButton>
    else if(type == "icon" && totalUnreadAnnouncements() != 0)
        return <Badge badgeContent={totalUnreadAnnouncements()} color={"error"} sx={{mr:2}}>
                                <MailOutlined color="action" onClick={(e) => gotToAnnouncements()}/>
                              </Badge>
    
    return null;

}