import { Card, Typography, CardHeader, CardContent} from "@mui/material";
import { useContext, useEffect, useState } from "react";
import KeAnnouncement from "../entities/KeAnnouncement";
import InfoIcon from '@mui/icons-material/Info';
import WarningAmberIcon from '@mui/icons-material/WarningAmber';
import MailOutlineIcon from '@mui/icons-material/MailOutline';
import CheckCircleOutlineIcon from '@mui/icons-material/CheckCircleOutline';
import MarkEmailReadIcon from '@mui/icons-material/MarkEmailRead';
import { markAnnouncementAsRead } from "../../facades/UserFacade";
import { UserContext } from "../../contexts/userContext";
export default function Announcement({the_announcement}){

    const [announcement, setAnnouncement] = useState(null);
    const userContext = useContext(UserContext);

    useEffect(() => {
        if(!the_announcement)
            return;

        setAnnouncement(new KeAnnouncement(the_announcement));
    }, [the_announcement])

    const getAvatar = (announcementType) => {
        if(announcementType === "info")
            return <InfoIcon sx={{ fontSize: 40 , color:"lightblue"}} />
        else if(announcementType === "warn")
            return <WarningAmberIcon sx={{ fontSize: 40 , color:"orange"}} />
        else
            return <MailOutlineIcon sx={{ fontSize: 40, color:"darkblue" }} />

    }

    const markAsRead = () => {
        markAnnouncementAsRead(userContext.user, announcement.announceid)
        .then(res => {
            if(res.status === 200)
            {
                let new_announcement = new KeAnnouncement({...announcement});
                new_announcement.dismissedStatus = "read";
                setAnnouncement(new_announcement);
            } 

        });
    }

    const getAction = (status) => {
        if(status === "read")
            return <CheckCircleOutlineIcon sx={{ fontSize: 40, color:"green" }}/>
        else
            return <MarkEmailReadIcon onClick={(e) => markAsRead()} sx={{ fontSize: 40}}/>
    }

    if(announcement == null)
        return null;
    return <Card sx={{ maxWidth: 500, width:500 }}>
        <CardHeader title={announcement.title} subheader={`Published ${announcement.published}`}
            avatar={getAvatar(announcement.type)}
            action={getAction(announcement.dismissedStatus)}
            />
        <CardContent>
        
        <Typography variant="h5">
            <div dangerouslySetInnerHTML={{__html: announcement.content}}>
            </div>
        </Typography>
        </CardContent>
        
    </Card>
}