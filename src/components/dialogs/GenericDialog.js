import { Dialog, DialogContent, DialogTitle, Button, DialogActions, DialogContentText } from "@mui/material";
import { useEffect, useState } from "react";

export default function GenericDialog({title, text, showDialog, toggleDialog})
{
    const [the_title, setTitle] = useState("");
    const [the_text, setText] = useState("");


    useEffect(() => {
        if(title)
            setTitle(title);
        if(text)
            setText(text);
    }, [title, text]);

    return <Dialog onClose={toggleDialog} open={showDialog}>
            <DialogTitle>{the_title}</DialogTitle>
            <DialogContent>
                <DialogContentText>
                    {the_text}
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button onClick={toggleDialog}>OK</Button>
            </DialogActions>
    </Dialog>
}