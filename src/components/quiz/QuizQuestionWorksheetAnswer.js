import { TextField } from "@mui/material";
import { useEffect, useState } from "react";
import { NumericFormat } from 'react-number-format';

export default function QuizQuestionWorksheetAnswer(
    {answerIndex, answerCallback, initVal, fieldType}
)
{
    const [answerText, setAnswerText] = useState("");
    const [index, setIndex] = useState(null);

    useEffect(() => { if(answerIndex) setIndex(answerIndex)}, [answerIndex])
    useEffect(() => { if(initVal) setAnswerText(initVal)}, [initVal])

    const updateAnswer = (event) => {
      setAnswerText(event.target.value)
      answerCallback(index, event.target.value);
    }

    if(fieldType === "decimal")
      return <NumericFormat value={answerText} customInput={TextField} onChange={updateAnswer} decimalScale={2} fixedDecimalScale />
    else if(fieldType == "integer")
      return <NumericFormat value={answerText} customInput={TextField} onChange={updateAnswer} decimalScale={0} fixedDecimalScale/>
    else
      return <TextField onChange={updateAnswer} value={answerText}></TextField>
}