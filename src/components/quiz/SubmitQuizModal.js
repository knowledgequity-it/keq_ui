import Dialog from "@mui/material/Dialog"
import { DialogTitle, DialogActions, DialogContentText, DialogContent } from "@mui/material";
import { Button } from "@mui/material";
import { BrandingWatermarkOutlined } from "@mui/icons-material";

export default function SubmitQuizModal(props) 
{

return (
<div>
    <Dialog
    open={props.open} 
    onClose={props.close}
    aria-labelledby="alert-dialog-title"
    aria-describedby="alert-dialog-description"
    >
    <DialogTitle id="alert-dialog-title">
        {"Submit the quiz?"}
    </DialogTitle>
    <DialogContent>
        <DialogContentText id="alert-dialog-description">
        Submitting will lock in all your answers. Are you sure you want to submit?
        </DialogContentText>
    </DialogContent>
    <DialogActions>
        <Button onClick={props.close}>Cancel</Button>
        <Button onClick={props.submit} autoFocus>Submit</Button>
    </DialogActions>
    </Dialog>
    </div>
);


}

