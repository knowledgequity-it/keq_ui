import Dialog from "@mui/material/Dialog"
import { DialogTitle, DialogActions, DialogContentText, DialogContent } from "@mui/material";
import { Button } from "@mui/material";


export default function UnansweredWarningModel(props) 
{

return (
<div>
    <Dialog
    open={props.open} 
    onClose={props.close}
    aria-labelledby="alert-dialog-title"
    aria-describedby="alert-dialog-description"
    >
    <DialogTitle id="alert-dialog-title">
        {"Unanswered Questions Found!"}
    </DialogTitle>
    <DialogContent>
        <DialogContentText id="alert-dialog-description">
        You have unanswered questions. Are you sure you want to submit?
        </DialogContentText>
    </DialogContent>
    <DialogActions>
        <Button onClick={props.close}>Cancel</Button>
        <Button onClick={props.submit} autoFocus>Submit</Button>
    </DialogActions>
    </Dialog>
    </div>
);


}

