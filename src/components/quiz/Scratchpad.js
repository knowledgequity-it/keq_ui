import React, { useContext, useEffect, useState, useRef } from "react";
import Drawer from "@mui/material/Drawer";
import { Button, IconButton } from "@mui/material";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import BundledEditor from "../BundledEditor";
import { useLocation } from "react-router-dom";
import CloseIcon from "@mui/icons-material/Close";
import UndoIcon from "@mui/icons-material/Undo";
import RedoIcon from "@mui/icons-material/Redo";
import { QuizContext } from "../../contexts/QuizContext";
import { UserContext } from "../../contexts/userContext";
import {
  writeScratchpadContent,
  getScratchpadContent,
} from "../../facades/QuizFacade";

export default function Scratchpad({
  scratchpadVisible,
  setScratchpadVisible,
  scratchpadHeight,
}) {
  const quizContext = useContext(QuizContext);
  const userContext = useContext(UserContext);
  const editorRef = useRef(null);

  const [initialValue, setInitialValue] = useState(undefined);

  useEffect(() => {
    if (userContext.user !== null && quizContext.quizId !== "") {
      getScratchpadContent(userContext.user, quizContext.quizId)
        .then((response) => response.json())
        .then((data) => {
          let content = data.content !== null ? data.content : "";
          setInitialValue(content);
        });
    }
  }, [quizContext.quizId, userContext.user]);

  const [dirty, setDirty] = useState(false);

  useEffect(() => setDirty(false), [initialValue]);

  const location = useLocation();

  useEffect(() => {
    //when location changes we need to close the scratchpad
    setScratchpadVisible(false);
  }, [location]);

  const save = () => {
    if (editorRef.current) {
      const content = editorRef.current.getContent();
      setDirty(false);
      editorRef.current.setDirty(false);

      writeScratchpadContent(userContext.user, quizContext.quizId, content)
        .then((response) => response.json())
        .then((endData) => {
          console.log("Scratchpad saved");
        });
    }
  };

  const cut = (event) => {
    event.preventDefault();
    if (editorRef.current) {
      quizContext.setScratchpadClipboard(
        editorRef.current.selection.getContent()
      );
      editorRef.current.selection.setContent("");
      editorRef.current.undoManager.add();
    }
    console.log("Scratchpad cut");
  };

  const copy = (event) => {
    event.preventDefault();
    if (editorRef.current) {
      quizContext.setScratchpadClipboard(
        editorRef.current.selection.getContent()
      );
    }
    console.log("Scratchpad copy");
  };

  const paste = (event) => {
    event.preventDefault();
    if (editorRef.current) {
      editorRef.current.selection.setContent(quizContext.scratchpadClipboard);
      editorRef.current.undoManager.add();
    }
    console.log("Scratchpad paste");
  };

  const undo = () => {
    if (editorRef.current) {
      editorRef.current.undoManager.undo();
    }
    console.log("Scratchpad undo");
  };
  const redo = () => {
    if (editorRef.current) {
      editorRef.current.undoManager.redo();
    }
    console.log("Scratchpad redo");
  };

  return (
    <Drawer
      variant="persistent"
      anchor="bottom"
      open={scratchpadVisible}
      sx={{ height: scratchpadHeight }}
      classes={{
        paper: {
          height: scratchpadHeight,
          padding: "3px 20px",
        },
      }}
    >
      <Grid container justifyContent="space-between" alignItems="center">
        <Typography variant="h6">Scratchpad</Typography>
        <Grid item container xs={3} spacing={3} justifyContent="center">
          <IconButton onClick={undo} label="Undo" size="small">
            <UndoIcon />
          </IconButton>
          <IconButton onClick={redo} label="Undo" size="small">
            <RedoIcon />
          </IconButton>
          <Button size="small" onClick={cut}>
            Cut
          </Button>
          <Button size="small" onClick={copy}>
            Copy
          </Button>
          <Button size="small" onClick={paste}>
            Paste
          </Button>
        </Grid>
        <Button
          onClick={() => {
            setScratchpadVisible(false);
          }}
          startIcon={<CloseIcon />}
        >
          Close
        </Button>
      </Grid>
      {quizContext.quizId !== null && (
        <BundledEditor
          onInit={(evt, editor) => (editorRef.current = editor)}
          initialValue={initialValue}
          onDirty={() => setDirty(true)}
          onCut={cut}
          onCopy={copy}
          onPaste={paste}
          onChange={() => save()}
          init={{
            height: 400,
            width: "100%",
            menubar: false,
            plugins: [],
            toolbar: false,
            content_style:
              "body { font-family:Helvetica,Arial,sans-serif; font-size:14px }",
            setup: (editor) => {
              let customClipboard = "";

              editor.ui.registry.addButton("customCopy", {
                text: "Copy",
                onAction: function (_) {
                  customClipboard = editor.selection.getContent();
                },
              });

              editor.ui.registry.addButton("customPaste", {
                text: "Paste",
                onAction: function (_) {
                  editor.selection.setContent(customClipboard);
                  editor.undoManager.add();
                },
              });

              editor.on("copy", function (event) {
                event.preventDefault();
                customClipboard = editor.selection.getContent();
              });

              editor.on("paste", function (event) {
                event.preventDefault();
                if (customClipboard !== "") {
                  if (
                    customClipboard.indexOf("<p>") == -1 &&
                    customClipboard.indexOf("<br>") == -1 &&
                    customClipboard.indexOf("<br/>") == -1
                  ) {
                    customClipboard = customClipboard.split("\n").join("<br>");
                  }
                  editor.selection.setContent(customClipboard);
                  editor.undoManager.add();
                }
              });
            },
          }}
        />
      )}
    </Drawer>
  );
}
