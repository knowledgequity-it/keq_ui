import { Button, Typography } from "@mui/material";
import QuizSubmitButton from "./QuizSubmitButton";

export default function SubmitQuizError({submitQuizCallback})
{

    return <>
    <Typography>An Error Occured submitting your quiz.</Typography>
    <QuizSubmitButton
        onSubmitCallback={submitQuizCallback}
        text={"Try Resubmitting Again"}
        buttontype="small"
      />
    </>
}