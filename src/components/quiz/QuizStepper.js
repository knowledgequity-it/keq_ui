import React, { useEffect, useRef } from "react";
import Stepper from "@mui/material/Stepper";
import Card from "@mui/material/Card";
import Step from "@mui/material/Step";
import { StepLabel } from "@mui/material";
import FlagCircleIcon from '@mui/icons-material/FlagCircle';
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import PanoramaFishEyeIcon from '@mui/icons-material/PanoramaFishEye';
import RadioButtonCheckedIcon from '@mui/icons-material/RadioButtonChecked';

function CustomStepperLabel({isComplete, isFlagged, isCurrent}) {

  

  if(isFlagged )
    return <FlagCircleIcon color="secondary"/>
  else if(isComplete)
    return <CheckCircleIcon color="primary"/>
  else if(isCurrent)
    return <RadioButtonCheckedIcon color="grey"/>
  else if(!isComplete)
    return <PanoramaFishEyeIcon color="grey"/>
  
}

export default function QuizStepper({
  questions,
  activeQuestion,
  isCompletedCallback,
  onStepButtonClickCallback,
}) {

  const stepperRef = useRef(null);
  const questionsRef = useRef(null);

  if(stepperRef.current != null)
  {
    let width = stepperRef.current.scrollWidth;
    let widthPerItem = Math.floor(width/questions.length);
    let currentPosition = stepperRef.current.scrollLeft;

    if(activeQuestion > Math.floor(currentPosition/widthPerItem))
    {
      stepperRef.current.scrollLeft += widthPerItem;
    }
    else if(activeQuestion < Math.floor(currentPosition/widthPerItem))
    {
      stepperRef.current.scrollLeft -= widthPerItem;
    }
  }

  return (
   
      <Card
        sx={{
          overflowX: "scroll",
          p: 3,
          maxWidth: { md: "680px", sm: "450px", xs: "340px" },
        }}
        ref={stepperRef}
        elevation={0}
      >
        <Stepper
          alternativeLabel
          nonLinear
          activeStep={activeQuestion}
          orientation="horizontal"
          ref={questionsRef}
        >
          {questions.map((question, index) => (
            <Step completed={isCompletedCallback(index)} key={question.id}>
              <StepLabel onClick={onStepButtonClickCallback(index)} 
                        StepIconComponent={(props) => <CustomStepperLabel 
                                                            isComplete={isCompletedCallback(index)} 
                                                            isFlagged={question.flaggedForReview}
                                                            isCurrent={activeQuestion == index }/>}>
                Q{index + 1}

              </StepLabel>
            </Step>
          ))}
        </Stepper>
      </Card>

  );
}
