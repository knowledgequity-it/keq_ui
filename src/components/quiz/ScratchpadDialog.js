import React, { useContext, useEffect, useState, useRef } from "react";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import Paper from "@mui/material/Paper";
import Draggable from "react-draggable";
import { Button } from "@mui/material";
import BundledEditor from "../BundledEditor";

function PaperComponent(props) {
  const nodeRef = useRef(null);

  return (
    <Draggable
      nodeRef={nodeRef}
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper ref={nodeRef} {...props} />
    </Draggable>
  );
}

export default function Scratchpad({ open, handleClose }) {
  const editorRef = useRef(null);

  const [customClipboard, setCustomClipboard] = useState("");

  return (
    <div>
      <Dialog
        disableEnforceFocus
        hideBackdrop
        open={open}
        onClose={handleClose}
        PaperComponent={PaperComponent}
        aria-labelledby="draggable-dialog-title"
        style={{ pointerEvents: "none" }}
        PaperProps={{
          style: {
            pointerEvents: "auto",
          },
        }}
      >
        <DialogTitle style={{ cursor: "move" }} id="draggable-dialog-title">
          Scratchpad
        </DialogTitle>
        <DialogContent>
          <BundledEditor
            onInit={(evt, editor) => (editorRef.current = editor)}
            initialValue="<p>This is the initial content of the editor.</p>"
            init={{
              height: 500,
              width: 500,
              menubar: false,
              plugins: [],
              toolbar: "undo redo | customCut customCopy customPaste | ",
              content_style:
                "body { font-family:Helvetica,Arial,sans-serif; font-size:14px }",
              setup: (editor) => {
                let customClipboard = "";

                editor.ui.registry.addButton("customCut", {
                  text: "Cut",
                  onAction: function (_) {
                    customClipboard = editor.selection.getContent();
                    editor.selection.setContent("");
                    editor.undoManager.add();
                  },
                });

                editor.ui.registry.addButton("customCopy", {
                  text: "Copy",
                  onAction: function (_) {
                    customClipboard = editor.selection.getContent();
                  },
                });

                editor.ui.registry.addButton("customPaste", {
                  text: "Paste",
                  onAction: function (_) {
                    editor.selection.setContent(customClipboard);
                    editor.undoManager.add();
                  },
                });

                editor.on("copy", function (event) {
                  event.preventDefault();
                  customClipboard = editor.selection.getContent();
                });

                editor.on("paste", function (event) {
                  event.preventDefault();
                  if (customClipboard !== "") {
                    if (
                      customClipboard.indexOf("<p>") == -1 &&
                      customClipboard.indexOf("<br>") == -1 &&
                      customClipboard.indexOf("<br/>") == -1
                    ) {
                      customClipboard = customClipboard
                        .split("\n")
                        .join("<br>");
                    }
                    editor.selection.setContent(customClipboard);
                    editor.undoManager.add();
                  }
                });
              },
            }}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Close
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
