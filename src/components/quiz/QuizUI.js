import React, { useContext, useState } from "react";
import Grid from "@mui/material/Grid";
import QuestionAnswers from "./QuestionAnswers";
import QuizStepper from "./QuizStepper";
import QuizQuestionContent from "./QuizQuestionContent";
import QuizNextAndBackButtons from "./QuizNextAndBackButtons";
import QuizNetworkSpeedCheck from "../QuizNetworkSpeedCheck";
import useLiveLogging from "../../hooks/useLiveLogging";
import Timer from "../Timer";
import { QuizContext } from "../../contexts/QuizContext";
import SubmitQuizModal from "./SubmitQuizModal";
import UnansweredWarningModel from "./UnansweredWarningModel";
import QuizSubmitButton from "./QuizSubmitButton";
import NoteIcon from "@mui/icons-material/Note";
import Button from "@mui/material/Button";
import Box from '@mui/material/Box';

export default function QuizUI({
  quiz,
  quiz_id,
  submitQuizCallback,
  submitAnswerCallback,
  quizDuration,
  timerFunction,
  scratchpadVisible,
  setScratchpadVisible,
  hasScratchpad,
}) {

  const quizContext = useContext(QuizContext);
  const candidateAction = useLiveLogging();
  const [activeQuestion, setActiveQuestionIndex] = useState(0);
  const [submitClicked, setSubmitClicked] = useState(false);
  const [unanswereWarningDismissed, setUnanswereWarningDismissed] = useState(false);


  const openSubmitDialog = () => {
    setSubmitClicked(true);
  };

  const setSelectedAnswer = (index, answer, type) => {
    quiz.updateUserAnswer(index, answer, type);
    
    submitAnswerCallback(
      quiz.getQuestionId(index),
      quiz.getCurrentUserAnswersForQuestions(index).join(",")
    );
    candidateAction({
      component: "Quiz",
      activity: "Question Answered",
      event: `Answer Clicked. Question ID ${quiz.getQuestionId(
        activeQuestion
      )} Answer ${Array.isArray(answer) ? answer.join(",") : answer}`,
    });
   
  };

  const toggleFlagForReview = (index) => {
    console.log("Flagging ", index, " for review", quiz.getQuestion(index).flaggedForReview);
    quiz.getQuestion(index).flaggedForReview ? quiz.unFlagQuestionForReview(index) : quiz.flagQuestionForReview(index);
  }

  const handleStep = (step) => () => {
    if (step >= quiz.totalQuestions) return;
    setActiveQuestionIndex(step);
    candidateAction({
      component: "Quiz",
      activity: "Quiz",
      event: "Step Clicked. Activating Question " + step,
    });
  };
  const handleBack = () => {
    setActiveQuestionIndex(activeQuestion - 1);
    candidateAction({
      component: "Quiz",
      activity: "Quiz",
      event: "Back Button Clicked. Activating Question " + activeQuestion - 1,
    });
  };

  const handleNext = () => {
    if (activeQuestion > quiz.totalQuestions) return;

    setActiveQuestionIndex(activeQuestion + 1);

    candidateAction({
      component: "Quiz",
      activity: "Quiz",
      event: "Next button Clicked. Activating Question " + (activeQuestion + 1),
    });
  };

  const closeSubmitDialog = (event, reason) => {
    setSubmitClicked(false);
  };

  const getTimeFromContext = () => {
    return quizContext.quizData[quiz_id];
  };

  if (!quiz) return null;

  let submitButton = null;

  if (quiz.questions.length === activeQuestion + 1)
    submitButton = (
      <QuizSubmitButton
        onSubmitCallback={openSubmitDialog}
        text={quiz.title}
        buttontype="small"
      />
    );

  return (
    <React.Fragment>
      <UnansweredWarningModel
        open={
          submitClicked &&
          quiz.hasUnansweredQuestions() &&
          !unanswereWarningDismissed
        }
        close={closeSubmitDialog}
        submit={() => {
          setUnanswereWarningDismissed(true);
        }}
      />
      <SubmitQuizModal
        open={
          submitClicked &&
          (!quiz.hasUnansweredQuestions() || // and there are no unanswered _questions
            (quiz.hasUnansweredQuestions() && unanswereWarningDismissed)) // or there were unanswered _questions but user dismiised the warning
        }
        close={closeSubmitDialog}
        submit={submitQuizCallback}
      />
      <Grid
        container
        direction="column"
        justifyContent="center"
        alignItems="stretch"
        spacing={3}
      >
        <Grid item xs={12}>
          <Grid
            container
            direction="row"
            justifyContent="space-around"
            alignItems="center"
            spacing={3}
          >
            <Grid item xs={12} sm={4} md={3}>
              <Grid
                container
                direction="column"
                justifyContent="center"
                alignItems="center"
              >
                <Timer
                  time={getTimeFromContext()}
                  tickFunc={timerFunction}
                  duration={quizDuration}
                />
                <QuizNetworkSpeedCheck />
                <Box style={{margin:"10px", fontSize:"1em"}}>
                  <QuizSubmitButton
                    onSubmitCallback={openSubmitDialog}
                    text={quiz.title}
                    buttontype="big"
                  />
                </Box>
                {hasScratchpad && (
                  <Button
                    startIcon={<NoteIcon />}
                    onClick={() => {
                      setScratchpadVisible(!scratchpadVisible);
                    }}
                  >
                    Scratchpad
                  </Button>
                )}
              </Grid>
            </Grid>
            <Grid item xs={12} sm={8} md={9}>
              <QuizStepper
                questions={quiz.questions}
                activeQuestion={activeQuestion}
                isCompletedCallback={(index) => quiz.questionHasAnswer(index)}
                onStepButtonClickCallback={handleStep}
              />
            </Grid>
          </Grid>
        </Grid>
        <Grid item>
          <QuizQuestionContent
            key={quiz.getQuestionId(activeQuestion)}
            index={activeQuestion}
            question={quiz.getQuestion(activeQuestion)}
            toggleFlagged={toggleFlagForReview}
            quizName={quiz.title}
          />
        </Grid>
        <Grid item>
          <QuestionAnswers
            key={quiz.getQuestionId(activeQuestion)}
            index={activeQuestion}
            question={quiz.getQuestion(activeQuestion)}
            setAnswerCallback={setSelectedAnswer}
            hasScratchpad={quiz.hasScratchpad}
          />
        </Grid>
        <Grid item container justifyContent="flex-start">
          <QuizNextAndBackButtons
            currentIndex={activeQuestion}
            maxIndex={quiz.totalQuestions - 1}
            onNextCallback={handleNext}
            onPreviousCallback={handleBack}
          />
          <Box style={{padding:"10px"}}>
          {submitButton}
          </Box>
          
        </Grid>
      </Grid>
    </React.Fragment>
  );
}
