
export default class Quiz{

    constructor(quiz)
    {
        this.quiz = quiz;
        this.removeAmpersands()
        this.removeEmptyAnswerStrings();
    }

    removeEmptyAnswerStrings()
    {
     this.quiz.questions.map(q => {
        if(q.type === "multiple")
        {
            if(q.user_answer.length == 1 && q.user_answer[0] === '')
                q.user_answer = [];
        }
     })
    }

    removeAmpersands()
    {
        this.quiz.questions.map((question) => {
            if (question.options) {
              question.options = question.options.map((option) =>
                option.replace(/&amp;/gi, "\u0026")
              );
            }
            return question;
          });
    }

    
    get questions(){ return this.quiz?.questions}
    get id() { return this.quiz.id }
    get totalQuestions(){return this.quiz.questions.length}
    get title(){ return this.quiz.title }
    get course(){ return this.quiz.quiz_course }
    get hasScratchpad(){ return this.quiz.has_scratchpad }
    get quizCourseId() {return this.quiz.quiz_course_id }
    get quizCourseName(){ return this.quiz.quiz_course}
    get status(){ return this.quiz.status }
    

    getQuestion(index) 
    {
        return this.quiz.questions[index]
    }

    getQuestionId(index)
    {
        return this.quiz.questions[index].id
    }
    getQuestionOptions(index)
    {
        return this.quiz.questions[index].options
    }
    
    getQuestionType(index)
    {
        return this.quiz.questions[index].type
    }

    getCurrentUserAnswersForQuestions(index)
    {
        return this.quiz.questions[index].user_answer
    }

    flagQuestionForReview(index)
    {
        this.quiz.questions[index].flaggedForReview = true;
    }

    unFlagQuestionForReview(index)
    {
        this.quiz.questions[index].flaggedForReview = false;
    }

    updateMultiChoiceUserAnswer(index, selection)
    {
        if(this.quiz.questions[index].type !== 'multiple')
            return
/*
        let foundIndex = this.quiz.questions[index].user_answer.indexOf(selection);
        if (foundIndex < 0) 
        {
            this.quiz.questions[index].user_answer.push(selection);
        } 
        else 
        {
            this.quiz.questions[index].user_answer.splice(foundIndex, 1);
        }
        */
        this.quiz.questions[index].user_answer = selection
    }

    updateSingleChoiceUserAnswer(index, selection)
    {
        if(this.quiz.questions[index].type !== 'single')
            return

        this.quiz.questions[index].user_answer[0] = selection;
    }

    updateLongTextUserAnswer(index, newText)
    {
        if(this.quiz.questions[index].type !== 'largetext')
            return
        
        this.quiz.questions[index].user_answer[0] = newText
    }

    updateFillBlankUserAnswer(index, answers)
    {
        if(this.quiz.questions[index].type !== 'fillblank')
            return
        this.quiz.questions[index].user_answer[0] = answers
    }
    updateUserAnswer(index, selection, type)
    {
        switch(type)
        {
            case "single":
                this.updateSingleChoiceUserAnswer(index, selection)
                break;
            case "multiple":
                this.updateMultiChoiceUserAnswer(index, selection)
                break;
            case "largetext":
                this.updateLongTextUserAnswer(index, selection)
                break;
            case "fillblank":
                this.updateFillBlankUserAnswer(index, selection)
                break;
            default:
                return;
        }
    }

    questionHasAnswer(index)
    {
        let user_answer = this.quiz.questions[index].user_answer;

        if (
            user_answer !== null &&
            user_answer.length !== 0 &&
            user_answer[0] !== ""
            )
            return true;
        else 
            return false;
    }

    hasUnansweredQuestions()
    {
        return this.quiz.questions.findIndex((question, index) => {
            return !this.questionHasAnswer(index);
          }) > -1
            ? true
            : false;
    }


}