import React, { useState, useEffect, useContext } from "react";
import { useTheme } from "@mui/material/styles";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Grid from "@mui/material/Grid";
import { CircularProgress } from "@mui/material";
import useLiveLogging from "../../hooks/useLiveLogging";
import { getUserCourseQuizInstructions } from "../../facades/CourseFacade";
import { UserContext } from "../../contexts/userContext";
import Box from "@mui/material/Box";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import AssignmentIcon from "@mui/icons-material/Assignment";
import AssessmentIcon from "@mui/icons-material/Assessment";
import QuizStats from "./QuizStats";
import { useLocation } from "react-router-dom";
import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  DialogContentText,
} from "@mui/material";

const epochToFullDateTime = (epochValue) => {
  let dateObject = new Date(epochValue * 1000);
  let timeStamp = dateObject.toLocaleTimeString([], {
    year: "numeric",
    month: "2-digit",
    day: "2-digit",
    hour: "2-digit",
    minute: "2-digit",
  });

  //let fullDateTime = Intl.DateTimeFormat('en-GB', { dateStyle: 'full', timeStyle: 'long' }).format(timeStamp);
  return timeStamp;
};

const QuizStartButton = ({ text, startCallback }) => {
  const theme = useTheme();
  const [confirmDialogOpen, setConfirmDialogOpen] = useState(false);

  const startQuiz = () => {
    if (
      text.toLowerCase().includes("practice exam") ||
      text.toLowerCase().includes("mid semester")
    ) {
      if (!text.toLowerCase().includes("continue")) setConfirmDialogOpen(true);
      else if (text.toLowerCase().includes("retake"))
        setConfirmDialogOpen(true);
      else startCallback();
    } else startCallback();
  };

  const closeDialog = () => {
    setConfirmDialogOpen(false);
  };

  const cancelDialog = () => {
    setConfirmDialogOpen(false);
  };

  const confirmDialog = () => {
    setConfirmDialogOpen(false);
    startCallback();
  };

  return (
    <React.Fragment>
      <Button
        variant="contained"
        color="primary"
        onClick={startQuiz}
        sx={{
          margin: theme.spacing(1),
          height: "80px",
          width: "350px",
          fontSize: "1em",
          fontWeight: "bold",
          color: "darkslategrey",
          padding: "15px",
          fontSize: "1em",
          margin: "10px",
        }}
      >
        {text}
      </Button>
      <Dialog open={confirmDialogOpen} onClose={closeDialog}>
        <DialogTitle id="alert-dialog-title">{text}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Do you want to start your{" "}
            {text.toLowerCase().replace("start ", "").replace("retake", "")}?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={cancelDialog}>Cancel</Button>
          <Button onClick={confirmDialog}>Start</Button>
        </DialogActions>
      </Dialog>
    </React.Fragment>
  );
};

// const QuizTabs = withStyles({
//   root: {
//     borderBottomColor: "grey.300",
//   },
//   flexContainer: {
//     justifyContent: "flex-end",
//   },
// })(Tabs);

// const QuizTab = withStyles({
//   root: {
//     "&:hover": {
//       color: "#40a9ff",
//       opacity: 1,
//     },
//     minWidth: "unset",
//   },
// })((props) => <Tab disableRipple {...props} />);

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <Box
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      borderTop={1}
      borderColor="grey.300"
      paddingTop={3}
      {...other}
    >
      {value === index && <Box>{children}</Box>}
    </Box>
  );
}

export default function StartQuizPanel({
  statusData: {
    status,
    retakes_allowed,
    retakes_count,
    retakes_remaining,
    last_attempted,
    question_count,
    duration,
  },
  lastTakenTime,
  startCallback,
  checkResultsCallback,
  quiz_id,
  buttonText,
}) {
  const theme = useTheme();
  const candidateAction = useLiveLogging();
  const userContext = useContext(UserContext);
  const [instructions, setInstructions] = useState(null);
  const [post_quiz_message, setPostQuizMessage] = useState(null);
  const [quiz_type, setQuizType] = useState("Quiz");

  const [value, setValue] = useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const renderHTML = (rawHTML: string) =>
    React.createElement("span", {
      dangerouslySetInnerHTML: { __html: rawHTML },
    });

  useEffect(() => {
    if (instructions) {
      if (
        instructions.toLowerCase().includes("practice exam") &&
        !instructions.toLowerCase().includes("revision quiz")
      ) {
        setQuizType("Practice Exam");
      } else if (instructions.toLowerCase().includes("mid-semester test")) {
        setQuizType("Mid-Semester Test");
      } else {
        setQuizType("Quiz");
      }
    }
  }, [instructions]);

  useEffect(() => {
    if (!quiz_id) return;
    else {
      getUserCourseQuizInstructions(quiz_id, userContext.user)
        .then((response) => response.json())
        .then((instructions) => {
          setInstructions(instructions.content);
          setPostQuizMessage(instructions.post_quiz_message);
        });
    }
  }, [quiz_id, userContext.user]);

  const location = useLocation();

  useEffect(() => {
    //when location changes default to results tab
    setValue(0);
  }, [location]);

  if (status === null) {
    return <CircularProgress color="primary" />;
  }

  let button_text = "Start Quiz";
  let message = "";
  let show_button = true;
  let show_retry_info = false;
  let text = null;

  if (parseInt(status) === 4 && retakes_remaining > 0) {
    candidateAction({
      component: "Quiz",
      activity: "Quiz",
      event: "Quiz Start Status 4. Remaining Retakes: " + retakes_remaining,
    });

    message = `You attempted this ${quiz_type} on ${epochToFullDateTime(
      last_attempted
    )}`;
    button_text = `Retake ${quiz_type}`;
    text = renderHTML(post_quiz_message);
    show_retry_info = true;
  } else if (parseInt(status) === 1) {
    candidateAction({
      component: "Quiz",
      activity: "Quiz",
      event: "Quiz Start Status 1. Remaining Retakes: " + retakes_remaining,
    });
    message = `You have already started this ${quiz_type} at  ${epochToFullDateTime(
      last_attempted
    )} click the button below to continue.`;
    button_text = `Continue ${quiz_type}`;
    text = renderHTML(instructions);
    show_retry_info = true;
  } else if (status === "" || parseInt(status) === 0) {
    candidateAction({
      component: "Quiz",
      activity: "Quiz",
      event: "Quiz Start Status not 4. Remaining Retakes: " + retakes_remaining,
    });
    button_text = `Start ${quiz_type}`;
    text = renderHTML(instructions);
  } else if (retakes_remaining <= 0) {
    candidateAction({
      component: "Quiz",
      activity: "Quiz",
      event: "Remaining Retakes: " + retakes_remaining,
    });
    button_text = `Retake ${quiz_type}`;
    text = renderHTML(post_quiz_message);
    message = "You have no retakes left.";
    show_button = false;
  }

  function secondsToHms(d) {
    d = Number(d);
    var h = Math.floor(d / 3600);
    var m = Math.floor((d % 3600) / 60);
    var s = Math.floor((d % 3600) % 60);

    var hDisplay = h > 0 ? h + (h == 1 ? " hour " : " hours ") : "";
    var mDisplay = m > 0 ? m + (m == 1 ? " minute " : " minutes ") : "";
    var sDisplay = s > 0 ? s + (s == 1 ? " second" : " seconds") : "";
    return hDisplay + mDisplay + sDisplay;
  }

  var the_instructions = <CircularProgress />;

  if (instructions !== null)
    the_instructions = <Typography component="div">{text}</Typography>;
  let quiz_info = null;

  let retry_info;

  if (show_retry_info) {
    retry_info = (
      <Typography component="span">
        Number of retakes:{" "}
        <Box
          component="span"
          fontWeight="bold"
          sx={{
            color: "grey.100",
          }}
        >
          {retakes_count} out of {retakes_allowed}
        </Box>
      </Typography>
    );
  }

  if (duration && question_count)
    quiz_info = (
      <>
        <Typography component="span">
          This {quiz_type} has{" "}
          <Box component="span" fontWeight="bold">
            {question_count}
          </Box>{" "}
          questions and is{" "}
          <Box component="span" fontWeight="bold">
            {secondsToHms(duration)}
          </Box>{" "}
          long. {retry_info}
        </Typography>
      </>
    );

  return (
    <>
      <Tabs
        sx={{
          ".MuiTabs-root": {
            borderBottomColor: "grey.300",
          },
          "	.MuiTabs-flexContainer": {
            justifyContent: "flex-end",
          },
        }}
        value={value}
        onChange={handleChange}
        aria-label="tabs"
      >
        <Tab
          disableRipple
          sx={{
            ".MuiTab-root": {
              "&:hover": {
                color: "#40a9ff",
                opacity: 1,
              },
              minWidth: "unset",
            },
          }}
          icon={<AssignmentIcon />}
        />
        <Tab
          disableRipple
          sx={{
            ".MuiTab-root": {
              "&:hover": {
                color: "#40a9ff",
                opacity: 1,
              },
              minWidth: "unset",
            },
          }}
          icon={<AssessmentIcon />}
        />
      </Tabs>
      <TabPanel value={value} index={0}>
        {retakes_allowed > 0 && (
          <Typography color="error">{message}</Typography>
        )}
        {the_instructions}
        {show_button && quiz_info}
        <Grid
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
          }}
        >
          {show_button && (
            <QuizStartButton text={button_text} startCallback={startCallback} />
          )}

          {parseInt(status) === 4 && (
            <Button
              variant="contained"
              color="primary"
              onClick={checkResultsCallback}
              sx={{
                margin: theme.spacing(1),
                height: "80px",
                width: "350px",
                fontSize: "1em",
                fontWeight: "bold",
                color: "darkslategrey",
                padding: "15px",
                fontSize: "1em",
                margin: "10px",
              }}
            >
              Check Results
            </Button>
          )}
        </Grid>
      </TabPanel>
      <TabPanel value={value} index={1}>
        <QuizStats quiz_id={quiz_id} />
      </TabPanel>
    </>
  );
}
