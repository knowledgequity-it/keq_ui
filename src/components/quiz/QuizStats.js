import React, { useContext, useEffect, useState } from "react";
import { makeStyles } from "@mui/material/styles";
import Typography from "@mui/material/Typography";
import { UserContext } from "../../contexts/userContext";
import Button from "@mui/material/Button";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import Link from "@mui/material/Link";
import { Chart as ChartJS, ArcElement, Tooltip, Legend } from "chart.js";
import { Doughnut } from "react-chartjs-2";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import { quizStatsGraph, quizStats } from "../../facades/QuizFacade";
import { CircularProgress } from "@mui/material";

ChartJS.register(ArcElement, Tooltip, Legend);

// export const data = {
//   labels: ["score ", "Blue", "Yellow", "Green", "Purple", "Orange"],
//   datasets: [
//     {
//       label: "# of Votes",
//       data: [12, 19, 3, 5, 2, 3],
//       backgroundColor: [
//         "rgba(255, 99, 132, 0.8)",
//         "rgba(54, 162, 235, 0.8)",
//         "rgba(255, 206, 86, 0.8)",
//         "rgba(75, 192, 192, 0.8)",
//         "rgba(153, 102, 255, 0.8)",
//         "rgba(255, 159, 64, 0.8)",
//       ],
//       borderColor: ["white", "white", "white", "white", "white", "white"],
//       borderWidth: 4,
//     },
//   ],
// };

export default function QuizStats({
  quiz_id,
  onRetakeClicked,
  course_url,
  quiz_name,
}) {
  const [buttonText, setButtonText] = useState("Retake Quiz");
  const userContext = useContext(UserContext);
  const [statsGraph, setStatsGraph] = useState(null);
  const [statsData, setStatsData] = useState(null);

  useEffect(() => {
    if (!quiz_id) return;
    else {
      quizStatsGraph(userContext.user, quiz_id)
        .then((response) => response.json())
        .then((quiz) => {
          setStatsGraph(quiz.graph);
        });

      quizStats(userContext.user, quiz_id)
        .then((response) => response.json())
        .then((quiz) => {
          setStatsData(quiz.stats);
        });
    }
  }, [quiz_id]);
  return (
    <>
      <Grid container justifyContent="center" spacing={3}>
        <Grid item xs={12}>
          <Typography variant="h5" component="h2">
            Statistics
          </Typography>
        </Grid>
        <Grid item container xs={8} justifyContent="center">
          {!statsGraph && <CircularProgress />}
          {statsGraph && (
            <Doughnut
              data={{
                labels: statsGraph.labels,
                datasets: [
                  {
                    label: "My First Dataset",
                    data: statsGraph.data,
                    backgroundColor: [
                      "rgb(255, 99, 132)",
                      "rgb(54, 162, 235)",
                      "rgb(255, 205, 86)",
                      "rgb(112, 201, 137)",
                    ],
                  },
                ],
              }}
              height={400}
              options={{ maintainAspectRatio: false }}
            />
          )}
        </Grid>
        <Grid item container xs={12} justifyContent="center">
          {!statsData && <CircularProgress />}
          {statsData && (
            <TableContainer component={Paper} elevation={0}>
              <Table>
                <TableBody>
                  <TableRow>
                    <TableCell component="th" scope="row">
                      Average (marks)
                    </TableCell>
                    <TableCell align="right">{statsData.avg}</TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell component="th" scope="row">
                      Max (marks)
                    </TableCell>
                    <TableCell align="right">{statsData.max}</TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell component="th" scope="row">
                      Mode (marks)
                    </TableCell>
                    <TableCell align="right">{statsData.mode}</TableCell>
                  </TableRow>
                </TableBody>
              </Table>
            </TableContainer>
          )}
        </Grid>
        <Grid item xs={12}>
          <Typography variant="h5" component="h2">
            Leaderboard
          </Typography>
        </Grid>
        <Grid item container xs={12} justifyContent="center">
          {!statsData && <CircularProgress />}
          {statsData && (
            <TableContainer component={Paper} elevation={0}>
              <Table>
                <TableBody>
                  {statsData.results.map((user) => {
                    return (
                      <TableRow key={user.user_id}>
                        <TableCell component="th" scope="row">
                          {user.username}
                        </TableCell>
                        <TableCell align="right">{user.marks}</TableCell>
                      </TableRow>
                    );
                  })}
                </TableBody>
              </Table>
            </TableContainer>
          )}
        </Grid>
      </Grid>
    </>
  );
}
