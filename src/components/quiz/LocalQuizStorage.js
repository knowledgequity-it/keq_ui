export default class LocalQuizStorage
{
    constructor(){
        this.UNSUBMITTED_ANSWERS = 'unsubmitted-answers'
    }

    addAnswerToLocalStorage(quizId, questionId, answer)
    {
        let answers = JSON.parse(localStorage.getItem(this.UNSUBMITTED_ANSWERS));
        if(answers === null)
            localStorage.setItem(this.UNSUBMITTED_ANSWERS, JSON.stringify({}))
        
        let quizAnswers = JSON.parse(localStorage.getItem(this.UNSUBMITTED_ANSWERS));//get answers for this quiz
        if(!quizAnswers[quizId])
            quizAnswers[quizId] = {}
        
        quizAnswers[quizId][questionId] = answer;
        localStorage.setItem(this.UNSUBMITTED_ANSWERS, JSON.stringify(quizAnswers));
    }

    removeAnswerFromLocalStorage(quizId, questionId)
    {
        let quizAnswers = JSON.parse(localStorage.getItem(this.UNSUBMITTED_ANSWERS))
        
        if(quizAnswers[quizId] && quizAnswers[quizId][questionId])
                delete quizAnswers[quizId][questionId]

        localStorage.setItem(this.UNSUBMITTED_ANSWERS, JSON.stringify(quizAnswers))

    }

    removeQuizFromLocalStorage(quizId)
    {
        let allQuizzes = JSON.parse(localStorage.getItem(this.UNSUBMITTED_ANSWERS))
        delete allQuizzes[quizId]
        localStorage.setItem(this.UNSUBMITTED_ANSWERS, JSON.stringify(allQuizzes))
    }
    getAllQuizAnswersFromLocalStorage(quizId)
    {
        let unsubmitted = localStorage.getItem(this.UNSUBMITTED_ANSWERS);
        if(unsubmitted != null)
            return JSON.parse(localStorage.getItem(this.UNSUBMITTED_ANSWERS))[quizId]
        else
            return null;
    }

    removeAllQuizAnswersFromLocalStorage(quizId)
    {
        let allQuizzes = JSON.parse(localStorage.getItem(this.UNSUBMITTED_ANSWERS))
        delete allQuizzes[quizId]
    }
}