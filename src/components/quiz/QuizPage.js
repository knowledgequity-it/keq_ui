import React, { useContext, useEffect, useState } from "react";

import { getUserCourseQuiz } from "../../facades/CourseFacade";
import { UserContext } from "../../contexts/userContext";
import { QuizContext } from "../../contexts/QuizContext";
import {
  startQuiz,
  submitQuestionAnswer,
  quizStatus,
  submitQuiz,
  getServerTime,
  quizCourse,
} from "../../facades/QuizFacade";
import Grid from "@mui/material/Grid";
import useLiveLogging from "../../hooks/useLiveLogging";
import PostQuizDisplay from "./PostQuizDisplay";
import QuizUI from "./QuizUI";
import StartQuizPanel from "./StartQuizPanel";
import Link from "@mui/material/Link";
import MarkUnitCompletePanel from "../course/MarkUnitCompletePanel";
import {
  ContactsOutlined,
  StayCurrentLandscapeTwoTone,
} from "@mui/icons-material";
import { CircularProgress } from "@mui/material";
import {
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  DialogContentText,
  Typography,
} from "@mui/material";
import Quiz from "./Quiz";
import LocalQuizStorage from "./LocalQuizStorage";
import { useLocation } from "react-router-dom";
import SubmitQuizError from "./SubmitQuizError.";

export default function QuizPage({
  quiz_id,
  onUnitCompleted,
  scratchpadVisible,
  setScratchpadVisible,
  unit_return,
}) {
  const userContext = useContext(UserContext);
  const quizContext = useContext(QuizContext);
  const candidateAction = useLiveLogging();
  //quiz state vars
  //const [questions, setQuiz] = useState([]);
  const [statusData, setStatus] = useState(null);
  const [startData, setStartData] = useState(null);
  const [quiz, setQuiz] = useState(null);
  const [course_url, setCourseUrl] = useState("");
  //const [quiz_name, setQuizName] = useState("");
  //const [has_scratchpad, setHasScratchpad] = useState(false);
  const [showErrorDialog, setShowErrorDialog] = useState(false);
  const [quizIsLoading, setQuizIsLoading] = useState(false);

  //state vars that determine whether to display start/quiz/end pages
  const [showPreQuizPage, setShowPreQuizPage] = useState(true);
  const [showPostQuizPage, setShowPostQuizPage] = useState(false);
  const [showQuizPage, setShowQuizPage] = useState(false);
  const [showSubmitError, setShowSubmitError] = useState(false);
  const [error_message, setErrorMessage] = useState("");


  const LOCAL_QUIZ_STORAGE = new LocalQuizStorage();

  let url_param = new URLSearchParams(useLocation().search).get("ru");

  const normaliseUri = (uri) => {
    let normalised = uri.replace(`${process.env.REACT_APP_AB_URL_PREFIX}`, "");
    if (normalised.substr(-1) == "/")
      normalised = normalised.substring(0, normalised.length - 1);
    return normalised;
  };

  useEffect(() => {
    if (!quiz_id) return;
    else {
      quizCourse(userContext.user, quiz_id)
        .then((response) => response.json())
        .then((quiz) => {
          if(!quiz.quiz_course && !url_param)//quiz not attached to course, and no ru url param
          {
            let parts = normaliseUri(window.location.pathname).split("/");
            if (parts.length === 4)
              //course url with unit part (/course/coursename/unitname) return last bit
              setCourseUrl(parts[2]);
          }
          else
            setCourseUrl(quiz.quiz_course);
        });

      quizStatus(userContext.user, quiz_id)
        .then((response) => response.json())
        .then((statusData) => {
          setStatus(statusData); //'en-GB', { dateStyle: 'full', timeStyle: 'long' } 'en-US', {year: 'numeric', month: '2-digit',day: '2-digit', hour: '2-digit', minute: '2-digit', second: '2-digit'}
          setShowQuizPage(false);
          setShowPreQuizPage(true);
          setShowPostQuizPage(false);
        });
      quizContext.setQuizId(quiz_id);
    }
  }, [quiz_id, userContext.user]);

  const getQuizQuestions = () => {
    return getUserCourseQuiz(quiz_id, userContext.user)
      .then((response) => response.json())
      .then((quiz) => {
        const q = new Quiz(quiz);
        setQuiz(q);
        //setCourseUrl(quiz.quiz_course);
        //setQuizName(quiz.title);
        //setHasScratchpad(quiz.has_scratchpad);

        //setQuiz(questions);
        candidateAction({
          component: "Quiz",
          activity: "Quiz",
          event: "Succesfully loaded quiz questions",
        });
      })
      .catch((error) => {
        candidateAction({
          component: "Quiz",
          event: `Failed to load Quiz Questions ${quiz_id}`,
          error: error,
        });
      });
  };

  const timerTickFunc = (time) => {
    let times = quizContext.quizData;
    times[quiz_id] = time;

    getServerTime()
      .then((response) => response.json())
      .then((data) => {
        let t = parseInt(startData["expire-time"]) - parseInt(data["body"]);
        times[quiz_id] = t;
        quizContext.setQuizData(times);
      });

    if (time <= 0) {
      doSubmitQuiz();
    }
  };

  const doSubmitQuiz = () => {
    console.log("submitting");
    let savePromises = [];
    let unsavedAnswers =
      LOCAL_QUIZ_STORAGE.getAllQuizAnswersFromLocalStorage(quiz_id);
    if (unsavedAnswers) {
      for (const [key, value] of Object.entries(unsavedAnswers)) {
        savePromises.push(sendQuestonAnswerToServer(key, value));
      }
    }
    //check questions are submitted
    unsavedAnswers =
      LOCAL_QUIZ_STORAGE.getAllQuizAnswersFromLocalStorage(quiz_id);
    if (unsavedAnswers && Object.entries(unsavedAnswers).length === 0)
      LOCAL_QUIZ_STORAGE.removeQuizFromLocalStorage(quiz_id);

    //TODO: Do something for the case when there is still no internet connection. Display some kind of message etc.
    Promise.all(savePromises)
      .then((values) => submitQuiz(userContext.user, quiz_id))
      .then((response) => response.json())
      .then((endData) => {
        if (typeof onUnitCompleted == "function") {
          onUnitCompleted();
        }
        if (typeof setScratchpadVisible === "function")
          setScratchpadVisible(false);
        setShowPostQuizPage(true);
      })
      .catch((error) => {
        //error occurred while submitting
        setShowSubmitError(true);
        console.log("ERROR");
      })
      .then(() => {
        setShowQuizPage(false);
        setShowPreQuizPage(false);

        candidateAction({
          component: "Quiz",
          activity: "Submitted Quiz",
          event: `User Submitted Quiz ${quiz_id}`,
        });
      });
  };

  const handleStart = () => {
    candidateAction({
      component: "Quiz",
      activity: "Quiz",
      event: `User Click the start button quiz ${quiz_id}`,
    });
    setQuizIsLoading(true);
    startQuiz(quiz_id, userContext.user)
      .then((response) => response.json())
      .then((data) => {
        //check if error response (eg too many retakes)
        if (data["status"] !== "error") {
          //console.log("START", quiz_id, data, quizContext.quizData)
          setStartData(data);
          setQuiz([]);
          let time = data["expire-time"] - parseInt(data["stime"]);
          let qData = quizContext.quizData;
          qData[quiz_id] = time;
          quizContext.setQuizData(qData);
          candidateAction({
            component: "Quiz",
            activity: "Quiz",
            event: `Sucessfully loaded Quiz Start Data for quiz ${quiz_id}`,
          });
        } else {
          return Promise.reject({ message: data["message"] });
        }
      })
      .then((res) => {
        return getQuizQuestions();
      })
      .then((res) => {
        setShowPreQuizPage(false);
        setShowQuizPage(true);
        setShowPostQuizPage(false);
        setQuizIsLoading(false);
      })
      .catch((error) => {
        setErrorMessage(error.message);
        setShowErrorDialog(true);
        candidateAction({
          component: "Quiz",
          activity: "Quiz",
          event: `Failed to obtain StartData for Quiz ${quiz_id}`,
          error: error,
        });
      });
  };

  const handleCheckResultsClick = () => {
    setShowPostQuizPage(true);
    setShowPreQuizPage(false);
    setShowQuizPage(false);
  };

  const handleQuizErrorClose = () => {
    setShowErrorDialog(false);
    setErrorMessage("");
  };

  const sendQuestonAnswerToServer = (question_id, currentAnswer) =>
    new Promise((resolve, reject) => {
      submitQuestionAnswer(
        userContext.user,
        quiz_id,
        question_id,
        currentAnswer
      )
        .then((response) => response.json())
        .then((res) => {
          LOCAL_QUIZ_STORAGE.removeAnswerFromLocalStorage(quiz_id, question_id);
          candidateAction({
            component: "Quiz",
            activity: "Quiz",
            event:
              "Candidate Answer: " +
              currentAnswer +
              "for Question: " +
              question_id +
              " submitted successfully",
          });
          resolve("Answer saved");
        })
        .catch((error) => {
          LOCAL_QUIZ_STORAGE.addAnswerToLocalStorage(
            quiz_id,
            question_id,
            currentAnswer
          );
          candidateAction({
            component: "Quiz",
            activity: "Quiz",
            error: error,
            event:
              "Failed to submit Candidate Answer: " +
              currentAnswer +
              "for Question: " +
              question_id,
          });
          reject("Failed to save answer");
        });
    });

  const CheckResults = () => {
    return (
      <Grid
        container
        direction="column"
        justifyContent="center"
        alignItems="center"
      >
        <Grid item xs={12}>
          {
            <PostQuizDisplay
              quiz_id={quiz_id}
              onRetakeClicked={handleStart}
              course_url={course_url}
              unit_return={unit_return !== undefined ? unit_return : url_param}
            />
          }
        </Grid>
      </Grid>
    );
  };

  var the_ui = <CircularProgress />;

  if (statusData == null || quizIsLoading)
    //still waiting for API return, show nothing
    the_ui = <>
    <CircularProgress />
    <Typography>Preparing questions...</Typography>
    </>;
  else if (showPreQuizPage)
    //start or retake button
    the_ui = (
      <React.Fragment>
        <StartQuizPanel
          quiz_id={quiz_id}
          statusData={statusData}
          startCallback={handleStart}
          checkResultsCallback={handleCheckResultsClick}
          buttonText={quiz?.title}
        />
        <Dialog
          open={showErrorDialog}
          onClose={handleQuizErrorClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">{"Warning"}</DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              {error_message}
              <br></br>
              KnowledgEquity can be contacted at enquiries@knowledgequity.com.au
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleQuizErrorClose} autoFocus>
              OK
            </Button>
          </DialogActions>
        </Dialog>
      </React.Fragment>
    );
  else if (showQuizPage) {
    //the actual quiz
    the_ui = (
      <QuizUI
        quiz={quiz}
        quiz_id={quiz_id}
        submitQuizCallback={doSubmitQuiz}
        submitAnswerCallback={sendQuestonAnswerToServer}
        quizDuration={
          parseInt(startData["quiz-duration"]) ||
          parseInt(startData["expire-time"] - startData["stime"])
        }
        timerFunction={timerTickFunc}
        scratchpadVisible={scratchpadVisible}
        setScratchpadVisible={setScratchpadVisible}
        hasScratchpad={quiz.hasScratchpad}
      />
    );
  } else if (showPostQuizPage) {
    //post submit page
    the_ui = <CheckResults />;
  } 
  else if (showSubmitError)
  {
    console.log("UI OK ")
    the_ui = <SubmitQuizError submitQuizCallback={doSubmitQuiz} />
  }
  else 
  { console.log("UI not OK")
    the_ui = <CircularProgress />;
  }

  return the_ui;
}
