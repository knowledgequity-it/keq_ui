import React, { useEffect, useState } from "react";
import Box from "@mui/material/Box";
import { Typography } from "@mui/material";
import { useTheme } from "@mui/material/styles";
import FlagCircleIcon from '@mui/icons-material/FlagCircle';
import { Stack } from "@mui/system";

export default function QuizQuestionContent({ index, question, toggleFlagged, quizName }) {

  let x = question.flaggedForReview ? true : false;
  const theme = useTheme();
  const [isFlagged, setIsFlagged] = useState(false);

  useEffect(() => {
    if(question)
      question.flaggedForReview ? setIsFlagged(true) : setIsFlagged(false)

  }, [question]);

  const toggleFlaggedForReview = () => {
    setIsFlagged(!isFlagged)
    toggleFlagged(index)
  }

  const parseQuizContentAndRemoveWorksheetTable = (html) => {

    if(question.type !== "fillblank")
      return html;

    const parser = new DOMParser();
    let content = parser.parseFromString(html, "text/html");
    let tables = content.querySelectorAll("table");

    
    tables.forEach(table => {
     //remove table only if it is one that has answers in it. 
      if( table.getElementsByClassName("live-edit").length != 0)
        table.remove()
    })      
    return content.querySelector("body").innerHTML
  }

  let flagForReviewComponent = null;
  if(quizName.toLowerCase().includes("practice exam") || quizName.toLowerCase().includes("semester") )
    flagForReviewComponent = <>
    <Typography sx={{ml:10}} color="grey" onClick={() => toggleFlaggedForReview()}>
      {!question.flaggedForReview ? "Flag this question for later review." : "Unflag this question" }
    </Typography>
    <FlagCircleIcon sx={{fontSize : 36}} color="primary"  onClick={() => toggleFlaggedForReview()}/>
    </>


  return (
    <React.Fragment>
      <Box>
        <Stack direction="row" alignItems="center" justifyContent="flex-start">
          <Typography
            variant="h5"
            sx={{
              marginTop: theme.spacing(1),
              marginBottom: theme.spacing(1),
              paddingTop: theme.spacing(2),
              paddingBottom: theme.spacing(2),
            }}
          >
            Question {index + 1}
          </Typography>
          
          {flagForReviewComponent}
         
        </Stack>
        <div  dangerouslySetInnerHTML={{__html: parseQuizContentAndRemoveWorksheetTable(question.content)}}></div>
       
      </Box>
    </React.Fragment>
  );
}
