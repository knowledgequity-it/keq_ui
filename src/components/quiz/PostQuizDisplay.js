import React, { useContext, useEffect, useState } from "react";
import { useTheme } from "@mui/material/styles";
import Typography from "@mui/material/Typography";
import SingleQuizResults from "../SingleQuizResults";
import { UserContext } from "../../contexts/userContext";
import { quizStatus, quizTitle } from "../../facades/QuizFacade";
import Button from "@mui/material/Button";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import Link from "@mui/material/Link";

export default function PostQuizDisplay({
  quiz_id,
  onRetakeClicked,
  course_url,
  unit_return,
}) {
  const theme = useTheme();
  const [quizStatusData, setQuizStatusData] = useState(null);
  const [quizName, setQuizName] = useState("");
  const [buttonText, setButtonText] = useState("");
  const userContext = useContext(UserContext);

  useEffect(() => {
    quizStatus(userContext.user, quiz_id)
      .then((response) => response.json())
      .then((statusData) => {
        setQuizStatusData(statusData);
      });

    quizTitle(userContext.user, quiz_id)
      .then((response) => response.json())
      .then((data) => {
        setQuizName(data.title);
      });
  }, [quiz_id]);

  useEffect(() => {
    if (quizName)
      if (quizName.toLowerCase().includes("practice exam"))
        setButtonText("Retake Practice Exam");
      else setButtonText("Retake Quiz");
  }, [quizName]);

  if (quizStatusData == null) return null;
  return (
    <React.Fragment>
      <Grid
        container
        direction="row"
        alignItems="center"
        justifyContent="center"
      >
        <Grid item xs={12}>
          <Typography>You have submitted the quiz.</Typography>
        </Grid>
        <Grid item xs={12} md={6}>
          {quizStatusData.retakes_remaining > 0 && (
            <Button
              variant="contained"
              color="primary"
              onClick={onRetakeClicked}
              sx={{
                margin: theme.spacing(1),
                height: "60px",
                width: "300px",
                padding: "15px",
                fontSize: "1em",
                margin: "10px",
                fontWeight: "bold",
                color: "darkslategrey",
              }}
            >
              {buttonText}
            </Button>
          )}
        </Grid>
        <Grid item xs={12} md={6}>
          <Link
            href={`${
              process.env.REACT_APP_AB_URL_PREFIX
            }/course/${course_url}/${
              unit_return !== undefined && unit_return !== null
                ? unit_return
                : ""
            }`}
          >
            Back to Course
          </Link>
        </Grid>
      </Grid>
      <SingleQuizResults open={true} quiz_id={quiz_id} showTotal={true} />
    </React.Fragment>
  );
}
