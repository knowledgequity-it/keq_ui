import React, { useEffect, useState } from "react";
import Button from "@mui/material/Button";

const sxBig = {
  padding: "10px",
  fontSize: "1em",
  margin: "10px",
};

export default function QuizSubmitButton({
  onSubmitCallback,
  text,
  buttontype,
}) {
  const [buttonText, setButtonText] = useState("Quiz");

  useEffect(() => {
    if (text) {
      if (text.toLowerCase().includes("practice exam"))
        setButtonText("Submit Practice Exam");
      else if (text.toLowerCase().includes("mid-semester test"))
        setButtonText("Submit Mid-Semester Test");
      else if (text.toLowerCase().includes("resubmitting"))
        setButtonText(text);
      else setButtonText("Submit Quiz");
    }
  }, [text]);

  return (
    <React.Fragment>
      <Button
        onClick={onSubmitCallback}
        color="primary"
        variant="contained"
        sx={{
          ...(buttontype == "big" && sxBig),
        }}
      >
       {buttonText}
      </Button>
    </React.Fragment>
  );
}
