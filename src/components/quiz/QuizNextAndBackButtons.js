import React from "react";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import { useTheme } from "@mui/material/styles";

export default function QuizNextAndBackButtons({
  currentIndex,
  maxIndex,
  onNextCallback,
  onPreviousCallback,
}) {
  const theme = useTheme();

  return (
    <React.Fragment>
      <Box sx={{ padding: "10px" }}>
        <Button
          disabled={currentIndex === 0}
          onClick={onPreviousCallback}
          sx={{ marginRight: theme.spacing(1) }}
        >
          Previous Question
        </Button>
        <Button
          disabled={currentIndex === maxIndex}
          variant="contained"
          color="primary"
          onClick={onNextCallback}
          sx={{ marginRight: theme.spacing(1) }}
        >
          Next Question
        </Button>
      </Box>
    </React.Fragment>
  );
}
