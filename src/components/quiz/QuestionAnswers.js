import React, { useEffect, useState, useContext, useRef } from "react";
import { useTheme } from "@mui/material/styles";
import Box from "@mui/material/Box";
import FormControl from "@mui/material/FormControl";
import RadioGroup from "@mui/material/RadioGroup";
import Radio from "@mui/material/Radio";
import FormControlLabel from "@mui/material/FormControlLabel";
import {
  FormGroup,
  Paper,
  TableBody,
  TableCell,
  TableRow,
  TableContainer,
  Typography,
  Table,
} from "@mui/material";
import { setSyntheticLeadingComments } from "typescript";
import TextField from "@mui/material/TextField";
import { QuizContext } from "../../contexts/QuizContext";
import QuizQuestionWorksheetAnswer from "./QuizQuestionWorksheetAnswer";

const SingleAnswerPanel = ({ index, question, setAnswerCallback }) => {
  const [current_answer, setCurrentAnswer] = useState(question?.user_answer[0]);
  //const [options, setOptions] = useState([]);

  // setOptions(question.options)
  //const current_answer = parseInt(question?.user_answer?.[0]) || "";

  /*

  useEffect(() => {
    if (question.user_answer[0] !== "")
      setCurrentAnswer(parseInt(question.user_answer[0]));
    else if (question.user_answer[0] === "") setCurrentAnswer("");

    setOptions(question.options);
  }, [question]);

  //let options = question.options;

  useEffect(() => {
    if (current_answer !== "") {
      setAnswerCallback(current_answer, false);
    }
  }, [current_answer]);
*/

  function updateAnswer(event) {
    //console.log("EVENT", event.target.value, current_answer,  parseInt(current_answer))
    if (event.target.value === parseInt(current_answer)) {
      //if anser the same, do nothing
      return;
    } else {
      setCurrentAnswer(parseInt(event.target.value));
      setAnswerCallback(index, parseInt(event.target.value), "single");
    }
  }

  return (
    <React.Fragment>
      <Box sx={{ padding: "10px" }}>
        <FormControl component="fieldset">
          <RadioGroup
            aria-label="answers"
            name="answers"
            value={parseInt(question.user_answer[0])}
            onChange={updateAnswer}
          >
            {question?.options.map((option, index) => {
              return (
                <FormControlLabel
                  key={option}
                  value={index + 1}
                  control={<Radio />}
                  label={option}
                  sx={{ paddingBottom: "12px" }}
                />
              );
            })}
          </RadioGroup>
        </FormControl>
      </Box>
    </React.Fragment>
  );
};

const MultipleChoiceAnswer = ({ index, question, setAnswerCallback }) => {
  const [currentAnswers, setCurrentAnswers] = useState(question?.user_answer);

  //setCurrentAnswers(question?.user_answer)
  /*
  useEffect(() => {
    if (question.user_answer.length == 1 && question.user_answer[0] === "")
      setCurrentAnswers([]);
    else setCurrentAnswers(question.user_answer);

    setOptions(question.options);
  }, [question]);

  useEffect(() => {
   
  }, [currentAnswers]);
  */

  function optionChanged(event) {
    let selection = event.target.value;
    let idx = currentAnswers.indexOf(selection);
    if (idx < 0) {
      //new answer, add it
      currentAnswers.push(selection);
      setCurrentAnswers([...currentAnswers]);
    } else {
      //existing answer, remove it
      let newarr = currentAnswers.splice(idx, 1);
      setCurrentAnswers([...currentAnswers]);
    }
    setAnswerCallback(index, currentAnswers, "multiple");
  }

  function isChecked(idx) {
    let inc = currentAnswers.includes(idx + 1);
    return currentAnswers.includes((idx + 1).toString());
  }

  return (
    <React.Fragment>
      <FormGroup>
        {question?.options.map((option, idx) => {
          return (
            <FormControlLabel
              key={option}
              value={idx + 1}
              control={<Radio onClick={optionChanged} />}
              label={option}
              checked={isChecked(idx)}
            />
          );
        })}
      </FormGroup>
    </React.Fragment>
  );
};

const FreeTextAnswer = ({
  index,
  question,
  setAnswerCallback,
  hasScratchpad,
}) => {
  const quizContext = useContext(QuizContext);
  const [answerText, setAnswerText] = useState(question.user_answer[0]);
  const [charCount, setCharCount] = useState(0);
  const inputRef = useRef();

  //if (question && question.user_answer.length == 1)
  //  setAnswerText(question.user_answer[0]);

  const handleTextChange = (event) => {
    if (charCount >= 10) {
      setAnswerCallback(index, event.target.value, "largetext");
      setCharCount(0);
    }
    /* else{
        
        let compareTxt = event.target.value.substring(0, event.target.value.length-charCount)
        console.log("comparing", compareTxt, " to ", answerText, answerText === compareTxt, charCount)
        if(answerText.substring(0, answerText.length - charCount) !== compareTxt)
        {
            //setAnswerText(event.target.value)
            setAnswerCallback(index, event.target.value, 'largetext');
            setCharCount(0)
        }
    }*/
    setCharCount(charCount + 1);
    setAnswerText(event.target.value);
    if (charCount > 10) setCharCount(0);
  };

  const handleBlur = (event) => {
    setAnswerCallback(index, event.target.value, "largetext");
  };

  const handlePaste = (event) => {
    if (hasScratchpad) {
      event.preventDefault();
      if (inputRef.current) {
        const [start, end] = [
          inputRef.current.selectionStart,
          inputRef.current.selectionEnd,
        ]; /*
        inputRef.current.setRangeText(
          quizContext.scratchpadClipboard,
          start,
          end,
          "end"
        );*/
        let newstr =
          answerText.substring(0, start) +
          quizContext.scratchpadClipboard +
          answerText.substring(end);
        setAnswerText(newstr);
        setAnswerCallback(index, newstr, "largetext");
        setCharCount(0);
      }
    }
  };
  return (
    <React.Fragment>
      <TextField
        variant="outlined"
        fullWidth
        multiline
        rows={10}
        id="answer-field"
        label="Answer text"
        value={answerText}
        onInput={handleTextChange}
        onPaste={handlePaste}
        onBlur={handleBlur}
        inputRef={inputRef}
      />
    </React.Fragment>
  );
};

const WorksheetAnswers = ({
  index,
  question,
  setAnswerCallback,
  hasScratchpad,
}) => {
  const [answers, setAnswers] = useState([]);
  let parser = new DOMParser();
  let content = parser.parseFromString(question.content, "text/html");
  let tables = content.querySelectorAll("table");

  useEffect(() => {
    if (question) {
      var totalAnswers = 0;

      for (var i = 0; i < tables.length; i++) {
        let tot = tables[i].getElementsByClassName("live-edit")
          ? tables[i].getElementsByClassName("live-edit").length
          : 0;
        totalAnswers += tot; //find how many answers we're supposed to have
      }

      let ansString = question.user_answer[0];
      if (ansString && ansString.slice(-1) === "|")
        ansString = ansString.slice(0, -1);

      let userAnswerArray = ansString.split("|");
      if (userAnswerArray.length == totalAnswers) {
        //there are user answers. | separated, so if they arent equal we can't match answers to index
        let answers = Array(totalAnswers.length).fill(""); //make new array with total number of answers
        for (
          let i = 0;
          i < totalAnswers;
          i++ //set the user answers to the right indexes.
        )
          answers[i] = userAnswerArray[i];

        setAnswers(answers);
      } //if the user answers are less/more than the blanks we have, throw them away since we don't know where to put them and initialise blank answers.
      else {
        setAnswers(Array(totalAnswers).fill(""));
      }
    }
  }, [question]);
  const updateAnswers = (index, answer) => {
    let ans = Array.from(answers);
    ans[index] = answer;
    setAnswers(ans);
    submitAnswers(ans);
  };
  const submitAnswers = (ans) => {
    for (let i = 0; i < ans.length; i++) {
      if (!ans[i]) ans[i] = "";
    }
    setAnswerCallback(index, ans.join("|") + "|", "fillblank");
  };

  //parse inline html styles and then create a react object we can pass to the sx prop
  const parseInlineStyle = (style) => {
    const template = document.createElement("template");
    template.setAttribute("style", style);
    return Object.entries(template.style)
      .filter(([key]) => !/^[0-9]+$/.test(key))
      .filter(([, value]) => Boolean(value))
      .reduce((acc, [key, value]) => ({ ...acc, [key]: value }), {});
  };
  var answerIndex = 0;
  return (
    <React.Fragment>
      {
        <React.Fragment>
          {Array.from(tables).map((table, tindex) => {
            if (table.getElementsByClassName("live-edit").length == 0)
              return null;

            let editableCells = table.getElementsByClassName("live-edit");

            for (let i = 0; i < editableCells.length; i++) {
              editableCells[i].setAttribute("index", answerIndex);
              answerIndex++;
            }
            let bodyStyle = parseInlineStyle(
              table.getElementsByTagName("tbody")[0].getAttribute("style"),
            );
            return (
              <TableContainer component={Paper} key={tindex}>
                {
                  <Table>
                    <TableBody sx={bodyStyle}>
                      {Array.from(
                        table.getElementsByTagName("tbody")[0].children,
                      ).map((row, rindex) => {
                        //console.log("TABLE ROW", row)
                        let rowStyle = parseInlineStyle(
                          row.getAttribute("style"),
                        );
                        return (
                          <TableRow key={rindex} sx={rowStyle}>
                            {Array.from(row.children).map((cell, cindex) => {
                              //console.log("TABLE CELL", cell.getElementsByClassName("live-edit"))
                              let fillBlank =
                                cell.getElementsByClassName("live-edit");

                              if (fillBlank.length == 0)
                                return (
                                  <TableCell
                                    key={cindex}
                                    dangerouslySetInnerHTML={{
                                      __html: cell.innerHTML,
                                    }}
                                  ></TableCell>
                                );
                              else {
                                return (
                                  <TableCell key={cindex}>
                                    <QuizQuestionWorksheetAnswer
                                      answerIndex={fillBlank[0].getAttribute(
                                        "index",
                                      )}
                                      answerCallback={updateAnswers}
                                      initVal={
                                        answers[
                                          fillBlank[0].getAttribute("index")
                                        ]
                                      }
                                      fieldType={question.answer_type}
                                    />
                                  </TableCell>
                                );
                              }
                            })}
                          </TableRow>
                        );
                      })}
                    </TableBody>
                  </Table>
                }
              </TableContainer>
            );
          })}
        </React.Fragment>
      }
    </React.Fragment>
  );
};

export default function QuestionAnswers({
  index,
  question,
  setAnswerCallback,
  hasScratchpad,
}) {
  if (question.type === "single")
    return (
      <SingleAnswerPanel
        index={index}
        question={question}
        setAnswerCallback={setAnswerCallback}
      />
    );
  else if (question.type === "multiple")
    return (
      <MultipleChoiceAnswer
        index={index}
        question={question}
        setAnswerCallback={setAnswerCallback}
      />
    );
  else if (question.type === "largetext")
    return (
      <FreeTextAnswer
        index={index}
        question={question}
        setAnswerCallback={setAnswerCallback}
        hasScratchpad={hasScratchpad}
      />
    );
  else if (question.type === "fillblank")
    return (
      <WorksheetAnswers
        index={index}
        question={question}
        setAnswerCallback={setAnswerCallback}
        hasScratchpad={hasScratchpad}
      />
    );
}
