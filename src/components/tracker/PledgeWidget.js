import { Button } from "@mui/material";
import { useEffect, useState } from "react"

class Pledge{

    static PASS = "Pass";
    static CREDIT = "Credit";
    static DISTINCTION = "Distinction";
    static HIGH_DISTINCTION = "High Distinction";
  }
  const getPledgeColor = (pledge) => {
    switch(pledge)
        {
          case Pledge.PASS:
            return "gradeP";
          case Pledge.CREDIT:
            return "gradeC";
          case Pledge.DISTINCTION:
            return "gradeD";
          case Pledge.HIGH_DISTINCTION:
            return "gradeHD";
          default:
            return "gradeNone";
        }
  }

export default function PledgeWidget({pledge})
{
    const [_pledge, setPledge] = useState(null);

    useEffect(() => {
     
       console.log("PLEDGE",pledge)
      switch(pledge)
      {
        case Pledge.PASS:
          setPledge(Pledge.PASS);
          break;
        case Pledge.CREDIT:
          setPledge(Pledge.CREDIT);
          break;
        case Pledge.DISTINCTION:
          setPledge(Pledge.DISTINCTION);
          break;
        case Pledge.HIGH_DISTINCTION:
          setPledge(Pledge.HIGH_DISTINCTION);
          break;
        default:
          setPledge('No pledge');
          break;
      }

    }, [pledge])

    

    return <Button color={getPledgeColor(_pledge)} 
    variant="contained" 
    sx={{
        textTransform: "none",
        width: "120px",
        height: "50%",
        borderRadius:7
      }}>{_pledge}</Button>
}