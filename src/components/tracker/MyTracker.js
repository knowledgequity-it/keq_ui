import { Button, Card, Grid, InputAdornment, TextField, Typography, Stack, Container } from "@mui/material";
import { useContext, useEffect, useState } from "react";
import { UserContext } from "../../contexts/userContext";
import { getUserResults, getUserStudyData, submitStudyData } from "../../facades/UserFacade";
import { BarChart } from '@mui/x-charts/BarChart';
import dayjs from 'dayjs';
import { fontSize } from "@mui/system";
import PledgeWidget from "./PledgeWidget";
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import { LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import AvTimerIcon from '@mui/icons-material/AvTimer';
import { AvTimer } from "@mui/icons-material";


const CONFIG = {
   // 'semester_start' : "2024-07-08",
   // "semester_end" : "2024-10-01",
   'semester_start' : "2025-01-13",
   "semester_end" : "2025-04-08",
    "default" : {
        "default" : {
            "hours" : 120
        },
        "Pass" : {
            "hours" : 120,
            "mid-target" : 8,
            "mid-max" : 15,
            "pe-target" : 36,
            "pe-max" : 63
        },
        "Credit" : {
            "hours" : 120,
            "mid-target" : 10,
            "mid-max" : 15,
            "pe-target" : 46,
            "pe-max" : 63
        },
        "Distinction" : {
            "hours" : 120,
            "mid-target" : 11,
            "mid-max" : 15,
            "pe-target" : 51,
            "pe-max" : 63
        },
        "High Distinction" : {
            "hours" : 120,
            "mid-target" : 12,
            "mid-max" : 15,
            "pe-target" : 56,
            "pe-max" : 63
        }
    },
    "Ethics & Governance" : {
        "Pass" : {
            "hours" : 120,
            "mid-target" : 8,
            "mid-max" : 15,
            "pe-target" : 36,
            "pe-max" : 63
        },
        "Credit" : {
            "hours" : 120,
            "mid-target" : 10,
            "mid-max" : 15,
            "pe-target" : 46,
            "pe-max" : 63
        },
        "Distinction" : {
            "hours" : 120,
            "mid-target" : 11,
            "mid-max" : 15,
            "pe-target" : 51,
            "pe-max" : 63
        },
        "High Distinction" : {
            "hours" : 120,
            "mid-target" : 12,
            "mid-max" : 15,
            "pe-target" : 56,
            "pe-max" : 63
        }
    },
    "Advanced Audit & Assurance" : {
        "Pass" : {
            "hours" : 120,
            "mid-target" : 8,
            "mid-max" : 15,
            "pe-target" : 51,
            "pe-max" : 90
        },
        "Credit" : {
            "hours" : 120,
            "mid-target" : 10,
            "mid-max" : 15,
            "pe-target" : 56,
            "pe-max" : 90
        },
        "Distinction" : {
            "hours" : 120,
            "mid-target" : 11,
            "mid-max" : 15,
            "pe-target" : 66,
            "pe-max" : 90
        },
        "High Distinction" : {
            "hours" : 120,
            "mid-target" : 12,
            "mid-max" : 15,
            "pe-target" : 71,
            "pe-max" : 90
        }
    },
    "Australia Taxation" : {
        "Pass" : {
            "hours" : 120,
            "mid-target" : 8,
            "mid-max" : 15,
            "pe-target" : 26,
            "pe-max" : 50
        },
        "Credit" : {
            "hours" : 120,
            "mid-target" : 10,
            "mid-max" : 15,
            "pe-target" : 31,
            "pe-max" : 50
        },
        "Distinction" : {
            "hours" : 120,
            "mid-target" : 11,
            "mid-max" : 15,
            "pe-target" : 36,
            "pe-max" : 50
        },
        "High Distinction" : {
            "hours" : 120,
            "mid-target" : 12,
            "mid-max" : 15,
            "pe-target" : 41,
            "pe-max" : 50
        },
    },
    "Australia Taxation - Advanced" : {
        "Pass" : {
            "hours" : 120,
            "mid-target" : 8,
            "mid-max" : 15,
            "pe-target" : 30,
            "pe-max" : 53
        },
        "Credit" : {
            "hours" : 120,
            "mid-target" : 10,
            "mid-max" : 15,
            "pe-target" : 36,
            "pe-max" : 53
        },
        "Distinction" : {
            "hours" : 120,
            "mid-target" : 11,
            "mid-max" : 15,
            "pe-target" : 41,
            "pe-max" : 53
        },
        "High Distinction" : {
            "hours" : 120,
            "mid-target" : 12,
            "mid-max" : 15,
            "pe-target" : 46,
            "pe-max" : 53
        },
    },
    "Contemporary Business Issues" : {
        "Pass" : {
            "hours" : 120,
            "mid-target" : 8,
            "mid-max" : 15,
            "pe-target" : 36,
            "pe-max" : 75
        },
        "Credit" : {
            "hours" : 120,
            "mid-target" : 10,
            "mid-max" : 15,
            "pe-target" : 46,
            "pe-max" : 75
        },
        "Distinction" : {
            "hours" : 120,
            "mid-target" : 11,
            "mid-max" : 15,
            "pe-target" : 51,
            "pe-max" : 75
        },
        "High Distinction" : {
            "hours" : 120,
            "mid-target" : 12,
            "mid-max" : 15,
            "pe-target" : 56,
            "pe-max" : 75
        }
    },
    "Digital Finance" : {
        "Pass" : {
            "hours" : 120,
            "mid-target" : 8,
            "mid-max" : 15,
            "pe-target" : 22,
            "pe-max" : 71
        },
        "Credit" : {
            "hours" : 120,
            "mid-target" : 10,
            "mid-max" : 15,
            "pe-target" : 31,
            "pe-max" : 71
        },
        "Distinction" : {
            "hours" : 120,
            "mid-target" : 11,
            "mid-max" : 15,
            "pe-target" : 35,
            "pe-max" : 71
        },
        "High Distinction" : {
            "hours" : 120,
            "mid-target" : 12,
            "mid-max" : 15,
            "pe-target" : 40,
            "pe-max" : 71
        }
    },
    "Financial Reporting" : {
        "Pass" : {
            "hours" : 120,
            "mid-target" : 8,
            "mid-max" : 15,
            "pe-target" : 20,
            "pe-max" : 44
        },
        "Credit" : {
            "hours" : 120,
            "mid-target" : 10,
            "mid-max" : 15,
            "pe-target" : 30,
            "pe-max" : 44
        },
        "Distinction" : {
            "hours" : 120,
            "mid-target" : 11,
            "mid-max" : 15,
            "pe-target" : 35,
            "pe-max" : 44
        },
        "High Distinction" : {
            "hours" : 120,
            "mid-target" : 12,
            "mid-max" : 15,
            "pe-target" : 39,
            "pe-max" : 44
        }
    },
    "Financial Risk Management" : {
        "Pass" : {
            "hours" : 120,
            "mid-target" : 8,
            "mid-max" : 15,
            "pe-target" : 36,
            "pe-max" : 70
        },
        "Credit" : {
            "hours" : 120,
            "mid-target" : 10,
            "mid-max" : 15,
            "pe-target" : 46,
            "pe-max" : 70
        },
        "Distinction" : {
            "hours" : 120,
            "mid-target" : 11,
            "mid-max" : 15,
            "pe-target" : 56,
            "pe-max" : 70
        },
        "High Distinction" : {
            "hours" : 120,
            "mid-target" : 12,
            "mid-max" : 15,
            "pe-target" : 61,
            "pe-max" : 70
        }
    },
    "Global Strategy & Leadership " : {
        "Pass" : {
            "hours" : 120,
            "mid-target" : 8,
            "mid-max" : 15,
            "pe-target" : 31,
            "pe-max" : 68
        },
        "Credit" : {
            "hours" : 120,
            "mid-target" : 10,
            "mid-max" : 15,
            "pe-target" : 41,
            "pe-max" : 68
        },
        "Distinction" : {
            "hours" : 120,
            "mid-target" : 11,
            "mid-max" : 15,
            "pe-target" : 46,
            "pe-max" : 68
        },
        "High Distinction" : {
            "hours" : 120,
            "mid-target" : 12,
            "mid-max" : 15,
            "pe-target" : 51,
            "pe-max" : 68
        }
    },
    "Strategic Management Accounting" : {
        "Pass" : {
            "hours" : 140,
            "mid-target" : 8,
            "mid-max" : 15,
            "pe-target" : 20,
            "pe-max" : 52
        },
        "Credit" : {
            "hours" : 140,
            "mid-target" : 10,
            "mid-max" : 15,
            "pe-target" : 31,
            "pe-max" : 52
        },
        "Distinction" : {
            "hours" : 140,
            "mid-target" : 11,
            "mid-max" : 15,
            "pe-target" : 36,
            "pe-max" : 52
        },
        "High Distinction" : {
            "hours" : 140,
            "mid-target" : 12,
            "mid-max" : 15,
            "pe-target" : 41,
            "pe-max" : 52
        }
    }
}

const TEST = [
    {"2024-07-08" : 2},
    {"2024-07-09" : 2},
    {"2024-07-11" : 2},
    {"2024-07-18" : 2},
    {"2024-07-24" : 2},
]
export default function MyTracker()
{
    const userContext = useContext(UserContext);
    const [userResults, setUserResults] = useState([])
    const [userStudyData, setUserStudyData] = useState([])
    const [studyDate, setStudyDate] = useState(dayjs());
    const [selectedHours, setSelectedHours]  = useState(0)
    
    useEffect(() => {
        getUserResults(userContext.user)
        .then(res => res.json())
        .then(results => {
            setUserResults(results);
        })

        getUserStudyData(userContext.user)
        .then(res => res.json())
        .then(res =>{ setUserStudyData(res)})

    }, [])

    const changeHours = (newDate, subjectId) => {
        
        setStudyDate(newDate)
        setSelectedHours(userStudyData.find(({id}) => id == subjectId)['data'][newDate.format('YYYY-MM-DD')])
    }

    const studyHoursAreValid = (hours) => {

        return (hours <= 24 && hours >= 0)
    }
    const submitTodaysHours = (event, subjectCode) => {
        if(!event.target.value)
            return
        
        if(!studyHoursAreValid(event.target.value))
            return

        setSelectedHours(event.target.value);
        let today = dayjs().format('YYYY-MM-DD');
        let amount = event.target.value
        let selectedDay = studyDate.format("YYYY-MM-DD")
        console.log(studyDate.format("YYYY-MM-DD"))
        
        submitStudyData(userContext.user, subjectCode,selectedDay , amount )
        .then(res => res.json())
        .then(res => {
            let newUserStudyData = [...userStudyData]
            for(var i = 0; i < newUserStudyData.length; i++)
            {
                if(newUserStudyData[i]['id'] === subjectCode){
                    if(newUserStudyData[i]['data'])
                        newUserStudyData[i]['data'][selectedDay] = amount;
                    else
                    {
                        newUserStudyData[i]['data'] = {}
                        newUserStudyData[i]['data'][selectedDay] = amount;
                    }

                    break;
                }
            }    
            setUserStudyData(newUserStudyData)
        })
        
    }
    const generateDateRange = () => {
        const dates = [];
        const dayjsStart = dayjs(CONFIG['semester_start']);
        const dayjsEnd = dayjs(CONFIG['semester_end']);

        let current = dayjsStart;
        while (current.isBefore(dayjsEnd)) {
            dates.push(current.format('YYYY-MM-DD'));
            current = current.add(1, 'day');
        }

        return dates;
    }

  const makeArrayFromUserStudyData = (data) => {
    console.log("data", data);
        let newArr = Array(calculateDateDiff()).fill(0);
        let startDay = dayjs(CONFIG['semester_start']);

        for(var i = 0; i < newArr.length; i++)
        {
            if(data[startDay.format("YYYY-MM-DD")])
            {
                newArr[i] = Number(data[startDay.format("YYYY-MM-DD")])
            }

            startDay = startDay.add(1, 'day');
        }
    console.log("newArr", newArr);
        return newArr
  }

  
    const calculateDateDiff = () => {

        const dayjsStart = dayjs(CONFIG['semester_start']);
        const dayjsEnd = dayjs(CONFIG['semester_end']);

        return dayjsEnd.diff(dayjsStart, 'day');
    }

    const getRecommendedHoursPerDay = (subject, grade) => {
        let hours = CONFIG['default']['default']["hours"]

        if(! grade || !CONFIG[subject] || !CONFIG[subject][grade]) 
            return hours/calculateDateDiff();


        if( CONFIG[subject][grade]['hours'])
            hours = CONFIG[subject][grade]['hours'];

        return hours/calculateDateDiff();
    }
    
    const getTotalRecommendedHours = (subject, grade) => {
        let hours = CONFIG['default']['default']["hours"]

        if(CONFIG[subject] && grade)
            hours = CONFIG[subject][grade]["hours"];
        
        return hours;
    }

    const calculateTotalHours = (hoursArr) => {
        return hoursArr.reduce((accumulator, currentValue) => accumulator + currentValue, 0);
    }

    const calculateDaysLeftInSemester = () => {
        const today = dayjs();
        const semesterEnd = dayjs(CONFIG['semester_end']);

        return semesterEnd.diff(today, 'day');
    }
    const calculateDaysFromStartOfSemester = () => {
        const today = dayjs();
        const semesterStart = dayjs(CONFIG['semester_start']);
      
        return today.diff(semesterStart, 'day');
    }

  const calculateRemainingHoursRequired = (subject, grade, data) => {
    //console.log("Subject", subject);
    //console.log("Grade", grade);
    //console.log("Data", data);
    let requiredHours = getTotalRecommendedHours(subject, grade);
    //console.log("requiredHours", requiredHours);
    let actualHours = calculateTotalHours(makeArrayFromUserStudyData(data))
    //console.log("actualHours", actualHours);
    let remainingDays = calculateDaysLeftInSemester();
    //console.log("remainingDays", remainingDays);
    let requiredRemainingHours = (requiredHours - actualHours)/remainingDays;
    //console.log("requiredRemainingHours", requiredRemainingHours);
        return requiredRemainingHours;
    }

    const getStudentStatus = (subject, grade, data) => {
        let remhours = calculateRemainingHoursRequired(subject, grade, data)
        let requiredHours = getRecommendedHoursPerDay(subject, grade)

        if(remhours <= requiredHours)
            return "ON TARGET";
        else if(requiredHours/remhours > 0.85)
            return "A LITTLE BEHIND TARGET";
        else
            return "BEHIND TARGET";
    }

    const disableKeyboardEntry = (e: any) => {
        if (e?.preventDefault) { 
        e?.preventDefault();
        e?.stopPropagation();
        }
    }


  return <Container maxWidth="xl">
    
    <Stack direction="row">
    <Typography sx={{p:1}} fontSize={30} fontWeight={800}> My Study Tracker</Typography>
    <Typography sx={{color: "red", pt: 1}}>Beta</Typography>
    </Stack>
        <Typography sx={{p:1}} fontSize={18} fontWeight={400}>Monitor how you're tracking against what's requred to achieve your pledge.</Typography>
        
        <Grid container direction="column" sx={{p:2}} spacing={2}>
            {
                userStudyData.map(subject => {
                    return <Grid item>
                            <Card sx={{borderRadius:5}}>
                                <Grid container direction="row" alignItems="center" justifyItems="center">
                                    <Grid item>
                                        <Typography sx={{p:2, }} fontSize={24} fontWeight={600}>{subject['course_nicename']}</Typography>
                                    </Grid>
                                    <Grid item>
                                        <PledgeWidget pledge={subject.pledge}></PledgeWidget>
                                    </Grid>
                                </Grid>
                                
                               
                                <Typography sx={{pl:2, pt:1}}  fontSize={18} fontWeight={600}>
                                    You have studied a total of 
                                        <span style={{fontWeight:"500", fontSize:"20"}}> {calculateTotalHours(makeArrayFromUserStudyData(subject.data))} </span> 
                                    hours so far. 
                                </Typography>
                                <Typography  sx={{pl:2, pt:1 }} >
                                    You are <span style={{fontWeight:"500", fontSize:"20", fontColor:"red"}}>{getStudentStatus(subject.course_nicename, subject.pledge, subject.data)}</span> 
                                     You will need to study for an average of <span style={{fontWeight:"500", fontSize:"20", fontColor:"red"}}>{calculateRemainingHoursRequired(subject.course_nicename, subject.pledge, subject.data).toFixed(2)} hours per day</span> {getStudentStatus(subject.course_nicename, subject.pledge, subject.data) == "ON TARGET" ? " to keep on track" : " to catch up"}.
                                </Typography>
                                <Grid container direction="row" alignItems="center" justifyItems="flex-start" sx={{pl:2, pt:2}} spacing={2}>
                                    <Grid item>
                                        <Typography>Add study hours - Date :  </Typography>
                                    </Grid>
                                    <Grid item>
                                        <LocalizationProvider dateAdapter={AdapterDayjs}>
                                            <DatePicker
                                                format="YYYY-MM-DD"
                                                label="Pick study date"
                                                value={studyDate }
                                                onChange={(newValue) => {
                                                    changeHours(newValue, subject.id);
                                                }}
                                                //shouldDisableDate={(date) => checkPracticeExamDate(date, "PE1")}
                                                slotProps={{ 
                                                textField: { 
                                                    placeholder:"",
                                                    disabled: false, 
                                                    onBeforeInput: disableKeyboardEntry,
                                                    InputProps: { 
                                                    size: 'normal', 
                                                    sx: { fontSize: 18, maxWidth: 200}, 
                                                    disableUnderline: true,
                                                    readOnly: true
                                                    }, 
                                                    
                                                 } 
                                                }}
                                            />
                                            </LocalizationProvider>
                                        </Grid>
                                        <Grid item>
                                            <Typography>Hours : </Typography>
                                        </Grid>
                                        <Grid item>
                                            <TextField 
                                                type="number" 
                                                value={selectedHours}
                                                error={!studyHoursAreValid(selectedHours)}
                                                sx={{width:"120px"}} 
                                                onChange={(e) => submitTodaysHours(e, subject.id)}
                                                InputProps={{
                                                    startAdornment: (
                                                        <InputAdornment position="start">
                                                        <AvTimerIcon />
                                                        </InputAdornment>
                                                    ),
                                                    }}>
                                            </TextField>
                                        </Grid>
                                    <Grid item>
                                       
                                    </Grid>
                                </Grid>
                                <BarChart
                                    series={[
                                        { data: makeArrayFromUserStudyData(subject.data),  label: 'Actual hours studied'},
                                      { data: Array(calculateDateDiff()).fill(getRecommendedHoursPerDay(subject.course_nicename, subject.pledge)),  label: 'Recommended hours across whole semester', color: '#e8f4f8'},
                                      { data: Array(calculateDateDiff()).fill(0).fill(calculateRemainingHoursRequired(subject.course_nicename, subject.pledge, subject.data).toFixed(2), calculateDaysFromStartOfSemester()), label: 'Average hours required until end of semester', color: '#fdbf6f' }
                                    ]}
                                    height={290}
                                    xAxis={[{ data: generateDateRange(), scaleType: 'band'}]}
                                    yAxis={[{ label: 'Hours per day'}]}
                                    //margin={{ top: 10, bottom: 30, left: 40, right: 10 }}
                                    />
                            </Card>
                        </Grid>
                })
            }

        </Grid>
    </Container>
}
