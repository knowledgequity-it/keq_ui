import React, { useEffect, useState, useRef } from "react";
import { useTheme } from "@mui/material/styles";
import Snackbar from "@mui/material/Snackbar";
import Slide from "@mui/material/Slide";

const propCheck = (prev, next) => {
  if (prev.time === next.time) return true;
  else return false;
};

const Timer = (props) => {
  const TIMER_INTERVAL = 10;
  const theme = useTheme();

  const [time, setTime] = useState(props.time);
  const [currentTime, setCurrentTime] = useState(props.time);
  const [ticks, setTicks] = useState(0);
  const tickRef = useRef(ticks);
  tickRef.current = ticks;

  const [parsedTime, setParsedTime] = useState({});
  const [showEoqNotice, setShowEoqNotice] = useState(false);

  // degree change in arc of the timer per second
  const arcDelta = 360 / ((props.duration || 360) == 0 ? 360 : props.duration);
  const EOQ_PERIOD = 600;

  //setTime(props.time);
  //setCurrentTime(props.time)
  //console.log(props)

  const parseTime = () => {
    let timeLeft = {
      days: Math.floor(currentTime / (60 * 60 * 24)),
      hours: Math.floor((currentTime / (60 * 60)) % 24),
      minutes: Math.floor((currentTime / 60) % 60),
      seconds: Math.floor(currentTime % 60),
    };
    setParsedTime(timeLeft);
  };

  useEffect(() => {
    setTime(props.time);
  }, [props.time]);

  useEffect(() => {
    setCurrentTime(currentTime - 1);
    parseTime();
    props.tickFunc(currentTime);
  }, [time]);

  useEffect(() => {
    const timer = setTimeout(() => {
      setCurrentTime(currentTime - 1);
      setTicks((tick) => (tick >= TIMER_INTERVAL ? 0 : tick + 1));
      if (tickRef.current >= TIMER_INTERVAL) {
        props.tickFunc(currentTime);
      }
      parseTime();
    }, 1000);
    return () => clearTimeout(timer);
  }, [currentTime]);

  if (!showEoqNotice && currentTime == EOQ_PERIOD) {
    setShowEoqNotice(true);
  }

  const getSkew = (pos) => {
    let currentArcAngle = 360 - currentTime * arcDelta;

    if (currentArcAngle < 0 || pos > 4 || pos < 1) {
      return;
    }

    if (currentArcAngle >= (pos - 1) * 90 && currentArcAngle <= pos * 90)
      // segment in progress
      return "" + (pos * 90 - currentArcAngle) + "deg";
    else if (currentArcAngle > pos * 90) {
      // segment that is over
      return "0deg";
    } else {
      // segment that has not started yet
      return "90deg";
    }
  };

  const getTimerColor = () => {
    if (currentTime < EOQ_PERIOD) return "#ff0000";
    else return theme.palette?.primary?.main;
  };

  const niceFormatTime = (theTime) => {
    let formattedTime = "";
    if (theTime.hours > 0) formattedTime += `${theTime.hours}h `;
    if (theTime.minutes >= 0 || theTime.hours > 0)
      formattedTime += `${theTime.minutes}m `;
    if (theTime.seconds >= 0) formattedTime += `${theTime.seconds}s`;
    if (currentTime <= 0) formattedTime = "ENDED";
    return formattedTime;
  };

  function eoqNoticeSliderTransition(props) {
    return <Slide {...props} direction="right" />;
  }

  //style="transform: rotate(0deg) skew(0deg)">
  return (
    <div
      style={{ position: "relative", display: "inline-block", marginRight: 20 }}
    >
      <div
        style={{
          position: "relative",
          width: 200,
          height: 200,
          background: "transparent",
          borderRadius: "50%",
          overflow: "hidden",
        }}
      >
        <div
          id="bottom-right"
          style={{
            background: getTimerColor(),
            transform: "rotate(0deg) skew(" + getSkew(2) + ")",
            position: "absolute",
            top: "50%",
            left: "50%",
            width: "100vw",
            height: "100vw",
            transformOrigin: "0 0",
          }}
        ></div>
        <div
          id="bottom-left"
          style={{
            background: getTimerColor(),
            transform: "rotate(90deg) skew(" + getSkew(3) + ")",
            position: "absolute",
            top: "50%",
            left: "50%",
            width: "100vw",
            height: "100vw",
            transformOrigin: "0 0",
          }}
        ></div>
        <div
          id="top-left"
          style={{
            background: getTimerColor(),
            transform: "rotate(180deg) skew(" + getSkew(4) + ")",
            position: "absolute",
            top: "50%",
            left: "50%",
            width: "100vw",
            height: "100vw",
            transformOrigin: "0 0",
          }}
        ></div>
        <div
          id="top-right"
          style={{
            background: getTimerColor(),
            transform: "rotate(270deg) skew(" + getSkew(1) + ")",
            position: "absolute",
            top: "50%",
            left: "50%",
            width: "100vw",
            height: "100vw",
            transformOrigin: "0 0",
          }}
        ></div>
      </div>
      <div
        style={{
          position: "absolute",
          left: "50%",
          top: "50%",
          width: 180,
          height: 180,
          transform: "translateX(-50%) translateY(-50%)",
          background: "#ffffff",
          borderRadius: "50%",
          boxShadow: "inset rgba(34, 36, 38, 0.15) 0px 0px 3px 2px",
        }}
      ></div>
      <div
        style={{
          position: "absolute",
          fontWeight: "bold",
          left: "50%",
          top: "50%",
          transform: "translateX(-50%) translateY(-50%)",
          fontSize: currentTime > 3600 ? "100%" : "150%",
          whiteSpace: currentTime > 3600 ? "wrap" : "nowrap",
        }}
      >
        {niceFormatTime(parsedTime)}
      </div>

      {/* <Snackbar
      open={showEoqNotice}
        onClose={()=>{ setShowEoqNotice(false)}}
        autoHideDuration={10000}
        TransitionComponent={eoqNoticeSliderTransition}
        message="You have 10 minutes remaining"

      /> */}
    </div>
  );
}; //, [propCheck])

export default Timer;
