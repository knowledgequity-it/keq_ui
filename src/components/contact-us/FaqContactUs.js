import { Stack, Typography, Link } from "@mui/material";
import OpenInNewIcon from '@mui/icons-material/OpenInNew';

const questionStyle = {
    padding:2,
    fontWight: "bold",
    verticalAlign: 'middle',
   // display: 'flex',
    alignItems: 'center',
   // flexWrap: 'wrap',
}
const linkStyle = {
    ml:1,
    color:"blue"

}
const iconStyle = {
    ml:1, 
    mr:1, 
    verticalAlign: "middle"
}

export default function FaqContactUs()
{
    return <Stack sx={{padding:5}}>
        <Typography variant="h5">General Enquiries</Typography>
        <Typography variant="body1" sx={questionStyle}>
        Email: enquiries@knowledgequity.com.au

Available during business hours (Melbourne) and monitored out of hours.
        </Typography>
        <Typography variant="h5">Subject Matter Queries</Typography>
        <Typography variant="body1" sx={questionStyle}>Ask-the-Expert online <span><Link sx={linkStyle} href="https://knowledgequity.com.au/forums/">forum </Link></span><OpenInNewIcon sx={iconStyle}/> (logged in users only)</Typography>
        <Typography variant="body1" sx={questionStyle}>Available during the CPA Program semester.</Typography>
        <Typography variant="body1" sx={questionStyle}>CPA Australia contact details: <span><Link sx={linkStyle} target="_blank" rel="noopener noreferrer" href="https://www.cpaaustralia.com.au/about-us/contact-us">https://www.cpaaustralia.com.au/about-us/contact-us</Link></span><OpenInNewIcon sx={iconStyle}/></Typography>
        <Typography variant="h5">Frequently Asked Questions (FAQs):</Typography>
        <Typography variant="body1" sx={questionStyle}>
            <b>Important Notice</b> If you are unable to see your Guided Learning course in your Dashboard, you first need to sign in via your <span><Link sx={linkStyle} target="_blank" rel="noopener noreferrer" href="https://login.cpaaustralia.com.au/Account/SignIn?ReturnUrl=%2Fissue%2Fsaml2">CPA MYOL</Link></span><OpenInNewIcon sx={iconStyle}/> login, select your subject, and then access Guided Learning.
        </Typography>
        <Typography variant="body1" sx={questionStyle}>Note: Study Guides for Ethics & Governance, and Financial Risk Management are changing in S1 2024.</Typography>

        <Typography variant="body1" sx={questionStyle}>1. Facebook groups for S1, 2024 are open. You can search for the subject groups using this naming format: “SMA (CPA Strategic Management Accounting) Semester 1 2024”.</Typography>
        <Typography variant="body1" sx={questionStyle}>
        2. Guided Learning will open on 15 January 2024 for Semester 1, 2024. Semester 1, 2024 officially starts on Monday 29 January 2024.
        </Typography>
        <Typography variant="body1" sx={questionStyle}>
        3. All the study guides are not changing for Semester 1 2024 except for Ethics & Governance, and Financial Risk Management (which have been updated – so don’t use an old version of the study guide).
        </Typography>
        <Typography variant="body1" sx={questionStyle} >
        4. In the break between semesters, you can enrol in ourfree Assist courses to help with your subject selection and get a start on your studies. Go to <span><Link sx={linkStyle} href="https://knowledgequity.com.au">https://knowledgequity.com.au</Link></span><OpenInNewIcon sx={iconStyle}/>and choose your subject then sign up.
        </Typography>
        <Typography variant="body1" sx={questionStyle}>
        5. Calculators – You can use a financial, scientific, or standard calculator with no programmable functions, storage capabilities or internet connectivity.
        </Typography>
        <Typography variant="body1" sx={questionStyle}>
        6. Have questions on how the CPA Program Exams are marked? See this <span><Link sx={linkStyle} target="_blank" rel="noopener noreferrer" href="https://www.youtube.com/watch?v=vI3xmPi3lig">video</Link></span> <OpenInNewIcon sx={iconStyle}/>
        </Typography>
        <Typography variant="body1" sx={questionStyle}>
        7. CPA Program Exam Results – FAQs (including – ‘Can I get a remark?’). See the CPA Australia website link here:<span><Link sx={linkStyle} target="_blank" rel="noopener noreferrer" href="https://www.cpaaustralia.com.au/become-a-cpa/completing-the-cpa-program/exams-and-assessment/understanding-your-exam-results">CPA Australia - Understanding Your Exam Results</Link></span><OpenInNewIcon sx={iconStyle}/>
        </Typography>
        <Typography variant="body1" sx={questionStyle}>
        8. CPA Australia’s important dates including when results are released and when enrolments open are here:<span><Link sx={linkStyle} target="_blank" rel="noopener noreferrer" href="https://www.cpaaustralia.com.au/cpa-program/cpa-program-candidates/your-enrolment/important-dates-and-fees">CPA Australia - Important dates and fees</Link></span><OpenInNewIcon sx={iconStyle}/>
        </Typography>
        <Typography variant="body1" sx={questionStyle}>
        9. Foundation exams – Yes, we currently offer support for the Financial Accounting and Reporting (FAR) foundation exam only. However, you may find our free Assist courses useful for some topic areas, as well as our <span><Link sx={linkStyle} target="_blank" rel="noopener noreferrer" href="https://www.youtube.com/KnowledgEquity">YouTube channel</Link></span><OpenInNewIcon sx={iconStyle}/> for a range of short video tutorials.
        </Typography>
        <Typography variant="body1" sx={questionStyle}>
        10. Where can I get past exam papers? CPA Australia does not provide past exam papers and, as a result, KnowledgEquity does not have any past exam papers. KnowledgEquity provides two full-length practice exams as well as a range of tests and quizzes, which will be more than enough to prepare you for your exam.
        </Typography>
        <Typography variant="body1" sx={questionStyle}>
        In any case, we aim to respond within 2 business days.
        </Typography>
    </Stack>
}
