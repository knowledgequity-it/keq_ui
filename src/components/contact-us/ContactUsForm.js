import { TextField, Button, Select, MenuItem, FormControl, InputLabel, Typography } from "@mui/material";
import { Stack } from "@mui/system";
import { useState } from "react";
import { sendContactUsMessage } from "../../facades/UserFacade";


const SUCCESS_MESSAGE = "Your mesage was sent to KnowledgEquity.";
const ERROR_MESSAGE = "There was an error submitting your message";

export default function ContactUsForm()
{
    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [phone, setPhone] = useState("");
    const [message, setMessage] = useState("")
    const [queryType, setQueryType] = useState("");
    const [sentMessage, setSentMessage] = useState(false);
    const [onSubmitMessage, setOnSubmitMessage] = useState(SUCCESS_MESSAGE)




    const handleSubmit = () =>
    {
        if(name !== "" && email != "" && message != "" && queryType != "")
        {
            setSentMessage(true)
            sendContactUsMessage(name, email, phone, queryType, message)
            .then(res => {
                setSentMessage(true);
                setOnSubmitMessage(SUCCESS_MESSAGE);
            })
            .catch(err => {
                setSentMessage(false);
                setOnSubmitMessage(ERROR_MESSAGE);
                
            })
        }
    }

    const messageChanged = (text) => {
        setMessage(text);
        setSentMessage(false);

    } 
    
    return <Stack alignItems="center" justifyContent="center" sx={{pt:4}}>
        <TextField value={name} placeholder="Name" onChange={(e)=>setName(e.target.value)} error={name==""} helperText={"Name is required"} fullWidth></TextField>
        <TextField value={email} placeholder="Email" onChange={(e)=>setEmail(e.target.value)} error={email==""} helperText={"Email is required"} fullWidth></TextField>
        <TextField value={phone} placeholder="Phone" onChange={(e)=>setPhone(e.target.value)} fullWidth></TextField>
        <FormControl fullWidth sx={{pt:2}}>
            <InputLabel sx={{pt:2}}>Query Type</InputLabel>
            <Select
            value={queryType}
            label={"Query type"}
            onChange={(e)=>setQueryType(e.target.value)}
            >
                <MenuItem value={"New Enrolments"}>New Enrolments</MenuItem>
                <MenuItem value={"Subject Selections"}>Subject Selections</MenuItem>
                <MenuItem value={"Webinars"}>Webinars</MenuItem>
                <MenuItem value={"Practice Exams"}>Practice Exams</MenuItem>
                <MenuItem value={"IT/Systems"}>IT/Systems</MenuItem>
                <MenuItem value={"Feedback"}>Feedback</MenuItem>
                <MenuItem value={"Other"}>Other</MenuItem>
            </Select>
        </FormControl>
        <TextField value={message} 
                   placeholder="Your message" 
                   onChange={(e)=>messageChanged(e.target.value)} 
                   multiline 
                   rows={5} 
                   error={message==""} 
                   helperText={"Message is required"} 
                   fullWidth
                   sx={{pt:2}}
                   >
        </TextField>
        
        <Button variant="contained" size="large" onClick={(e) => handleSubmit()} disabled={sentMessage}>Send Message</Button>
        {
            sentMessage ? <Typography variant="subtitle1">{onSubmitMessage}</Typography> : null
        }
    </Stack>
}