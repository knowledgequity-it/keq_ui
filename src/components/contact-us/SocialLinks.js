import { Grid, Typography, Link } from "@mui/material"
import linkedIn from '../../assets/linked_in_icon.png';
import facebook from '../../assets/facebook_icon.png';
import twitter from '../../assets/twitter_icon.png';

export default function SocialLinks()
{

    return <Grid container direction="row" alignItems="center" justifyContent="space-between" >
        <Grid item sx={{pl:4,pr:4,pt:6}}>
            <Grid container direction="column" alignItems="center" justifyContent="center">
                <Link target="_blank" rel="noopener noreferrer" href="https://www.facebook.com/KnowledgEquity-Support-for-your-CPA-Studies-1505986809677618/">
                    <img src={facebook} />
                </Link>
                <Typography>Connect on Facebook</Typography>
            </Grid>
        </Grid>
        <Grid item sx={{pl:4,pr:4,pt:6}}>
            <Grid container direction="column" alignItems="center" justifyContent="center">
                <Link target="_blank" rel="noopener noreferrer" href="https://www.linkedin.com/company/knowledgequity">
                    <img src={linkedIn} />
                </Link>
                <Typography>Link up on LinkedIn</Typography>
            </Grid>
        </Grid>
        <Grid item sx={{pl:4,pr:4,pt:6}}>
            <Grid container direction="column" alignItems="center" justifyContent="center">
                <Link target="_blank" rel="noopener noreferrer" href="https://twitter.com/knowledgequity">
                    <img src={twitter} />
                </Link>
                <Typography>Twitter with us</Typography>
            </Grid>
        </Grid>
    </Grid>
}
