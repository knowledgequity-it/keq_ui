import Card from "@mui/material/Card";
import CardActionArea from "@mui/material/CardActionArea";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import { useNavigate } from "react-router-dom";
import useLiveLogging from "../hooks/useLiveLogging";
import barchartLogo from "../assets/bar-chart.png";

// const useStyles = makeStyles((theme) => ({
//   root: {
//     //        maxWidth: 345,
//     //        width: '380px',
//     height: "200px",
//   },
//   media: {
//     height: 140,
//   },
//   title: {
//     height: "50px",
//   },
//   subtitle: {
//     height: "20px",
//     paddingLeft: "20px",
//   },
// }));

export default function ResultsCard(props) {
  const navigate = useNavigate();
  const candidateAction = useLiveLogging();

  function handleClick(e) {
    candidateAction({
      component: "ResultsCard",
      event: "User Clicked the ResultsCard",
    });
    navigate(`${process.env.REACT_APP_AB_URL_PREFIX}/my-results`);
  }

  return (
    <Card sx={{ mb: 3, borderRadius:5}}>
      <CardActionArea onClick={(e) => handleClick(e)}>
        <CardContent>
          <img src={barchartLogo} alt="" />
          <Typography
            sx={{ height: "30px" }}
            gutterBottom
            variant="h5"
            component="h2"
          >
            My Quiz & Test Results
          </Typography>
          <Typography
            sx={{ height: "40px" }}
            variant="subtitle1"
            color="textSecondary"
            component="p"
          >
            See the questions & solution explanations
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>
  );
}
