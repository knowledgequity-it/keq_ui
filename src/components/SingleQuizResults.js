import React, { useContext, useEffect, useState, useRef } from "react";
import { useTheme } from "@mui/material/styles";
import List from "@mui/material/List";
import { Avatar } from "@mui/material";
import CheckCircleIcon from "@mui/icons-material/CheckCircle";
import { ListItemAvatar } from "@mui/material";
import { ListItemText } from "@mui/material";
import ListItem from "@mui/material/ListItem";
import Badge from "@mui/material/Badge";
import RadioButtonCheckedIcon from "@mui/icons-material/RadioButtonChecked";
import { UserContext } from "../contexts/userContext";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Divider from "@mui/material/Divider";
import CircularProgress from "@mui/material/CircularProgress";
import { getSingleQuizResults } from "../facades/UserFacade";
import ResultsGraph from "./ResultsGraph";
import DoneIcon from "@mui/icons-material/Done";
import CloseIcon from "@mui/icons-material/Close";
import ReactToPrint from "react-to-print";
import PrintIcon from "@mui/icons-material/Print";
import Button from "@mui/material/Button";
import AssessmentIcon from "@mui/icons-material/Assessment";
import QuizStats from "./quiz/QuizStats";

const getAnswerLetterFromIndex = (index) => {
  if (Array.isArray(index))
    return index.map((x) => String.fromCharCode(64 + parseInt(x))).toString();
  else return String.fromCharCode(65 + index);
};

const optionIsInSelection = (option, selection_arr) => {
  return selection_arr.includes((option + 1).toString());
};
const AnswerOptions = (props) => {
  //const classes = useRowStyles();
  //console.log("props.user_answers", props.user_answers);
  //console.log("props.correct_answer", props.correct_answer);
  return (
    <React.Fragment>
      <Typography variant="h6" gutterBottom>
        Answer Options
      </Typography>
      <Typography gutterBottom>
        {props.user_answers != ""
          ? "You answered " + getAnswerLetterFromIndex(props.user_answers)
          : "You did not record an answer"}
        . The correct answer is {getAnswerLetterFromIndex(props.correct_answer)}
      </Typography>

      <Grid container alignItems="center">
        <Grid item container alignItems="center" xs={12}>
          <RadioButtonCheckedIcon
            color="secondary"
            style={{ marginRight: "5px" }}
            fontSize="small"
          />
          <Typography variant="overline" style={{ marginRight: "20px" }}>
            User Selection
          </Typography>
          <CheckCircleIcon
            color="primary"
            style={{ marginRight: "5px" }}
            fontSize="small"
          />
          <Typography variant="overline">Correct Answer</Typography>{" "}
        </Grid>
      </Grid>

      <List>
        {Array.isArray(props.answer_options) ? (
          props.answer_options.map((option, index) => {
            return (
              <React.Fragment key={index}>
                <ListItem>
                  <ListItemAvatar>
                    <Badge
                      badgeContent={
                        <RadioButtonCheckedIcon color="secondary" />
                      }
                      invisible={
                        !optionIsInSelection(index, props.user_answers)
                      }
                      anchorOrigin={{
                        vertical: "top",
                        horizontal: "right",
                      }}
                    >
                      <Badge
                        overlap="circular"
                        invisible={
                          !optionIsInSelection(index, props.correct_answer)
                        }
                        anchorOrigin={{
                          vertical: "bottom",
                          horizontal: "right",
                        }}
                        badgeContent={<CheckCircleIcon color="primary" />}
                      >
                        <Avatar>{getAnswerLetterFromIndex(index)}</Avatar>
                      </Badge>
                    </Badge>
                  </ListItemAvatar>
                  <ListItemText
                    primary={
                      <div
                        align="left"
                        dangerouslySetInnerHTML={{ __html: option }}
                      ></div>
                    }
                  />
                </ListItem>
              </React.Fragment>

              /*
            <Grid
              container
              direction="row"
              alignItems="center"
              justifyContent="flex-start"
              key={index}
            >
              <Grid item xs={1}>
                <Typography variant="h6" align="left">
                  {getAnswerLetterFromIndex(index)}
                </Typography>
              </Grid>
              <Grid item xs={11}>
                <div
                  align="left"
                  dangerouslySetInnerHTML={{ __html: option }}
                ></div>
              </Grid>
            </Grid>*/
            );
          })
        ) : (
          <div dangerouslySetInnerHTML={{ __html: props.answer_options }}></div>
        )}
      </List>
    </React.Fragment>
  );
};

export default function SingleQuizResults(props) {
  const theme = useTheme();
  const [showStats, setShowStats] = useState(false);
  const [quizid, setQuizId] = useState(null);
  const [answers, setAnswers] = useState([]);
  const [quizType, setQuizType] = useState([]);
  const [quizTitle, setQuizTitle] = useState(null);
  const [tagData, setTagData] = useState({
    custom_tag_matcher: "",
    custom_tag_label: "",
    default_tag_matcher: "[\\w]+-m[\\d]+$",
    default_tag_label: "Module",
  });
  const userContext = useContext(UserContext);
  const [open, setOpen] = useState(props.open);
  const componentRef = useRef();

  useEffect(() => {
    let isMounted = true;
    if (open) {
      getSingleQuizResults(userContext.user, props.quiz_id)
        .then((response) => response.json())
        .then((results) => {
          if (isMounted) {
            //setQuizId(props.quiz_id);
            setAnswers(results.questions);
            setQuizType(results.quiz_type);
            setQuizTitle(results.title);
            setTagData({
              custom_tag_matcher: results.custom_tag_matcher,
              custom_tag_label: results.custom_tag_label,
              default_tag_matcher: results.default_tag_matcher,
              default_tag_label: results.default_tag_label,
            });
          }
        });
    }
    return () => {
      isMounted = false;
    };
  }, [open, userContext.user, props.quiz_id]);

  //  useEffect(() => {
  //    setOpen(props.open);
  //  }, [props.open]);

  //Inbuilt JS functions isNan others are kind of shit - answer marks can be numberic, string, boolean, nulls etc. god knows why.
  function IsNumeric(val) {
    if (val === "")
      //empty answer (question metadata exists but no value )
      return false;
    else if (val === null) return false;
    else if (val === false)
      //empty answer - question metadata does not exist at all.
      return false;
    //theoretically a string or int.
    else return !isNaN(val);
  }
  const getScore = () => {
    let score = 0;
    answers.forEach((q) => {
      if (q.question_type === "fillblank") 
        score += calculateFillBlankScore(q.correct_answer, q.user_answers, q.available_marks)
      else
        !IsNumeric(q.marks) ? (score += 0) : (score += parseInt(q.marks));
    });

    return score;
  };

  const getMaxScore = () => {
    let max_score = 0;
    const excluded_question_types = ["largetext", "smalltext"];
    answers.forEach((q) => {
      max_score += !excluded_question_types.includes(q.question_type)
        ? q.available_marks
        : 0;
    });
    return max_score;
  };

  const printResults = () => {
    var printContents = document.getElementById("quiz-results").innerHTML;
    var originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
  };

  const getPageMargins = () => {
    return `@page { margin:3em} !important; }`;
  };

  const maybeReplaceWorkshetAnswers = (html, answers, questionType) => {
    if (questionType !== "fillblank") return html;

    let parser = new DOMParser();
    let dom = parser.parseFromString(html, "text/html");
    let answerCells = dom.getElementsByClassName("vibe_fillblank");

    let answersArray = answers.split("|");

    for (let i = 0; i < answerCells.length; i++) {
      answerCells[i].innerHTML = answersArray[i];
    }

    return dom.body.innerHTML;
  };
  let statsIcon = (
    <AssessmentIcon
      onClick={() => {
        //console.log(showStats);
        setShowStats(!showStats);
      }}
    />
  );
  let visualDisplay = null;

  if (showStats) {
    visualDisplay = <QuizStats quiz_id={props.quiz_id} />;
  } else {
    if (
      quizType.length >= 1 &&
      !["mini-quiz", "module-quiz", "quiz"].includes(quizType)
    ) {
      visualDisplay = (
        <ResultsGraph quiz_data={answers} tagData={tagData}></ResultsGraph>
      );
    } else {
      visualDisplay = null;
      <QuizStats quiz_id={props.quiz_id} />;
    }
  }

  const calculateFillBlankScore = (answerArr, userAnswerArr, totalMarks) => {
    
      let correctAnswerCount = 0;
      let answers = answerArr[0].split("|");
      let userAnswers = userAnswerArr[0].split("|");

      for(let i = 0; i < answers.length;i++)
      {
        if(answers[i] == userAnswers[i])
            correctAnswerCount++;
      }

      return (totalMarks/answers.length) * correctAnswerCount;
  }


  return props.open && answers.length != 0 ? (
    <React.Fragment>
      <Box id="quiz-results" ref={componentRef}>
        <style>{getPageMargins()}</style>
        <Box display="none" displayPrint="block">
          <h2
            dangerouslySetInnerHTML={{
              __html: quizTitle,
            }}
          />
        </Box>
        {statsIcon}
        {visualDisplay}

        {props.showTotal ? (
          <React.Fragment>
            <div>
              <Typography
                variant="button"
                display="inline"
                style={{ marginLeft: ".5rem" }}
              >
                You scored
              </Typography>
              <Typography
                variant="h5"
                display="inline"
                style={{ marginLeft: ".5rem" }}
              >
                {getScore()}
              </Typography>
              <Typography
                variant="button"
                display="inline"
                style={{ marginLeft: ".5rem" }}
              >
                out of a possible
              </Typography>
              <Typography
                variant="h5"
                display="inline"
                style={{ marginLeft: ".5rem" }}
              >
                {getMaxScore()}
              </Typography>
              <Typography
                variant="h6"
                display="inline"
                style={{ marginLeft: ".5rem" }}
              >
                [{Math.round((getScore() / getMaxScore()) * 100)}%]
              </Typography>
            </div>
            <ReactToPrint
              trigger={() => (
                <Box displayPrint="none" style={{ marginBottom: "10px" }}>
                  <Button startIcon={<PrintIcon />}>Print Results</Button>
                </Box>
              )}
              content={() => componentRef.current}
            />
          </React.Fragment>
        ) : null}
        <Grid container spacing={2}>
          {answers.map((answer, index) => {
            if (answer.user_answers[0] === null) {
              answer.user_answers[0] = "";
            }
            return (
              <Grid item xs={12} key={answer.idx}>
                <Card elevation={0} variant="outlined">
                  <CardContent>
                    <Grid container spacing={2}>
                      <Grid item xs={12}>
                        <Typography variant="h5">
                          Question {index + 1}{" "}
                          {answer.marks != "0" ||
                          answer.question_type === "largetext" ? (
                            <DoneIcon
                              color="primary"
                              style={{ marginTop: "5px" }}
                            />
                          ) : (
                            <CloseIcon color="error" />
                          )}
                          {"  "}Marks:{" "}
                          {
                            answer.question_type === "fillblank" ?
                            calculateFillBlankScore(answer.correct_answer, answer.user_answers,  answer.available_marks)
                            : 
                          answer.question_type === "largetext"
                            ? "Self marked"
                            : answer.marks
                              ? answer.marks
                              : "0"}
                        </Typography>
                        <Box
                          sx={{
                            "& table": {
                              width: "100% !important",
                            },
                          }}
                          dangerouslySetInnerHTML={{
                            __html: maybeReplaceWorkshetAnswers(
                              answer.question,
                              answer.user_answers[0],
                              answer.question_type,
                            ),
                          }}
                        />
                      </Grid>
                      <Grid item xs={12}>
                        <Divider />
                      </Grid>

                      <Grid item xs={12}>
                        {answer.question_type === "largetext" && (
                          <>
                            <Typography variant="h6" gutterBottom>
                              Your Answer
                            </Typography>
                            <Typography
                              dangerouslySetInnerHTML={{
                                __html: answer.user_answers[0].replaceAll(
                                  "\n",
                                  "<br>",
                                ),
                              }}
                            />
                          </>
                        )}
                        {answer.question_type === "fillblank" && (
                          <>
                            <Typography>
                              {answer.correct_answer[0].split("|").length != 1 ? "Your answers were " : "Your answer was "}
                              {answer.user_answers[0].split("|").join(", ").replace(/, $/, '')}
                            </Typography>
                            <Typography>                            
                             {answer.correct_answer[0].split("|").length != 1 ? "The correct answers were " : "The correct answer was " }
                              {answer.correct_answer[0].split("|").join(", ").replace(/,$/, '')}
                            </Typography>
                          </>
                        )}
                        {answer.question_type !== "largetext" &&
                          answer.question_type !== "fillblank" && (
                            <AnswerOptions
                              answer_options={answer.answer_options}
                              correct_answer={answer.correct_answer}
                              user_answers={answer.user_answers}
                            ></AnswerOptions>
                          )}
                      </Grid>
                      <Grid item xs={12}>
                        <Divider />
                      </Grid>
                      <Grid item xs={12}>
                        <Box>
                          <Typography variant="h6">
                            Answer Explanation
                          </Typography>
                          <div
                            dangerouslySetInnerHTML={{
                              __html: answer.explanation,
                            }}
                          />

                          {
                            <Typography
                              variant="h6"
                              style={{ fontWeight: 600 }}
                            >
                              {answer.module && answer.module != ""
                                ? " Module: " + answer.module
                                : ""}
                              {answer.part && answer.part != ""
                                ? " > Part: " + answer.part
                                : ""}
                              {answer.major_topic && answer.major_topic != ""
                                ? " > " + answer.major_topic
                                : ""}
                              {answer.minor_topic && answer.minor_topic != ""
                                ? " > " + answer.minor_topic
                                : ""}
                              {answer.other_detail && answer.other_detail != ""
                                ? " > " + answer.other_detail
                                : ""}
                              {answer.page && answer.page != ""
                                ? " > Page: " + answer.page
                                : ""}
                            </Typography>
                          }

                          {answer.other_reference &&
                            answer.other_reference != "" && (
                              <Typography style={{ fontWeight: 600 }}>
                                Also see: {answer.other_reference}
                              </Typography>
                            )}
                        </Box>
                      </Grid>
                    </Grid>
                  </CardContent>
                </Card>
              </Grid>
            );
          })}
        </Grid>
      </Box>
    </React.Fragment>
  ) : (
    <Box>
      <CircularProgress />
    </Box>
  );
}

/*
{quizType.length >= 1 &&
        !["mini-quiz", "module-quiz", "quiz"].includes(quizType) ? (
          <ResultsGraph
            quiz_data={answers}
            className={classes.resultsGraph}
            tagData={tagData}
          ></ResultsGraph>
        ) : null}
        */
