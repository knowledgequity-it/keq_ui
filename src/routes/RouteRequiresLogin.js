import React, { useContext } from "react";
import { Navigate } from "react-router-dom";
import { UserContext } from "../contexts/userContext";

const RouteRequiresLogin = ({ children }) => {
  const userContext = useContext(UserContext);
  const userIsLogged = userContext.user !== null;

  if (!userIsLogged) return <Navigate to="/login" />;

  return children;
};

export default RouteRequiresLogin;
