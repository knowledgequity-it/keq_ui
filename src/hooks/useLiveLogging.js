import log from "loglevel";
import remote from "loglevel-plugin-remote";
import Platform from "react-platform-js";
var mock = require("loglevel-plugin-mock");
var token;

function configureLiveLogs(token) {
  if (log.isSet) {
    return; // already set
  }

  log.token = token;

  const logStruct = (log) => ({
    msg: log.message,
    level: log.level.label,
    stacktrace: log.stacktrace,
  });

  remote.apply(log, {
    format: logStruct,
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
      Authorization: `Bearer ${token}`,
    },
    url: `${process.env.REACT_APP_BASE_URL}/wp-json/app/v1/logging/candidate_action_log`,
  });

  log.enableAll();
  log.isSet = true;
}

function useLiveLogging() {
  return function candidateAction(action) {
    for (const property in action) {
      if (
        typeof action[property] == "object" ||
        typeof action[property] == "array"
      ) {
        action[property] = JSON.stringify(action[property]);
      }
    }

    action.plaform = Platform.OS; // OS name, Mac OS
    action.platform_version = Platform.OSVersion; // OS version, 10.11
    action.browser = Platform.Browser; // Browser name, Chrome
    action.browser_version = Platform.BrowserVersion; // Browser Version
    action.engine = Platform.Engine; // browser engine name

    action.cpu = Platform.CPU;
    action.deviceType = Platform.DeviceType;
    action.deviceModel = Platform.DeviceModel;
    action.deviceVender = Platform.DeviceVendor;
    action.ua = Platform.UA;

    let message = JSON.stringify(action);
    log.info(message);
  };
}

export { configureLiveLogs };
export default useLiveLogging;
