const webpack = require("webpack");

module.exports = {
  webpack: {
    plugins: {
      add: [
        new webpack.ProvidePlugin({ process: "process/browser" }),
        new webpack.DefinePlugin({
          "process.env": JSON.stringify(process.env),
        }),
      ],
    },
    configure: {
      resolve: {
        fallback: {
          crypto: require.resolve("crypto-browserify"),
          stream: require.resolve("stream-browserify"),
          util: require.resolve("util/"),
          buffer: require.resolve("buffer/"),
          'process/browser': require.resolve('process/browser')
        },
      },
    },
  },
};
