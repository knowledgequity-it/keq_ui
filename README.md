This app serves as the UI component of KE tech stack
This app is created by using create-react-app npm package 
"" How to run locally ""

(1) You need to have latest Nodejs installed on your machine
(2) Install npm
(3) you need to have an aws cli configured with a named profile

Once source for the repo is downloaded to your machine



(4)Move into project root folder, i.e., keq_ui  Issue a command "" sudo npm install ""


(5) Once done, issue "" sudo npm start""


It will take a few seconds first time but you will have it running locally at 3000 unless you mention a different port in index.js

(6) For better debugging experience you should use react dev tools but for simple dubugging code should be availble below static folder on the source tab

(8) in package.json, within the scripts node change the profile localdev to your profile
**    "deploy": "sudo aws s3 cp --recursive ./build s3://kequi --profile localdev " **

(7) In order to deploy to s3 first issue ** sudo npm build ** this will create a build folder and then issue "sudo npm deploy"


(8) website should reflect your changes at http://kequi.s3-website-ap-southeast-2.amazonaws.com/"

